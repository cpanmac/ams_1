package com.njcb.ams.pojo.dto;

import com.njcb.ams.pojo.dto.CheckTokenInfo;

/**
 * @author LOONG
 */
public class CheckTokenInfoResponse {
    private boolean success;
    private String code;
    private String message;
    private CheckTokenInfo entity;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CheckTokenInfo getEntity() {
        return entity;
    }

    public void setEntity(CheckTokenInfo entity) {
        this.entity = entity;
    }
}
