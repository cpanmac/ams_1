package com.njcb.ams.support.security.config;

import com.njcb.ams.support.security.bo.LoginModel;
import com.njcb.ams.support.security.bo.SecurityUser;
import com.njcb.ams.support.security.bo.SessionModel;
import com.njcb.ams.support.security.util.TokenUtil;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import java.util.List;
import java.util.Map;

/**
 * @author liuyanlong
 */
public interface AmsSecurityConfiguration {
	
	/**
	 * 配置登录方式，用户密码模式/授权码模式
	 * 默认用户密码模式
	 * @return
	 */
	default LoginModel loginModel(){
		return LoginModel.PASSWORD;
	}


	/**
	 * 默认用户密码编码方式
	 * @return
	 */
	default String encodingId(){
		return "MD5";
	}

	/**
	 * 用户维持机制， session/token
	 * 默认用户密码模式
	 * @return
	 */
	default SessionModel sessionModel(){
		return SessionModel.SESSION;
	}


	/**
	 * 加载用户，根据用户名称加载用于登录的用户
	 * @param loginCode 登录用户名,用户密码登录场景为用户名，授权码场景时为授权码
	 * @param loginModel 登录方式
	 * @param param  登录时支持的扩展参数
	 * @param isLogin 是否初次登陆
	 * @return
	 */
	SecurityUser loadUserByUsername(String loginCode, LoginModel loginModel, Map<String, Object> param, Boolean isLogin);

	/**
	 * 用户登录动作通知更新
	 * @param securityUser
	 */
	default void updateUser(SecurityUser securityUser){
		return;
	}


	/**
	 * 存储token
	 * @param tokenId
	 * @param claims
	 */
	default void storage(String tokenId, Map<String, Object> claims){
		TokenUtil.cacheAddToken(tokenId, claims);
	}
	
	/**
	 * 获取token
	 */
	default Map<String,Object> obtain(String tokenId){
		return TokenUtil.cacheGetToken(tokenId);
	}
	
	/**
	 * spring安全配置通过此处来配置
	 * @param http
	 */
	default void configureHttpSecurity(HttpSecurity http){
		return;
	}
	
	/**
	 * 开发权限的URL列表
	 * @param antPatterns
	 */
	default void permitMatchers(List<String> antPatterns){
	}

	/**
	 * 默认的登录处理地址
	 * @return
	 */
	default String loginProcessingUrl(){
		return "/login";
	}

	/**
	 * 用户登录页面
	 * @return
	 */
	default String loginPage(){
		return "/login";
	}

}
