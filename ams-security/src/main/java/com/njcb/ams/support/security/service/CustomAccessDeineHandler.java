package com.njcb.ams.support.security.service;

import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.util.AmsJsonUtils;
import com.njcb.ams.util.AmsUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 用来解决认证过的用户访问无权限资源时的异常
 * @author liuyanlong
 */
public class CustomAccessDeineHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
            AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setStatus(HttpStatus.FORBIDDEN.value());
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=utf-8");
        /**
         * 如果有登录URL参数,则重定向URL
         */
        String loginUrl = request.getParameter(SysBaseDefine.REDIRECT);
        if (AmsUtils.isNotNull(loginUrl)) {
            response.sendRedirect(loginUrl);
        }
        ServletOutputStream out = response.getOutputStream();
		out.write(AmsJsonUtils.objectToJson(Response.buildFail("403", "此用户没有访问权限!")).getBytes("UTF-8"));
		out.flush();
    }

}
