package com.njcb.ams.support.security.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.njcb.ams.factory.comm.DataBus;
import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.support.codevalue.EnumCode;
import com.njcb.ams.support.exception.ExceptionUtil;
import com.njcb.ams.support.security.bo.LoginModel;
import com.njcb.ams.util.AmsHttpUtils;
import com.njcb.ams.util.AmsJsonUtils;
import com.njcb.ams.util.AmsUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.Assert;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Map;

/**
 * 支持rest登录(json)和表单登录及授权码(authcode)登录的AuthenticationFilter
 * 
 * @author liuyanlong
 */
public class CustomAuthenticationFilter extends AbstractAuthenticationProcessingFilter {
	private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationFilter.class);

	public static final String SPRING_SECURITY_FORM_USERNAME_KEY = "username";
	public static final String SPRING_SECURITY_FORM_PASSWORD_KEY = "password";

	private String usernameParameter = SPRING_SECURITY_FORM_USERNAME_KEY;
	private String passwordParameter = SPRING_SECURITY_FORM_PASSWORD_KEY;
	private boolean postOnly = true;

	public CustomAuthenticationFilter() {
		super(new AntPathRequestMatcher("/login"));
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		try {
			super.doFilter(req,res,chain);
		}catch (Exception e){
			e.printStackTrace();
			PrintWriter out = response.getWriter();
			out.write(AmsJsonUtils.objectToJson(Response.buildFail(e.getMessage())));
			out.flush();
			out.close();
		}
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {

		AmsHttpUtils.parseQueryString(request).forEach((key, value) -> {
			DataBus.setAttribute(key, value);
		});

		/**
		 * 登录模式
		 */
		LoginModel model = setLoginModel(request);

		/**
		 * 支持授权码时登录
		 */
		if (LoginModel.AUTHCODE.equals(model)) {
			String authCode = getAuthcode(request);
			UsernamePasswordAuthenticationToken authRequest = null;
			if (AmsUtils.isNull(authCode)) {
				logger.warn("loginModel为authcode时Header中必须包含authcode值");
			}
			authRequest = new UsernamePasswordAuthenticationToken(authCode, authCode);
			setDetails(request, authRequest);
			return this.getAuthenticationManager().authenticate(authRequest);
		}
		/**
		 * 支持ContentType=application/json时登录
		 */
		else if ((null != request.getContentType()) && (MediaType.APPLICATION_JSON_UTF8_VALUE.equals(request.getContentType())
				|| MediaType.APPLICATION_JSON_UTF8_VALUE.toLowerCase().equals(request.getContentType().toLowerCase())
				|| MediaType.APPLICATION_JSON_VALUE.equals(request.getContentType()))) {
			ObjectMapper mapper = new ObjectMapper();
			UsernamePasswordAuthenticationToken authRequest = null;
			try (InputStream is = request.getInputStream()) {
				@SuppressWarnings("unchecked")
				Map<String, String> authenticationBean = mapper.readValue(is, Map.class);
				String username = authenticationBean.get("username");
				if(AmsUtils.isNull(username)){
					username = "";
				}
				String password = authenticationBean.get("password");
				if(AmsUtils.isNull(password)){
					password = "";
				}
				authenticationBean.forEach((key, value) -> {
					DataBus.setAttribute(key, value);
				});
				authRequest = new UsernamePasswordAuthenticationToken(username, password);
				setDetails(request, authRequest);
			} catch (Exception e) {
				e.printStackTrace();
				throw new AuthenticationServiceException(e.getMessage());
			}
			return this.getAuthenticationManager().authenticate(authRequest);
		}
		/**
		 * 支持ContentType为x-www-form-urlencoded时登录
		 */
		else {
			String username = obtainUsername(request);
			if(AmsUtils.isNull(username)){
				username = "";
			}
			String password = obtainPassword(request);
			if(AmsUtils.isNull(password)){
				password = "";
			}
			request.getParameterMap().forEach((key, value) -> {
				DataBus.setAttribute(key, value);
			});

			username = username.trim();
			UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username,
					password);
			setDetails(request, authRequest);
			return this.getAuthenticationManager().authenticate(authRequest);
		}

	}

	private LoginModel setLoginModel(HttpServletRequest request) {
		/**
		 * 登录模式
		 */
		LoginModel model = LoginModel.PASSWORD;
		Object loginModel = request.getHeader("loginModel");
		if (AmsUtils.isNull(loginModel)) {
			loginModel = AmsHttpUtils.parseQueryString(request).get("loginModel");
		}
		if (AmsUtils.isNotNull(loginModel)) {
			model = EnumCode.valueOfCode(LoginModel.class,loginModel.toString());
			if(null == model){
				ExceptionUtil.throwAppException("认证时请求头中[loginModel]填写不正确");
			}
		}
		DataBus.setAttribute("LoginModel", model);
		return model;
	}

	protected String obtainPassword(HttpServletRequest request) {
		return request.getParameter(passwordParameter);
	}

	/**
	 * Enables subclasses to override the composition of the username, such as
	 * by including additional values and a separator.
	 *
	 * @param request
	 *            so that request attributes can be retrieved
	 *
	 * @return the username that will be presented in the
	 *         <code>Authentication</code> request token to the
	 *         <code>AuthenticationManager</code>
	 */
	protected String obtainUsername(HttpServletRequest request) {
		return request.getParameter(usernameParameter);
	}

	protected void setDetails(HttpServletRequest request, UsernamePasswordAuthenticationToken authRequest) {
		authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
	}

	public void setUsernameParameter(String usernameParameter) {
		Assert.hasText(usernameParameter, "Username parameter must not be empty or null");
		this.usernameParameter = usernameParameter;
	}

	public void setPasswordParameter(String passwordParameter) {
		Assert.hasText(passwordParameter, "Password parameter must not be empty or null");
		this.passwordParameter = passwordParameter;
	}

	public void setPostOnly(boolean postOnly) {
		this.postOnly = postOnly;
	}

	public final String getUsernameParameter() {
		return usernameParameter;
	}

	public final String getPasswordParameter() {
		return passwordParameter;
	}

	// @Override
	// public Authentication attemptAuthentication(HttpServletRequest request,
	// HttpServletResponse response) throws AuthenticationException {}

	/**
	 * 获取请求授权码参数，兼容字符串authcode、code、access_token
	 * @param request
	 * @return
	 */
	private String getAuthcode(HttpServletRequest request){
		Object authCode = request.getHeader("authcode");
		if (AmsUtils.isNull(authCode)) {
			authCode = AmsHttpUtils.parseQueryString(request).get("authcode");
		}
		if (AmsUtils.isNull(authCode)) {
			authCode = request.getHeader("code");
		}
		if (AmsUtils.isNull(authCode)) {
			authCode = AmsHttpUtils.parseQueryString(request).get("code");
		}
		if (AmsUtils.isNull(authCode)) {
			authCode = request.getHeader("access_token");
		}
		if (AmsUtils.isNull(authCode)) {
			authCode = AmsHttpUtils.parseQueryString(request).get("access_token");
		}
		return authCode.toString();
	}
}