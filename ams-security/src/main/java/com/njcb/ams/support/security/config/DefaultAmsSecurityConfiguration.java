package com.njcb.ams.support.security.config;

import java.util.Map;

import com.njcb.ams.support.security.bo.LoginModel;
import org.springframework.stereotype.Component;

import com.njcb.ams.support.security.bo.SecurityUser;

/**
 * @author srxhx207
 */
@Component
public class DefaultAmsSecurityConfiguration implements AmsSecurityConfiguration {

	@Override
	public SecurityUser loadUserByUsername(String loginCode, LoginModel loginModel, Map<String, Object> param, Boolean isLogin) {
		return new SecurityUser();
	}
	
}
