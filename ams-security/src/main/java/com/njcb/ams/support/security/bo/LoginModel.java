package com.njcb.ams.support.security.bo;

import com.njcb.ams.support.codevalue.EnumCode;

/**
 * 授权模式
 * @author LIUYANLONG
 *
 */
public enum LoginModel implements EnumCode {
	PASSWORD("password", "用户密码"),
	AUTHCODE("authcode", "授权码"),
	NONE("none", "不限制");

	private String code;
	private String desc;

	LoginModel(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDesc() {
		return this.desc;
	}

}
