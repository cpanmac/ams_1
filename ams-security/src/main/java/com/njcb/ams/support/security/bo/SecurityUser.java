package com.njcb.ams.support.security.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.njcb.ams.util.AmsStringUtils;

/**
 * 安全属性用户
 * 
 * @author liuyanlong
 *
 */
public class SecurityUser implements UserDetails {
    private Integer id;
    private String token;
	private String loginName;
    private String userName;
    @JsonIgnore
    private String passWord;
    private String empNo;
    @JsonIgnore
    private String loginTime;
    @JsonIgnore
    private String loginIp;
	private List<String> roleCodes;
	private String orgnNo;
	private String orgnName;
	private String sysName;
	private String busiDate;
	@JsonIgnore
	private LoginModel loginModel = LoginModel.PASSWORD;

	private static final long serialVersionUID = 1L;

	
	/**
	 * 权限集合
	 */
	@Override
	@JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<GrantedAuthority> authorities = new ArrayList<>();
		List<String> roleList = this.getRoleCodes();
		if(null != roleList){
			for (String roleCode : roleList) {
				SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ROLE_"+AmsStringUtils.upperCase(roleCode));
				authorities.add(authority);
			}
		}
		if (AmsStringUtils.isNotEmpty(userName)) {
			SimpleGrantedAuthority authority = new SimpleGrantedAuthority("USER_"+AmsStringUtils.upperCase(userName));
			authorities.add(authority);
		}
		if (AmsStringUtils.isNotEmpty(orgnNo)) {
			SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ORG_"+AmsStringUtils.upperCase(orgnNo));
			authorities.add(authority);
		}
		return authorities;
	}

	@Override
	@JsonIgnore
	public String getPassword() {
		return passWord;
	}

	@Override
	@JsonIgnore
	public String getUsername() {
		return userName;
	}

	@JsonIgnore
	public void setLoginModel(LoginModel loginModel) {
		this.loginModel = loginModel;
	}

	@JsonIgnore
	public LoginModel getLoginModel() {
		return loginModel;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	
	public String getPassWord() {
		return passWord;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getUserName() {
		return userName;
	}

	// 账户是否未过期,过期无法验证
	@Override
	@JsonIgnore
	public boolean isAccountNonExpired() {
		return true;
	}

	// 指定用户是否解锁,锁定的用户无法进行身份验证
	@Override
	@JsonIgnore
	public boolean isAccountNonLocked() {
		return true;
	}

	// 指示是否已过期的用户的凭据(密码),过期的凭据防止认证
	@Override
	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return true;
	}

	// 是否可用 ,禁用的用户不能身份验证
	@Override
	@JsonIgnore
	public boolean isEnabled() {
		return true;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setSysName(String sysName) {
		this.sysName = sysName;
	}
	
	public String getSysName() {
		return sysName;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	@JsonIgnore
	public String getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}

	@JsonIgnore
	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public void setRoleCodes(List<String> roleCodes) {
		this.roleCodes = roleCodes;
	}
	
	public List<String> getRoleCodes() {
		return roleCodes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getToken() {
		return token;
	}
	
	public void setOrgnNo(String orgnNo) {
		this.orgnNo = orgnNo;
	}
	
	public String getOrgnNo() {
		return orgnNo;
	}
	
	public void setOrgnName(String orgnName) {
		this.orgnName = orgnName;
	}
	
	public String getOrgnName() {
		return orgnName;
	}
	
	public void setBusiDate(String busiDate) {
		this.busiDate = busiDate;
	}
	
	public String getBusiDate() {
		return busiDate;
	}

	@Override
	public int hashCode() {
		return loginName.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SecurityUser) {
			return loginName.equals(((SecurityUser) obj).loginName);
		}
		return false;
	}

}
