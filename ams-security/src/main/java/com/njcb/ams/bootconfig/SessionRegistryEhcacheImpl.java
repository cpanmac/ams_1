package com.njcb.ams.bootconfig;

import com.njcb.ams.util.AmsCacheUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.util.Assert;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author LOONG
 */
public class SessionRegistryEhcacheImpl implements SessionRegistry,
        ApplicationListener<SessionDestroyedEvent> {

    protected final Log logger = LogFactory.getLog(org.springframework.security.core.session.SessionRegistryImpl.class);

    private final ConcurrentMap<Object, Set<String>> principalMap;
    private final Map<String, SessionInformation> sessionIdMap;

    public SessionRegistryEhcacheImpl() {
        this.principalMap = new ConcurrentHashMap<>();
        this.sessionIdMap = new ConcurrentHashMap<>();
    }

    public SessionRegistryEhcacheImpl(ConcurrentMap<Object, Set<String>> principals, Map<String, SessionInformation> sessionIds) {
        this.principalMap = principals;
        this.sessionIdMap = sessionIds;
    }

    @Override
    public List<Object> getAllPrincipals() {
        return new ArrayList<>(principalMap.keySet());
    }

    @Override
    public List<SessionInformation> getAllSessions(Object principal,
                                                   boolean includeExpiredSessions) {
        final Set<String> sessionsUsedByPrincipal = getPrincipal(principal);

        if (sessionsUsedByPrincipal == null) {
            return Collections.emptyList();
        }

        List<SessionInformation> list = new ArrayList<>(
                sessionsUsedByPrincipal.size());

        for (String sessionId : sessionsUsedByPrincipal) {
            SessionInformation sessionInformation = getSessionInformation(sessionId);

            if (sessionInformation == null) {
                continue;
            }

            if (includeExpiredSessions || !sessionInformation.isExpired()) {
                list.add(sessionInformation);
            }
        }

        return list;
    }

    @Override
    public SessionInformation getSessionInformation(String sessionId) {
        Assert.hasText(sessionId, "SessionId required as per interface contract");
        return getSessionId(sessionId);
    }

    @Override
    public void onApplicationEvent(SessionDestroyedEvent event) {
        String sessionId = event.getId();
        removeSessionInformation(sessionId);
    }

    @Override
    public void refreshLastRequest(String sessionId) {
        Assert.hasText(sessionId, "SessionId required as per interface contract");

        SessionInformation info = getSessionInformation(sessionId);

        if (info != null) {
            info.refreshLastRequest();
        }
    }

    @Override
    public void registerNewSession(String sessionId, Object principal) {
        Assert.hasText(sessionId, "SessionId required as per interface contract");
        Assert.notNull(principal, "Principal required as per interface contract");

        if (logger.isDebugEnabled()) {
            logger.debug("Registering session " + sessionId + ", for principal "
                    + principal);
        }

        if (getSessionInformation(sessionId) != null) {
            removeSessionInformation(sessionId);
        }

        putSessionId(principal, sessionId);

        Set<String> sessionsUsedByPrincipal = getPrincipal(principal);

        if (sessionsUsedByPrincipal == null) {
            sessionsUsedByPrincipal = new CopyOnWriteArraySet<>();
            Set<String> prevSessionsUsedByPrincipal = putIfAbsentPrincipal(principal,
                    sessionsUsedByPrincipal);
            if (prevSessionsUsedByPrincipal != null) {
                sessionsUsedByPrincipal = prevSessionsUsedByPrincipal;
            }
        }

        sessionsUsedByPrincipal.add(sessionId);

        if (logger.isTraceEnabled()) {
            logger.trace("Sessions used by '" + principal + "' : "
                    + sessionsUsedByPrincipal);
        }
    }

    @Override
    public void removeSessionInformation(String sessionId) {
        Assert.hasText(sessionId, "SessionId required as per interface contract");

        SessionInformation info = getSessionInformation(sessionId);

        if (info == null) {
            return;
        }

        if (logger.isTraceEnabled()) {
            logger.debug("Removing session " + sessionId
                    + " from set of registered sessions");
        }

        removeSessionId(sessionId);

        Set<String> sessionsUsedByPrincipal = getPrincipal(info.getPrincipal());

        if (sessionsUsedByPrincipal == null) {
            return;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Removing session " + sessionId
                    + " from principal's set of registered sessions");
        }

        sessionsUsedByPrincipal.remove(sessionId);

        if (sessionsUsedByPrincipal.isEmpty()) {
            // No need to keep object in principals Map anymore
            if (logger.isDebugEnabled()) {
                logger.debug("Removing principal " + info.getPrincipal()
                        + " from registry");
            }
            removePrincipal(info.getPrincipal());
        }

        if (logger.isTraceEnabled()) {
            logger.trace("Sessions used by '" + info.getPrincipal() + "' : "
                    + sessionsUsedByPrincipal);
        }
    }

    private Set<String> getPrincipal(Object principal){
        String cacheKey = "session_principal_" + principal.hashCode();
        return (Set<String>)AmsCacheUtils.get(AmsCacheUtils.USER_DYNAMIC_CACHE,cacheKey);
//        return principalMap.get(principal);
    }

    private void removePrincipal(Object principal){
        String cacheKey = "session_principal_" + principal.hashCode();
        AmsCacheUtils.evict(AmsCacheUtils.USER_DYNAMIC_CACHE,cacheKey);
//        principalMap.remove(principal);
    }

    private Set<String> putIfAbsentPrincipal(Object principal,Set<String> sessionsUsedByPrincipal){
        String cacheKey = "session_principal_" + principal.hashCode();
        return (Set<String>)AmsCacheUtils.putIfAbsent(AmsCacheUtils.USER_DYNAMIC_CACHE,cacheKey,sessionsUsedByPrincipal);
//        return principalMap.putIfAbsent(principal,sessionsUsedByPrincipal);
    }


    private SessionInformation getSessionId(String sessionId){
        String cacheKey = "session_sessionId_" + sessionId;
        return (SessionInformation)AmsCacheUtils.get(AmsCacheUtils.USER_DYNAMIC_CACHE,cacheKey);
//        return sessionIdMap.get(sessionId);
    }

    private void putSessionId(Object principal,String sessionId){
        String cacheKey = "session_sessionId_" + sessionId;
        AmsCacheUtils.put(AmsCacheUtils.USER_DYNAMIC_CACHE,cacheKey,new SessionInformation(principal, sessionId, new Date()));
//        sessionIdMap.put(sessionId,new SessionInformation(principal, sessionId, new Date()));
    }

    private void removeSessionId(String sessionId){
        String cacheKey = "session_sessionId_" + sessionId;
        AmsCacheUtils.evict(AmsCacheUtils.USER_DYNAMIC_CACHE,cacheKey);
//        sessionIdMap.remove(sessionId);
    }

}