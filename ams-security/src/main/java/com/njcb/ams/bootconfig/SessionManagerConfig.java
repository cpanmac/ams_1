package com.njcb.ams.bootconfig;

import org.apache.catalina.Context;
import org.apache.catalina.Session;
import org.apache.catalina.session.StandardManager;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author LOONG
 */
@Configuration
public class SessionManagerConfig extends TomcatServletWebServerFactory {

    @Override
    protected void postProcessContext(Context context) {
        context.setManager(new CustomSessionManager());
    }

    @Bean
    public CustomSessionManager sessionManager() {
        return new CustomSessionManager();
    }

    class CustomSessionManager extends StandardManager {
        @Override
        public void add(Session session) {
            super.add(session);
        }
    }
}