/*
 * Copyright 2010-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.njcb.ams.bootconfig;

import com.njcb.ams.factory.comm.DataBus;
import com.njcb.ams.factory.comm.WebGlobalInfo;
import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.support.config.AmsConfigUtil;
import com.njcb.ams.support.security.bo.SecurityUser;
import com.njcb.ams.support.security.config.AmsSecurityConfiguration;
import com.njcb.ams.support.security.service.TokenAuthFilter;
import com.njcb.ams.util.AmsDateUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ClientAuthenticationProcessingFilter extends AbstractAuthenticationProcessingFilter {

	protected ClientAuthenticationProcessingFilter(String defaultFilterProcessesUrl) {
		super(defaultFilterProcessesUrl);
	}

	protected ClientAuthenticationProcessingFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
		super(requiresAuthenticationRequestMatcher);
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		super.doFilter(req, res, chain);
		try{
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			if(null != authentication){
				String username = authentication.getPrincipal().toString();
				if (null != AmsConfigUtil.getBean(AmsSecurityConfiguration.class)) {
					AmsSecurityConfiguration securityConfig = AmsConfigUtil.getBean(AmsSecurityConfiguration.class);
					SecurityUser securityUser = securityConfig.loadUserByUsername(username,null,null,null);
					securityUser.setSysName(StringUtils.isEmpty(SysBaseDefine.SYS_NAME) ? "管理系统" : SysBaseDefine.SYS_NAME);
					securityUser.setLoginIp(req.getRemoteAddr());
					securityUser.setLoginTime(AmsDateUtils.getCurrentTime14());
					WebGlobalInfo globalInfo = TokenAuthFilter.initWebGlobalInfo(securityUser);
					DataBus.setCurrentInstance(globalInfo);
					System.out.println(req.getClass());
					if(req instanceof HttpServletRequest){
						WebGlobalInfo.setGlobalInfo2HttpSession(((HttpServletRequest) req).getSession(), globalInfo);
					}
					DataBus.setObject(securityUser);
					securityUser.setToken("no activate");
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	 	return authentication;
	}
}