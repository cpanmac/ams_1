package com.njcb.ams.bootconfig;

import com.fasterxml.classmate.TypeResolver;
import com.google.common.collect.Sets;
import com.njcb.ams.pojo.dto.UsernamePasswordLogin;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.OperationBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiDescription;
import springfox.documentation.service.Operation;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ApiListingScannerPlugin;
import springfox.documentation.spi.service.contexts.DocumentationContext;
import springfox.documentation.spring.web.readers.operation.CachingOperationNameGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author LOONG
 */
@Component
public class SwaggerSecurityAddtion implements ApiListingScannerPlugin {

    @Override
    public List<ApiDescription> apply(DocumentationContext documentationContext) {
        documentationContext.getTags().add(new Tag("login", "登录、登出"));
        ApiDescription loginDesc = getLoginDesc();
        ApiDescription logoutDesc = getLogoutDesc();
        List<ApiDescription> apiDescriptionList = new ArrayList<>(Arrays.asList(loginDesc, logoutDesc));
        return apiDescriptionList;
    }

    @Override
    public boolean supports(DocumentationType documentationType) {
        return DocumentationType.SWAGGER_2.equals(documentationType);
    }


    private ApiDescription getLoginDesc(){
        //登录接口
        //1.定义参数
        Parameter loginParam = new ParameterBuilder()
                .name("UsernamePasswordLogin")
                .description("用户密码输入实体json")
                .type(new TypeResolver().resolve(UsernamePasswordLogin.class))
                .modelRef(new ModelRef("UsernamePasswordLogin"))
                .parameterType("body")
                .required(true).scalarExample("{\"username\":\"admin\",\"password\":\"88888888\"}")
                .defaultValue("{\"username\":\"admin\",\"password\":\"88888888\"}")
                .build();
        Operation loginOperation = new OperationBuilder(new CachingOperationNameGenerator())
                .method(HttpMethod.POST)
                .summary("登录-用户密码模式")
                .notes("用户密码模式")
                .tags(Sets.newHashSet("login"))
                .responseMessages(Sets.newHashSet(new ResponseMessageBuilder().responseModel(new ModelRef("EntityResponse")).code(200).message("OK").build()))
                .consumes(Sets.newHashSet(MediaType.APPLICATION_JSON_VALUE))
                .produces(Sets.newHashSet(MediaType.APPLICATION_JSON_VALUE))
                .parameters(Arrays.asList(loginParam))
                .build();
        //3.每个接口路径对应一个 ApiDescription
        ApiDescription loginDesc = new ApiDescription(null, "/login", "登入", Arrays.asList(loginOperation), false);
        return loginDesc;
    }

    private ApiDescription getLogoutDesc(){
        //登出接口
        Operation logoutOperation = new OperationBuilder(new CachingOperationNameGenerator())
                .method(HttpMethod.GET)
                .summary("登出")
                .notes("退出登录")
                .tags(Sets.newHashSet("login"))
                .responseMessages(Sets.newHashSet(new ResponseMessageBuilder().code(200).message("OK").build()))
                .build();
        ApiDescription logoutDesc = new ApiDescription(null, "/logout", "登出", Arrays.asList(logoutOperation), false);
        return logoutDesc;
    }
}
