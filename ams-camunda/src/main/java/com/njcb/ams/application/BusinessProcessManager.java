package com.njcb.ams.application;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.support.annotation.Interaction;
import com.njcb.ams.support.annotation.Uncheck;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;

/**
 * 工作流管理类
 *
 * @author liuyanlong
 */
@Lazy(false)
@Interaction(groupName = "ZZ.AMS.WORKFLOW.PROCESS")
public class BusinessProcessManager {
	private static final Logger logger = LoggerFactory.getLogger(BusinessProcessManager.class);


	@Uncheck
	public static BusinessProcessManager getInstance() {
		logger.debug("Instance BusinessManager");
		return AppContext.getBean(BusinessProcessManager.class);
	}

}
