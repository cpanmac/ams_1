package com.njcb.ams.portal;

import com.njcb.ams.support.autoconfigure.annotation.EnableAmsManagement;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.WebApplicationInitializer;

@SpringBootApplication(scanBasePackages = {"com.njcb"})
@MapperScan("com.njcb.*.mapper")
@EnableAmsManagement(scanBasePackages = { "com.njcb" })
public class CamundaApplication extends SpringBootServletInitializer implements WebApplicationInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(CamundaApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(CamundaApplication.class, args);
	}
}
