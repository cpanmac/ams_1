package com.njcb.ams.service.workflow;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.njcb.ams.pojo.vo.ApprovalCommentsVO;
import com.njcb.ams.portal.WebApplication;

/**
 * 流程任务处理服务
 * 
 * @author liuyanlong
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { WebApplication.class })
@ActiveProfiles("dev") // 指定使用application-test.yml
public class WrokflowTaskServiceTest {
	
	@Resource
	public WrokflowTaskService service;

	@Test
	public void getProcessVariables() {
		List<ApprovalCommentsVO> list = service.getProcessVariables("90023");
		for (ApprovalCommentsVO task : list) {
			System.out.println(task);
		}
	}

}
