package com.njcb.ams.web.controller;

import java.util.HashMap;
import java.util.List;

import org.activiti.engine.impl.persistence.entity.SuspensionState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.njcb.ams.application.BusinessWrokflowManager;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.pojo.vo.ApprovalCommentsVO;
import com.njcb.ams.pojo.vo.RunTaskQueryVO;
import com.njcb.ams.pojo.vo.RunTaskVO;
import com.njcb.ams.pojo.vo.TaskDoWorkVO;
import com.njcb.ams.factory.comm.DataBus;
import com.njcb.ams.store.page.Page;
import com.njcb.ams.store.page.PageHandle;
import com.njcb.ams.util.AmsAssert;

import io.swagger.annotations.Api;

/**
 * 代办任务处理
 * 
 * @author liuyanlong
 *
 */
@RestController
@RequestMapping("/workflow/task")
@Api(value = "task", tags = "task")
public class TaskRunController {
	@Autowired
	private BusinessWrokflowManager businessworkManager;

	/**
	 * 代办任务查询
	 *
	 * @param inBean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "list/todo", method = RequestMethod.POST)
	@ResponseBody
	public PageResponse<RunTaskVO> runTaskInfoQuery(@RequestBody RunTaskQueryVO inBean) throws Exception {
		PageHandle.startPage(inBean);
		List<RunTaskVO> list = businessworkManager.queryRuTask(inBean);
		Page page = PageHandle.endPage();
		return PageResponse.build(list, page.getTotal());
	}

	
	/**
	 * 当前用户代办任务查询
	 *
	 * @param inBean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "list/currusertodo", method = RequestMethod.POST)
	@ResponseBody
	public PageResponse<RunTaskVO> runTaskInfoQueryByCurrUser(@RequestBody RunTaskQueryVO inBean) throws Exception {
		inBean.setAssignee(DataBus.getLoginName());
		StringBuffer roleCodes = new StringBuffer();
		DataBus.getRoleCodes().forEach(code -> {
			roleCodes.append(", '" + code + "'");
		});
		roleCodes.deleteCharAt(0);
		inBean.setCandidateGroup(roleCodes.toString());
		inBean.setSuspensionState(SuspensionState.ACTIVE.getStateCode());
		return runTaskInfoQuery(inBean);
	}
	
	
	/**
	 * 任务办理
	 * 
	 * @param doWorkVO
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "dowork", method = RequestMethod.POST)
	@ResponseBody
	public Response doWork(@RequestBody TaskDoWorkVO doWorkVO) throws Exception {
		AmsAssert.notNull(doWorkVO.getResult(),"结果不能为空");
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("handleUser", doWorkVO.getHandleUser());
		param.put("result", doWorkVO.getResult());
		param.put("comments", doWorkVO.getComments());
		businessworkManager.doWork(doWorkVO.getProcInstId(), doWorkVO.getTaskId(), param);
		return Response.buildSucc();
	}
	
	/**
	 * 当前用户代办任务查询
	 *
	 * @param inBean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "list/comments", method = RequestMethod.POST)
	@ResponseBody
	public PageResponse<ApprovalCommentsVO> approvalComments(@RequestBody ApprovalCommentsVO inBean) throws Exception {
		List<ApprovalCommentsVO> taskList = businessworkManager.getProcessVariables(inBean.getProcInstId());
		return PageResponse.build(taskList, 100);
	}

}
