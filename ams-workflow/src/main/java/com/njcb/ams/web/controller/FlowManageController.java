package com.njcb.ams.web.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.njcb.ams.application.BusinessWrokflowManager;
import com.njcb.ams.pojo.dto.standard.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 流程配置服务
 * 
 * @author liuyanlong
 *
 */
@RestController
@RequestMapping("/workflow/manage")
@Api(value = "workflow", tags = "workflow")
public class FlowManageController {

	@Autowired
	private BusinessWrokflowManager businessworkManager;
	
	/**
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "挂起流程", notes = "处理流程任务")
	@RequestMapping(value = "suspend", method = RequestMethod.POST)
	@ResponseBody
	public Response suspend(@RequestBody HashMap<String, String> map) throws Exception {
		businessworkManager.suspendWork(map.get("procInstId"));
		return Response.buildSucc();
	}
	
	/**
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "唤醒流程", notes = "处理流程任务")
	@RequestMapping(value = "activate", method = RequestMethod.POST)
	@ResponseBody
	public Response activateWork(@RequestBody HashMap<String, String> map) throws Exception {
		businessworkManager.activateWork(map.get("procInstId"));
		return Response.buildSucc();
	}
	
	
	/**
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "删除代办流程", notes = "处理流程任务")
	@RequestMapping(value = "deletetodo", method = RequestMethod.POST)
	@ResponseBody
	public Response deleteTodo(@RequestBody HashMap<String, String> map) throws Exception {
		businessworkManager.deleteTodoWork(map.get("procInstId"), map.get("reason"));
		return Response.buildSucc();
	}
	
	/**
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "删除已办流程", notes = "处理流程任务")
	@RequestMapping(value = "deletedown", method = RequestMethod.POST)
	@ResponseBody
	public Response deleteDown(@RequestBody HashMap<String, String> map) throws Exception {
		businessworkManager.deleteDownWork(map.get("procInstId"), map.get("reason"));
		return Response.buildSucc();
	}

}
