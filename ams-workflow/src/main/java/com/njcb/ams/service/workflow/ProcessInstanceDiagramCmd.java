package com.njcb.ams.service.workflow;

import java.io.InputStream;
import java.util.List;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.spring.ProcessEngineFactoryBean;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProcessInstanceDiagramCmd implements Command<InputStream> {

	@Autowired
	private ProcessEngineFactoryBean processEngine;
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private SpringProcessEngineConfiguration processEngineConfiguration;

	protected String processDefinitionId;
	protected String processInstanceId;
	protected List<String> highLightedActivitis;
	protected List<String> highLightedFlows;

	public void setProcessInstanceInfo(String processDefinitionId, String processInstanceId, List<String> highLightedActivitis, List<String> highLightedFlows) {
		this.processDefinitionId = processDefinitionId;
		this.processInstanceId = processInstanceId;
		this.highLightedActivitis = highLightedActivitis;
		this.highLightedFlows = highLightedFlows;
	}

	@Override
	public InputStream execute(CommandContext cmd) {
		BpmnModel pmnModel = repositoryService.getBpmnModel(processDefinitionId);
		String fontName = processEngineConfiguration.getActivityFontName();
		InputStream is = processEngine.getProcessEngineConfiguration().getProcessDiagramGenerator().generateDiagram(pmnModel, "png", highLightedActivitis, highLightedFlows, fontName, fontName, fontName, null,
				1.0);
		return is;
	}
}
