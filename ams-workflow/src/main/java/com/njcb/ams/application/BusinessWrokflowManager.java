package com.njcb.ams.application;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.pojo.vo.ApprovalCommentsVO;
import com.njcb.ams.pojo.vo.HistoricTaskInstanceVO;
import com.njcb.ams.pojo.vo.RunTaskQueryVO;
import com.njcb.ams.pojo.vo.RunTaskVO;
import com.njcb.ams.repository.entity.CommFlowHandleview;
import com.njcb.ams.service.workflow.WrokflowConfigService;
import com.njcb.ams.service.workflow.WrokflowDefinitionService;
import com.njcb.ams.service.workflow.WrokflowTaskService;
import com.njcb.ams.support.annotation.Interaction;
import com.njcb.ams.support.annotation.Trader;
import com.njcb.ams.support.annotation.Uncheck;
import com.njcb.ams.util.AmsDateUtils;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.persistence.entity.SuspensionState;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 工作流交易类
 *
 * @author liuyanlong
 */
@Lazy(false)
@Interaction(groupName = "ZZ.AMS.WORKFLOW.MANAGER")
public class BusinessWrokflowManager {
	private static final Logger logger = LoggerFactory.getLogger(BusinessWrokflowManager.class);

	@Autowired
	private WrokflowDefinitionService wrokflowDefinitionService;
	@Autowired
	private WrokflowTaskService wrokflowDisposedService;
	@Autowired
	private WrokflowConfigService wrokflowConfigService;

	@Uncheck
	public static BusinessWrokflowManager getInstance() {
		logger.debug("Instance BusinessManager");
		return AppContext.getBean(BusinessWrokflowManager.class);
	}

	@Trader(tradeCode = "PD1001", tradeName = "流程发布")
	public void processDeploy(String processName) {
		wrokflowDefinitionService.processDeploy(processName);
	}
	

	@Trader(tradeCode = "TQ1001", tradeName = "已部署流程查询")
	public List<ProcessDefinition> taskInfoPendingQuery(ProcessDefinition inBean) {
		return wrokflowDefinitionService.queryProcessDefinition(inBean);
	}
	
	@Trader(tradeCode = "TQ1002", tradeName = "已部署流程删除")
	public void taskInfoPendingDelete(ProcessDefinition inBean) {
		wrokflowDefinitionService.deleteProcessDefinition(inBean);
	}

	@Trader(tradeCode = "TQ2001", tradeName = "待办任务查询")
	public List<RunTaskVO> queryRuTask(RunTaskQueryVO queryBean) {
		List<Task> taskList = wrokflowDisposedService.queryNativeRuTask(queryBean);
		List<RunTaskVO> rtList = new ArrayList<RunTaskVO>();
		for (Task task : taskList) {
			List<ProcessInstance> instanceList = wrokflowDisposedService.queryProcessInstance(task.getProcessInstanceId());
			ProcessInstance processInstance = instanceList.get(0);
			RunTaskVO tvo = new RunTaskVO();
			tvo.setId(task.getId());
			tvo.setBusinessKey(processInstance.getBusinessKey());
			tvo.setExecutionId(task.getExecutionId());
			tvo.setProcInstId(task.getProcessInstanceId());
			tvo.setProcDefId(task.getProcessDefinitionId());
			tvo.setProcDefName(processInstance.getProcessDefinitionName());
			tvo.setName(task.getName());
			tvo.setTaskDefKey(task.getTaskDefinitionKey());
			tvo.setOwner(task.getOwner());
			tvo.setAssignee(task.getAssignee());
			tvo.setCreateTime(AmsDateUtils.getString18(task.getCreateTime()));
			tvo.setSuspensionState(task.isSuspended()?SuspensionState.SUSPENDED.getStateCode():SuspensionState.ACTIVE.getStateCode());
			rtList.add(tvo);
		}
		return rtList;
	}

	@Trader(tradeCode = "TQ3001", tradeName = "已办任务查询")
	public List<HistoricTaskInstanceVO> queryHaveDone(HistoricTaskInstance queryBean) {
		List<HistoricTaskInstance> taskList = wrokflowDisposedService.queryHaveDone(queryBean);
		List<HistoricTaskInstanceVO> rtList = new ArrayList<HistoricTaskInstanceVO>();
		for (HistoricTaskInstance historicTaskInstance : taskList) {
			HistoricTaskInstanceVO hvo = new HistoricTaskInstanceVO();
			List<HistoricProcessInstance> instanceList = wrokflowDisposedService.queryHistoricProcessInstance(historicTaskInstance.getProcessInstanceId());
			HistoricProcessInstance hiProcessInstance = instanceList.get(0);
			hvo.setId(historicTaskInstance.getId());
			hvo.setProcDefId(historicTaskInstance.getProcessDefinitionId());
			hvo.setProcDefName(hiProcessInstance.getProcessDefinitionName());
			hvo.setBusinessKey(hiProcessInstance.getBusinessKey());
			hvo.setTaskDefKey(historicTaskInstance.getTaskDefinitionKey());
			hvo.setProcInstId(historicTaskInstance.getProcessInstanceId());
			hvo.setExecutionId(historicTaskInstance.getExecutionId());
			hvo.setParentTaskId(historicTaskInstance.getParentTaskId());
			hvo.setName(historicTaskInstance.getName());
			hvo.setDescription(historicTaskInstance.getDescription());
			hvo.setOwner(historicTaskInstance.getOwner());
			hvo.setAssignee(historicTaskInstance.getAssignee());
			hvo.setStartTime(AmsDateUtils.getString18(historicTaskInstance.getStartTime()));
			hvo.setClaimTime(AmsDateUtils.getString18(historicTaskInstance.getClaimTime()));
			hvo.setEndTime(AmsDateUtils.getString18(historicTaskInstance.getEndTime()));
			hvo.setDeleteReason(historicTaskInstance.getDeleteReason());
			hvo.setPriority(historicTaskInstance.getPriority());
			hvo.setDueDate(AmsDateUtils.getString18(historicTaskInstance.getDueDate()));
			hvo.setFormKey(historicTaskInstance.getFormKey());
			hvo.setCategory(historicTaskInstance.getCategory());
			hvo.setTenantId(historicTaskInstance.getTenantId());
			rtList.add(hvo);
		}
		return rtList;
	}

	@Trader(tradeCode = "TW1001", tradeName = "流程启动")
	public String workFlowStart(String processkey, String businessKey, HashMap<String, Object> param) {
		return wrokflowDisposedService.startProcessInstanceByKey(processkey, businessKey, param);
	}

	@Trader(tradeCode = "TW2001", tradeName = "流程任务处理")
	public void doWork(String procInstId, String taskId, HashMap<String, Object> param) {
		wrokflowDisposedService.completeTask(procInstId, taskId, param);
	}

	@Trader(tradeCode = "TW2003", tradeName = "设置流程变量")
	public void setVariables(String taskId, HashMap<String, Object> param) {
		wrokflowDisposedService.setVariables(taskId, param);
	}

	@Trader(tradeCode = "TW2004", tradeName = "设置流程变量")
	public Map<String, Object> getVariables(String taskId) {
		return wrokflowDisposedService.getVariables(taskId);
	}
	
	@Trader(tradeCode = "TW3001", tradeName = "流程监控图")
	public String displayProcessDiagram(String taskId) {
		return wrokflowDisposedService.displayProcessDiagram(taskId);
	}
	
	@Trader(tradeCode = "TW4001", tradeName = "挂起流程")
	public void suspendWork(String procInstId) {
		wrokflowDisposedService.suspendWork(procInstId);
	}
	
	@Trader(tradeCode = "TW4002", tradeName = "唤醒流程")
	public void activateWork(String procInstId) {
		wrokflowDisposedService.activateWork(procInstId);
	}
	
	@Trader(tradeCode = "TW5001", tradeName = "删除代办")
	public void deleteTodoWork(String procInstId, String reason) {
		wrokflowDisposedService.deleteTodoWork(procInstId, reason);
	}
	
	@Trader(tradeCode = "TW5002", tradeName = "删除已办")
	public void deleteDownWork(String procInstId, String reason) {
		wrokflowDisposedService.deleteDownWork(procInstId, reason);
	}
	
	@Trader(tradeCode = "FC1001", tradeName = "查询视图配置")
	public CommFlowHandleview queryHandleView(String processDefKey,String taskDefKey) {
		return wrokflowConfigService.queryHandleView(processDefKey, taskDefKey);
	}
	
	@Trader(tradeCode = "PV1001", tradeName = "查询审批历史")
	public List<ApprovalCommentsVO> getProcessVariables(String processInstanceId) {
		return wrokflowDisposedService.getProcessVariables(processInstanceId);
	}

}
