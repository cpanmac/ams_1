package com.njcb.ams.application;

import java.util.List;

import org.activiti.engine.repository.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.service.workflow.ProcessDesignerService;
import com.njcb.ams.support.annotation.Interaction;
import com.njcb.ams.support.annotation.Trader;
import com.njcb.ams.support.annotation.Uncheck;

/**
 * 工作流管理类
 *
 * @author liuyanlong
 */
@Lazy(false)
@Interaction(groupName = "ZZ.AMS.WORKFLOW.PROCESS")
public class BusinessProcessManager {
	private static final Logger logger = LoggerFactory.getLogger(BusinessProcessManager.class);

	@Autowired
	private ProcessDesignerService processDesignerService;

	@Uncheck
	public static BusinessProcessManager getInstance() {
		logger.debug("Instance BusinessManager");
		return AppContext.getBean(BusinessProcessManager.class);
	}

	@Trader(tradeCode = "PD3001", tradeName = "新建流程模型")
	public String newModel() {
		return processDesignerService.newModel();
	}
	
	@Trader(tradeCode = "PD3002", tradeName = "获取流程模型")
	public List<Model> modelList() {
		return processDesignerService.modelList();
	}

	@Trader(tradeCode = "PD3003", tradeName = "删除流程模型")
	public void deleteModel(String modelId) {
		processDesignerService.deleteModel(modelId);
	}
	
	@Trader(tradeCode = "PD3004", tradeName = "发布流程模型")
	public void deploy(String modelId) throws Exception {
		processDesignerService.deploy(modelId);
	}
	
	@Trader(tradeCode = "PD3005", tradeName = "发布流程导出")
	public String export(String modelId) throws Exception {
		return processDesignerService.export(modelId);
	}
	
	@Trader(tradeCode = "PD3006", tradeName = "导入流程模型")
	public void importModel(String fileName, String bpmnFilePath) throws Exception {
		processDesignerService.importModel(fileName, bpmnFilePath);
	}
}
