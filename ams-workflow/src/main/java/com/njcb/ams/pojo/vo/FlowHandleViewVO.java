package com.njcb.ams.pojo.vo;

/**
 * @author 刘彦龙
 *
 */
public class FlowHandleViewVO {
	private String processDefKey;
	private String taskDefKey;
	private String businessKey;
	public String getProcessDefKey() {
		return processDefKey;
	}
	public void setProcessDefKey(String processDefKey) {
		this.processDefKey = processDefKey;
	}
	public String getTaskDefKey() {
		return taskDefKey;
	}
	public void setTaskDefKey(String taskDefKey) {
		this.taskDefKey = taskDefKey;
	}
	public String getBusinessKey() {
		return businessKey;
	}
	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

	@Override
	public String toString() {
		return "FlowHandleViewVO{" +
				"processDefKey='" + processDefKey + '\'' +
				", taskDefKey='" + taskDefKey + '\'' +
				", businessKey='" + businessKey + '\'' +
				'}';
	}
}