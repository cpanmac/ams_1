package com.njcb.ams.pojo.vo;

import com.njcb.ams.util.WorkFlowVariable.ApprovalResult;

/**
 * 
 * @author 刘彦龙
 *
 */
public class TaskDoWorkVO {
    
    private String procInstId;
    private String taskId;
    private String tasknodeId;

    /**
	 * 审批结果
	 */
	private ApprovalResult result = ApprovalResult.nothing;
	
	/**
	 * 意见
	 */
	private String comments;

	/**
	 * 处理人
	 */
	private String handleUser;

	public String getHandleUser() {
		return handleUser;
	}

	public void setHandleUser(String handleUser) {
		this.handleUser = handleUser;
	}

	public String getProcInstId() {
		return procInstId;
	}
	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getTasknodeId() {
		return tasknodeId;
	}
	public void setTasknodeId(String tasknodeId) {
		this.tasknodeId = tasknodeId;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getComments() {
		return comments;
	}
	
	public void setResult(String result) {
		this.result = ApprovalResult.init(result);
	}
	
	public String getResult() {
		return result.value;
	}

	@Override
	public String toString() {
		return "TaskDoWorkVO{" +
				"procInstId='" + procInstId + '\'' +
				", taskId='" + taskId + '\'' +
				", tasknodeId='" + tasknodeId + '\'' +
				", result=" + result +
				", comments='" + comments + '\'' +
				", handleUser='" + handleUser + '\'' +
				'}';
	}
}