package com.njcb.ams.pojo.vo;

import com.njcb.ams.store.page.Page;

/**
 * 流程定义对象
 * 
 * @author 刘彦龙
 *
 */
public class ProcessDefinitionVO extends Page {
	private String id;

	private Integer rev;

	private Object category;

	private String name;

	private String key;

	private Integer version;

	private Object deploymentId;

	private Object resourceName;

	private String dgrmResourceName;

	private Object description;

	private Integer hasStartFormKey;

	private Integer hasGraphicalNotation;

	private Integer suspensionState;

	private String tenantId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getRev() {
		return rev;
	}

	public void setRev(Integer rev) {
		this.rev = rev;
	}

	public Object getCategory() {
		return category;
	}

	public void setCategory(Object category) {
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Object getDeploymentId() {
		return deploymentId;
	}

	public void setDeploymentId(Object deploymentId) {
		this.deploymentId = deploymentId;
	}

	public Object getResourceName() {
		return resourceName;
	}

	public void setResourceName(Object resourceName) {
		this.resourceName = resourceName;
	}

	public String getDgrmResourceName() {
		return dgrmResourceName;
	}

	public void setDgrmResourceName(String dgrmResourceName) {
		this.dgrmResourceName = dgrmResourceName;
	}

	public Object getDescription() {
		return description;
	}

	public void setDescription(Object description) {
		this.description = description;
	}

	public Integer getHasStartFormKey() {
		return hasStartFormKey;
	}

	public void setHasStartFormKey(Integer hasStartFormKey) {
		this.hasStartFormKey = hasStartFormKey;
	}

	public Integer getHasGraphicalNotation() {
		return hasGraphicalNotation;
	}

	public void setHasGraphicalNotation(Integer hasGraphicalNotation) {
		this.hasGraphicalNotation = hasGraphicalNotation;
	}

	public Integer getSuspensionState() {
		return suspensionState;
	}

	public void setSuspensionState(Integer suspensionState) {
		this.suspensionState = suspensionState;
	}

	public Object getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@Override
	public String toString() {
		return "ProcessDefinitionVO{" +
				"id='" + id + '\'' +
				", rev=" + rev +
				", category=" + category +
				", name='" + name + '\'' +
				", key='" + key + '\'' +
				", version=" + version +
				", deploymentId=" + deploymentId +
				", resourceName=" + resourceName +
				", dgrmResourceName='" + dgrmResourceName + '\'' +
				", description=" + description +
				", hasStartFormKey=" + hasStartFormKey +
				", hasGraphicalNotation=" + hasGraphicalNotation +
				", suspensionState=" + suspensionState +
				", tenantId='" + tenantId + '\'' +
				'}';
	}
}