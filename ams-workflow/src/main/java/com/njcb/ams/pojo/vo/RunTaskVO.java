package com.njcb.ams.pojo.vo;

import java.util.Date;

/**
 * 待办任务对象
 * 
 * @author 刘彦龙
 *
 */
public class RunTaskVO {
    private String id;

    /**
     * 流程实例ID
     */
    private String procInstId;

    /**
     * 流程定义ID
     */
    private String procDefId;
   
    /**
     * 流程定义名称
     */
    private String procDefName;
    
    /**
     * 业务主键
     */
    private String businessKey;

    private String executionId;
    
    private String name;

    private String parentTaskId;

    private String description;

    private String taskDefKey;

    private String owner;

    private String assignee;

    private String delegation;

    private Integer priority;

    private String createTime;

    private String dueDate;

    private String category;

    /**
     * 挂起状态
     */
    private Integer suspensionState;

    private String tenantId;

    private String formKey;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusinessKey() {
		return businessKey;
	}
    
    public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

    public String getExecutionId() {
        return executionId;
    }

    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

    public String getProcInstId() {
        return procInstId;
    }

    public void setProcInstId(String procInstId) {
        this.procInstId = procInstId;
    }

    public String getProcDefId() {
        return procDefId;
    }

    public void setProcDefId(String procDefId) {
        this.procDefId = procDefId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentTaskId() {
        return parentTaskId;
    }

    public void setParentTaskId(String parentTaskId) {
        this.parentTaskId = parentTaskId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTaskDefKey() {
        return taskDefKey;
    }

    public void setTaskDefKey(String taskDefKey) {
        this.taskDefKey = taskDefKey;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getDelegation() {
        return delegation;
    }

    public void setDelegation(String delegation) {
        this.delegation = delegation;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setSuspensionState(Integer suspensionState) {
        this.suspensionState = suspensionState;
    }

    public Integer getSuspensionState() {
        return suspensionState;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getFormKey() {
        return formKey;
    }

    public void setFormKey(String formKey) {
        this.formKey = formKey;
    }
    
    public void setProcDefName(String procDefName) {
		this.procDefName = procDefName;
	}
    
    public String getProcDefName() {
		return procDefName;
	}

    @Override
    public String toString() {
        return "RunTaskVO{" +
                "id='" + id + '\'' +
                ", procInstId='" + procInstId + '\'' +
                ", procDefId='" + procDefId + '\'' +
                ", procDefName='" + procDefName + '\'' +
                ", businessKey='" + businessKey + '\'' +
                ", executionId='" + executionId + '\'' +
                ", name='" + name + '\'' +
                ", parentTaskId='" + parentTaskId + '\'' +
                ", description='" + description + '\'' +
                ", taskDefKey='" + taskDefKey + '\'' +
                ", owner='" + owner + '\'' +
                ", assignee='" + assignee + '\'' +
                ", delegation='" + delegation + '\'' +
                ", priority=" + priority +
                ", createTime='" + createTime + '\'' +
                ", dueDate='" + dueDate + '\'' +
                ", category='" + category + '\'' +
                ", suspensionState=" + suspensionState +
                ", tenantId='" + tenantId + '\'' +
                ", formKey='" + formKey + '\'' +
                '}';
    }
}