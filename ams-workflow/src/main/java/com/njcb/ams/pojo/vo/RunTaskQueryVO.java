package com.njcb.ams.pojo.vo;

import com.njcb.ams.store.page.Page;

/**
 * 待办任务对象
 * 
 * @author 刘彦龙
 *
 */
public class RunTaskQueryVO extends Page{
    
	/**
	 * 指定处理人
	 */
    private String assignee;
    
    /**
     * 候选处理组
     */
    private String candidateGroup;
    
    /**
     * 候选处理人
     */
    private String candidateUser;
    
    /**
     * 模糊任务名称
     */
    private String nameLike;

	/**
	 * 挂起状态
	 */
	private Integer suspensionState;

	public void setSuspensionState(Integer suspensionState) {
		this.suspensionState = suspensionState;
	}

	public Integer getSuspensionState() {
		return suspensionState;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
    
    public String getAssignee() {
		return assignee;
	}
    
    public void setCandidateGroup(String candidateGroup) {
		this.candidateGroup = candidateGroup;
	}
    
    public String getCandidateGroup() {
		return candidateGroup;
	}
    
    public void setCandidateUser(String candidateUser) {
		this.candidateUser = candidateUser;
	}
    
    public String getCandidateUser() {
		return candidateUser;
	}
    
    public String getNameLike() {
		return nameLike;
	}
    
    public void setNameLike(String nameLike) {
		this.nameLike = nameLike;
	}

	@Override
	public String toString() {
		return "RunTaskQueryVO{" +
				"assignee='" + assignee + '\'' +
				", candidateGroup='" + candidateGroup + '\'' +
				", candidateUser='" + candidateUser + '\'' +
				", nameLike='" + nameLike + '\'' +
				", suspensionState=" + suspensionState +
				'}';
	}
}