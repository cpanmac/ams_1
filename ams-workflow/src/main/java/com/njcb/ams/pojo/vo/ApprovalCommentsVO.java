package com.njcb.ams.pojo.vo;

import com.njcb.ams.util.WorkFlowVariable.ApprovalResult;

/**
 * 审批意见
 * 
 * @author 刘彦龙
 *
 */
public class ApprovalCommentsVO {
    
	/**
	 * 任务ID
	 */
    private String taskId;
    
    /**
     * 流程实例ID
     */
    private String procInstId;
    
    /**
     * 业务主键
     */
    private String businessKey;
    
    /**
     * 办理节点
     */
    private String handlerNode;
    
    /**
     * 办理人角色
     */
    private String handlerRole;
    
    /**
     * 办理人姓名
     */
    private String handlerName;
    
    /**
     * 办理结果
     */
    private ApprovalResult result = ApprovalResult.nothing;
    
    /**
     * 办理意见
     */
    private String comments;
    
    /**
     * 任务开始时间
     */
    private String startTime;
    
    /**
     * 任务结束时间
     */
    private String endTime;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getHandlerRole() {
		return handlerRole;
	}

	public void setHandlerRole(String handlerRole) {
		this.handlerRole = handlerRole;
	}

	public String getHandlerName() {
		return handlerName;
	}

	public void setHandlerName(String handlerName) {
		this.handlerName = handlerName;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getBusinessKey() {
		return businessKey;
	}
	
	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}
	
	public String getProcInstId() {
		return procInstId;
	}
	
	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}
	
	public void setResult(String result) {
		this.result = ApprovalResult.init(result);
	}
	
	public String getResult() {
		return result.value;
	}
	
	public void setHandlerNode(String handlerNode) {
		this.handlerNode = handlerNode;
	}
	
	public String getHandlerNode() {
		return handlerNode;
	}
	
	public String getEndTime() {
		return endTime;
	}
	
	public String getStartTime() {
		return startTime;
	}
	
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@Override
	public String toString() {
		return "ApprovalCommentsVO{" +
				"taskId='" + taskId + '\'' +
				", procInstId='" + procInstId + '\'' +
				", businessKey='" + businessKey + '\'' +
				", handlerNode='" + handlerNode + '\'' +
				", handlerRole='" + handlerRole + '\'' +
				", handlerName='" + handlerName + '\'' +
				", result=" + result +
				", comments='" + comments + '\'' +
				", startTime='" + startTime + '\'' +
				", endTime='" + endTime + '\'' +
				'}';
	}
}