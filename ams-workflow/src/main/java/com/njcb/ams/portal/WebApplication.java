package com.njcb.ams.portal;

import org.activiti.spring.boot.JpaProcessEngineAutoConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.WebApplicationInitializer;

@SpringBootApplication(scanBasePackages = { "com.njcb.ams" }, exclude = {
		org.activiti.spring.boot.SecurityAutoConfiguration.class, JpaProcessEngineAutoConfiguration.class })
@EnableTransactionManagement
@MapperScan("com.njcb.*.mapper")
@EnableCaching
public class WebApplication extends SpringBootServletInitializer implements WebApplicationInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(WebApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(WebApplication.class, args);
	}
}
