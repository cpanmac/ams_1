package com.njcb.ams.batch;

import com.njcb.ams.BaseTest;
import com.njcb.ams.factory.domain.AppContext;
import org.junit.Test;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @author LOONG
 */
@Component
public class JobInvokerTest extends BaseTest {
    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    @Qualifier("processJob")
    private Job processJob;

    @Test
    public void startJob() throws Exception {
        JobLauncher jobLauncher = (JobLauncher) AppContext.getBean("jobLauncher");
        JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
                .toJobParameters();
        jobLauncher.run(processJob, jobParameters);
        System.out.println("Batch job has been invoked");
    }
}
