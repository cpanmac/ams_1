package com.njcb.ams.web.controller;

import com.njcb.ams.pojo.dto.standard.Response;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author LOONG
 */
@RequestMapping("/open/job")
@RestController
public class BatchInvokerController {

	@Autowired
	private JobLauncher jobLauncher;

	@Autowired
	private Job processJob;
	
	/**
	 * 	首页
	 */
	@RequestMapping(value = "/invoker")
	public Response invoker() throws Exception {
		JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
				.toJobParameters();
		jobLauncher.run(processJob, jobParameters);
		return Response.buildSucc();
	}
	
}
