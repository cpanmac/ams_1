package com.njcb.ams.web.controller;

import com.njcb.ams.pojo.dto.standard.Response;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 批量管理接口
 *
 * @author LOONG
 */
@RequestMapping("/batch/admin")
@RestController
public class BatchAdminController {

    @RequestMapping(value = "/jobs")
    public Response jobs() {
        return Response.buildSucc();
    }

    @RequestMapping(value = "/jobs/{jobName}", method = RequestMethod.POST)
    public Response launch(@PathVariable("jobName") String jobName) {
        return Response.buildSucc();
    }

    @RequestMapping(value = "/jobs/{jobName}", method = RequestMethod.GET)
    public Response detils(@PathVariable("jobName") String jobName) {
        return Response.buildSucc();
    }

}
