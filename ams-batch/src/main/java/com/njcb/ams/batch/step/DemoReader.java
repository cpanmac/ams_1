package com.njcb.ams.batch.step;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.repository.dao.SysTradeLogDAO;
import com.njcb.ams.repository.entity.SysTradeLog;
import org.springframework.batch.item.ItemReader;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author LOONG
 */
@Component
public class DemoReader implements ItemReader<SysTradeLog> {

    private static List<SysTradeLog> sysTradeLogs;

    private Integer index = 0;

    static {
        SysTradeLog sysTradeLog = new SysTradeLog();
        sysTradeLog.setTradeCode("OG1001");
        sysTradeLogs = AppContext.getBean(SysTradeLogDAO.class).selectBySelective(sysTradeLog);
    }

    @Override
    public SysTradeLog read() throws Exception {
        System.out.println("总数据量: " + sysTradeLogs.size());
        if(index >= sysTradeLogs.size()){
            return null;
        }else{
            SysTradeLog sysTradeLog = sysTradeLogs.get(index);
            index ++ ;
            return sysTradeLog;
        }
    }

}