package com.njcb.ams.bootconfig;

import com.njcb.ams.repository.dao.SysExceptionInfoDAO;
import com.njcb.ams.service.DataDicService;
import com.njcb.ams.support.exception.ExceptionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Lazy(false)
public class AmsSystemInfoInit{
	private static final Logger logger = LoggerFactory.getLogger(AmsSystemInfoInit.class);
	@Autowired
	private SysExceptionInfoDAO sysExceptionInfoDAO;
	@Autowired
	private DataDicService dataDicService;

	@PostConstruct
	public void init() {
		initDataDic();
		initExceptionInfo();
	}
	
	private void initExceptionInfo() {
		try {
			logger.info("------------ -----初始化异常信息开始------  ----------");
			ExceptionUtil.EXCEPTION_TEXT.putAll(sysExceptionInfoDAO.getList());
			logger.info("------------------初始化异常信息完成------------------");
		} catch (Throwable e) {
			logger.error("------------------初始化异常信息错误------------------", e);
			System.exit(0);
		}
	}

	private void initDataDic() {
		logger.debug("-----------------初始化数据字典信息开始----------------");
		dataDicService.init();
		logger.debug("-----------------初始化数据字典信息完成----------------");
	}

	
}
