package com.njcb.ams.bootconfig;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.BusExtensionPostProcessor;
import org.apache.cxf.bus.spring.BusWiringBeanFactoryPostProcessor;
import org.apache.cxf.bus.spring.SpringBus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author liuyanlong
 */
@Configuration
public class AmsCxfDefaultConfig{
	
	@Bean(name=Bus.DEFAULT_BUS_ID,destroyMethod="shutdown")
	public SpringBus springBus() {
		return new SpringBus();
	}
	
	@Bean(name="org.apache.cxf.bus.spring.BusWiringBeanFactoryPostProcessor")
	public BusWiringBeanFactoryPostProcessor busWiringBeanFactoryPostProcessor() {
		return new BusWiringBeanFactoryPostProcessor();
	}
	
	@Bean(name="org.apache.cxf.bus.spring.BusExtensionPostProcessor")
	public BusExtensionPostProcessor busExtensionPostProcessor() {
		return new BusExtensionPostProcessor();
	}

	
}
