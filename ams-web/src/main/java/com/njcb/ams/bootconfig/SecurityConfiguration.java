package com.njcb.ams.bootconfig;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.repository.dao.CommOrgInfoDAO;
import com.njcb.ams.repository.dao.CommRoleInfoDAO;
import com.njcb.ams.repository.dao.CommUserInfoDAO;
import com.njcb.ams.repository.entity.CommOrgInfo;
import com.njcb.ams.repository.entity.CommRoleInfo;
import com.njcb.ams.repository.entity.CommRoleInfoExample;
import com.njcb.ams.repository.entity.CommUserInfo;
import com.njcb.ams.support.annotation.AmsConfigDefault;
import com.njcb.ams.support.security.bo.LoginModel;
import com.njcb.ams.support.security.bo.SecurityUser;
import com.njcb.ams.support.security.config.AmsSecurityConfiguration;
import com.njcb.ams.util.AmsAssert;
import com.njcb.ams.util.SysInfoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author srxhx207
 */
@AmsConfigDefault
public class SecurityConfiguration implements AmsSecurityConfiguration {

	@Autowired
	private CommUserInfoDAO commUserInfoDAO;
	@Autowired
	private CommRoleInfoDAO commRoleInfoDAO;
	@Autowired
	private CommOrgInfoDAO commOrgInfoDAO;
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public SecurityUser loadUserByUsername(String loginCode, LoginModel loginModel, Map<String, Object> param, Boolean isLogin) {
		SecurityUser securityUser = new SecurityUser();
		//授权码模式
		if(LoginModel.AUTHCODE.equals(loginModel)){
			//需要到第三方验证授权码loginCode
			//暂不支持
			securityUser.setId(1);
			securityUser.setLoginName(loginCode);
			securityUser.setUserName(loginCode);
			securityUser.setPassWord(passwordEncoder.encode(loginCode));
			securityUser.setBusiDate(SysInfoUtils.getBusiDate());
			return securityUser;
		}
		CommUserInfo userInfo = commUserInfoDAO.findSecurityUserByLoginName(loginCode);
		if (userInfo == null) {
			throw new UsernameNotFoundException("用户[" + loginCode + "]不存在");
		}else if(SysBaseDefine.STATUS_INVALID.equals(userInfo.getStatus())){
			throw new AccountExpiredException("用户[" + loginCode + "]状态无效");
		}
		List<Integer> roleIds = commUserInfoDAO.getMapper().selectRoleIdsByUserId(userInfo.getId());
		if(roleIds.size() > 0){
			CommRoleInfoExample example = new CommRoleInfoExample();
			example.createCriteria().andIdIn(roleIds);
			List<CommRoleInfo> roleList = commRoleInfoDAO.selectByExample(example);
			userInfo.setRoleList(roleList);
		}else{
			userInfo.setRoleList(new ArrayList<CommRoleInfo>());
		}
		
		CommOrgInfo commOrgInfo = commOrgInfoDAO.selectByPrimaryKey(userInfo.getOrgId());
		AmsAssert.notNull(commOrgInfo,"用户所属机构不存在");
		securityUser.setId(userInfo.getId());
		securityUser.setRoleCodes(userInfo.getRoleList().stream().map(CommRoleInfo::getRoleCode).collect(Collectors.toList()));
		securityUser.setLoginName(userInfo.getLoginName());
		securityUser.setUserName(userInfo.getUserName());
		securityUser.setPassWord(userInfo.getPassWord());
		securityUser.setOrgnNo(commOrgInfo.getOrgNo());
		securityUser.setOrgnName(commOrgInfo.getOrgName());
		securityUser.setBusiDate(SysInfoUtils.getBusiDate());
		securityUser.setEmpNo(userInfo.getEmpNo());
		return securityUser;
	}
	
	@Override
	public LoginModel loginModel() {
		return LoginModel.PASSWORD;
	}
	
	@Override
	public void permitMatchers(List<String> antPatterns) {
		antPatterns.add("/register");
		antPatterns.add("/index/register");
		antPatterns.add("/index/param");
		antPatterns.add("/html/**");
		antPatterns.add("/js/**");
		antPatterns.add("/css/**");
		antPatterns.add("/images/**");
		antPatterns.add("/health/**");
	}

}
