package com.njcb.ams.store.esbmodule.annotation;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * ESB服务实现类
 * @author LOONG
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public @interface EsbService {
    String name() default "";
}

