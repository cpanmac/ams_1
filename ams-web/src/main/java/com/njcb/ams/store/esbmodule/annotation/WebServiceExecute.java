package com.njcb.ams.store.esbmodule.annotation;

import com.njcb.ams.store.esbmodule.SysHeadBean;

/**
 * @author liuyanlong
 *
 */
public interface WebServiceExecute {
	/**
	 * webservice服务端请求处理前
	 * @param request
	 */
	void beforeExecute(Object request, SysHeadBean reqSysHeadBean);

	/**
	 * webservice服务端请求处理后
	 * @param request
	 * @param result
	 */
	void afterExecute(Object request,Object result, SysHeadBean rspSysHeadBean);
}
