package com.njcb.ams.store.esbmodule;

public class SysHeadBean {

    protected String pkgLength;
    protected String tmlCd;
    protected String serviceID;
    protected String serviceId;
    protected String channelID;
    protected String channelId;
    protected String legOrgID;
    protected String legOrgId;
    protected String reqDate;
    protected String reqTime;
    protected String mac;
    protected String version;
    protected String priority;
    protected String reqSysID;
    protected String reqSysId;
    protected String domainRef;
    protected String acceptLang;
    protected String globalSeq;
    protected String orgSysID;
    protected String orgSysId;
    protected String respSeq;
    protected String respDate;
    protected String respTime;
    protected String transStatus;
    protected String retCode;
    protected String retMsg;
    protected String respSysID;
    protected String respSysId;
    protected String trackCode;
    protected String reseFlg;
    protected String reseFld;
    protected String svcScn;
    protected String reqSeq;

    public String getPkgLength() {
        return pkgLength;
    }

    public void setPkgLength(String value) {
        this.pkgLength = value;
    }

    public String getTmlCd() {
        return tmlCd;
    }

    public void setTmlCd(String value) {
        this.tmlCd = value;
    }

    public String getServiceID() {
        if(null == serviceID){
            serviceID = serviceId;
        }
        return serviceID;
    }

    public String getServiceId() {
        if(null == serviceID){
            serviceID = serviceId;
        }
        return serviceID;
    }

    public void setServiceID(String value) {
        this.serviceID = value;
        this.serviceId = value;
    }

    public void setServiceId(String value) {
        this.serviceID = value;
        this.serviceId = value;
    }

    public String getChannelID() {
        if(null == channelID){
            channelID = channelId;
        }
        return channelID;
    }

    public String getChannelId() {
        if(null == channelID){
            channelID = channelId;
        }
        return channelID;
    }

    public void setChannelID(String value) {
        this.channelID = value;
        this.channelId = value;
    }

    public void setChannelId(String value) {
        this.channelID = value;
        this.channelId = value;
    }

    public String getLegOrgID() {
        if(null == legOrgID){
            legOrgID = legOrgId;
        }
        return legOrgID;
    }

    public String getLegOrgId() {
        if(null == legOrgID){
            legOrgID = legOrgId;
        }
        return legOrgID;
    }

    public void setLegOrgID(String value) {
        this.legOrgID = value;
        this.legOrgId = value;
    }

    public void setLegOrgId(String value) {
        this.legOrgID = value;
        this.legOrgId = value;
    }

    public String getReqDate() {
        return reqDate;
    }

    public void setReqDate(String value) {
        this.reqDate = value;
    }

    public String getReqTime() {
        return reqTime;
    }

    public void setReqTime(String value) {
        this.reqTime = value;
    }

    public String getMAC() {
        return mac;
    }

    public void setMAC(String value) {
        this.mac = value;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String value) {
        this.version = value;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String value) {
        this.priority = value;
    }

    public String getReqSysID() {
        if(null == reqSysID){
            reqSysID = reqSysId;
        }
        return reqSysID;
    }

    public String getReqSysId() {
        if(null == reqSysID){
            reqSysID = reqSysId;
        }
        return reqSysID;
    }

    public void setReqSysID(String reqSysID) {
        this.reqSysID = reqSysID;
        this.reqSysId = reqSysID;
    }

    public void setReqSysId(String reqSysID) {
        this.reqSysID = reqSysID;
        this.reqSysId = reqSysID;
    }

    public String getDomainRef() {
        return domainRef;
    }

    public void setDomainRef(String value) {
        this.domainRef = value;
    }

    public String getAcceptLang() {
        return acceptLang;
    }

    public void setAcceptLang(String value) {
        this.acceptLang = value;
    }

    public String getGlobalSeq() {
        return globalSeq;
    }

    public void setGlobalSeq(String value) {
        this.globalSeq = value;
    }

    public String getOrgSysID() {
        if(null == orgSysID){
            orgSysID = orgSysId;
        }
        return orgSysID;
    }

    public String getOrgSysId() {
        if(null == orgSysID){
            orgSysID = orgSysId;
        }
        return orgSysID;
    }

    public void setOrgSysID(String value) {
        this.orgSysID = value;
        this.orgSysId = value;
    }

    public void setOrgSysId(String value) {
        this.orgSysID = value;
        this.orgSysId = value;
    }

    public String getRespSeq() {
        return respSeq;
    }

    public void setRespSeq(String value) {
        this.respSeq = value;
    }

    public String getRespDate() {
        return respDate;
    }

    public void setRespDate(String value) {
        this.respDate = value;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String value) {
        this.respTime = value;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String value) {
        this.transStatus = value;
    }

    public String getRetCode() {
        return retCode;
    }

    public void setRetCode(String value) {
        this.retCode = value;
    }

    public String getRetMsg() {
        return retMsg;
    }

    public void setRetMsg(String value) {
        this.retMsg = value;
    }

    public String getRespSysID() {
        if(null == respSysID){
            respSysID = respSysId;
        }
        return respSysID;
    }

    public String getRespSysId() {
        if(null == respSysID){
            respSysID = respSysId;
        }
        return respSysID;
    }

    public void setRespSysID(String respSysID) {
        this.respSysID = respSysID;
        this.respSysId = respSysID;
    }

    public void setRespSysId(String respSysID) {
        this.respSysID = respSysID;
        this.respSysId = respSysID;
    }

    public String getTrackCode() {
        return trackCode;
    }

    public void setTrackCode(String value) {
        this.trackCode = value;
    }

    public String getReseFlg() {
        return reseFlg;
    }

    public void setReseFlg(String value) {
        this.reseFlg = value;
    }

    public String getReseFld() {
        return reseFld;
    }

    public void setReseFld(String value) {
        this.reseFld = value;
    }

    public String getSvcScn() {
        return svcScn;
    }

    public void setSvcScn(String value) {
        this.svcScn = value;
    }

    public String getReqSeq() {
        return reqSeq;
    }

    public void setReqSeq(String value) {
        this.reqSeq = value;
    }

}
