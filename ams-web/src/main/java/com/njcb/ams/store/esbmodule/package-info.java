/**
 * 业务组件包--ESB组件
 * 
 * @author liuyanlong
 */
@Information(tag="ESB组件",desc="提供ESB客户端及服务端支持")
package com.njcb.ams.store.esbmodule;

import com.njcb.ams.support.annotation.Information;
