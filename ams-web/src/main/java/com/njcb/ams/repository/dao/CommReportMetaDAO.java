/*
 * CommReportMeta.java
 * Copyright(C) trythis.cn
 * 2020年01月10日 11时05分42秒Created
 */
package com.njcb.ams.repository.dao;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.CommReportMetaMapper;
import com.njcb.ams.repository.entity.CommReportMeta;
import com.njcb.ams.repository.entity.CommReportMetaExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CommReportMetaDAO extends BaseMyBatisDAO<CommReportMeta, CommReportMetaExample, Integer> {

    @Autowired
    private CommReportMetaMapper mapper;

    public CommReportMetaDAO() {
        this.entityClass = CommReportMeta.class;
    }

    @Override
    public CommReportMetaMapper getMapper() {
        return mapper;
    }

    public CommReportMeta selectByReportNo(String reportNo){
        CommReportMetaExample example = new CommReportMetaExample();
        example.createCriteria().andReportNoEqualTo(reportNo);
        return selectOneByExample(example);
    }

}