/*
 * CommCalendar.java
 * Copyright(C) trythis.cn
 * 2021年03月02日 15时43分19秒Created
 */
package com.njcb.ams.repository.entity;

import java.io.Serializable;

public class CommCalendar extends CommCalendarKey implements Serializable {
    private String dataType;

    private String dataYear;

    private String remark;

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataYear() {
        return dataYear;
    }

    public void setDataYear(String dataYear) {
        this.dataYear = dataYear;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}