package com.njcb.ams.repository.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.njcb.ams.repository.entity.ResourceInfo;
import com.njcb.ams.repository.entity.ResourceInfoExample;
import org.springframework.stereotype.Component;

/**
 * @author srxhx207
 */
@Component
public interface ResourceInfoMapper extends BaseMapper<ResourceInfo, ResourceInfoExample, Integer> {

	/**
	 * 方法功能描述：
	 * 
	 * @param userID
	 * @return
	 */
	List<Integer> getResoByUser(int userID);

	/**
	 * 方法功能描述：
	 * 
	 * @param example
	 * @return
	 */
	List<ResourceInfo> findByUser(ResourceInfoExample example);

	List<ResourceInfo> findResourceInfoByUser(@Param("resourceType") String resourceType, @Param("orgId") Integer orgId,
			@Param("userId") Integer userId);

}