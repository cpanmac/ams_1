package com.njcb.ams.repository.entity;

import com.njcb.ams.store.page.Page;

import java.io.Serializable;

public class CommDataDic extends Page implements Serializable {
    private Integer id;

    private String dataNo;

    private String dataName;

    private String dataType;

    private String dataTypeName;

    private String dataAttr;

    private String dataLogic;
    
    private String queryType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDataNo() {
        return dataNo;
    }

    public void setDataNo(String dataNo) {
        this.dataNo = dataNo;
    }

    public String getDataName() {
        return dataName;
    }

    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataTypeName() {
        return dataTypeName;
    }

    public void setDataTypeName(String dataTypeName) {
        this.dataTypeName = dataTypeName;
    }

    public String getDataAttr() {
        return dataAttr;
    }

    public void setDataAttr(String dataAttr) {
        this.dataAttr = dataAttr;
    }

    public String getDataLogic() {
        return dataLogic;
    }

    public void setDataLogic(String dataLogic) {
        this.dataLogic = dataLogic;
    }
    
    public void setQueryType(String queryType) {
		this.queryType = queryType;
	}
    
    public String getQueryType() {
		return queryType;
	}

	@Override
	public String toString() {
		return "CommDataDic [id=" + id + ", dataNo=" + dataNo + ", dataName=" + dataName + ", dataType=" + dataType + ", dataTypeName=" + dataTypeName + ", dataAttr=" + dataAttr + ", dataLogic="
				+ dataLogic + "]";
	}
    
    
}