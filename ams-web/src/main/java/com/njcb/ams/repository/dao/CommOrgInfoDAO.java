/*
 * CommOrgInfo.java
 * Copyright(C) trythis.cn
 * 2021年06月29日 12时42分18秒Created
 */
package com.njcb.ams.repository.dao;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.CommOrgInfoMapper;
import com.njcb.ams.repository.entity.CommOrgInfo;
import com.njcb.ams.repository.entity.CommOrgInfoExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CommOrgInfoDAO extends BaseMyBatisDAO<CommOrgInfo, CommOrgInfoExample, Integer> {
    @Autowired
    private CommOrgInfoMapper mapper;

    public CommOrgInfoDAO() {
        this.entityClass = CommOrgInfo.class;
    }

    @Override
    @SuppressWarnings("unchecked")
    public CommOrgInfoMapper getMapper() {
        return mapper;
    }

    public List<CommOrgInfo> selectAll() {
        return mapper.selectByExample(new CommOrgInfoExample());
    }

    /**
     * 根据机构号查询
     *
     * @param orgNo
     * @return
     */
    public CommOrgInfo selectByOrgNo(String orgNo) {
        CommOrgInfoExample example = new CommOrgInfoExample();
        example.createCriteria().andOrgNoEqualTo(orgNo);
        return selectOneByExample(example);
    }

    /**
     * 根据核心机构号查询
     *
     * @param CoreOrgNo
     * @return
     */
    public CommOrgInfo selectByCoreOrgNo(String CoreOrgNo) {
        CommOrgInfoExample example = new CommOrgInfoExample();
        example.createCriteria().andOrgAliasEqualTo(CoreOrgNo);
        return selectOneByExample(example);
    }

    /**
     * 查询不包含当前ID记录的相同核心机构号记录
     *
     * @param CoreOrgNo
     * @param ID
     * @return
     */
    public CommOrgInfo selectByCoreOrgNoAndID(String CoreOrgNo, Integer ID) {
        CommOrgInfoExample example = new CommOrgInfoExample();
        example.createCriteria().andOrgAliasEqualTo(CoreOrgNo).andIdNotEqualTo(ID);
        return selectOneByExample(example);
    }

    /**
     * 根据上级机构号查询
     *
     * @param parentId
     * @return
     */
    public List<CommOrgInfo> selectByParentId(Integer parentId) {
        CommOrgInfoExample example = new CommOrgInfoExample();
        example.createCriteria().andParentIdEqualTo(parentId);
        return selectByExample(example);
    }

    /**
     * @param orgInfo
     */
    @Override
    public int saveOrUpdateByPrimaryKey(CommOrgInfo orgInfo) {
        if (null != orgInfo.getId() && 0 != orgInfo.getId()) {
            return getMapper().updateByPrimaryKey(orgInfo);
        } else {
            return getMapper().insert(orgInfo);
        }
    }

    /**
     * 取本机构及所属机构ID
     *
     * @param id
     * @return
     */
    public List<Integer> selectSelfAndSubById(Integer id) {
        return mapper.selectSelfAndSubById(id);
    }

    /**
     * 取本机构及所属机构信息
     *
     * @param id
     * @return
     */
    public List<CommOrgInfo> selectEntitySelfAndSubById(Integer id) {
        return mapper.selectEntitySelfAndSubById(id);
    }

    /**
     * 取本机构及所属机构信息
     *
     * @param orgNo
     * @return
     */
    public List<CommOrgInfo> selectEntitySelfAndSubByOrgNo(String orgNo) {
        CommOrgInfo orgInfo = selectByOrgNo(orgNo);
        return mapper.selectEntitySelfAndSubById(orgInfo.getId());
    }
}