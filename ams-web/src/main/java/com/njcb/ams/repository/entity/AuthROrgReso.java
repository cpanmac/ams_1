package com.njcb.ams.repository.entity;

import java.io.Serializable;

public class AuthROrgReso implements Serializable {
    private Integer id;

    private Integer orgId;

    private Integer resId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public Integer getResId() {
        return resId;
    }

    public void setResId(Integer resId) {
        this.resId = resId;
    }
}