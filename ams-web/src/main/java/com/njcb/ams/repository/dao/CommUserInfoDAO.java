package com.njcb.ams.repository.dao;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.CommUserInfoMapper;
import com.njcb.ams.repository.entity.CommUserInfo;
import com.njcb.ams.repository.entity.CommUserInfoExample;

@Repository
public class CommUserInfoDAO extends BaseMyBatisDAO<CommUserInfo, CommUserInfoExample, Integer> {
	public CommUserInfoDAO() {
		this.entityClass = CommUserInfo.class;
	}

	@Autowired
	private CommUserInfoMapper mapper;

	@Override
	@SuppressWarnings("unchecked")
	public CommUserInfoMapper getMapper() {
		return mapper;
	}

	/**
	 * 安全用户、包含密码
	 * @param loginName
	 * @return
	 */
	public CommUserInfo findSecurityUserByLoginName(String loginName) {
		return mapper.findSecurityUserByLoginName(loginName);
	}

	public CommUserInfo findByLoginName(String loginName) {
		CommUserInfoExample example = new CommUserInfoExample();
		example.createCriteria().andLoginNameEqualTo(loginName);
		return selectOneByExample(example);
	}

	public CommUserInfo findByUserName(String userName) {
		CommUserInfoExample example = new CommUserInfoExample();
		example.createCriteria().andUserNameEqualTo(userName);
		return selectOneByExample(example);
	}

	public Set<String> getUserRoles(String username) {
		return null;
	}

	public Set<String> findPermissions(String username) {
		return null;
	}

	public List<CommUserInfo> selectAll() {
		return mapper.selectByExample(new CommUserInfoExample());
	}

	public int updatePassword(CommUserInfo user) {
		return mapper.updatePassword(user);
	}

	@Override
    public int saveOrUpdateByPrimaryKey(CommUserInfo userInfo) {
		if (null != userInfo.getId()) {
			return getMapper().updateByPrimaryKeySelective(userInfo);
		} else {
			return getMapper().insert(userInfo);
		}
	}

}
