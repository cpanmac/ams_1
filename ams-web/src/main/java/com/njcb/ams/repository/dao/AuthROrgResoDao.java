package com.njcb.ams.repository.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.AuthROrgResoMapper;
import com.njcb.ams.repository.entity.AuthROrgReso;
import com.njcb.ams.repository.entity.AuthROrgResoExample;

/**
 * @author njcbkf045
 *
 *         类功能描述：
 *
 */
@Repository
public class AuthROrgResoDao extends BaseMyBatisDAO<AuthROrgReso, AuthROrgResoExample, Integer> {

	public AuthROrgResoDao() {
		this.entityClass = AuthROrgReso.class;
	}

	@Autowired
	private AuthROrgResoMapper mapper;

	@Override
	@SuppressWarnings("unchecked")
	public AuthROrgResoMapper getMapper() {
		return mapper;
	}

}
