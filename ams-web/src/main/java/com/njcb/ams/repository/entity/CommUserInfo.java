package com.njcb.ams.repository.entity;

import java.io.Serializable;
import java.util.List;

import com.njcb.ams.store.page.Page;
import com.njcb.ams.support.annotation.OperRecord;

import io.swagger.annotations.ApiModelProperty;

@OperRecord
public class CommUserInfo extends Page implements Serializable {
	@ApiModelProperty(value = "数据主键")
    private Integer id;
	@ApiModelProperty(value = "用户登录名")
    private String loginName;
	@ApiModelProperty(value = "用户密码")
    private String passWord;
	@ApiModelProperty(value = "用户姓名")
    private String userName;
	@ApiModelProperty(value = "机构ID")
    private Integer orgId;
	@ApiModelProperty(value = "用户状态")
    private String status;
	@ApiModelProperty(value = "用户工号")
    private String empNo;
	@ApiModelProperty(value = "上次登录时间")
    private String loginTime;
	@ApiModelProperty(value = "上次登录IP")
    private String loginIp;
	private String email;
	@ApiModelProperty(hidden = true)
    private String userCode;
	@ApiModelProperty(hidden = true)
    private Integer conductUser;
	@ApiModelProperty(hidden = true)
    private String conductTime;
    
    @ApiModelProperty(hidden = true)
	private List<Integer> orgIdList;
    @ApiModelProperty(hidden = true)
	private List<CommRoleInfo> roleList;
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmpNo() {
        return empNo;
    }

    public void setEmpNo(String empNo) {
        this.empNo = empNo;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public Integer getConductUser() {
        return conductUser;
    }

    public void setConductUser(Integer conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }
    
    public List<Integer> getOrgIdList() {
		return orgIdList;
	}

	public void setOrgIdList(List<Integer> orgIdList) {
		this.orgIdList = orgIdList;
	}
	
	public List<CommRoleInfo> getRoleList() {
		return roleList;
	}
	
	public void setRoleList(List<CommRoleInfo> roleList) {
		this.roleList = roleList;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail() {
		return email;
	}

	@Override
	public String toString() {
		return "CommUserInfo [loginName=" + loginName + ", passWord=" + passWord + ", userName=" + userName + ", orgId="
				+ orgId + ", status=" + status + ", empNo=" + empNo + ", loginTime=" + loginTime + ", loginIp="
				+ loginIp + ", email=" + email + "]";
	}
	
}