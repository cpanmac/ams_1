/*
 * CommCalendarMapper.java
 * Copyright(C) trythis.cn
 * 2021年03月02日 15时43分19秒Created
 */
package com.njcb.ams.repository.dao.mapper;

import com.njcb.ams.repository.entity.CommCalendar;
import com.njcb.ams.repository.entity.CommCalendarExample;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public interface CommCalendarMapper extends BaseMapper<CommCalendar, CommCalendarExample, Integer> {
     List<String> getSundayAndSatuday(Map<Object, Object> map);

     List<CommCalendar> selectOne(Map<String, String> map);

     Integer updateDateTypeByRemark(Map<String, String> map);

     Integer clearRemark(Map<String, String> map);

     List<CommCalendar> checkConflits(Map<String, String> map);

     String selectDataType(Map<String, String> map);

     Integer addTypeCalendar(Map<String, String> map);

     Integer upuserCalendar(Map<String, String> map);

     List<String> checkHoliday(Map<String,String> map);

     List<String> selectSysId(Map<String,String> map);
}