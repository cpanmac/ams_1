package com.njcb.ams.repository.entity;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author LOONG
 */
public class ResourceInfo implements Serializable {
    @ApiModelProperty(hidden = true, value = "数据主键")
    private Integer id;
    @ApiModelProperty(value = "资源名称")
    private String resourceName;

    private String accessPath;

    private Integer parentId;

    private Integer resourceGrade;

    private Integer resourceOrder;

    private String resourceAttr;

    @ApiModelProperty(value = "资源类型")
    private String resourceType;

    private String resourceImage;

    private String resourceStatus;

    private String resourceComponent;

    private String resourceDesc;
    
    private String authCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getAccessPath() {
        return accessPath;
    }

    public void setAccessPath(String accessPath) {
        this.accessPath = accessPath;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getResourceGrade() {
        return resourceGrade;
    }

    public void setResourceGrade(Integer resourceGrade) {
        this.resourceGrade = resourceGrade;
    }

    public Integer getResourceOrder() {
        return resourceOrder;
    }

    public void setResourceOrder(Integer resourceOrder) {
        this.resourceOrder = resourceOrder;
    }

    public String getResourceAttr() {
        return resourceAttr;
    }

    public void setResourceAttr(String resourceAttr) {
        this.resourceAttr = resourceAttr;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceImage() {
        return resourceImage;
    }

    public void setResourceImage(String resourceImage) {
        this.resourceImage = resourceImage;
    }

    public String getResourceStatus() {
        return resourceStatus;
    }

    public void setResourceStatus(String resourceStatus) {
        this.resourceStatus = resourceStatus;
    }

    public String getResourceComponent() {
        return resourceComponent;
    }

    public void setResourceComponent(String resourceComponent) {
        this.resourceComponent = resourceComponent;
    }

    public String getResourceDesc() {
        return resourceDesc;
    }

    public void setResourceDesc(String resourceDesc) {
        this.resourceDesc = resourceDesc;
    }
    
    /**
	 * @return the authCode
	 */
	public String getAuthCode() {
		return authCode;
	}
	
	/**
	 * @param authCode the authCode to set
	 */
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
}