/*
 * CommOrgInfo.java
 * Copyright(C) trythis.cn
 * 2021年06月29日 12时42分18秒Created
 */
package com.njcb.ams.repository.entity;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

public class CommOrgInfo implements Serializable {
    private Integer id;

    private String periods;

    private String orgNo;

    private String orgName;

    private Integer orgGrade;

    private String status;

    private Integer parentId;

    private String orgType;

    private String orgAlias;

    private String remark;

    private String valideDate;

    private String invalideDate;

    private Integer conductUser;

    private String conductTime;

    @ApiModelProperty(hidden = true)
    private List<Integer> orgIdList;

    public List<Integer> getOrgIdList() {
        return orgIdList;
    }

    public void setOrgIdList(List<Integer> orgIdList) {
        this.orgIdList = orgIdList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPeriods() {
        return periods;
    }

    public void setPeriods(String periods) {
        this.periods = periods;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Integer getOrgGrade() {
        return orgGrade;
    }

    public void setOrgGrade(Integer orgGrade) {
        this.orgGrade = orgGrade;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public String getOrgAlias() {
        return orgAlias;
    }

    public void setOrgAlias(String orgAlias) {
        this.orgAlias = orgAlias;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getValideDate() {
        return valideDate;
    }

    public void setValideDate(String valideDate) {
        this.valideDate = valideDate;
    }

    public String getInvalideDate() {
        return invalideDate;
    }

    public void setInvalideDate(String invalideDate) {
        this.invalideDate = invalideDate;
    }

    public Integer getConductUser() {
        return conductUser;
    }

    public void setConductUser(Integer conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }
}