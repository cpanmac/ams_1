/*
 * CommReportMeta.java
 * Copyright(C) trythis.cn
 * 2020年01月10日 11时05分42秒Created
 */
package com.njcb.ams.repository.entity;

import com.njcb.ams.store.page.Page;
import com.njcb.ams.util.AmsUtils;

import java.io.Serializable;

public class CommReportMeta extends Page implements Serializable {
    private Integer id;

    private String reportNo;

    private String reportName;

    private String viewDiv;

    private String reportSql;

    private String previewUrl;

    private Integer conductUser;

    private String conductTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReportNo() {
        return reportNo;
    }

    public void setReportNo(String reportNo) {
        this.reportNo = reportNo;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getViewDiv() {
        return viewDiv;
    }

    public void setViewDiv(String viewDiv) {
        this.viewDiv = viewDiv;
    }

    public String getReportSql() {
        if(AmsUtils.isNotNull(reportSql)){
            return reportSql.toUpperCase();
        }
        return reportSql;
    }

    public void setReportSql(String reportSql) {
        this.reportSql = reportSql;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    public Integer getConductUser() {
        return conductUser;
    }

    public void setConductUser(Integer conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }
}