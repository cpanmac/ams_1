/*
 * CommOrgInfoExample.java
 * Copyright(C) trythis.cn
 * 2021年06月29日 12时42分18秒Created
 */
package com.njcb.ams.repository.entity;

import java.util.ArrayList;
import java.util.List;

public class CommOrgInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CommOrgInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andPeriodsIsNull() {
            addCriterion("PERIODS is null");
            return (Criteria) this;
        }

        public Criteria andPeriodsIsNotNull() {
            addCriterion("PERIODS is not null");
            return (Criteria) this;
        }

        public Criteria andPeriodsEqualTo(String value) {
            addCriterion("PERIODS =", value, "periods");
            return (Criteria) this;
        }

        public Criteria andPeriodsNotEqualTo(String value) {
            addCriterion("PERIODS <>", value, "periods");
            return (Criteria) this;
        }

        public Criteria andPeriodsGreaterThan(String value) {
            addCriterion("PERIODS >", value, "periods");
            return (Criteria) this;
        }

        public Criteria andPeriodsGreaterThanOrEqualTo(String value) {
            addCriterion("PERIODS >=", value, "periods");
            return (Criteria) this;
        }

        public Criteria andPeriodsLessThan(String value) {
            addCriterion("PERIODS <", value, "periods");
            return (Criteria) this;
        }

        public Criteria andPeriodsLessThanOrEqualTo(String value) {
            addCriterion("PERIODS <=", value, "periods");
            return (Criteria) this;
        }

        public Criteria andPeriodsLike(String value) {
            addCriterion("PERIODS like", value, "periods");
            return (Criteria) this;
        }

        public Criteria andPeriodsNotLike(String value) {
            addCriterion("PERIODS not like", value, "periods");
            return (Criteria) this;
        }

        public Criteria andPeriodsIn(List<String> values) {
            addCriterion("PERIODS in", values, "periods");
            return (Criteria) this;
        }

        public Criteria andPeriodsNotIn(List<String> values) {
            addCriterion("PERIODS not in", values, "periods");
            return (Criteria) this;
        }

        public Criteria andPeriodsBetween(String value1, String value2) {
            addCriterion("PERIODS between", value1, value2, "periods");
            return (Criteria) this;
        }

        public Criteria andPeriodsNotBetween(String value1, String value2) {
            addCriterion("PERIODS not between", value1, value2, "periods");
            return (Criteria) this;
        }

        public Criteria andOrgNoIsNull() {
            addCriterion("ORG_NO is null");
            return (Criteria) this;
        }

        public Criteria andOrgNoIsNotNull() {
            addCriterion("ORG_NO is not null");
            return (Criteria) this;
        }

        public Criteria andOrgNoEqualTo(String value) {
            addCriterion("ORG_NO =", value, "orgNo");
            return (Criteria) this;
        }

        public Criteria andOrgNoNotEqualTo(String value) {
            addCriterion("ORG_NO <>", value, "orgNo");
            return (Criteria) this;
        }

        public Criteria andOrgNoGreaterThan(String value) {
            addCriterion("ORG_NO >", value, "orgNo");
            return (Criteria) this;
        }

        public Criteria andOrgNoGreaterThanOrEqualTo(String value) {
            addCriterion("ORG_NO >=", value, "orgNo");
            return (Criteria) this;
        }

        public Criteria andOrgNoLessThan(String value) {
            addCriterion("ORG_NO <", value, "orgNo");
            return (Criteria) this;
        }

        public Criteria andOrgNoLessThanOrEqualTo(String value) {
            addCriterion("ORG_NO <=", value, "orgNo");
            return (Criteria) this;
        }

        public Criteria andOrgNoLike(String value) {
            addCriterion("ORG_NO like", value, "orgNo");
            return (Criteria) this;
        }

        public Criteria andOrgNoNotLike(String value) {
            addCriterion("ORG_NO not like", value, "orgNo");
            return (Criteria) this;
        }

        public Criteria andOrgNoIn(List<String> values) {
            addCriterion("ORG_NO in", values, "orgNo");
            return (Criteria) this;
        }

        public Criteria andOrgNoNotIn(List<String> values) {
            addCriterion("ORG_NO not in", values, "orgNo");
            return (Criteria) this;
        }

        public Criteria andOrgNoBetween(String value1, String value2) {
            addCriterion("ORG_NO between", value1, value2, "orgNo");
            return (Criteria) this;
        }

        public Criteria andOrgNoNotBetween(String value1, String value2) {
            addCriterion("ORG_NO not between", value1, value2, "orgNo");
            return (Criteria) this;
        }

        public Criteria andOrgNameIsNull() {
            addCriterion("ORG_NAME is null");
            return (Criteria) this;
        }

        public Criteria andOrgNameIsNotNull() {
            addCriterion("ORG_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andOrgNameEqualTo(String value) {
            addCriterion("ORG_NAME =", value, "orgName");
            return (Criteria) this;
        }

        public Criteria andOrgNameNotEqualTo(String value) {
            addCriterion("ORG_NAME <>", value, "orgName");
            return (Criteria) this;
        }

        public Criteria andOrgNameGreaterThan(String value) {
            addCriterion("ORG_NAME >", value, "orgName");
            return (Criteria) this;
        }

        public Criteria andOrgNameGreaterThanOrEqualTo(String value) {
            addCriterion("ORG_NAME >=", value, "orgName");
            return (Criteria) this;
        }

        public Criteria andOrgNameLessThan(String value) {
            addCriterion("ORG_NAME <", value, "orgName");
            return (Criteria) this;
        }

        public Criteria andOrgNameLessThanOrEqualTo(String value) {
            addCriterion("ORG_NAME <=", value, "orgName");
            return (Criteria) this;
        }

        public Criteria andOrgNameLike(String value) {
            addCriterion("ORG_NAME like", value, "orgName");
            return (Criteria) this;
        }

        public Criteria andOrgNameNotLike(String value) {
            addCriterion("ORG_NAME not like", value, "orgName");
            return (Criteria) this;
        }

        public Criteria andOrgNameIn(List<String> values) {
            addCriterion("ORG_NAME in", values, "orgName");
            return (Criteria) this;
        }

        public Criteria andOrgNameNotIn(List<String> values) {
            addCriterion("ORG_NAME not in", values, "orgName");
            return (Criteria) this;
        }

        public Criteria andOrgNameBetween(String value1, String value2) {
            addCriterion("ORG_NAME between", value1, value2, "orgName");
            return (Criteria) this;
        }

        public Criteria andOrgNameNotBetween(String value1, String value2) {
            addCriterion("ORG_NAME not between", value1, value2, "orgName");
            return (Criteria) this;
        }

        public Criteria andOrgGradeIsNull() {
            addCriterion("ORG_GRADE is null");
            return (Criteria) this;
        }

        public Criteria andOrgGradeIsNotNull() {
            addCriterion("ORG_GRADE is not null");
            return (Criteria) this;
        }

        public Criteria andOrgGradeEqualTo(Integer value) {
            addCriterion("ORG_GRADE =", value, "orgGrade");
            return (Criteria) this;
        }

        public Criteria andOrgGradeNotEqualTo(Integer value) {
            addCriterion("ORG_GRADE <>", value, "orgGrade");
            return (Criteria) this;
        }

        public Criteria andOrgGradeGreaterThan(Integer value) {
            addCriterion("ORG_GRADE >", value, "orgGrade");
            return (Criteria) this;
        }

        public Criteria andOrgGradeGreaterThanOrEqualTo(Integer value) {
            addCriterion("ORG_GRADE >=", value, "orgGrade");
            return (Criteria) this;
        }

        public Criteria andOrgGradeLessThan(Integer value) {
            addCriterion("ORG_GRADE <", value, "orgGrade");
            return (Criteria) this;
        }

        public Criteria andOrgGradeLessThanOrEqualTo(Integer value) {
            addCriterion("ORG_GRADE <=", value, "orgGrade");
            return (Criteria) this;
        }

        public Criteria andOrgGradeIn(List<Integer> values) {
            addCriterion("ORG_GRADE in", values, "orgGrade");
            return (Criteria) this;
        }

        public Criteria andOrgGradeNotIn(List<Integer> values) {
            addCriterion("ORG_GRADE not in", values, "orgGrade");
            return (Criteria) this;
        }

        public Criteria andOrgGradeBetween(Integer value1, Integer value2) {
            addCriterion("ORG_GRADE between", value1, value2, "orgGrade");
            return (Criteria) this;
        }

        public Criteria andOrgGradeNotBetween(Integer value1, Integer value2) {
            addCriterion("ORG_GRADE not between", value1, value2, "orgGrade");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("STATUS is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("STATUS =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("STATUS <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("STATUS >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("STATUS >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("STATUS <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("STATUS <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("STATUS like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("STATUS not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("STATUS in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("STATUS not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("STATUS between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("STATUS not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNull() {
            addCriterion("PARENT_ID is null");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNotNull() {
            addCriterion("PARENT_ID is not null");
            return (Criteria) this;
        }

        public Criteria andParentIdEqualTo(Integer value) {
            addCriterion("PARENT_ID =", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotEqualTo(Integer value) {
            addCriterion("PARENT_ID <>", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThan(Integer value) {
            addCriterion("PARENT_ID >", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("PARENT_ID >=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThan(Integer value) {
            addCriterion("PARENT_ID <", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThanOrEqualTo(Integer value) {
            addCriterion("PARENT_ID <=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdIn(List<Integer> values) {
            addCriterion("PARENT_ID in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotIn(List<Integer> values) {
            addCriterion("PARENT_ID not in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdBetween(Integer value1, Integer value2) {
            addCriterion("PARENT_ID between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("PARENT_ID not between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andOrgTypeIsNull() {
            addCriterion("ORG_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andOrgTypeIsNotNull() {
            addCriterion("ORG_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andOrgTypeEqualTo(String value) {
            addCriterion("ORG_TYPE =", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeNotEqualTo(String value) {
            addCriterion("ORG_TYPE <>", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeGreaterThan(String value) {
            addCriterion("ORG_TYPE >", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeGreaterThanOrEqualTo(String value) {
            addCriterion("ORG_TYPE >=", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeLessThan(String value) {
            addCriterion("ORG_TYPE <", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeLessThanOrEqualTo(String value) {
            addCriterion("ORG_TYPE <=", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeLike(String value) {
            addCriterion("ORG_TYPE like", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeNotLike(String value) {
            addCriterion("ORG_TYPE not like", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeIn(List<String> values) {
            addCriterion("ORG_TYPE in", values, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeNotIn(List<String> values) {
            addCriterion("ORG_TYPE not in", values, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeBetween(String value1, String value2) {
            addCriterion("ORG_TYPE between", value1, value2, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeNotBetween(String value1, String value2) {
            addCriterion("ORG_TYPE not between", value1, value2, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgAliasIsNull() {
            addCriterion("ORG_ALIAS is null");
            return (Criteria) this;
        }

        public Criteria andOrgAliasIsNotNull() {
            addCriterion("ORG_ALIAS is not null");
            return (Criteria) this;
        }

        public Criteria andOrgAliasEqualTo(String value) {
            addCriterion("ORG_ALIAS =", value, "orgAlias");
            return (Criteria) this;
        }

        public Criteria andOrgAliasNotEqualTo(String value) {
            addCriterion("ORG_ALIAS <>", value, "orgAlias");
            return (Criteria) this;
        }

        public Criteria andOrgAliasGreaterThan(String value) {
            addCriterion("ORG_ALIAS >", value, "orgAlias");
            return (Criteria) this;
        }

        public Criteria andOrgAliasGreaterThanOrEqualTo(String value) {
            addCriterion("ORG_ALIAS >=", value, "orgAlias");
            return (Criteria) this;
        }

        public Criteria andOrgAliasLessThan(String value) {
            addCriterion("ORG_ALIAS <", value, "orgAlias");
            return (Criteria) this;
        }

        public Criteria andOrgAliasLessThanOrEqualTo(String value) {
            addCriterion("ORG_ALIAS <=", value, "orgAlias");
            return (Criteria) this;
        }

        public Criteria andOrgAliasLike(String value) {
            addCriterion("ORG_ALIAS like", value, "orgAlias");
            return (Criteria) this;
        }

        public Criteria andOrgAliasNotLike(String value) {
            addCriterion("ORG_ALIAS not like", value, "orgAlias");
            return (Criteria) this;
        }

        public Criteria andOrgAliasIn(List<String> values) {
            addCriterion("ORG_ALIAS in", values, "orgAlias");
            return (Criteria) this;
        }

        public Criteria andOrgAliasNotIn(List<String> values) {
            addCriterion("ORG_ALIAS not in", values, "orgAlias");
            return (Criteria) this;
        }

        public Criteria andOrgAliasBetween(String value1, String value2) {
            addCriterion("ORG_ALIAS between", value1, value2, "orgAlias");
            return (Criteria) this;
        }

        public Criteria andOrgAliasNotBetween(String value1, String value2) {
            addCriterion("ORG_ALIAS not between", value1, value2, "orgAlias");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andValideDateIsNull() {
            addCriterion("VALIDE_DATE is null");
            return (Criteria) this;
        }

        public Criteria andValideDateIsNotNull() {
            addCriterion("VALIDE_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andValideDateEqualTo(String value) {
            addCriterion("VALIDE_DATE =", value, "valideDate");
            return (Criteria) this;
        }

        public Criteria andValideDateNotEqualTo(String value) {
            addCriterion("VALIDE_DATE <>", value, "valideDate");
            return (Criteria) this;
        }

        public Criteria andValideDateGreaterThan(String value) {
            addCriterion("VALIDE_DATE >", value, "valideDate");
            return (Criteria) this;
        }

        public Criteria andValideDateGreaterThanOrEqualTo(String value) {
            addCriterion("VALIDE_DATE >=", value, "valideDate");
            return (Criteria) this;
        }

        public Criteria andValideDateLessThan(String value) {
            addCriterion("VALIDE_DATE <", value, "valideDate");
            return (Criteria) this;
        }

        public Criteria andValideDateLessThanOrEqualTo(String value) {
            addCriterion("VALIDE_DATE <=", value, "valideDate");
            return (Criteria) this;
        }

        public Criteria andValideDateLike(String value) {
            addCriterion("VALIDE_DATE like", value, "valideDate");
            return (Criteria) this;
        }

        public Criteria andValideDateNotLike(String value) {
            addCriterion("VALIDE_DATE not like", value, "valideDate");
            return (Criteria) this;
        }

        public Criteria andValideDateIn(List<String> values) {
            addCriterion("VALIDE_DATE in", values, "valideDate");
            return (Criteria) this;
        }

        public Criteria andValideDateNotIn(List<String> values) {
            addCriterion("VALIDE_DATE not in", values, "valideDate");
            return (Criteria) this;
        }

        public Criteria andValideDateBetween(String value1, String value2) {
            addCriterion("VALIDE_DATE between", value1, value2, "valideDate");
            return (Criteria) this;
        }

        public Criteria andValideDateNotBetween(String value1, String value2) {
            addCriterion("VALIDE_DATE not between", value1, value2, "valideDate");
            return (Criteria) this;
        }

        public Criteria andInvalideDateIsNull() {
            addCriterion("INVALIDE_DATE is null");
            return (Criteria) this;
        }

        public Criteria andInvalideDateIsNotNull() {
            addCriterion("INVALIDE_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andInvalideDateEqualTo(String value) {
            addCriterion("INVALIDE_DATE =", value, "invalideDate");
            return (Criteria) this;
        }

        public Criteria andInvalideDateNotEqualTo(String value) {
            addCriterion("INVALIDE_DATE <>", value, "invalideDate");
            return (Criteria) this;
        }

        public Criteria andInvalideDateGreaterThan(String value) {
            addCriterion("INVALIDE_DATE >", value, "invalideDate");
            return (Criteria) this;
        }

        public Criteria andInvalideDateGreaterThanOrEqualTo(String value) {
            addCriterion("INVALIDE_DATE >=", value, "invalideDate");
            return (Criteria) this;
        }

        public Criteria andInvalideDateLessThan(String value) {
            addCriterion("INVALIDE_DATE <", value, "invalideDate");
            return (Criteria) this;
        }

        public Criteria andInvalideDateLessThanOrEqualTo(String value) {
            addCriterion("INVALIDE_DATE <=", value, "invalideDate");
            return (Criteria) this;
        }

        public Criteria andInvalideDateLike(String value) {
            addCriterion("INVALIDE_DATE like", value, "invalideDate");
            return (Criteria) this;
        }

        public Criteria andInvalideDateNotLike(String value) {
            addCriterion("INVALIDE_DATE not like", value, "invalideDate");
            return (Criteria) this;
        }

        public Criteria andInvalideDateIn(List<String> values) {
            addCriterion("INVALIDE_DATE in", values, "invalideDate");
            return (Criteria) this;
        }

        public Criteria andInvalideDateNotIn(List<String> values) {
            addCriterion("INVALIDE_DATE not in", values, "invalideDate");
            return (Criteria) this;
        }

        public Criteria andInvalideDateBetween(String value1, String value2) {
            addCriterion("INVALIDE_DATE between", value1, value2, "invalideDate");
            return (Criteria) this;
        }

        public Criteria andInvalideDateNotBetween(String value1, String value2) {
            addCriterion("INVALIDE_DATE not between", value1, value2, "invalideDate");
            return (Criteria) this;
        }

        public Criteria andConductUserIsNull() {
            addCriterion("CONDUCT_USER is null");
            return (Criteria) this;
        }

        public Criteria andConductUserIsNotNull() {
            addCriterion("CONDUCT_USER is not null");
            return (Criteria) this;
        }

        public Criteria andConductUserEqualTo(Integer value) {
            addCriterion("CONDUCT_USER =", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserNotEqualTo(Integer value) {
            addCriterion("CONDUCT_USER <>", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserGreaterThan(Integer value) {
            addCriterion("CONDUCT_USER >", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserGreaterThanOrEqualTo(Integer value) {
            addCriterion("CONDUCT_USER >=", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserLessThan(Integer value) {
            addCriterion("CONDUCT_USER <", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserLessThanOrEqualTo(Integer value) {
            addCriterion("CONDUCT_USER <=", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserIn(List<Integer> values) {
            addCriterion("CONDUCT_USER in", values, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserNotIn(List<Integer> values) {
            addCriterion("CONDUCT_USER not in", values, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserBetween(Integer value1, Integer value2) {
            addCriterion("CONDUCT_USER between", value1, value2, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserNotBetween(Integer value1, Integer value2) {
            addCriterion("CONDUCT_USER not between", value1, value2, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductTimeIsNull() {
            addCriterion("CONDUCT_TIME is null");
            return (Criteria) this;
        }

        public Criteria andConductTimeIsNotNull() {
            addCriterion("CONDUCT_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andConductTimeEqualTo(String value) {
            addCriterion("CONDUCT_TIME =", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeNotEqualTo(String value) {
            addCriterion("CONDUCT_TIME <>", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeGreaterThan(String value) {
            addCriterion("CONDUCT_TIME >", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeGreaterThanOrEqualTo(String value) {
            addCriterion("CONDUCT_TIME >=", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeLessThan(String value) {
            addCriterion("CONDUCT_TIME <", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeLessThanOrEqualTo(String value) {
            addCriterion("CONDUCT_TIME <=", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeLike(String value) {
            addCriterion("CONDUCT_TIME like", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeNotLike(String value) {
            addCriterion("CONDUCT_TIME not like", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeIn(List<String> values) {
            addCriterion("CONDUCT_TIME in", values, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeNotIn(List<String> values) {
            addCriterion("CONDUCT_TIME not in", values, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeBetween(String value1, String value2) {
            addCriterion("CONDUCT_TIME between", value1, value2, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeNotBetween(String value1, String value2) {
            addCriterion("CONDUCT_TIME not between", value1, value2, "conductTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}