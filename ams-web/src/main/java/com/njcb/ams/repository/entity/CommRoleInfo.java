package com.njcb.ams.repository.entity;

import com.njcb.ams.store.page.Page;
import com.njcb.ams.support.annotation.OperRecord;

import java.io.Serializable;

@OperRecord
public class CommRoleInfo extends Page implements Serializable {
    private Integer id;

    private String roleCode;

    private String roleName;

    private String roleDesc;

    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}