package com.njcb.ams.repository.entity;

import java.io.Serializable;

public class CommHolidayInfo implements Serializable {
    private Integer id;

    private String year;

    private String holidayStr;

    private String conductUser;

    private String conductTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getHolidayStr() {
        return holidayStr;
    }

    public void setHolidayStr(String holidayStr) {
        this.holidayStr = holidayStr;
    }

    public String getConductUser() {
        return conductUser;
    }

    public void setConductUser(String conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }
}