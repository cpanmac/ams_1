/*
 * CommUserInfoMapper.java
 * Copyright(C) trythis.cn
 * 2019年11月28日 09时47分44秒Created
 */
package com.njcb.ams.repository.dao.mapper;

import java.util.List;

import com.njcb.ams.repository.entity.CommUserInfo;
import com.njcb.ams.repository.entity.CommUserInfoExample;
import org.springframework.stereotype.Component;

@Component
public interface CommUserInfoMapper extends BaseMapper<CommUserInfo, CommUserInfoExample, Integer> {

	int updatePassword(CommUserInfo user);

	List<Integer> selectRoleIdsByUserId(Integer userId);

	CommUserInfo findSecurityUserByLoginName(String loginName);

}