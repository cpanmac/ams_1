package com.njcb.ams.repository.dao.mapper;

import com.njcb.ams.repository.entity.CommRoleInfo;
import com.njcb.ams.repository.entity.CommRoleInfoExample;
import org.springframework.stereotype.Component;

@Component
public interface CommRoleInfoMapper extends BaseMapper<CommRoleInfo, CommRoleInfoExample, Integer> {

}