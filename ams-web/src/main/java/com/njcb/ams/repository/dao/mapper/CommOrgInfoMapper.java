/*
 * CommOrgInfoMapper.java
 * Copyright(C) trythis.cn
 * 2021年06月29日 12时42分18秒Created
 */
package com.njcb.ams.repository.dao.mapper;

import com.njcb.ams.repository.entity.CommOrgInfo;
import com.njcb.ams.repository.entity.CommOrgInfoExample;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CommOrgInfoMapper extends BaseMapper<CommOrgInfo, CommOrgInfoExample, Integer> {
    List<Integer> selectSelfAndSubById(Integer parentId);

    List<CommOrgInfo> selectEntitySelfAndSubById(Integer parentId);
}