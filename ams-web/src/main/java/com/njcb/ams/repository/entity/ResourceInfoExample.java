package com.njcb.ams.repository.entity;

import java.util.ArrayList;
import java.util.List;

public class ResourceInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ResourceInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andResourceNameIsNull() {
            addCriterion("RESOURCE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andResourceNameIsNotNull() {
            addCriterion("RESOURCE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andResourceNameEqualTo(String value) {
            addCriterion("RESOURCE_NAME =", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameNotEqualTo(String value) {
            addCriterion("RESOURCE_NAME <>", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameGreaterThan(String value) {
            addCriterion("RESOURCE_NAME >", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameGreaterThanOrEqualTo(String value) {
            addCriterion("RESOURCE_NAME >=", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameLessThan(String value) {
            addCriterion("RESOURCE_NAME <", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameLessThanOrEqualTo(String value) {
            addCriterion("RESOURCE_NAME <=", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameLike(String value) {
            addCriterion("RESOURCE_NAME like", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameNotLike(String value) {
            addCriterion("RESOURCE_NAME not like", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameIn(List<String> values) {
            addCriterion("RESOURCE_NAME in", values, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameNotIn(List<String> values) {
            addCriterion("RESOURCE_NAME not in", values, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameBetween(String value1, String value2) {
            addCriterion("RESOURCE_NAME between", value1, value2, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameNotBetween(String value1, String value2) {
            addCriterion("RESOURCE_NAME not between", value1, value2, "resourceName");
            return (Criteria) this;
        }

        public Criteria andAccessPathIsNull() {
            addCriterion("ACCESS_PATH is null");
            return (Criteria) this;
        }

        public Criteria andAccessPathIsNotNull() {
            addCriterion("ACCESS_PATH is not null");
            return (Criteria) this;
        }

        public Criteria andAccessPathEqualTo(String value) {
            addCriterion("ACCESS_PATH =", value, "accessPath");
            return (Criteria) this;
        }

        public Criteria andAccessPathNotEqualTo(String value) {
            addCriterion("ACCESS_PATH <>", value, "accessPath");
            return (Criteria) this;
        }

        public Criteria andAccessPathGreaterThan(String value) {
            addCriterion("ACCESS_PATH >", value, "accessPath");
            return (Criteria) this;
        }

        public Criteria andAccessPathGreaterThanOrEqualTo(String value) {
            addCriterion("ACCESS_PATH >=", value, "accessPath");
            return (Criteria) this;
        }

        public Criteria andAccessPathLessThan(String value) {
            addCriterion("ACCESS_PATH <", value, "accessPath");
            return (Criteria) this;
        }

        public Criteria andAccessPathLessThanOrEqualTo(String value) {
            addCriterion("ACCESS_PATH <=", value, "accessPath");
            return (Criteria) this;
        }

        public Criteria andAccessPathLike(String value) {
            addCriterion("ACCESS_PATH like", value, "accessPath");
            return (Criteria) this;
        }

        public Criteria andAccessPathNotLike(String value) {
            addCriterion("ACCESS_PATH not like", value, "accessPath");
            return (Criteria) this;
        }

        public Criteria andAccessPathIn(List<String> values) {
            addCriterion("ACCESS_PATH in", values, "accessPath");
            return (Criteria) this;
        }

        public Criteria andAccessPathNotIn(List<String> values) {
            addCriterion("ACCESS_PATH not in", values, "accessPath");
            return (Criteria) this;
        }

        public Criteria andAccessPathBetween(String value1, String value2) {
            addCriterion("ACCESS_PATH between", value1, value2, "accessPath");
            return (Criteria) this;
        }

        public Criteria andAccessPathNotBetween(String value1, String value2) {
            addCriterion("ACCESS_PATH not between", value1, value2, "accessPath");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNull() {
            addCriterion("PARENT_ID is null");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNotNull() {
            addCriterion("PARENT_ID is not null");
            return (Criteria) this;
        }

        public Criteria andParentIdEqualTo(Integer value) {
            addCriterion("PARENT_ID =", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotEqualTo(Integer value) {
            addCriterion("PARENT_ID <>", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThan(Integer value) {
            addCriterion("PARENT_ID >", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("PARENT_ID >=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThan(Integer value) {
            addCriterion("PARENT_ID <", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThanOrEqualTo(Integer value) {
            addCriterion("PARENT_ID <=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdIn(List<Integer> values) {
            addCriterion("PARENT_ID in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotIn(List<Integer> values) {
            addCriterion("PARENT_ID not in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdBetween(Integer value1, Integer value2) {
            addCriterion("PARENT_ID between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("PARENT_ID not between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andResourceGradeIsNull() {
            addCriterion("RESOURCE_GRADE is null");
            return (Criteria) this;
        }

        public Criteria andResourceGradeIsNotNull() {
            addCriterion("RESOURCE_GRADE is not null");
            return (Criteria) this;
        }

        public Criteria andResourceGradeEqualTo(Integer value) {
            addCriterion("RESOURCE_GRADE =", value, "resourceGrade");
            return (Criteria) this;
        }

        public Criteria andResourceGradeNotEqualTo(Integer value) {
            addCriterion("RESOURCE_GRADE <>", value, "resourceGrade");
            return (Criteria) this;
        }

        public Criteria andResourceGradeGreaterThan(Integer value) {
            addCriterion("RESOURCE_GRADE >", value, "resourceGrade");
            return (Criteria) this;
        }

        public Criteria andResourceGradeGreaterThanOrEqualTo(Integer value) {
            addCriterion("RESOURCE_GRADE >=", value, "resourceGrade");
            return (Criteria) this;
        }

        public Criteria andResourceGradeLessThan(Integer value) {
            addCriterion("RESOURCE_GRADE <", value, "resourceGrade");
            return (Criteria) this;
        }

        public Criteria andResourceGradeLessThanOrEqualTo(Integer value) {
            addCriterion("RESOURCE_GRADE <=", value, "resourceGrade");
            return (Criteria) this;
        }

        public Criteria andResourceGradeIn(List<Integer> values) {
            addCriterion("RESOURCE_GRADE in", values, "resourceGrade");
            return (Criteria) this;
        }

        public Criteria andResourceGradeNotIn(List<Integer> values) {
            addCriterion("RESOURCE_GRADE not in", values, "resourceGrade");
            return (Criteria) this;
        }

        public Criteria andResourceGradeBetween(Integer value1, Integer value2) {
            addCriterion("RESOURCE_GRADE between", value1, value2, "resourceGrade");
            return (Criteria) this;
        }

        public Criteria andResourceGradeNotBetween(Integer value1, Integer value2) {
            addCriterion("RESOURCE_GRADE not between", value1, value2, "resourceGrade");
            return (Criteria) this;
        }

        public Criteria andResourceOrderIsNull() {
            addCriterion("RESOURCE_ORDER is null");
            return (Criteria) this;
        }

        public Criteria andResourceOrderIsNotNull() {
            addCriterion("RESOURCE_ORDER is not null");
            return (Criteria) this;
        }

        public Criteria andResourceOrderEqualTo(Integer value) {
            addCriterion("RESOURCE_ORDER =", value, "resourceOrder");
            return (Criteria) this;
        }

        public Criteria andResourceOrderNotEqualTo(Integer value) {
            addCriterion("RESOURCE_ORDER <>", value, "resourceOrder");
            return (Criteria) this;
        }

        public Criteria andResourceOrderGreaterThan(Integer value) {
            addCriterion("RESOURCE_ORDER >", value, "resourceOrder");
            return (Criteria) this;
        }

        public Criteria andResourceOrderGreaterThanOrEqualTo(Integer value) {
            addCriterion("RESOURCE_ORDER >=", value, "resourceOrder");
            return (Criteria) this;
        }

        public Criteria andResourceOrderLessThan(Integer value) {
            addCriterion("RESOURCE_ORDER <", value, "resourceOrder");
            return (Criteria) this;
        }

        public Criteria andResourceOrderLessThanOrEqualTo(Integer value) {
            addCriterion("RESOURCE_ORDER <=", value, "resourceOrder");
            return (Criteria) this;
        }

        public Criteria andResourceOrderIn(List<Integer> values) {
            addCriterion("RESOURCE_ORDER in", values, "resourceOrder");
            return (Criteria) this;
        }

        public Criteria andResourceOrderNotIn(List<Integer> values) {
            addCriterion("RESOURCE_ORDER not in", values, "resourceOrder");
            return (Criteria) this;
        }

        public Criteria andResourceOrderBetween(Integer value1, Integer value2) {
            addCriterion("RESOURCE_ORDER between", value1, value2, "resourceOrder");
            return (Criteria) this;
        }

        public Criteria andResourceOrderNotBetween(Integer value1, Integer value2) {
            addCriterion("RESOURCE_ORDER not between", value1, value2, "resourceOrder");
            return (Criteria) this;
        }

        public Criteria andResourceAttrIsNull() {
            addCriterion("RESOURCE_ATTR is null");
            return (Criteria) this;
        }

        public Criteria andResourceAttrIsNotNull() {
            addCriterion("RESOURCE_ATTR is not null");
            return (Criteria) this;
        }

        public Criteria andResourceAttrEqualTo(String value) {
            addCriterion("RESOURCE_ATTR =", value, "resourceAttr");
            return (Criteria) this;
        }

        public Criteria andResourceAttrNotEqualTo(String value) {
            addCriterion("RESOURCE_ATTR <>", value, "resourceAttr");
            return (Criteria) this;
        }

        public Criteria andResourceAttrGreaterThan(String value) {
            addCriterion("RESOURCE_ATTR >", value, "resourceAttr");
            return (Criteria) this;
        }

        public Criteria andResourceAttrGreaterThanOrEqualTo(String value) {
            addCriterion("RESOURCE_ATTR >=", value, "resourceAttr");
            return (Criteria) this;
        }

        public Criteria andResourceAttrLessThan(String value) {
            addCriterion("RESOURCE_ATTR <", value, "resourceAttr");
            return (Criteria) this;
        }

        public Criteria andResourceAttrLessThanOrEqualTo(String value) {
            addCriterion("RESOURCE_ATTR <=", value, "resourceAttr");
            return (Criteria) this;
        }

        public Criteria andResourceAttrLike(String value) {
            addCriterion("RESOURCE_ATTR like", value, "resourceAttr");
            return (Criteria) this;
        }

        public Criteria andResourceAttrNotLike(String value) {
            addCriterion("RESOURCE_ATTR not like", value, "resourceAttr");
            return (Criteria) this;
        }

        public Criteria andResourceAttrIn(List<String> values) {
            addCriterion("RESOURCE_ATTR in", values, "resourceAttr");
            return (Criteria) this;
        }

        public Criteria andResourceAttrNotIn(List<String> values) {
            addCriterion("RESOURCE_ATTR not in", values, "resourceAttr");
            return (Criteria) this;
        }

        public Criteria andResourceAttrBetween(String value1, String value2) {
            addCriterion("RESOURCE_ATTR between", value1, value2, "resourceAttr");
            return (Criteria) this;
        }

        public Criteria andResourceAttrNotBetween(String value1, String value2) {
            addCriterion("RESOURCE_ATTR not between", value1, value2, "resourceAttr");
            return (Criteria) this;
        }

        public Criteria andResourceTypeIsNull() {
            addCriterion("RESOURCE_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andResourceTypeIsNotNull() {
            addCriterion("RESOURCE_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andResourceTypeEqualTo(String value) {
            addCriterion("RESOURCE_TYPE =", value, "resourceType");
            return (Criteria) this;
        }

        public Criteria andResourceTypeNotEqualTo(String value) {
            addCriterion("RESOURCE_TYPE <>", value, "resourceType");
            return (Criteria) this;
        }

        public Criteria andResourceTypeGreaterThan(String value) {
            addCriterion("RESOURCE_TYPE >", value, "resourceType");
            return (Criteria) this;
        }

        public Criteria andResourceTypeGreaterThanOrEqualTo(String value) {
            addCriterion("RESOURCE_TYPE >=", value, "resourceType");
            return (Criteria) this;
        }

        public Criteria andResourceTypeLessThan(String value) {
            addCriterion("RESOURCE_TYPE <", value, "resourceType");
            return (Criteria) this;
        }

        public Criteria andResourceTypeLessThanOrEqualTo(String value) {
            addCriterion("RESOURCE_TYPE <=", value, "resourceType");
            return (Criteria) this;
        }

        public Criteria andResourceTypeLike(String value) {
            addCriterion("RESOURCE_TYPE like", value, "resourceType");
            return (Criteria) this;
        }

        public Criteria andResourceTypeNotLike(String value) {
            addCriterion("RESOURCE_TYPE not like", value, "resourceType");
            return (Criteria) this;
        }

        public Criteria andResourceTypeIn(List<String> values) {
            addCriterion("RESOURCE_TYPE in", values, "resourceType");
            return (Criteria) this;
        }

        public Criteria andResourceTypeNotIn(List<String> values) {
            addCriterion("RESOURCE_TYPE not in", values, "resourceType");
            return (Criteria) this;
        }

        public Criteria andResourceTypeBetween(String value1, String value2) {
            addCriterion("RESOURCE_TYPE between", value1, value2, "resourceType");
            return (Criteria) this;
        }

        public Criteria andResourceTypeNotBetween(String value1, String value2) {
            addCriterion("RESOURCE_TYPE not between", value1, value2, "resourceType");
            return (Criteria) this;
        }

        public Criteria andResourceImageIsNull() {
            addCriterion("RESOURCE_IMAGE is null");
            return (Criteria) this;
        }

        public Criteria andResourceImageIsNotNull() {
            addCriterion("RESOURCE_IMAGE is not null");
            return (Criteria) this;
        }

        public Criteria andResourceImageEqualTo(String value) {
            addCriterion("RESOURCE_IMAGE =", value, "resourceImage");
            return (Criteria) this;
        }

        public Criteria andResourceImageNotEqualTo(String value) {
            addCriterion("RESOURCE_IMAGE <>", value, "resourceImage");
            return (Criteria) this;
        }

        public Criteria andResourceImageGreaterThan(String value) {
            addCriterion("RESOURCE_IMAGE >", value, "resourceImage");
            return (Criteria) this;
        }

        public Criteria andResourceImageGreaterThanOrEqualTo(String value) {
            addCriterion("RESOURCE_IMAGE >=", value, "resourceImage");
            return (Criteria) this;
        }

        public Criteria andResourceImageLessThan(String value) {
            addCriterion("RESOURCE_IMAGE <", value, "resourceImage");
            return (Criteria) this;
        }

        public Criteria andResourceImageLessThanOrEqualTo(String value) {
            addCriterion("RESOURCE_IMAGE <=", value, "resourceImage");
            return (Criteria) this;
        }

        public Criteria andResourceImageLike(String value) {
            addCriterion("RESOURCE_IMAGE like", value, "resourceImage");
            return (Criteria) this;
        }

        public Criteria andResourceImageNotLike(String value) {
            addCriterion("RESOURCE_IMAGE not like", value, "resourceImage");
            return (Criteria) this;
        }

        public Criteria andResourceImageIn(List<String> values) {
            addCriterion("RESOURCE_IMAGE in", values, "resourceImage");
            return (Criteria) this;
        }

        public Criteria andResourceImageNotIn(List<String> values) {
            addCriterion("RESOURCE_IMAGE not in", values, "resourceImage");
            return (Criteria) this;
        }

        public Criteria andResourceImageBetween(String value1, String value2) {
            addCriterion("RESOURCE_IMAGE between", value1, value2, "resourceImage");
            return (Criteria) this;
        }

        public Criteria andResourceImageNotBetween(String value1, String value2) {
            addCriterion("RESOURCE_IMAGE not between", value1, value2, "resourceImage");
            return (Criteria) this;
        }

        public Criteria andResourceStatusIsNull() {
            addCriterion("RESOURCE_STATUS is null");
            return (Criteria) this;
        }

        public Criteria andResourceStatusIsNotNull() {
            addCriterion("RESOURCE_STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andResourceStatusEqualTo(String value) {
            addCriterion("RESOURCE_STATUS =", value, "resourceStatus");
            return (Criteria) this;
        }

        public Criteria andResourceStatusNotEqualTo(String value) {
            addCriterion("RESOURCE_STATUS <>", value, "resourceStatus");
            return (Criteria) this;
        }

        public Criteria andResourceStatusGreaterThan(String value) {
            addCriterion("RESOURCE_STATUS >", value, "resourceStatus");
            return (Criteria) this;
        }

        public Criteria andResourceStatusGreaterThanOrEqualTo(String value) {
            addCriterion("RESOURCE_STATUS >=", value, "resourceStatus");
            return (Criteria) this;
        }

        public Criteria andResourceStatusLessThan(String value) {
            addCriterion("RESOURCE_STATUS <", value, "resourceStatus");
            return (Criteria) this;
        }

        public Criteria andResourceStatusLessThanOrEqualTo(String value) {
            addCriterion("RESOURCE_STATUS <=", value, "resourceStatus");
            return (Criteria) this;
        }

        public Criteria andResourceStatusLike(String value) {
            addCriterion("RESOURCE_STATUS like", value, "resourceStatus");
            return (Criteria) this;
        }

        public Criteria andResourceStatusNotLike(String value) {
            addCriterion("RESOURCE_STATUS not like", value, "resourceStatus");
            return (Criteria) this;
        }

        public Criteria andResourceStatusIn(List<String> values) {
            addCriterion("RESOURCE_STATUS in", values, "resourceStatus");
            return (Criteria) this;
        }

        public Criteria andResourceStatusNotIn(List<String> values) {
            addCriterion("RESOURCE_STATUS not in", values, "resourceStatus");
            return (Criteria) this;
        }

        public Criteria andResourceStatusBetween(String value1, String value2) {
            addCriterion("RESOURCE_STATUS between", value1, value2, "resourceStatus");
            return (Criteria) this;
        }

        public Criteria andResourceStatusNotBetween(String value1, String value2) {
            addCriterion("RESOURCE_STATUS not between", value1, value2, "resourceStatus");
            return (Criteria) this;
        }

        public Criteria andResourceComponentIsNull() {
            addCriterion("RESOURCE_COMPONENT is null");
            return (Criteria) this;
        }

        public Criteria andResourceComponentIsNotNull() {
            addCriterion("RESOURCE_COMPONENT is not null");
            return (Criteria) this;
        }

        public Criteria andResourceComponentEqualTo(String value) {
            addCriterion("RESOURCE_COMPONENT =", value, "resourceComponent");
            return (Criteria) this;
        }

        public Criteria andResourceComponentNotEqualTo(String value) {
            addCriterion("RESOURCE_COMPONENT <>", value, "resourceComponent");
            return (Criteria) this;
        }

        public Criteria andResourceComponentGreaterThan(String value) {
            addCriterion("RESOURCE_COMPONENT >", value, "resourceComponent");
            return (Criteria) this;
        }

        public Criteria andResourceComponentGreaterThanOrEqualTo(String value) {
            addCriterion("RESOURCE_COMPONENT >=", value, "resourceComponent");
            return (Criteria) this;
        }

        public Criteria andResourceComponentLessThan(String value) {
            addCriterion("RESOURCE_COMPONENT <", value, "resourceComponent");
            return (Criteria) this;
        }

        public Criteria andResourceComponentLessThanOrEqualTo(String value) {
            addCriterion("RESOURCE_COMPONENT <=", value, "resourceComponent");
            return (Criteria) this;
        }

        public Criteria andResourceComponentLike(String value) {
            addCriterion("RESOURCE_COMPONENT like", value, "resourceComponent");
            return (Criteria) this;
        }

        public Criteria andResourceComponentNotLike(String value) {
            addCriterion("RESOURCE_COMPONENT not like", value, "resourceComponent");
            return (Criteria) this;
        }

        public Criteria andResourceComponentIn(List<String> values) {
            addCriterion("RESOURCE_COMPONENT in", values, "resourceComponent");
            return (Criteria) this;
        }

        public Criteria andResourceComponentNotIn(List<String> values) {
            addCriterion("RESOURCE_COMPONENT not in", values, "resourceComponent");
            return (Criteria) this;
        }

        public Criteria andResourceComponentBetween(String value1, String value2) {
            addCriterion("RESOURCE_COMPONENT between", value1, value2, "resourceComponent");
            return (Criteria) this;
        }

        public Criteria andResourceComponentNotBetween(String value1, String value2) {
            addCriterion("RESOURCE_COMPONENT not between", value1, value2, "resourceComponent");
            return (Criteria) this;
        }

        public Criteria andResourceDescIsNull() {
            addCriterion("RESOURCE_DESC is null");
            return (Criteria) this;
        }

        public Criteria andResourceDescIsNotNull() {
            addCriterion("RESOURCE_DESC is not null");
            return (Criteria) this;
        }

        public Criteria andResourceDescEqualTo(String value) {
            addCriterion("RESOURCE_DESC =", value, "resourceDesc");
            return (Criteria) this;
        }

        public Criteria andResourceDescNotEqualTo(String value) {
            addCriterion("RESOURCE_DESC <>", value, "resourceDesc");
            return (Criteria) this;
        }

        public Criteria andResourceDescGreaterThan(String value) {
            addCriterion("RESOURCE_DESC >", value, "resourceDesc");
            return (Criteria) this;
        }

        public Criteria andResourceDescGreaterThanOrEqualTo(String value) {
            addCriterion("RESOURCE_DESC >=", value, "resourceDesc");
            return (Criteria) this;
        }

        public Criteria andResourceDescLessThan(String value) {
            addCriterion("RESOURCE_DESC <", value, "resourceDesc");
            return (Criteria) this;
        }

        public Criteria andResourceDescLessThanOrEqualTo(String value) {
            addCriterion("RESOURCE_DESC <=", value, "resourceDesc");
            return (Criteria) this;
        }

        public Criteria andResourceDescLike(String value) {
            addCriterion("RESOURCE_DESC like", value, "resourceDesc");
            return (Criteria) this;
        }

        public Criteria andResourceDescNotLike(String value) {
            addCriterion("RESOURCE_DESC not like", value, "resourceDesc");
            return (Criteria) this;
        }

        public Criteria andResourceDescIn(List<String> values) {
            addCriterion("RESOURCE_DESC in", values, "resourceDesc");
            return (Criteria) this;
        }

        public Criteria andResourceDescNotIn(List<String> values) {
            addCriterion("RESOURCE_DESC not in", values, "resourceDesc");
            return (Criteria) this;
        }

        public Criteria andResourceDescBetween(String value1, String value2) {
            addCriterion("RESOURCE_DESC between", value1, value2, "resourceDesc");
            return (Criteria) this;
        }

        public Criteria andResourceDescNotBetween(String value1, String value2) {
            addCriterion("RESOURCE_DESC not between", value1, value2, "resourceDesc");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}