package com.njcb.ams.repository.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.ResourceInfoMapper;
import com.njcb.ams.repository.entity.ResourceInfo;
import com.njcb.ams.repository.entity.ResourceInfoExample;

/**
 * @author srxhx207
 */
@Repository
public class ResourceInfoDAO extends BaseMyBatisDAO<ResourceInfo, ResourceInfoExample, Integer> {

	public ResourceInfoDAO() {
		this.entityClass = ResourceInfo.class;
	}

	@Autowired
	private ResourceInfoMapper mapper;

	@Override
	public ResourceInfoMapper getMapper() {
		return mapper;
	}

	public List<ResourceInfo> findByResourceType(String resourceType) {
		ResourceInfoExample example = new ResourceInfoExample();
		example.createCriteria().andResourceTypeEqualTo(resourceType);
		example.setOrderByClause("PARENT_ID,RESOURCE_ORDER");
		return mapper.selectByExample(example);
	}

	/**
	 * 方法功能描述： 根据用户ID获取其对应权限（角色权限与机构权限的交集）
	 * 
	 * @return
	 */
	public List<ResourceInfo> selectAll() {
		ResourceInfoExample example = new ResourceInfoExample();
		example.setOrderByClause("PARENT_ID,RESOURCE_ORDER");
		return mapper.selectByExample(example);
	}

	public List<Integer> getResoByUser(int userID) {
		return mapper.getResoByUser(userID);
	}

	/**
	 * 方法功能描述：
	 * 
	 * @param example
	 * @return
	 */
	public List<ResourceInfo> findByUser(ResourceInfoExample example) {
		return mapper.findByUser(example);
	}

	public List<ResourceInfo> findResourceInfoByUser(String resourceType, int orgId, int userId) {
		return mapper.findResourceInfoByUser(resourceType, orgId, userId);
	}
}
