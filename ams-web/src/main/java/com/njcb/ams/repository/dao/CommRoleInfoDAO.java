package com.njcb.ams.repository.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.CommRoleInfoMapper;
import com.njcb.ams.repository.entity.CommRoleInfo;
import com.njcb.ams.repository.entity.CommRoleInfoExample;
import com.njcb.ams.pojo.dto.Tree;

import javax.management.relation.RoleInfo;

/**
 * @author lisr
 *
 *         类功能描述：
 *
 */
@Repository
public class CommRoleInfoDAO extends BaseMyBatisDAO<CommRoleInfo, CommRoleInfoExample, Integer> {

	public CommRoleInfoDAO() {
		this.entityClass = CommRoleInfo.class;
	}

	@Autowired
	private CommRoleInfoMapper mapper;

	@Override
	@SuppressWarnings("unchecked")
	public CommRoleInfoMapper getMapper() {
		return mapper;
	}

	public List<CommRoleInfo> selectAll() {
		return mapper.selectByExample(new CommRoleInfoExample());
	}

	/**
	 * @param roleInfo
	 */
	@Override
    public int saveOrUpdateByPrimaryKey(CommRoleInfo roleInfo) {
		if (null != roleInfo.getId()) {
			return getMapper().updateByPrimaryKey(roleInfo);
		} else {
			return getMapper().insert(roleInfo);
		}
	}

	public CommRoleInfo selectByRoleCode(String roleCode){
		CommRoleInfoExample example = new CommRoleInfoExample();
		example.createCriteria().andRoleCodeEqualTo(roleCode);
		return selectOneByExample(example);
	}

	public List<Tree> selectTree() {
		List<CommRoleInfo> list = selectAll();
		List<Tree> trees = this.convertToTree(list);
		return trees;
	}

	public List<Tree> convertToTree(List<CommRoleInfo> roleInfos) {
		List<Tree> newTrees = new ArrayList<Tree>();
		for (CommRoleInfo roleInfo : roleInfos) {
			Tree tree = new Tree();
			tree.setId(roleInfo.getId());
			tree.setText(roleInfo.getRoleName());
			newTrees.add(tree);
		}
		return newTrees;
	}

}
