package com.njcb.ams.web.controller.system;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/document")
@Api(value = "document", tags = "document")
public class DocumentConfigController {

//    @Autowired
//    private DocumentConfigService documentConfigService;
//    private static final Logger logger = LoggerFactory.getLogger(FileUpDownloadController.class);
//
//    private String contentDisposition = new String();
//    private String baseUrl = "Z:\\ams_front\\ccs_front\\test\\";
//
//    @ApiOperation(value = "查询文档列表", notes = "根据任意条件查询文档列表")
//    @RequestMapping(value = "query", method = RequestMethod.POST)
//    @ResponseBody
//    public DataList<DocumentInfo> queryDocumentListByCondition(@RequestBody DocumentConfigSearchCondition documentConfigSearchCondition) {
//        return documentConfigService.getDocumentInfoListByConditions(documentConfigSearchCondition);
//    }
//
//    @ApiOperation(value = "新增文档", notes = "新增文档")
//    @RequestMapping(value = "addNewDocument", method = RequestMethod.POST)
//    @ResponseBody
//    public Response addNewDocument(HttpServletRequest request, HttpServletResponse response, @RequestParam String filename, @RequestParam String fileStatus, @RequestParam("upfile") MultipartFile upfile) throws Exception {
//        // 上传文档
//        response.setContentType("text/html");
//        response.setCharacterEncoding("UTF-8");
//        response.setHeader("Pragma", "No-cache");
//        response.setHeader("Cache-Control", "no-cache,no-store,max-age=0");
//        response.setDateHeader("Expires", 1);
//        String fileName = System.currentTimeMillis() + upfile.getOriginalFilename();
//        logger.info("用户[{}]在客户端[{}]上传了文件[{}]", DataBus.getUserName(), request.getRemoteAddr(), fileName);
//
//        if (!upfile.isEmpty()) {
//            // 上传文件路径
//            File filepath = new File(baseUrl);
//            // 判断路径是否存在，如果不存在就创建一个
//            if (!filepath.exists()) {
//                filepath.mkdirs();
//            }
//            // 将上传文件保存到一个目标文件当中
//            upfile.transferTo(new File(baseUrl + fileName));
//        } else {
//            return Response.buildFail("未选择上传文件");
//        }
//
//        String msg = "文件已保存为: " + upfile.getOriginalFilename() + "，文件大小: " + getFileSizeName(upfile.getSize());
//
//        DocumentInfo documentInfo = new DocumentInfo();
//        documentInfo.setFileName(filename + fileName.substring(fileName.lastIndexOf(".")));
//        documentInfo.setFileStatus(fileStatus);
//        documentInfo.setFileUrl(baseUrl + fileName);
//        documentConfigService.addNewDocument(documentInfo);
//        return Response.buildSucc(msg);
//    }
//
//    @ApiOperation(value = "删除文档", notes = "删除文档")
//    @RequestMapping(value = "removeDocument", method = RequestMethod.POST)
//    @ResponseBody
//    public Response removeDocument(@RequestBody DocumentInfo documentInfo) {
//        documentConfigService.removeDocument(documentInfo);
//        return Response.buildSucc();
//    }
//
//    @ApiOperation(value = "启停文档", notes = "启停文档")
//    @RequestMapping(value = "changeDocumentStatus", method = RequestMethod.POST)
//    @ResponseBody
//    public Response changeDocumentStatus(@RequestBody DocumentInfo documentInfo) {
//        documentConfigService.changeDocumentStatus(documentInfo);
//        return Response.buildSucc();
//    }
//
//    @ApiOperation(value = "下载文档", notes = "下载文档")
//    @RequestMapping(value = "downloadDocument", method = RequestMethod.POST)
//    @ResponseBody
//    public void downloadDocument(HttpServletRequest request, HttpServletResponse response, @RequestBody DocumentInfo documentInfo) throws Exception {
//        CommDocumentConfig commDocumentConfig = documentConfigService.getFileUrl(documentInfo);
//        //下载文档
//        response.setContentType("application/octet-stream");
//        response.setCharacterEncoding("UTF-8");
//        response.setHeader("Pragma", "No-cache");
//        response.setHeader("Cache-Control", "no-cache,no-store,max-age=0");
//        response.setDateHeader("Expires", 1);
//        String fileName = commDocumentConfig.getFileUrl();
//
//        response.setHeader("Content-Disposition", this.contentDisposition + " filename=" + fileName);
//
//        // 复制缓冲区的大小
//        int paramInt = 65000;
//        ServletOutputStream os = response.getOutputStream();
//        java.io.File localFile = new File(fileName);
//        FileInputStream localFileInputStream = new FileInputStream(localFile);
//        // 下载的文件长度
//        long fileLength = localFile.length();
//        byte[] arrayOfByte = new byte[paramInt];
//
//        this.contentDisposition = ((this.contentDisposition == null) ? "attachment;" : this.contentDisposition);
//        int i = 0;
//        int j = 0;
//        while (j < fileLength) {
//            i = localFileInputStream.read(arrayOfByte, 0, paramInt);
//            j += i;
//            os.write(arrayOfByte, 0, i);
//        }
//        os.flush();
//        os.close();
//
//        logger.info("用户[{}]在客户端[{}]下载了文件[{}]", DataBus.getUserName(), request.getRemoteAddr(), fileName);
//        response.setContentLength((int) fileLength);
//        response.flushBuffer();
//        localFileInputStream.close();
//    }
}
