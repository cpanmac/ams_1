package com.njcb.ams.web.controller.system;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.njcb.ams.application.BusinessSystemWebManager;
import com.njcb.ams.repository.entity.AuthROrgReso;
import com.njcb.ams.repository.entity.AuthRRoleReso;
import com.njcb.ams.repository.entity.AuthRUserRole;
import com.njcb.ams.pojo.dto.Tree;
import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.util.AmsJsonUtils;

/**
 * 权限管理
 * 
 * @author srxhx207
 *
 */
@RequestMapping("/authset")
@RestController
public class AuthSetController {

	@Autowired
	private BusinessSystemWebManager businessManager;

	/**
	 * 获取[机构的]功能资源树
	 * 
	 * @return
	 */
	@RequestMapping(value = "org/resource", method = RequestMethod.POST)
	@ResponseBody
	public List<Tree> getOrgResoTree(int orgId) {
		return businessManager.orgResoQuery(orgId);
	}

	/**
	 * 更新[机构的]功能资源树
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "org/resource/update")
	@ResponseBody
	public Response updateResoTree(HttpServletRequest request) throws Exception {
		// 后台接收到的数据
		String idList = request.getParameter("insertNodes");
		String deleteNodes = request.getParameter("deleteNodes");
		List<AuthROrgReso> insertList = AmsJsonUtils.toArrayList(idList, AuthROrgReso.class);
		List<AuthROrgReso> deleteList = AmsJsonUtils.toArrayList(deleteNodes, AuthROrgReso.class);
		businessManager.orgResoSave(insertList, deleteList);
		return Response.buildSucc();
	}

	/**
	 * 获取[角色的]功能资源树
	 * 
	 * @param roleId
	 * @return
	 */
	@RequestMapping(value = "role/resource")
	@ResponseBody
	public List<Tree> getRoleResoNodes(Integer roleId) {
		return businessManager.roleResoQuery(roleId);
	}

	@RequestMapping(value = "role/resource/update")
	@ResponseBody
	public Response updateRoleResoNodes(HttpServletRequest request) throws Exception {
		// 后台接收到的数据
		String idList = request.getParameter("insertNodes");
		String deleteNodes = request.getParameter("deleteNodes");
		List<AuthRRoleReso> insertList = AmsJsonUtils.toArrayList(idList, AuthRRoleReso.class);
		List<AuthRRoleReso> deleteList = AmsJsonUtils.toArrayList(deleteNodes, AuthRRoleReso.class);
		businessManager.roleResoSave(insertList, deleteList);
		return Response.buildSucc();
	}

	/**
	 * 获取[用户的]角色树
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "user/role")
	@ResponseBody
	public List<Tree> getUserRoleNodes(int userId) {
		return businessManager.userRoleQuery(userId);
	}

	@RequestMapping(value = "user/role/update")
	@ResponseBody
	public Response updateUserRoleNodes(HttpServletRequest request) throws Exception {
		// 后台接收到的数据
		String idList = request.getParameter("insertNodes");
		String deleteNodes = request.getParameter("deleteNodes");
		List<AuthRUserRole> insertList = AmsJsonUtils.toArrayList(idList, AuthRUserRole.class);
		List<AuthRUserRole> deleteList = AmsJsonUtils.toArrayList(deleteNodes, AuthRUserRole.class);
		businessManager.userRoleSave(insertList, deleteList);
		return Response.buildSucc();
	}
}
