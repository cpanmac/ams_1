package com.njcb.ams.web.controller.system;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.njcb.ams.application.BusinessSystemManager;
import com.njcb.ams.repository.entity.SysParam;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.store.page.Page;
import com.njcb.ams.store.page.PageHandle;
import com.njcb.ams.util.AmsUtils;

import io.swagger.annotations.Api;

@RequestMapping("/system/param")
@Api(value = "param",tags = "param")
@RestController
public class ParamSetController {
	private static final Logger logger = LoggerFactory.getLogger(ParamSetController.class);

	@Autowired
	private BusinessSystemManager businessManager;
	@Autowired
	private ObjectMapper objectMapper;

	@RequestMapping(value = "query", method = RequestMethod.POST)
	@ResponseBody
	public PageResponse<SysParam> queryParam(@RequestBody SysParam sysParam) throws Exception {
		PageHandle.startPage(sysParam);
		businessManager.queryParam(sysParam);
		Page page = PageHandle.endPage();
		List<SysParam> list = page.getResult(SysParam.class);
		return PageResponse.build(list, page.getTotal());
	}

	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public Response setParam(@RequestBody SysParam sysParam) throws Exception {
		businessManager.saveSysParam(sysParam);
		return Response.buildSucc();
	}

	@ApiOperation(value = "删除参数", notes = "单笔删除参数")
	@RequestMapping(value = "{name}/remove", method = RequestMethod.DELETE)
	@ResponseBody
	public Response orgInfoSimpleRemove(@ApiParam(required = true, value = "参数名") @PathVariable("name") String name) throws Exception {
		businessManager.deleteSysParam(name);
		return Response.buildSucc();
	}

	@RequestMapping(value = "save/overall", method = RequestMethod.POST)
	@ResponseBody
	public Response setParam(HttpServletRequest request) throws Exception {
		String insertRows = request.getParameter("insertRows");
		String deleteRows = request.getParameter("deleteRows");
		String updateRows = request.getParameter("updateRows");
		logger.debug("insertRows json : {}", insertRows);
		logger.debug("deleteRows json : {}", deleteRows);
		logger.debug("updateRows json : {}", updateRows);
		List<SysParam> insertList = AmsUtils.isNull(insertRows)? new ArrayList<SysParam>():objectMapper.readValue(insertRows, new TypeReference<List<SysParam>>() {});
		List<SysParam> deleteList = AmsUtils.isNull(deleteRows)? new ArrayList<SysParam>():objectMapper.readValue(deleteRows, new TypeReference<List<SysParam>>() {});
		List<SysParam> updateList = AmsUtils.isNull(updateRows)? new ArrayList<SysParam>():objectMapper.readValue(updateRows, new TypeReference<List<SysParam>>() {});
		businessManager.updateParam(insertList, deleteList, updateList);
		return Response.buildSucc();
	}

}
