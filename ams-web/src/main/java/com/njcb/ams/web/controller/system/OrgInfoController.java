package com.njcb.ams.web.controller.system;

import com.njcb.ams.application.BusinessSystemManager;
import com.njcb.ams.assembler.OrgInfoConvertor;
import com.njcb.ams.factory.comm.DataBus;
import com.njcb.ams.pojo.dto.Tree;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.pojo.vo.OrgInfoQueryInput;
import com.njcb.ams.pojo.vo.OrgInfoQueryOutput;
import com.njcb.ams.pojo.vo.OrgInfoSaveInput;
import com.njcb.ams.repository.entity.CommOrgInfo;
import com.njcb.ams.service.BusinessAuthorityService;
import com.njcb.ams.store.page.Page;
import com.njcb.ams.store.page.PageHandle;
import com.njcb.ams.util.AmsUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 机构操作类
 *
 * @author LOONG
 */
@RestController
@RequestMapping("/system/orginfo")
@Api(value = "orginfo", tags = "orginfo")
public class OrgInfoController {

    @Autowired
    private BusinessSystemManager businessManager;
    @Autowired
    private BusinessAuthorityService authorityService;
    @Autowired
    private OrgInfoConvertor convertor;

    @ApiOperation(value = "获取机构列表", notes = "根据任意条件查询机构列表")
    @RequestMapping(value = "query", method = RequestMethod.POST)
    @ResponseBody
    public PageResponse<OrgInfoQueryOutput> orgInfoQuery(@RequestBody OrgInfoQueryInput inputOrgInfo) throws Exception {
        CommOrgInfo orgInfo = convertor.inputQueryVoToEntity(inputOrgInfo);
        orgInfo.setOrgIdList(authorityService.selectSelfAndSubById());
        PageHandle.startPage(inputOrgInfo);
        List<CommOrgInfo> list = businessManager.orgInfoQuery(orgInfo);
        Page page = PageHandle.endPage();
        List<OrgInfoQueryOutput> rtList = new ArrayList<OrgInfoQueryOutput>();
        for (CommOrgInfo orgInfoTmp : list) {
            rtList.add(convertor.entityToOutputQueryVo(orgInfoTmp));
        }
        return PageResponse.build(rtList, page.getTotal());
    }

    @ApiOperation(value = "保存机构", notes = "单笔保存机构")
    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public Response orgInfoSimpleSave(@RequestBody OrgInfoSaveInput inputOrgInfo) throws Exception {
        CommOrgInfo orgInfo = convertor.inputSaveVoToEntity(inputOrgInfo);
        businessManager.saveOrgInfo(orgInfo);
        return Response.buildSucc();
    }

    @ApiOperation(value = "删除机构", notes = "单笔删除机构")
    @RequestMapping(value = "{orgNo}/remove", method = RequestMethod.DELETE)
    @ResponseBody
    public Response orgInfoSimpleRemove(@ApiParam(required = true, value = "机构号") @PathVariable("orgNo") String orgNo) {
        businessManager.deleteOrgInfo(orgNo);
        return Response.buildSucc();
    }

    @ApiOperation(value = "删除机构", notes = "单笔删除机构")
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    @ResponseBody
    public Response orgInfoSimpleRemoveOld(@RequestBody OrgInfoSaveInput input) {
        businessManager.deleteOrgInfo(input.getOrgNo());
        return Response.buildSucc();
    }


    /**
     * 获取机构树
     *
     * @return
     */
    @ApiOperation(value = "获取机构树", notes = "获取本机构及所属子机构树")
    @RequestMapping(value = "tree", method = RequestMethod.POST)
    @ResponseBody
    public List<Tree> getOrgTree(@ApiParam(value = "机构号") String orgNo) {
        //如果参数非空、则根据机构获取树，如果参数为空、则默认根据登录用户归属机构查询
        if (AmsUtils.isNull(orgNo)) {
            orgNo = DataBus.getOrgNo();
        }
        return businessManager.orgQuery(orgNo);
    }
}
