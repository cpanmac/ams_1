package com.njcb.ams.web.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.njcb.ams.application.BusinessCoreManager;
import com.njcb.ams.repository.entity.SysTradeConsole;
import com.njcb.ams.repository.entity.SysTradeLog;
import com.njcb.ams.pojo.dto.SysTradeLogDTO;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.store.page.Page;
import com.njcb.ams.store.page.PageHandle;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/servicegovern")
@Api(value = "service",tags = "service")
public class ServerConsoleController {

	@Autowired
	private BusinessCoreManager businessManager;

	@ApiOperation(value = "查询控制信息", notes = "查询控制信息")
	@RequestMapping(value = "query", method = RequestMethod.POST)
	@ResponseBody
	public PageResponse<SysTradeConsole> serverConsoleQuery(@RequestBody SysTradeConsole inBean) throws Exception {
		PageHandle.startPage(inBean);
		List<SysTradeConsole> list = businessManager.getServerConsole(inBean);
		Page page = PageHandle.endPage();
		return PageResponse.build(list, page.getTotal());
	}

	@ApiOperation(value = "保存控制信息", notes = "保存控制信息")
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public Response serverConsoleSave(@RequestBody SysTradeConsole inBean) throws Exception {
		businessManager.serverConsoleSave(inBean);
		return Response.buildSucc();
	}

	@RequestMapping(value = "tradeLog", method = RequestMethod.POST)
	@ResponseBody
	public PageResponse<SysTradeLog> tradeLogQuery(@RequestBody SysTradeLogDTO tradeLog) throws Exception {
		PageResponse<SysTradeLog> pageResponse =  businessManager.tradeLogQuery(tradeLog);
		return pageResponse;
	}

}
