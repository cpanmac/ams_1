/**
 * 描述：
 * Copyright：Copyright （c） 2017
 * Company：南京银行
 *
 * @author njcbkf045
 * @version 1.0 2017年3月6日
 * @see history
 * 2017年3月6日
 */

package com.njcb.ams.web.controller;

import com.njcb.ams.pojo.dto.standard.EntityResponse;
import com.njcb.ams.repository.entity.CommOrgInfo;
import com.njcb.ams.util.SysInfoUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuyanlong
 */
@RequestMapping("/open/ams")
@RestController
public class AmsTestController {

    @RequestMapping(value = "test", method = RequestMethod.GET)
    @ResponseBody
    public EntityResponse<CommOrgInfo> queryRuleConfig() throws Exception {
        CommOrgInfo orgInfo = new CommOrgInfo();
        orgInfo.setId(1);
//        orgInfo.setOrgGrade(90);
        orgInfo.setOrgType("123");
        return EntityResponse.build(orgInfo);
    }

}