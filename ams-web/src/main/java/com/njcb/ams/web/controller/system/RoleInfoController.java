/**
 * 描述：
 * Copyright：Copyright （c） 2017
 * Company：南京银行
 *
 * @author njcbkf045
 * @version 1.0 2017年3月6日
 * @see history
 * 2017年3月6日
 */

package com.njcb.ams.web.controller.system;

import com.njcb.ams.application.BusinessSystemManager;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.repository.entity.CommRoleInfo;
import com.njcb.ams.store.page.Page;
import com.njcb.ams.store.page.PageHandle;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色控制类
 *
 * @author LOONG
 */
@RequestMapping("/system/roleinfo")
@Api(value = "roleinfo", tags = "roleinfo")
@RestController
public class RoleInfoController {

    @Autowired
    private BusinessSystemManager businessManager;

    @ApiOperation(value = "获取角色列表", notes = "根据任意条件查询角色列表")
    @RequestMapping(value = "query", method = RequestMethod.POST)
    @ResponseBody
//	@RolesAllowed("ADMIN")
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
    public PageResponse<CommRoleInfo> queryRoleInfo(@RequestBody CommRoleInfo roleInfo) throws Exception {
        PageHandle.startPage(roleInfo);
        List<CommRoleInfo> list = businessManager.queryRoleInfo(roleInfo);
        Page page = PageHandle.endPage();
        return PageResponse.build(list, page.getTotal());
    }

    @ApiOperation(value = "保存角色信息", notes = "保存角色信息")
    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public Response saveRoleInfo(@RequestBody CommRoleInfo roleInfo) throws Exception {
        businessManager.saveRoleInfo(roleInfo);
        return Response.buildSucc();
    }

    @ApiOperation(value = "删除角色信息", notes = "删除角色信息")
    @RequestMapping(value = "remove", method = RequestMethod.POST)
    @ResponseBody
    public Response removeRoleInfo(@RequestBody CommRoleInfo roleInfo) throws Exception {
        businessManager.removeRoleInfo(roleInfo);
        return Response.buildSucc();
    }
}
