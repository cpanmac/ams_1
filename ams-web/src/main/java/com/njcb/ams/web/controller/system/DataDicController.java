package com.njcb.ams.web.controller.system;

import java.util.*;
import java.util.Map.Entry;

import com.njcb.ams.pojo.dto.standard.EntityResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.njcb.ams.repository.entity.CommDataDic;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.service.DataDicService;
import com.njcb.ams.store.page.Page;
import com.njcb.ams.store.page.PageHandle;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author liuyanlong
 *
 *         类功能描述：
 *
 */
@RestController
@RequestMapping("/datadic")
@Api(value = "datadic",tags = "datadic")
public class DataDicController {

	@Autowired
	private DataDicService dataDicService;

	@ApiOperation(value = "获取字典列表", notes = "根据任意条件查询字典列表")
	@RequestMapping(value = "query", method = RequestMethod.POST)
	@ResponseBody
	public PageResponse<CommDataDic> dataDicQuery(@RequestBody CommDataDic inBean) throws Exception {
		PageHandle.startPage(inBean);
		List<CommDataDic> list = null;
		if (StringUtils.isNotEmpty(inBean.getQueryType()) && "cache".equals(inBean.getQueryType())) {
			list = dataDicCacheQuery(inBean);
		} else {
			list = dataDicService.findDicDic(inBean);
		}
		Page page = PageHandle.endPage();
		return PageResponse.build(list, page.getTotal());
	}

	@ApiOperation(value = "新增字典", notes = "新增字典")
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public Response dataDicSave(@RequestBody CommDataDic inBean) throws Exception {
		if (null != inBean) {
			inBean.setDataAttr("01");
			dataDicService.dataDicSave(inBean);
			// 刷新缓存
			reloadDicSave(inBean,inBean.getDataType());
		}
		return Response.buildSucc();
	}

	@ApiOperation(value = "字典刷新", notes = "指定字典类型刷新")
	@RequestMapping(value = "reload", method = RequestMethod.POST)
	@ResponseBody
	public Response reloadDicSave(CommDataDic inBean,String dataType) throws Exception {
		if (null != inBean && StringUtils.isNotEmpty(dataType)) {
			dataDicService.reloadDataDicByType(dataType);
			return Response.buildSucc();
		} else {
			return Response.buildFail("必须指定数据字典类型刷新");
		}
	}

	@ApiOperation(value = "字典查询", notes = "指定字典类型查询")
	@RequestMapping(value = "loadDataDic", method = RequestMethod.POST)
	@ResponseBody
	public EntityResponse<Map<String, Map<String, String>>> getDataDicMap(@RequestBody List<String> dataDic) throws Exception {
		Map<String, Map<String, String>> dataDicMap = new HashMap<String, Map<String, String>>();
		for (int i = 0; i < dataDic.size(); i++) {
			String dataType = dataDic.get(i);
			dataDicMap.put(dataType, DataDicService.getDataType(dataType));
		}
		return EntityResponse.build(dataDicMap);
	}

	private List<CommDataDic> dataDicCacheQuery(CommDataDic inBean) throws Exception {
		List<CommDataDic> list = new ArrayList<CommDataDic>();
		Map<String, String> retMap = Optional.ofNullable(DataDicService.getDataType(inBean.getDataType())).orElse(new HashMap<String, String>());
		Iterator<Entry<String, String>> it = retMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, String> entry = it.next();
			CommDataDic data = new CommDataDic();
			data.setDataNo(entry.getKey());
			data.setDataName(entry.getValue());
			data.setDataType(inBean.getDataType());
			list.add(data);
		}
		return list;
	}

}
