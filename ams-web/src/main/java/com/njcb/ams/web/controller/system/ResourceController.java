package com.njcb.ams.web.controller.system;

import com.njcb.ams.factory.comm.DataBus;
import com.njcb.ams.application.BusinessSystemWebManager;
import com.njcb.ams.repository.entity.ResourceInfo;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.pojo.vo.ResourceQueryInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author LOONG
 */
@RestController
@Api(value = "resource",tags = "resource")
public class ResourceController {
    @Autowired
    private BusinessSystemWebManager businessManager;

    /**
     * 菜单加载
     *
     * @throws Exception
     */
    @ApiOperation(value = "用户资源权限", notes = "用户资源权限")
    @RequestMapping(value = "resource/query" , method = RequestMethod.POST)
    @ResponseBody
    public PageResponse<ResourceInfo> resourceQuery(@RequestBody ResourceQueryInput resourceInfo) throws Exception {
        List<ResourceInfo> funList = null;
        // 如果有系统管理员角色，则给所有权限
        if ((null != DataBus.getRoleCodes() && DataBus.getRoleCodes().contains("admin")) || "admin".equals(DataBus.getLoginName())) {
            funList = businessManager.queryMenu(resourceInfo.getResourceType());
        }else{
            funList = businessManager.queryResoMenuByUser(resourceInfo.getResourceType());
        }

        return PageResponse.build(funList);
    }

}
