package com.njcb.ams.web.controller.system;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.njcb.ams.pojo.dto.JvmMonitorDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.njcb.ams.application.BusinessSystemManager;
import com.njcb.ams.pojo.dto.CacheMonitorDTO;
import com.njcb.ams.pojo.dto.TableSpaceTableUseDTO;
import com.njcb.ams.pojo.dto.TableSpaceUseDTO;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.pojo.dto.standard.Response;

import io.swagger.annotations.Api;

/**
 * @author LOONG
 */
@RestController
@RequestMapping("/monitor")
@Api(value = "monitor", tags = "monitor")
public class MonitorController {
	/**
	 * 内存概况
	 */
	public static final String JMTYPE_1 = "1";
	/**
	 * 线程概况
	 */
	public static final String JMTYPE_2 = "2";
	/**
	 * 主要线程
	 */
	public static final String JMTYPE_3 = "3";

	@Autowired
	private BusinessSystemManager businessManager;

	@RequestMapping(value = "cache/query", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> cacheMonitorQuery(@RequestBody CacheMonitorDTO inBean) throws Exception {
		List<CacheMonitorDTO> list = businessManager.cacheMonitorQuery(inBean);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		modelMap.put("success", true);
		modelMap.put("total", inBean.getTotal());
		modelMap.put("rows", list);
		return modelMap;
	}

	@RequestMapping(value = "cache/remove", method = RequestMethod.POST)
	@ResponseBody
	public Response cacheRemove(@RequestBody CacheMonitorDTO inBean) throws Exception {
		businessManager.cacheRemove(inBean);
		return Response.buildSucc();
	}
	
	@RequestMapping(value = "tablespace/query", method = RequestMethod.POST)
	@ResponseBody
	public PageResponse<TableSpaceUseDTO> getTableSpaceUse() throws Exception {
		List<TableSpaceUseDTO> rtList = businessManager.getTableSpaceUse();
		return PageResponse.build(rtList, rtList.size());
	}
	
	@RequestMapping(value = "tablespace/table/query", method = RequestMethod.POST)
	@ResponseBody
	public PageResponse<TableSpaceTableUseDTO> getTableSpaceTableUse() throws Exception {
		List<TableSpaceTableUseDTO> rtList = businessManager.getTableSpaceTableUse();
		return PageResponse.build(rtList, rtList.size());
	}
	
	@RequestMapping(value = "jvm")
	@ResponseBody
	public PageResponse<Object> queryParam(@RequestBody JvmMonitorDTO jvmMonitorDTO) throws Exception {
		String jmType = jvmMonitorDTO.getJmType();
		List<Object> returnList = new ArrayList<Object>();
		if (JMTYPE_1.equals(jmType)) {
			java.text.DecimalFormat formatter = new java.text.DecimalFormat("###,###,###,###,###,### KB");

			MemoryUsage mu = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("memoryName", "堆内存");
			// 初始值
			map.put("initMemory", formatter.format(mu.getInit() / 1024));
			// 实际使用的值
			map.put("usedMemory", formatter.format(mu.getUsed() / 1024));
			// 分配值，JVM向操作系统实际分配的地址()
			map.put("committedMemory", formatter.format(mu.getCommitted() / 1024));
			// 最大值
			map.put("maxMemory", formatter.format(mu.getMax() / 1024));
			returnList.add(map);

			MemoryUsage mu2 = ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage();
			Map<String, Object> map2 = new HashMap<String, Object>();
			map2.put("memoryName", "非堆内存");
			map2.put("initMemory", formatter.format(mu2.getInit() / 1024));
			map2.put("usedMemory", formatter.format(mu2.getUsed() / 1024));
			map2.put("committedMemory", formatter.format(mu.getCommitted() / 1024));
			map2.put("maxMemory", formatter.format(mu2.getMax() / 1024));
			returnList.add(map2);

			List<MemoryPoolMXBean> mpools = ManagementFactory.getMemoryPoolMXBeans();
			for (MemoryPoolMXBean memoryPool : mpools) {
				Map<String, Object> map3 = new HashMap<String, Object>();
				map3.put("memoryName", memoryPool.getName());
				map3.put("initMemory", formatter.format(memoryPool.getUsage().getInit() / 1024));
				map3.put("usedMemory", formatter.format(memoryPool.getUsage().getUsed() / 1024));
				map3.put("committedMemory", formatter.format(memoryPool.getUsage().getCommitted() / 1024));
				map3.put("maxMemory", formatter.format(memoryPool.getUsage().getMax() / 1024));
				returnList.add(map3);
			}
		}
		if (JMTYPE_2.equals(jmType)) {
			for (int i = 0; i < 1; i++) {
				ThreadMXBean threadMXInfo = ManagementFactory.getThreadMXBean();
				returnList.add(threadMXInfo);
			}
		}
		if (JMTYPE_3.equals(jmType)) {
			for (long threadId : ManagementFactory.getThreadMXBean().getAllThreadIds()) {
				returnList.add(ManagementFactory.getThreadMXBean().getThreadInfo(threadId, Integer.MAX_VALUE));
			}
		}
		return PageResponse.build(returnList, returnList.size());
	}

}
