package com.njcb.ams.web.controller.template;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.druid.sql.ast.expr.SQLPropertyExpr;
import com.alibaba.druid.sql.ast.statement.SQLSelectItem;
import com.njcb.ams.application.BusinessReportManager;
import com.njcb.ams.repository.dao.CommReportMetaDAO;
import com.njcb.ams.repository.entity.CommReportMeta;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.store.page.Page;
import com.njcb.ams.store.page.PageHandle;
import com.njcb.ams.util.AmsAssert;
import com.njcb.ams.util.SqlParserUtils;

@Controller
@RequestMapping("/template")
public class ReportController {
    @Autowired
    private BusinessReportManager businessManager;
    @Autowired
    private CommReportMetaDAO commReportMetaDAO;

    @RequestMapping("/report")
    public ModelAndView report(HttpServletRequest request) {
        ModelAndView mv = new ModelAndView("report.ftl");
        try {
            String reportNo = request.getParameter("reportno");
            AmsAssert.notNull(reportNo, "报表编号参数不能为空");
            CommReportMeta commReportMeta = commReportMetaDAO.selectByReportNo(reportNo);
            AmsAssert.notNull(commReportMeta, "报表编号不存在");
            Map<String, String> columnMap = new HashMap<>();
            List<SQLSelectItem> selectItems = SqlParserUtils.parserSelectItems(commReportMeta.getReportSql());
            selectItems.forEach(selectItem -> {
                if (selectItem.getExpr() instanceof SQLPropertyExpr) {
                    SQLPropertyExpr expr = (SQLPropertyExpr) selectItem.getExpr();
                    String filedName = expr.getName();
                    String filedAlias = null != selectItem.getAlias() ? selectItem.getAlias() : filedName;
                    columnMap.put(filedAlias.toUpperCase(), filedAlias.toUpperCase());
                } else {
                    String filedName = selectItem.getExpr().toString();
                    String filedAlias = null != selectItem.getAlias() ? selectItem.getAlias() : filedName;
                    columnMap.put(filedAlias.toUpperCase(), filedAlias.toUpperCase());
                }
            });
            mv.addObject("reportNo", commReportMeta.getReportNo());
            mv.addObject("reportName", commReportMeta.getReportName());
            /**
             * 字段及别名
             */
            mv.addObject("columnMap", columnMap);
            mv.addObject("dataDicList", "{}");
        } catch (Exception e) {
            e.printStackTrace();
            mv.setViewName("error");
        }
        return mv;
    }

    @RequestMapping("/report/query")
    @ResponseBody
    public PageResponse<Map<String, Object>> reportQuery(HttpServletRequest request, @RequestBody HashMap<String, Object> param) {
        String reportNo = request.getParameter("reportno");
        CommReportMeta commReportMeta = commReportMetaDAO.selectByReportNo(reportNo);
        AmsAssert.notNull(commReportMeta, "编号" + reportNo + "不存在");
        Page paramPage = new Page(1, 5000);
        if (param.containsKey("page") && param.containsKey("rows")) {
            paramPage = new Page(Integer.parseInt(param.get("page").toString()), Integer.parseInt(param.get("rows").toString()));
        }
        PageHandle.startPage(paramPage);
        List<Map<String, Object>> list = businessManager.queryReport(commReportMeta, param);
        Page page = PageHandle.endPage();
        return PageResponse.build(list, page.getTotal());
    }
}
