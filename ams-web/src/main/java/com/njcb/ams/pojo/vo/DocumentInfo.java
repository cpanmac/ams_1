package com.njcb.ams.pojo.vo;

public class DocumentInfo {
    private Integer id;
    private String fileName;
    private String fileUrl;
    private String fileStatus;
    private Integer conductUser;
    private String conductUserName;
    private String conductTime;

    public Integer getId() {
        return id;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public void setFileStatus(String fileStatus) {
        this.fileStatus = fileStatus;
    }

    public void setConductUser(Integer conductUser) {
        this.conductUser = conductUser;
    }

    public void setConductUserName(String conductUserName) {
        this.conductUserName = conductUserName;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }

    public String getFileStatus() {
        return fileStatus;
    }

    public Integer getConductUser() {
        return conductUser;
    }

    public String getConductUserName() {
        return conductUserName;
    }

    public String getConductTime() {
        return conductTime;
    }
}
