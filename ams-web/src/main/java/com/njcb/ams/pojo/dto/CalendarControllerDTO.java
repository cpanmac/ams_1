package com.njcb.ams.pojo.dto;


/**
 * @author: liujw
 * @create: 2021-02-24 15:21
 *
 **/

public class CalendarControllerDTO {

    private String code;

    private String code_desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode_desc() {
        return code_desc;
    }

    public CalendarControllerDTO(String code, String code_desc) {
        this.code = code;
        this.code_desc = code_desc;
    }

    public void setCode_desc(String code_desc) {
        this.code_desc = code_desc;
    }

    @Override
    public String toString() {
        return "CalendarControllerDTO{" +
                "code='" + code + '\'' +
                ", code_desc='" + code_desc + '\'' +
                '}';
    }
}
