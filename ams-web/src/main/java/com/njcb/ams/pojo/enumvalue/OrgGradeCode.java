package com.njcb.ams.pojo.enumvalue;

import com.njcb.ams.support.annotation.DataDic;
import com.njcb.ams.support.codevalue.EnumCode;

/**
 * @author LOONG
 */
@DataDic(dataType = "OrgGrade", dataTypeName = "机构级别")
public enum OrgGradeCode implements EnumCode {

    HEAD_BANK("10", "总行"), BRANCH_BANK("20", "分行"), SUB_BRANCH("30", "支行");

    private String code;
    private String desc;

    OrgGradeCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
