package com.njcb.ams.pojo.vo;

import com.njcb.ams.store.page.Page;

public class DocumentConfigSearchCondition extends Page {
    // 文件名称
    private String fileName;
    // 文件状态： 0-停用 1-启用
    private String status;
    // 操作人姓名
    private String conductUser;
    // 操作开始日期
    private String startDate;
    // 操作结束日期
    private String endDate;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getConductUser() {
        return conductUser;
    }

    public void setConductUser(String conductUser) {
        this.conductUser = conductUser;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
