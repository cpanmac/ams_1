package com.njcb.ams.pojo.dto;

public class TableSpaceUseDTO {
	private String tablespaceName;
	private String totGrootteMb;
	private String useGrootteMb;
	private String usePercentage;
	private String totalBytes;
	private String maxBytes;
	public String getTablespaceName() {
		return tablespaceName;
	}
	public void setTablespaceName(String tablespaceName) {
		this.tablespaceName = tablespaceName;
	}
	public String getTotGrootteMb() {
		return totGrootteMb;
	}
	public void setTotGrootteMb(String totGrootteMb) {
		this.totGrootteMb = totGrootteMb;
	}
	public String getUseGrootteMb() {
		return useGrootteMb;
	}
	public void setUseGrootteMb(String useGrootteMb) {
		this.useGrootteMb = useGrootteMb;
	}
	public String getUsePercentage() {
		return usePercentage;
	}
	public void setUsePercentage(String usePercentage) {
		this.usePercentage = usePercentage;
	}
	public String getTotalBytes() {
		return totalBytes;
	}
	public void setTotalBytes(String totalBytes) {
		this.totalBytes = totalBytes;
	}
	public String getMaxBytes() {
		return maxBytes;
	}
	public void setMaxBytes(String maxBytes) {
		this.maxBytes = maxBytes;
	}

	@Override
	public String toString() {
		return "TableSpaceUseDTO{" +
				"tablespaceName='" + tablespaceName + '\'' +
				", totGrootteMb='" + totGrootteMb + '\'' +
				", useGrootteMb='" + useGrootteMb + '\'' +
				", usePercentage='" + usePercentage + '\'' +
				", totalBytes='" + totalBytes + '\'' +
				", maxBytes='" + maxBytes + '\'' +
				'}';
	}
}
