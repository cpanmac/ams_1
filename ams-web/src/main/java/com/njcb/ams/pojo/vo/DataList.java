package com.njcb.ams.pojo.vo;

import com.njcb.ams.pojo.dto.standard.Response;

import java.util.ArrayList;

public class DataList<T> extends Response {
    private Integer total;
    private ArrayList<T> dataList;

    public DataList(Integer total, ArrayList<T> dataList) {
        this.total = total;
        this.dataList = dataList;
    }

    public DataList() {
        this.dataList = new ArrayList<T>();
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public ArrayList<T> getDataList() {
        return dataList;
    }

    public void setDataList(ArrayList<T> dataList) {
        this.dataList = dataList;
    }
}
