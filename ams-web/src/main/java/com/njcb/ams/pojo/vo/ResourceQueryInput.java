package com.njcb.ams.pojo.vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author LOONG
 */
public class ResourceQueryInput {
    @ApiModelProperty(value = "资源名称")
    private String resourceName;
    @ApiModelProperty(value = "上级资源ID")
    private Integer parentId;
    @ApiModelProperty(value = "资源级别")
    private Integer resourceGrade;
    @ApiModelProperty(value = "资源类型")
    private String resourceType;

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getResourceGrade() {
        return resourceGrade;
    }

    public void setResourceGrade(Integer resourceGrade) {
        this.resourceGrade = resourceGrade;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

}