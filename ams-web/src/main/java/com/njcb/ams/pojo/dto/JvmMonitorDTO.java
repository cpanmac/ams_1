package com.njcb.ams.pojo.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author liuyanlong
 *
 */
public class JvmMonitorDTO{
	private static final long serialVersionUID = 1L;
	/**
	 * 指标类型
	 */
	@ApiModelProperty(value = "指标类型")
	private String jmType;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getJmType() {
		return jmType;
	}

	public void setJmType(String jmType) {
		this.jmType = jmType;
	}

	@Override
	public String toString() {
		return "JvmMonitorDTO{" +
				"jmType='" + jmType + '\'' +
				'}';
	}
}
