package com.njcb.ams.pojo.vo;

import io.swagger.annotations.ApiModelProperty;

public class OrgInfoSaveInput{
	@ApiModelProperty(value = "机构ID")
	private Integer id;

	@ApiModelProperty(value = "机构号")
	private String orgNo;

	@ApiModelProperty(value = "机构名称")
	private String orgName;

	@ApiModelProperty(value = "机构级别")
	private Integer orgGrade;

	@ApiModelProperty(value = "机构状态")
	private String status;

	@ApiModelProperty(value = "上级机构ID")
	private Integer parentId;

	@ApiModelProperty(value = "机构类型")
	private String orgType;

	@ApiModelProperty(value = "核心机构号")
	private String orgAlias;

	@ApiModelProperty(value = "备注")
	private String remark;

	@ApiModelProperty(value = "生效日期")
	private String valideDate;

	@ApiModelProperty(value = "失效日期")
	private String invalideDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Integer getOrgGrade() {
		return orgGrade;
	}

	public void setOrgGrade(Integer orgGrade) {
		this.orgGrade = orgGrade;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	public String getValideDate() {
		return valideDate;
	}

	public void setValideDate(String valideDate) {
		this.valideDate = valideDate;
	}

	public String getInvalideDate() {
		return invalideDate;
	}

	public void setInvalideDate(String invalideDate) {
		this.invalideDate = invalideDate;
	}

	public void setOrgAlias(String orgAlias) {
		this.orgAlias = orgAlias;
	}

	public String getOrgAlias() {
		return orgAlias;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark() {
		return remark;
	}

	@Override
	public String toString() {
		return "[" + orgNo + "]@[" + orgName + "]";
	}
}