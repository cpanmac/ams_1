package com.njcb.ams.pojo.dto;

public class TableSpaceTableUseDTO {
	private String segmentName;
	private String tableSize;
	private String tablespaceName;
	public String getSegmentName() {
		return segmentName;
	}
	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}
	public String getTableSize() {
		return tableSize;
	}
	public void setTableSize(String tableSize) {
		this.tableSize = tableSize;
	}
	public String getTablespaceName() {
		return tablespaceName;
	}
	public void setTablespaceName(String tablespaceName) {
		this.tablespaceName = tablespaceName;
	}

	@Override
	public String toString() {
		return "TableSpaceTableUseDTO{" +
				"segmentName='" + segmentName + '\'' +
				", tableSize='" + tableSize + '\'' +
				", tablespaceName='" + tablespaceName + '\'' +
				'}';
	}
}
