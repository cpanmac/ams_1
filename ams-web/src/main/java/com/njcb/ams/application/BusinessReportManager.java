package com.njcb.ams.application;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.njcb.ams.repository.dao.CommReportMetaDAO;
import com.njcb.ams.repository.entity.CommReportMeta;
import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.service.ReportService;
import com.njcb.ams.support.annotation.Interaction;
import com.njcb.ams.support.annotation.Trader;
import com.njcb.ams.support.annotation.Uncheck;
import com.njcb.ams.support.annotation.enums.TradeType;

/**
 * 系统基础功能。
 * 
 * @author liuyanlong
 *
 */
@Service
@Lazy(false)
@Interaction(groupName = "ZZ.AMS.WEB.REPORT")
public class BusinessReportManager {
	private static final Logger logger = LoggerFactory.getLogger(BusinessReportManager.class);
	@Autowired
	private ReportService reportService;
	@Autowired
	private CommReportMetaDAO commReportMetaDAO;

	@Uncheck
	public static BusinessReportManager getInstance() {
		logger.debug("Instance BusinessManager");
		return AppContext.getBean(BusinessReportManager.class);
	}

	@Trader(tradeCode = "RT1001", tradeName = "灵活报表", tradeType = TradeType.TYPE_QUERY)
	public List<Map<String, Object>> queryReport(CommReportMeta commReportMeta, Map<String, Object> conditionMap) {
		List<Map<String, Object>> retList = reportService.queryForList(commReportMeta,conditionMap);
		return retList;
	}

	@Trader(tradeCode = "RM1001", tradeName = "灵活报表")
	public List<CommReportMeta> reportMetaQuery(CommReportMeta reportMeta) {
		return commReportMetaDAO.selectBySelective(reportMeta);
	}

	@Trader(tradeCode = "RM1002", tradeName = "灵活报表")
	public void reportMetaSave(CommReportMeta reportMeta) {
		commReportMetaDAO.saveOrUpdateByPrimaryKey(reportMeta);
	}

	@Trader(tradeCode = "RM1003", tradeName = "灵活报表")
	public void reportMetaRemove(CommReportMeta reportMeta) {
		commReportMetaDAO.deleteByPrimaryKey(reportMeta.getId());
	}
}
