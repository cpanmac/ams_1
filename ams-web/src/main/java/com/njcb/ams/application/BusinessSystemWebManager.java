package com.njcb.ams.application;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.pojo.dto.Tree;
import com.njcb.ams.repository.entity.*;
import com.njcb.ams.service.AuthROrgResoService;
import com.njcb.ams.service.AuthRRoleResoService;
import com.njcb.ams.service.ResourceManageService;
import com.njcb.ams.service.UserManageService;
import com.njcb.ams.support.annotation.Interaction;
import com.njcb.ams.support.annotation.Trader;
import com.njcb.ams.support.annotation.Uncheck;
import com.njcb.ams.support.annotation.enums.TradeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 系统基础功能
 * 
 * @author liuyanlong
 *
 */
@Service
@Lazy(false)
@Interaction(groupName = "ZZ.AMS.WEB.MANAGE")
public class BusinessSystemWebManager {
	private static final Logger logger = LoggerFactory.getLogger(BusinessSystemManager.class);
	@Autowired
	private ResourceManageService resourceManageService;
	@Autowired
	private UserManageService userManageService;
	@Autowired
	private AuthRRoleResoService authRRoleResoService;
	@Autowired
	private AuthROrgResoService authROrgResoService;

	@Uncheck
	public static BusinessSystemWebManager getInstance() {
		logger.debug("Instance BusinessManager");
		return AppContext.getBean(BusinessSystemWebManager.class);
	}

	@Trader(tradeCode = "UA1003", tradeName = "用户密码重置", tradeGroup = "ZZ.AMS.WEB.User")
	public int resetPwd(String loginName, String newPassword) {
		return userManageService.resetPwd(loginName, newPassword);
	}

	@Trader(tradeCode = "UA1004", tradeName = "解锁用户", tradeGroup = "ZZ.AMS.WEB.User")
	public void unlockUser(String loginName) {
		userManageService.unlockUser(loginName);
	}

	@Trader(tradeCode = "UG1001", tradeName = "用户注册", tradeGroup = "ZZ.AMS.WEB.User")
	public void registerUser(CommUserInfo userInfo) {
		userManageService.registerUser(userInfo);
	}

	@Trader(tradeCode = "UG1002", tradeName = "用户删除", tradeGroup = "ZZ.AMS.WEB.User")
	public void removeUserInfo(CommUserInfo userInfo) {
		userManageService.removeUserInfo(userInfo);
	}

	@Trader(tradeCode = "UG1003", tradeName = "用户信息查询", tradeGroup = "ZZ.AMS.WEB.User")
	public List<CommUserInfo> queryUser(CommUserInfo userInfo) {
		return userManageService.queryUser(userInfo);
	}

	@Trader(tradeCode = "UG1004", tradeName = "用户信息修改", tradeGroup = "ZZ.AMS.WEB.User")
	public void saveUserInfo(CommUserInfo userInfo) {
		userManageService.saveUserInfo(userInfo);
	}

	@Trader(tradeCode = "UG2001", tradeName = "踢出在线用户", tradeGroup = "ZZ.AMS.WEB.User")
	public void kickOutOnlineUser(String loginName) {
		userManageService.kickOutOnlineUser(loginName);
	}

	@Trader(tradeCode = "UG2002", tradeName = "清除用户会话", tradeGroup = "ZZ.AMS.WEB.User")
	public void clearUserSession(String loginName) {
		userManageService.clearUserSession(loginName);
	}

	@Trader(tradeCode = "RS1001", tradeName = "功能权限查询", tradeGroup = "ZZ.AMS.WEB.Auth", tradeType = TradeType.TYPE_QUERY)
	public List<ResourceInfo> queryMenu(String resourceType) {
		return resourceManageService.queryMenu(resourceType);
	}

	@Trader(tradeCode = "RS1002", tradeName = "用户菜单查询", tradeGroup = "ZZ.AMS.WEB.Auth", tradeType = TradeType.TYPE_QUERY) // 根据用户Id查询权限内的菜单
	public List<ResourceInfo> queryResoMenuByUser(String resourceType) {
		return resourceManageService.queryResoMenuByUser(resourceType);
	}

	@Trader(tradeCode = "AO1001", tradeName = "角色权限查询", tradeGroup = "ZZ.AMS.WEB.Auth", tradeType = TradeType.TYPE_QUERY)
	public List<Tree> roleResoQuery(int roleID) {
		return authRRoleResoService.roleResoQuery(roleID);
	}

	@Trader(tradeCode = "AO1002", tradeName = "角色权限保存", tradeGroup = "ZZ.AMS.WEB.Auth")
	public void roleResoSave(List<AuthRRoleReso> insertNodes, List<AuthRRoleReso> deleteNodes) {
		authRRoleResoService.roleResoSave(insertNodes, deleteNodes);
	}

	@Trader(tradeCode = "AO2001", tradeName = "机构权限查询", tradeGroup = "ZZ.AMS.WEB.Auth", tradeType = TradeType.TYPE_QUERY) // 机构权限查询
	public List<Tree> orgResoQuery(int orgId) {
		return authROrgResoService.orgResoQuery(orgId);
	}

	@Trader(tradeCode = "AO2002", tradeName = "机构权限保存", tradeGroup = "ZZ.AMS.WEB.Auth") // 机构权限保存
	public void orgResoSave(List<AuthROrgReso> insertNodes, List<AuthROrgReso> deleteNodes) {
		authROrgResoService.orgResoSave(insertNodes, deleteNodes);
	}

	@Trader(tradeCode = "AO3001", tradeName = "用户角色查询", tradeGroup = "ZZ.AMS.WEB.Auth", tradeType = TradeType.TYPE_QUERY)
	public List<Tree> userRoleQuery(int userId) {
		return authRRoleResoService.userRoleQuery(userId);
	}

	@Trader(tradeCode = "AO3002", tradeName = "用户角色保存", tradeGroup = "ZZ.AMS.WEB.Auth")
	public void userRoleSave(List<AuthRUserRole> insertNodes, List<AuthRUserRole> deleteNodes) {
		authRRoleResoService.userRoleSave(insertNodes, deleteNodes);
	}

}
