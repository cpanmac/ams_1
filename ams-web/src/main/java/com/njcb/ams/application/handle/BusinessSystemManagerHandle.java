package com.njcb.ams.application.handle;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.njcb.ams.repository.entity.ResourceInfo;
import com.njcb.ams.repository.entity.SysTradeLog;
import com.njcb.ams.service.ResourceManageService;
import com.njcb.ams.support.annotation.HandleService;
import com.njcb.ams.support.annotation.Trader;

/**
 * 
 * @author liuyanlong
 *         挡板服务
 * 
 */
@Service
@Lazy(false)
@HandleService
public class BusinessSystemManagerHandle {
	@Autowired
	private ResourceManageService resourceManageService;

	@Trader(tradeCode = "CH1001", tradeName = "交易量Top")
	public List<SysTradeLog> queryCountOfBusinessTop(SysTradeLog sysTradeLog) {
		return new ArrayList<SysTradeLog>();
	}
	
	@Trader(tradeCode = "CH1002", tradeName = "交易耗时Top")
	public List<SysTradeLog> queryTimeOfBusinessTop(SysTradeLog sysTradeLog) {
		return new ArrayList<SysTradeLog>();
	}
	
	/**
	 * 挡板服务用作给所有用户开放全部菜单权限
	 * @param resourceType
	 * @param userId
	 * @return
	 */
	@Trader(tradeCode = "RS1002", tradeName = "用户菜单查询")
	public List<ResourceInfo> queryResoMenuByUser(String resourceType) {
		return resourceManageService.queryMenu(resourceType);
	}

}
