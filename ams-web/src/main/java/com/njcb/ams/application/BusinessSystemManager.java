package com.njcb.ams.application;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.pojo.dto.CacheMonitorDTO;
import com.njcb.ams.pojo.dto.TableSpaceTableUseDTO;
import com.njcb.ams.pojo.dto.TableSpaceUseDTO;
import com.njcb.ams.pojo.dto.Tree;
import com.njcb.ams.repository.dao.SysInfoDAO;
import com.njcb.ams.repository.dao.SysTradeLogDAO;
import com.njcb.ams.repository.entity.*;
import com.njcb.ams.service.*;
import com.njcb.ams.support.annotation.Interaction;
import com.njcb.ams.support.annotation.Trader;
import com.njcb.ams.support.annotation.Uncheck;
import com.njcb.ams.support.annotation.enums.TradeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 系统基础功能。
 * 
 * @author liuyanlong
 *
 */
@Service
@Lazy(false)
@Interaction(groupName = "ZZ.AMS.WEB.SYSTEM")
public class BusinessSystemManager {
	private static final Logger logger = LoggerFactory.getLogger(BusinessSystemManager.class);
	@Autowired
	private CacheMonitorService cacheMonitorService;
	@Autowired
	private MonitorManageService monitorManageService;
	@Autowired
	private OrgInfoService orgInfoService;
	@Autowired
	private ParamManageService paramManageService;
	@Autowired
	private RoleInfoService roleInfoService;
	@Autowired
	private SysInfoDAO sysInfoDAO;
	@Autowired
	private SysTradeLogDAO sysTradeLogDAO;
	@Autowired
	private DataDicService dataDicService;

	@Uncheck
	public static BusinessSystemManager getInstance() {
		logger.debug("Instance BusinessManager");
		return AppContext.getBean(BusinessSystemManager.class);
	}

	@Trader(tradeCode = "RI1001", tradeName = "角色信息查询", tradeType = TradeType.TYPE_QUERY)
	public List<CommRoleInfo> queryRoleInfo(CommRoleInfo roleInfo) {
		return roleInfoService.queryRoleInfo(roleInfo);
	}

	@Trader(tradeCode = "RI1002", tradeName = "角色信息保存")
	public void saveRoleInfo(CommRoleInfo roleInfo) {
		if(null == roleInfo.getId()){
			roleInfoService.addRoleInfo(roleInfo);
		}else{
			roleInfoService.modifyRoleInfo(roleInfo);
		}
	}

	@Trader(tradeCode = "RI1003", tradeName = "角色信息删除")
	public void removeRoleInfo(CommRoleInfo roleInfo) {
		roleInfoService.removeRoleInfo(roleInfo);
	}

	@Trader(tradeCode = "OG1001", tradeName = "机构查询", tradeType = TradeType.TYPE_QUERY)
	public List<CommOrgInfo> orgInfoQuery(CommOrgInfo orgInfo) {
		return orgInfoService.orgInfoQuery(orgInfo);
	}

	@Trader(tradeCode = "OG1002", tradeName = "机构保存")
	public int saveOrgInfo(CommOrgInfo orgInfo) {
		int ret = orgInfoService.saveOrgInfo(orgInfo);
		dataDicService.reloadDataDicByType("ORG");
		dataDicService.reloadDataDicByType("DIM");
		return ret;
	}

	@Trader(tradeCode = "OG1003", tradeName = "机构删除")
	public int deleteOrgInfo(String orgNo) {
		int ret = orgInfoService.deleteOrgInfo(orgNo);
		dataDicService.reloadDataDicByType("ORG");
		dataDicService.reloadDataDicByType("DIM");
		return ret;
	}
	
	@Trader(tradeCode = "OG1004", tradeName = "机构树查询", tradeType = TradeType.TYPE_QUERY) // 机构树查询
	public List<Tree> orgQuery(String orgNo) {
		return orgInfoService.orgQueryTree(orgNo);
	}

	@Trader(tradeCode = "QM1002", tradeName = "缓存监控查询", tradeType = TradeType.TYPE_QUERY)
	public List<CacheMonitorDTO> cacheMonitorQuery(CacheMonitorDTO inBean) {
		return cacheMonitorService.getAllCache(inBean);
	}

	@Trader(tradeCode = "QM1003", tradeName = "缓存清除")
	public List<CacheMonitorDTO> cacheRemove(CacheMonitorDTO inBean) {
		return cacheMonitorService.cacheRemove(inBean);
	}
	
	@Trader(tradeCode = "QM2001", tradeName = "表空间使用情况")
	public List<TableSpaceUseDTO> getTableSpaceUse() {
		return monitorManageService.getTableSpaceUse();
	}
	
	@Trader(tradeCode = "QM2002", tradeName = "数据表空间占用情况")
	public List<TableSpaceTableUseDTO> getTableSpaceTableUse() {
		return monitorManageService.getTableSpaceTableUse();
	}

	@Trader(tradeCode = "SP1001", tradeName = "系统参数查询", tradeType = TradeType.TYPE_QUERY)
	public List<SysParam> queryParam(SysParam sysParam) {
		return paramManageService.queryParam(sysParam);
	}

	@Trader(tradeCode = "SP1002", tradeName = "系统参数修改")
	public void updateParam(List<SysParam> insertList, List<SysParam> deleteList, List<SysParam> updateList) {
		paramManageService.updateParam(insertList, deleteList, updateList);
	}

	@Trader(tradeCode = "SP1003", tradeName = "参数保存")
	public int saveSysParam(SysParam sysParam) {
		int ret = paramManageService.saveSysParam(sysParam);
		return ret;
	}

	@Trader(tradeCode = "SP1004", tradeName = "参数删除")
	public int deleteSysParam(String name) {
		int ret = paramManageService.deleteSysParam(name);
		return ret;
	}

	@Trader(tradeCode = "ST1001", tradeName = "系统信息查询", tradeType = TradeType.TYPE_QUERY)
	public SysInfo sysInfoQuery(SysInfo inBean) {
		return sysInfoDAO.selectSysInfo();
	}

	@Trader(tradeCode = "ST1002", tradeName = "系统信息修改")
	public Integer sysInfoSave(SysInfo sysInfo) {
		return sysInfoDAO.updateByPrimaryKeySelective(sysInfo);
	}
	
	@Trader(tradeCode = "CH1001", tradeName = "交易量Top")
	public List<SysTradeLog> queryCountOfBusinessTop(SysTradeLog sysTradeLog) {
		return sysTradeLogDAO.getMapper().queryCountOfBusinessTop(sysTradeLog);
	}
	
	@Trader(tradeCode = "CH1002", tradeName = "交易耗时Top")
	public List<SysTradeLog> queryTimeOfBusinessTop(SysTradeLog sysTradeLog) {
		return sysTradeLogDAO.getMapper().queryTimeOfBusinessTop(sysTradeLog);
	}
	
	@Trader(tradeCode = "CH1004", tradeName = "交易成功率Top")
	public List<SysTradeLog> querySuccRateOfBusinessTop(SysTradeLog sysTradeLog) {
		return sysTradeLogDAO.getMapper().querySuccRateOfBusinessTop(sysTradeLog);
	}


}
