/**
 * 描述：
 * Copyright：Copyright （c） 2017
 * Company：南京银行
 *
 * @author njcbkf045
 * @version 1.0 2017年3月6日
 * @see history
 * 2017年3月6日
 */

package com.njcb.ams.service;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.repository.dao.CommRoleInfoDAO;
import com.njcb.ams.repository.entity.CommRoleInfo;
import com.njcb.ams.support.exception.ExceptionUtil;
import com.njcb.ams.util.AmsAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lisr
 *
 *         类功能描述：
 *
 */

@Service
public class RoleInfoService {

    @Autowired
    private CommRoleInfoDAO roleInfoDAO;

    public static RoleInfoService getInstance() {
        return AppContext.getBean(RoleInfoService.class);
    }

    public List<CommRoleInfo> queryRoleInfo(CommRoleInfo roleInfo) {
        List<CommRoleInfo> paramList = roleInfoDAO.selectBySelective(roleInfo);
        return paramList;
    }

    public void addRoleInfo(CommRoleInfo roleInfo) {
        AmsAssert.notNull(roleInfo.getRoleCode(), roleInfo.getRoleName(), roleInfo.getStatus(), "角色代码、角色名称、角色状态都不能为空");
        CommRoleInfo orgRoleInfo = roleInfoDAO.selectByRoleCode(roleInfo.getRoleCode());
        AmsAssert.isNull(orgRoleInfo, "角色代码[" + roleInfo.getRoleCode() + "]已存在");
        roleInfoDAO.insert(roleInfo);
    }

    public void modifyRoleInfo(CommRoleInfo roleInfo) {
        AmsAssert.notNull(roleInfo.getRoleCode(), roleInfo.getRoleName(), roleInfo.getStatus(), "角色代码、角色名称、角色状态都不能为空");
        CommRoleInfo orgRoleInfo = roleInfoDAO.selectByPrimaryKey(roleInfo.getId());
        //如何修改了角色代码
        if(!roleInfo.getRoleCode().equals(orgRoleInfo.getRoleCode())){
            orgRoleInfo = roleInfoDAO.selectByRoleCode(roleInfo.getRoleCode());
            AmsAssert.isNull(orgRoleInfo, "角色代码[" + roleInfo.getRoleCode() + "]已存在");
        }
        roleInfoDAO.updateByPrimaryKey(roleInfo);
    }

    public void removeRoleInfo(CommRoleInfo roleInfo) {
        Integer succCount = roleInfoDAO.deleteByPrimaryKey(roleInfo.getId());
        if(0 == succCount){
            ExceptionUtil.throwAppException("没有删除成功的数据，请确认结果");
        }
    }
}
