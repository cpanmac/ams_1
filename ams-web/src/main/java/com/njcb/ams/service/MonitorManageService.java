/**
 * 描述：
 * Copyright：Copyright （c） 2017
 * Company：南京银行
 * @author xxjsb036 
 * @version 1.0 2017年4月8日
 * @see history
 * 2017年4月8日
 */

package com.njcb.ams.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.njcb.ams.repository.dao.TableSpaceMonitoringDAO;
import com.njcb.ams.pojo.dto.TableSpaceTableUseDTO;
import com.njcb.ams.pojo.dto.TableSpaceUseDTO;
import com.njcb.ams.factory.domain.AppContext;

/**
 * @author liuyanlong
 *
 *         类功能描述：
 *
 */
@Service
public class MonitorManageService {

	@Autowired
	private TableSpaceMonitoringDAO tableSpaceMonitoringDAO;

	public static MonitorManageService getInstance() {
		return AppContext.getBean(MonitorManageService.class);
	}

	
	public List<TableSpaceTableUseDTO> getTableSpaceTableUse() {
		return tableSpaceMonitoringDAO.getTableSpaceTableUse();
	}
	
	
	public List<TableSpaceUseDTO> getTableSpaceUse() {
		return tableSpaceMonitoringDAO.getTableSpaceUse();
	}
	
}
