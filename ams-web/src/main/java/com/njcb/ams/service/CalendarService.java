package com.njcb.ams.service;

import com.njcb.ams.repository.dao.mapper.CommCalendarMapper;
import com.njcb.ams.repository.entity.CommCalendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author liujw2
 */
@Service
public class CalendarService {

    @Autowired
    private CommCalendarMapper mapper;


    /**
     * 根据年份批量初始化日历
     *
     * @param year
     * @param sysId
     */
    @Transactional
    public void initCalendar(String sysId, String year) {

        List<CommCalendar> selectOne;
        Map<String, String> pDto = new HashMap<>();
        String yearStr = year + "0101";
        if ("".equals(sysId)) {
            sysId = "CHN";
        }
        pDto.put("data_str", yearStr);
        pDto.put("sys_id", sysId);
        pDto.put("data_year", year);
        selectOne = mapper.selectOne(pDto);
        if ("BFIX".equals(sysId)) {
            if (selectOne.size() == 0) {//未查询到数据时新增整年记录
                Calendar calendar = new GregorianCalendar();//年初
                Calendar calendarEnd = new GregorianCalendar();//年末
                calendar.set(Calendar.YEAR, Integer.parseInt(year));
                calendar.set(Calendar.MONTH, 0);
                calendar.set(Calendar.DAY_OF_MONTH, 1);//设置年初的日期为1月1日
                calendarEnd.set(Calendar.YEAR, Integer.parseInt(year));
                calendarEnd.set(Calendar.MONTH, 11);
                calendarEnd.set(Calendar.DAY_OF_MONTH, 31);//设置年末的日期为12月31日

                while (calendar.getTime().getTime() <= calendarEnd.getTime().getTime()) {
                    SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd", Locale.CHINA);
                    String format2 = format.format(calendar.getTime());
                    CommCalendar sbmSysCalendar = new CommCalendar();
                    //
                    sbmSysCalendar.setSysId(sysId);

                    sbmSysCalendar.setDataType("0");//data_type: 0-工作日 1-节假日

                    sbmSysCalendar.setDataYear(year);
                    sbmSysCalendar.setDataStr(format2);
                    mapper.insert(sbmSysCalendar);
                    calendar.add(Calendar.DAY_OF_MONTH, 1);//日期+1
                }

            }
        } else {
            if (selectOne.size() == 0) {//未查询到数据时新增整年记录
                Calendar calendar = new GregorianCalendar();//年初
                Calendar calendarEnd = new GregorianCalendar();//年末
                calendar.set(Calendar.YEAR, Integer.parseInt(year));
                calendar.set(Calendar.MONTH, 0);
                calendar.set(Calendar.DAY_OF_MONTH, 1);//设置年初的日期为1月1日
                calendarEnd.set(Calendar.YEAR, Integer.parseInt(year));
                calendarEnd.set(Calendar.MONTH, 11);
                calendarEnd.set(Calendar.DAY_OF_MONTH, 31);//设置年末的日期为12月31日

                while (calendar.getTime().getTime() <= calendarEnd.getTime().getTime()) {
                    SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd", Locale.CHINA);
                    String format2 = format.format(calendar.getTime());
                    CommCalendar sbmSysCalendar = new CommCalendar();
                    sbmSysCalendar.setSysId(sysId);
                    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
                            || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                        sbmSysCalendar.setDataType("1");//data_type: 0-工作日 1-节假日
                    } else {
                        sbmSysCalendar.setDataType("0");//data_type: 0-工作日 1-节假日
                    }
                    sbmSysCalendar.setDataYear(year);
                    sbmSysCalendar.setDataStr(format2);
                    mapper.insert(sbmSysCalendar);
                    calendar.add(Calendar.DAY_OF_MONTH, 1);//日期+1
                }

            }
        }


    }


    public int getSuppDays(String year) {
        int days = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(year + "0101"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int week = c.get(Calendar.DAY_OF_WEEK) - 1;
        if (week == 5 || week == 6) {
            days = week;
        } else {
            days = 7 + week;
        }
        return days;
    }


    /**
     * @Param: 日期 dataStr
     * @return: List<String>
     * @Author；liujw2
     * @Date: 2021/2/25
     * @Time: 15:10
     */
    public List<String> checkHoliday(String dataStr) {

        Map<String, String> map = new HashMap<>();
        map.put("dataStr", dataStr);
        List<String> list = mapper.checkHoliday(map);

        //如果List大于0,则是假日，List中存放假期对应的假期类型
        return list;
    }

    /**
     * @Param: dateStr, term, flag, sysIdList
     * @return: 实际到期日
     * @Author；liujw2
     * @Date: 2021/2/25
     * @Time: 17:10
     */
    public String getTrueMatuDt(String dateStr, String term, boolean flag, List<String> sysIdList) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String year = dateStr.substring(0, 4);
        String month = dateStr.substring(4, 6);
        String day = dateStr.substring(6);
        calendar.set(Calendar.YEAR, Integer.parseInt(year));
        calendar.set(Calendar.MONTH, Integer.parseInt(month) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day));
        String date = "";
        List<String> list = new ArrayList<>();
        boolean typeFlag = true; //循环开关
        String date3 = "";
        if (flag == false) {
            //不顺延
            calendar.add(Calendar.DAY_OF_MONTH, Integer.parseInt(term));
            date3 = sdf.format(calendar.getTime());
        } else {
            //顺延
            calendar.add(Calendar.DAY_OF_MONTH, Integer.parseInt(term));
            date = sdf.format(calendar.getTime());
            Map<String, String> map = new HashMap<>();
            map.put("data_str", date);
            list = mapper.selectSysId(map); //当list.size()为0，说明是共同的工作日
            while (list.size() != 0) {
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                String date2 = sdf.format(calendar.getTime());

                Map<String, String> map2 = new HashMap<>();
                map2.put("data_str", date2);
                list = mapper.selectSysId(map2);
            }

            date3 = sdf.format(calendar.getTime());


        }
        return date3;

    }

    /**
     * @Param: dateStr 到期日
     * @return: Map
     * @Author；liujw2
     * @Date: 2021/2/25
     * @Time: 17:09
     */
    public Map<String, String> getDingPanRi(String dateStr) {
        Map<String, String> map = new HashMap<>();
        int i = 1;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String year = dateStr.substring(0, 4);
        String month = dateStr.substring(4, 6);
        String day = dateStr.substring(6);
        calendar.set(Calendar.YEAR, Integer.parseInt(year));
        calendar.set(Calendar.MONTH, Integer.parseInt(month) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day));

        while (i < 3) {
            calendar.add(Calendar.DAY_OF_MONTH, -1);
            if (!(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
                    || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)) {
                i++;
            }
        }

        String dateStr2 = sdf.format(calendar.getTime());
        String msg = "";
        Map<String, String> map2 = new HashMap<>();
        map2.put("data_str", dateStr2);
        List<String> list = mapper.selectSysId(map2);
        if (list.size() > 0) {
            for (int j = 0; i < list.size(); i++) {
                msg += list.get(j) + "|";
            }
        }
        map.put("dateStr", dateStr2);
        map.put("msg", msg);
        return map;
    }


}