package com.njcb.ams.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.njcb.ams.repository.dao.AuthRUserRoleDAO;
import com.njcb.ams.repository.dao.CommOrgInfoDAO;
import com.njcb.ams.repository.dao.CommRoleInfoDAO;
import com.njcb.ams.repository.entity.CommOrgInfo;
import com.njcb.ams.repository.entity.CommRoleInfo;
import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.factory.comm.DataBus;
import com.njcb.ams.support.exception.ExceptionCode;
import com.njcb.ams.support.exception.ExceptionUtil;

/**
 * 权限控制服务 业务条线权限及机构归属权限
 * 
 * @author liuyanlong
 *
 */
@Service
public class BusinessAuthorityService {

	@Autowired
	private CommRoleInfoDAO roleInfoDAO;
	@Autowired
	private CommOrgInfoDAO orgInfoDAO;
	@Autowired
	private AuthRUserRoleDAO authRUserRoleDAO;


	/**
	 * 判断操作用户有无权限操作此条线数据
	 */
	public boolean isRoleOfBusinessLine(String line) {
		boolean isHave = false;
		boolean isAdmin = false;
		Integer userId = DataBus.getUserId();
		List<Integer> roleIdList = authRUserRoleDAO.findByRoleIdByUserId(userId);
		List<String> roleList = new ArrayList<String>();
		for (Integer roleId : roleIdList) {
			CommRoleInfo roleInfo = roleInfoDAO.selectByPrimaryKey(roleId);
			roleList.add(getBusinessLine(roleInfo));
			// 如果无管理员权限，则继续尝试获取管理员权限
			if (!isAdmin) {
				isAdmin = isHaveAdminOfBusinessLine(roleInfo);
			}
		}
		isHave = roleList.contains(line);
		if (!isHave) {
			return isAdmin;
		}
		return isHave;
	}

	/**
	 * 判断操作用户有无权限操作此条线数据
	 */
	public boolean isAdminRole() {
		boolean isAdmin = false;
		Integer userId = DataBus.getUserId();
		List<Integer> roleIdList = authRUserRoleDAO.findByRoleIdByUserId(userId);
		for (Integer roleId : roleIdList) {
			CommRoleInfo roleInfo = roleInfoDAO.selectByPrimaryKey(roleId);
			// 如果无管理员全是，则继续尝试获取管理员权限
			if (!isAdmin) {
				isAdmin = isHaveAdminOfBusinessLine(roleInfo);
			}
		}
		return isAdmin;
	}

	/**
	 * 获取条线权限
	 */
	public void tryBusinessLineAuth(String line) {
		if (!isRoleOfBusinessLine(line)) {
			ExceptionUtil.throwAppException("用户无此权限", ExceptionCode.AUTH_INSUFFLCIENT);
		}
	}

	/**
	 * 获取管理员权限
	 */
	public void tryAdminAuth(String line) {
		if (!isRoleOfBusinessLine(line)) {
			ExceptionUtil.throwAppException("无管理员权限", ExceptionCode.AUTH_INSUFFLCIENT);
		}
	}

	/**
	 * 判断用户是否有管理员权限，暂时写死判断.以后考虑用权限代码 authcode
	 */
	private boolean isHaveAdminOfBusinessLine(CommRoleInfo roleInfo) {
		// 系统管理员 和 限额管理岗
		if (SysBaseDefine.STATUS_VALID.equals(roleInfo.getStatus())) {
			if (roleInfo.getId() == 1 || roleInfo.getId() == 107) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断吃角色的业务条线
	 * 
	 * @param roleInfo
	 * @return
	 */
	public String getBusinessLine(CommRoleInfo roleInfo) {
//		if (roleInfo.getRoleName().contains("非小企业条线")) {
//			return SysBaseDefine.LINE_COMP_0301;
//		} else if (roleInfo.getRoleName().contains("小企业条线")) {
//			return SysBaseDefine.LINE_SMCM_0302;
//		} else if (roleInfo.getRoleName().contains("个人条线")) {
//			return SysBaseDefine.LINE_PERS_0303;
//		}
		return "NONE";
	}

	/**
	 * 获取本机构及所属机构
	 */
	public List<Integer> selectSelfAndSubById() {
		List<Integer> rtList = new ArrayList<Integer>();
		if (null != DataBus.getUncheckInstance()) {
			CommOrgInfo orgInfo = orgInfoDAO.selectByOrgNo(DataBus.getOrgNo());
			rtList = orgInfoDAO.selectSelfAndSubById(orgInfo.getId());
		}
		return rtList;
	}

	/**
	 * 获取本机构及所属机构
	 */
	public List<CommOrgInfo> getSelfAndOwnerOrg() {
		CommOrgInfo orgInfo = orgInfoDAO.selectByOrgNo(DataBus.getOrgNo());
		List<CommOrgInfo> rtList = getOwnerOrg(orgInfo.getId());
		rtList.add(orgInfo);
		return rtList;
	}

	public List<CommOrgInfo> getOwnerOrg(Integer orgId) {
		List<CommOrgInfo> rtList = new ArrayList<CommOrgInfo>();
		List<CommOrgInfo> temp = orgInfoDAO.selectByParentId(orgId);
		rtList.addAll(temp);
		if (null != temp && temp.size() > 0) {
			temp.forEach(orgInfo -> {
				rtList.addAll(getOwnerOrg(orgInfo.getId()));
			});

		}
		return rtList;
	}

}
