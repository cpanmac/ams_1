/**
 * 描述：
 * Copyright：Copyright （c） 2017
 * Company：南京银行
 * 
 * @author xxjsb036
 * @version 1.0 2017年3月13日
 * @see history
 *      2017年3月13日
 */

package com.njcb.ams.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import com.njcb.ams.pojo.dto.CacheMonitorDTO;
import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.store.page.Page;
import com.njcb.ams.store.page.PageHandle;
import com.thoughtworks.xstream.XStream;

import net.sf.ehcache.Element;

/**
 * @author 刘彦龙
 *
 *         类功能描述：缓存监控
 *
 */
@Service
public class CacheMonitorService {

	public static CacheMonitorService getInstance() {
		return AppContext.getBean(CacheMonitorService.class);
	}

	public Collection<String> getCacheNames() {
		CacheManager cacheManager = (CacheManager) AppContext.getBean("cacheManager");
		return cacheManager.getCacheNames();
	}

	@SuppressWarnings("unchecked")
	private List<CacheMonitorDTO> getCache(String cacheName, String cacheKey) {
		List<CacheMonitorDTO> rtlist = new ArrayList<CacheMonitorDTO>();
		XStream xs = new XStream();
		CacheManager cacheManager = (CacheManager) AppContext.getBean("cacheManager");
		Cache cache = cacheManager.getCache(cacheName);
		Object obj = cache.getNativeCache();
		if (obj instanceof net.sf.ehcache.Cache) {
			net.sf.ehcache.Cache natCache = (net.sf.ehcache.Cache) obj;
			List<String> keyList = new ArrayList<String>();
			if (StringUtils.isEmpty(cacheKey)) {
				keyList = natCache.getKeys();
			} else {
				keyList.add(cacheKey);
			}
			for (Object key : keyList) {
				Element ele = natCache.get(key);
				if (null == ele) {
					continue;
				}
				Object value = ele.getObjectValue();
				if (value instanceof ArrayList) {
					List<Object> valList = (List<Object>) value;
					for (Object val : valList) {
						String cacheVal = xs.toXML(val);
						CacheMonitorDTO bean = new CacheMonitorDTO();
						bean.setCacheName(cacheName);
						bean.setCacheKey(key.toString());
						bean.setCacheValType(value.getClass().getSimpleName());
						bean.setCacheValue(cacheVal);
						rtlist.add(bean);
					}
				} else {
					String cacheVal = xs.toXML(value);
					CacheMonitorDTO bean = new CacheMonitorDTO();
					bean.setCacheName(cacheName);
					bean.setCacheKey(key.toString());
					if (value != null) {
						bean.setCacheValType(value.getClass().getSimpleName());
					}
					bean.setCacheValue(cacheVal);
					rtlist.add(bean);
				}
			}
		}
		return rtlist;
	}

	public List<CacheMonitorDTO> getAllCache(CacheMonitorDTO inBean) {
		Page page = PageHandle.getPlatformPage();
		List<CacheMonitorDTO> rtlist = new ArrayList<CacheMonitorDTO>();
		CacheManager cacheManager = (CacheManager) AppContext.getBean("cacheManager");
		if (StringUtils.isEmpty(inBean.getCacheName())) {
			Collection<String> ct = cacheManager.getCacheNames();
			for (String cacheName : ct) {
				rtlist.addAll(getCache(cacheName, null));
			}
		} else {
			if (StringUtils.isEmpty(inBean.getCacheKey())) {
				rtlist.addAll(getCache(inBean.getCacheName(), null));
			} else {
				rtlist.addAll(getCache(inBean.getCacheName(), inBean.getCacheKey()));
			}

		}
		if(null != page){
			page.setTotal(rtlist.size());
			int endRow = page.getEndRow() > rtlist.size() ? rtlist.size() : page.getEndRow();
			List<CacheMonitorDTO> pageList = rtlist.subList(page.getStartRow(), endRow);
			page.setResult(pageList);
			return pageList;
		}
		return rtlist;
	}

	/**
	 * 清除缓存
	 * 方法功能描述：
	 * 
	 * @param inBean
	 * @return
	 */
	public List<CacheMonitorDTO> cacheRemove(CacheMonitorDTO inBean) {
		CacheManager cacheManager = (CacheManager) AppContext.getBean("cacheManager");
		List<CacheMonitorDTO> rtlist = new ArrayList<CacheMonitorDTO>();
		Collection<String> cln = getCacheNames();
		for (String cacheName : cln) {
			Cache cache = cacheManager.getCache(cacheName);
			cache.evict(inBean.getCacheKey());
		}
		return rtlist;
	}
}
