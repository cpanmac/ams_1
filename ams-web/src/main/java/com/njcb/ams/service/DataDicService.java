package com.njcb.ams.service;

import com.njcb.ams.assembler.DataDicConvertor;
import com.njcb.ams.pojo.bo.DataDicConfig;
import com.njcb.ams.repository.dao.CommDataDicDAO;
import com.njcb.ams.repository.entity.CommDataDic;
import com.njcb.ams.repository.entity.CommDataDicExample;
import com.njcb.ams.support.config.AmsConfigUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 数据字段管理类
 *
 * @author LIUYANLONG
 */
@Service
public class DataDicService {
    private static final Logger logger = LoggerFactory.getLogger(DataDicService.class);

    @Autowired
    private CommDataDicDAO dataDicDAO;
    @Autowired
    private DataDicConvertor dataDicConvertor;

    private static Map<String, Map<String, String>> dataDicMap = new HashMap<String, Map<String, String>>();

    @Async
    public void init() {
        writeDataDicConfig();
        reloadDataDic();
    }

    private void writeDataDicConfig() {
        Map<String, Map<String, DataDicConfig>> dataDicMap = AmsConfigUtil.getDataDicMap();
        dataDicMap.forEach((key, configMap) -> {
            List<CommDataDic> dataDicList = new ArrayList<>();
            configMap.forEach((dataNo, tmpConfig) -> {
                dataDicList.add(dataDicConvertor.dtoToEntity(tmpConfig));
            });
            CommDataDicExample example = new CommDataDicExample();
            example.createCriteria().andDataTypeEqualTo(key);
            dataDicDAO.deleteByExample(example);
            dataDicDAO.insertBatch(dataDicList);
            dataDicList.clear();
        });
    }

    public void reloadDataDic() {
        logger.info("开始读取数据字典信息表");
        List<CommDataDic> list = dataDicDAO.findAll();
        Collections.sort(list, new Comparator<CommDataDic>() {
            @Override
            public int compare(CommDataDic o1, CommDataDic o2) {
                if (o1.getDataType().compareTo(o2.getDataType()) > 0) {
                    if (null == o1 || null == o1.getDataType()) {
                        return -1;
                    }
                    return 1;
                } else if (o1.getDataType().equals(o2.getDataType())) {
                    if (null == o1.getDataName()) {
                        o1.setDataName(o1.getDataNo());
                        if (null == o1.getDataName()) {
                            o1.setDataNo(o1.getId().toString());
                            o1.setDataName(o1.getId().toString());
                        }
                    }
                    if (null == o2.getDataName()) {
                        o2.setDataName(o2.getDataNo());
                        if (null == o2.getDataName()) {
                            o2.setDataNo(o2.getId().toString());
                            o2.setDataName(o2.getId().toString());
                        }
                    }
                    return o1.getDataName().compareTo(o2.getDataName());
                } else {
                    return -1;
                }

            }
        });
        Map<String, Map<String, String>> subMap = new HashMap<String, Map<String, String>>();
        Map<String, String> subSubMap = null;
        String dataType = null;
        for (int i = 0; i < list.size(); i++) {
            CommDataDic dataDic = (CommDataDic) list.get(i);
            dataType = dataDic.getDataType();
            String dataNo = dataDic.getDataNo();
            if (StringUtils.isEmpty(dataNo)) {
                continue;
            }
            String dataName = dataDic.getDataName();
            if (null == dataName) {
                dataName = dataDic.getDataNo();
            }
            if (subMap.containsKey(dataType)) {
                subSubMap = subMap.get(dataType);
                subSubMap.put(dataNo, dataName);
                subMap.put(dataType, subSubMap);
            } else {
                subSubMap = new HashMap<String, String>();
                subSubMap.put(dataNo, dataName);
                subMap.put(dataType, subSubMap);
            }
        }
        if (null != subSubMap && subSubMap.size() > 0) {
            subMap.put(dataType, subSubMap);
        }
        dataDicMap = subMap;
        logger.info("完成读取数据字典信息表SIZE=" + list.size());
    }

    /**
     * 指定字典类型刷新
     *
     * @param dataType
     */
    public void reloadDataDicByType(String dataType) {
        logger.info("刷新数据字典信息表" + dataType);
        List<CommDataDic> list = dataDicDAO.findDataNameByType(dataType);
        Map<String, String> subSubMap = new HashMap<String, String>();
        for (CommDataDic dataDic : list) {
            String dataNo = dataDic.getDataNo();
            String dataName = dataDic.getDataName();
            subSubMap.put(dataNo, dataName);
        }
        if (null != subSubMap) {
            dataDicMap.put(dataType, subSubMap);
        }
    }

    /**
     * 获取此类型数据
     *
     * @param dataType
     * @return
     */
    public static Map<String, String> getDataType(String dataType) {
        return dataDicMap.get(dataType);
    }

    /**
     * 获取数据项值
     *
     * @param dataType
     * @param dataNo
     * @return
     */
    public static String getDataNameByNo(String dataType, String dataNo) {
        Map<String, String> subSubMap = dataDicMap.get(dataType);
        if (null == subSubMap) {
            return null;
        }
        return subSubMap.get(dataNo);
    }


    /**
     * 获取数据项值
     *
     * @param dataType
     * @param dataName
     * @return
     */
    public static String getDataNoByName(String dataType, String dataName) {
        if (null == dataName) {
            return null;
        }
        Map<String, String> subSubMap = dataDicMap.get(dataType);
        if (null == subSubMap) {
            return null;
        }
        Iterator iterator = subSubMap.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry entry = (Map.Entry) iterator.next();
            if(dataName.equals(entry.getValue())){
                return String.valueOf(entry.getKey());
            }
        }
        return null;
    }

    /**
     * 获取此类型数据
     *
     * @param dataDic
     */
    public void dataDicSave(CommDataDic dataDic) {
        dataDicDAO.saveOrUpdateByPrimaryKey(dataDic);
    }

    public void dataDicRemove(CommDataDic dataDic) {
        dataDicDAO.deleteByPrimaryKey(dataDic.getId());
    }

    public List<CommDataDic> findDicDic(CommDataDic dataDic) {
        return dataDicDAO.findDataNameByType(dataDic);
    }

}
