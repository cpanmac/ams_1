package com.njcb.ams.service;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.pojo.dto.Tree;
import com.njcb.ams.repository.dao.CommOrgInfoDAO;
import com.njcb.ams.repository.entity.CommOrgInfo;
import com.njcb.ams.support.exception.ExceptionUtil;
import com.njcb.ams.util.AmsAssert;
import com.njcb.ams.util.AmsUtils;
import com.njcb.ams.util.TreeNodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 机构信息服务类
 *
 * @author LOONG
 */
@Service
public class OrgInfoService {

    @Autowired
    private CommOrgInfoDAO orgInfoDAO;

    public static OrgInfoService getInstance() {
        return AppContext.getBean(OrgInfoService.class);
    }

    public List<CommOrgInfo> orgInfoQuery(CommOrgInfo orgInfo) {
        return orgInfoDAO.selectBySelective(orgInfo);
    }

    @Cacheable(value = "SysParamCache", key = "'orgInfoId_'+#id")
    public CommOrgInfo orgInfoQueryById(int id) {
        return orgInfoDAO.selectByPrimaryKey(id);
    }

    @Cacheable(value = "SysParamCache", key = "'orgInfoNo_'+#orgNo")
    public CommOrgInfo orgInfoQueryByOrgNo(String orgNo) {
        return orgInfoDAO.selectByOrgNo(orgNo);
    }

    /**
     * @param orgInfo
     * @return
     */
    @CacheEvict(value = {"SysParamCache", "SysParamCache"}, allEntries = true)
    public int saveOrgInfo(CommOrgInfo orgInfo) {
        AmsAssert.notNull(orgInfo.getOrgNo(), orgInfo.getOrgName(), "机构号和机构名称不能为空");
        if (AmsUtils.isNotNull(orgInfo)) {
            //新增时校验：
            if (AmsUtils.isNull(orgInfo.getId())) {
                //机构号唯一性校验
                CommOrgInfo orgOrgInfo = orgInfoDAO.selectByOrgNo(orgInfo.getOrgNo());
                AmsAssert.isNull(orgOrgInfo, "新增时机构号[" + orgInfo.getOrgNo() + "]已存在");
                //核心机构号唯一性校验
                CommOrgInfo CoreOrgInfo = orgInfoDAO.selectByCoreOrgNo(orgInfo.getOrgAlias());
                AmsAssert.isNull(CoreOrgInfo, "新增时核心机构号[" + orgInfo.getOrgAlias() + "]已存在");
            } else {//修改时校验：
                //不包含当前ID记录的核心机构号唯一性校验
                CommOrgInfo CoreOrgInfo = orgInfoDAO.selectByCoreOrgNoAndID(orgInfo.getOrgAlias(), orgInfo.getId());
                AmsAssert.isNull(CoreOrgInfo, "修改时核心机构号[" + orgInfo.getOrgAlias() + "]已存在");
            }

        }
        return orgInfoDAO.saveOrUpdateByPrimaryKey(orgInfo);
    }

    /**
     * @param orgNo
     * @return
     */
    @CacheEvict(value = {"SysParamCache", "SysParamCache"}, allEntries = true)
    public int deleteOrgInfo(String orgNo) {
        CommOrgInfo orgInfo = orgInfoDAO.selectByOrgNo(orgNo);
        AmsAssert.notNull(orgInfo,"机构号["+orgNo+"]不存在");
        List<Integer> selfAndSubByIdList = orgInfoDAO.selectSelfAndSubById(orgInfo.getId());
        if (selfAndSubByIdList.size() > 1) {
            ExceptionUtil.throwAppException("含有子机构不允许直接删除", "CRM3001");
        }
        return orgInfoDAO.deleteByPrimaryKey(orgInfo.getId());
    }

    /**
     * 方法功能描述： 获取本机构及其下级机构的机构树
     */
    public List<Tree> orgQueryTree(String orgNo) {
        List<CommOrgInfo> paramList = orgInfoDAO.selectEntitySelfAndSubByOrgNo(orgNo);

        List<Tree> orgTree = convertToTree(paramList);
        List<Tree> trees = TreeNodeUtil.getTierTrees(orgTree);
        return trees;
    }

    private List<Tree> convertToTree(List<CommOrgInfo> orgInfos) {
        List<Tree> newTrees = new ArrayList<Tree>();
        for (CommOrgInfo orgInfo : orgInfos) {
            Tree tree = new Tree();
            tree.setId(orgInfo.getId());
            if (null == orgInfo.getParentId()) {
                tree.setpId(0);
            } else {
                tree.setpId(orgInfo.getParentId());
            }
            tree.setText(orgInfo.getOrgName());
            newTrees.add(tree);
        }
        return newTrees;
    }
}
