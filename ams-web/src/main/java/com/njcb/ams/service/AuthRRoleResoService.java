/**
 * 描述：
 * Copyright：Copyright （c） 2017
 * Company：南京银行
 * 
 * @author njcbkf045
 * @version 1.0 2017年3月13日
 * @see history
 *      2017年3月13日
 */

package com.njcb.ams.service;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.pojo.dto.Tree;
import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.repository.dao.AuthRRoleResoDAO;
import com.njcb.ams.repository.dao.AuthRUserRoleDAO;
import com.njcb.ams.repository.dao.CommRoleInfoDAO;
import com.njcb.ams.repository.entity.AuthRRoleReso;
import com.njcb.ams.repository.entity.AuthRRoleResoExample;
import com.njcb.ams.repository.entity.AuthRUserRole;
import com.njcb.ams.repository.entity.AuthRUserRoleExample;
import com.njcb.ams.util.AmsAssert;
import com.njcb.ams.util.TreeNodeUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author njcbkf045
 *
 *         类功能描述：
 *
 */
@Service
public class AuthRRoleResoService {

	@Autowired
	private AuthRRoleResoDAO authRRoleResoDAO;
	@Autowired
	private AuthRUserRoleDAO authRUserRoleDAO;
	@Autowired
	private CommRoleInfoDAO roleInfoDAO;
	@Autowired
	private ResourceManageService resourceManageService;

	public static AuthRRoleResoService getInstance() {
		return AppContext.getBean(AuthRRoleResoService.class);
	}

	/**
	 * 方法功能描述：
	 * 
	 * @param roleId
	 */
	public List<Tree> roleResoQuery(int roleId) {

		// 获取所有权限
		List<Tree> trees = new ArrayList<Tree>();
		List<Tree> treeList = resourceManageService.selectAllMenu();

		// 获取角色对应的权限ID
		List<AuthRRoleReso> authRRoleResos = new ArrayList<AuthRRoleReso>();
		AuthRRoleResoExample example = new AuthRRoleResoExample();
		AuthRRoleResoExample.Criteria criteria = example.createCriteria();
		if (roleId != 0) {
			criteria = criteria.andRoleIdEqualTo(roleId);
			authRRoleResos = authRRoleResoDAO.selectByExample(example);
		}

		// 修改权限树对应的checked状态
		// tree_id是reso_id，关联表的主键作为树的一个属性
		for (AuthRRoleReso rr : authRRoleResos) {
			int resID = rr.getResId().intValue();
			for (Tree tree : treeList) {
				int treeid = tree.getId();
				if (treeid == resID) {
					tree.setChecked("true");
					Map<String, Object> attributes = new HashMap<String, Object>();
					attributes.put("keyID", rr.getId());
					tree.setAttributes(attributes);
				}
			}
		}

		// 改变树的形式，便于页面展示
		trees = TreeNodeUtil.getTierTrees(treeList);
		return trees;

	}

	public List<Tree> userRoleQuery(int userId) {

		// 获取所有权限
		List<Tree> trees = new ArrayList<Tree>();
		List<Tree> treeList = roleInfoDAO.selectTree();

		// 获取角色对应的权限ID
		List<AuthRUserRole> authRUserRoles = null;
		AuthRUserRoleExample example = new AuthRUserRoleExample();
		AuthRUserRoleExample.Criteria criteria = example.createCriteria();
		if (userId != 0) {
			criteria = criteria.andUserIdEqualTo(userId);
			authRUserRoles = authRUserRoleDAO.selectByExample(example);
		}else{
			authRUserRoles = new ArrayList<AuthRUserRole>();
		}

		// 修改权限树对应的checked状态
		// tree_id是reso_id，关联表的主键作为树的一个属性
		for (AuthRUserRole rr : authRUserRoles) {
			int roleID = rr.getRoleId().intValue();
			for (Tree tree : treeList) {
				int treeid = tree.getId();
				if (treeid == roleID) {
					tree.setChecked("true");
					Map<String, Object> attributes = new HashMap<String, Object>();
					attributes.put("keyID", rr.getId());
					tree.setAttributes(attributes);
				}
			}
		}

		// 改变树的形式，便于页面展示
		trees = TreeNodeUtil.getTierTrees(treeList);
		return trees;

	}

	/**
	 * 方法功能描述：
	 * 
	 * @param insertNodes
	 * @param deleteNodes
	 */
	public void roleResoSave(List<AuthRRoleReso> insertNodes, List<AuthRRoleReso> deleteNodes) {

		for (AuthRRoleReso rr : insertNodes) {
			AmsAssert.notNull(rr.getRoleId(),rr.getResId(),"roleId和resId不完整");
			if (StringUtils.isEmpty(rr.getAuthCode())) {
				rr.setAuthCode(SysBaseDefine.RULE_AUTH_CODE_DEFAULT);
			}
			authRRoleResoDAO.insert(rr);
		}

		for (AuthRRoleReso rr : deleteNodes) {
			authRRoleResoDAO.deleteByPrimaryKey(rr.getId());
		}

	}

	public void userRoleSave(List<AuthRUserRole> insertNodes, List<AuthRUserRole> deleteNodes) {

		for (AuthRUserRole rr : insertNodes) {
			AmsAssert.notNull(rr.getRoleId(),rr.getUserId(),"roleId和userId不完整");
			authRUserRoleDAO.insert(rr);
		}
		for (AuthRUserRole rr : deleteNodes) {
			authRUserRoleDAO.deleteByPrimaryKey(rr.getId());
		}

	}
}
