/**
 * 引用JS和CSS头文件
 */
var rootPath = getRootPath(); //项目路径

var projectName = getProjectName(); //项目名称

var jsRandomNum = Math.random(); //js引用随机数

/**
 * 动态加载CSS和JS文件
 */
var dynamicLoading = {
	meta : function(){
		document.write('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">');
	},
    css: function(path){
		if(!path || path.length === 0){
			throw new Error('argument "path" is required!');
		}
		document.write('<link rel="stylesheet" type="text/css" href="' + path + '">');
    },
    js: function(path, charset){
		if(!path || path.length === 0){
			throw new Error('argument "path" is required!');
		}
        document.write('<script charset="' + (charset ? charset : "utf-8") + '" src="' + path + '"></script>');
    }
};


/**
 * 取得项目名称
 */
function getProjectName() {
	//取得当前URL
	var path = window.document.location.href;
	//取得主机地址后的目录
	var pathName = window.document.location.pathname;
	var post = path.indexOf(pathName);
	//取得项目名
	return pathName.substring(0, pathName.substr(1).indexOf("/") + 1);
}

/**
 * 取得项目路径
 */
function getRootPath() {
	//取得当前URL
	var path = window.document.location.href;
	//取得主机地址后的目录
	var pathName = window.document.location.pathname;
	var post = path.indexOf(pathName);
	//取得主机地址
	var hostPath = path.substring(0, post);
	//取得项目名
	var name = pathName.substring(0, pathName.substr(1).indexOf("/") + 1);
	return hostPath + name + "/";
}

document.write('<div id="loadingDiv"></div> ');

//动态生成meta
dynamicLoading.meta();

//动态加载项目 JS文件
dynamicLoading.js(rootPath + "js/json/json2.js", "utf-8");
dynamicLoading.js(rootPath + "js/jquery-easyui/jquery.min.js", "utf-8");
dynamicLoading.js(rootPath + "js/jquery-easyui/jquery.easyui.min.js", "utf-8");
dynamicLoading.js(rootPath + "js/jquery-easyui/locale/easyui-lang-zh_CN.js", "utf-8");
dynamicLoading.js(rootPath + "js/ztree/jquery.ztree.core-3.5.js", "utf-8");
dynamicLoading.js(rootPath + "js/artDialog/artDialog.js?skin=default", "utf-8");
dynamicLoading.js(rootPath + "js/artDialog/jquery.artDialog.js?skin=default", "utf-8");
dynamicLoading.js(rootPath + "js/echarts/echarts.js", "utf-8");
dynamicLoading.js(rootPath + "js/common/common.js?" + jsRandomNum, "utf-8");
//动态加载项目 CSS文件
dynamicLoading.css(rootPath + "js/jquery-easyui/themes/bootstrap/easyui.css");
dynamicLoading.css(rootPath + "js/jquery-easyui/themes/icon.css");
dynamicLoading.css(rootPath + "js/jquery-easyui/demo/demo.css");
dynamicLoading.css(rootPath + "js/ztree/zTreeStyle.css");

var loadingMask = document.getElementById('loadingDiv');
if(loadingMask != null){
	loadingMask.parentNode.removeChild(loadingMask);
}