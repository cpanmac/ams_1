var aim_div="";//目标div 放置日历的位置
var m=0;//使用标识,之前页面记录的几列
var n=0;//使用标识,根据页面宽度决定日历分为几列
var language="cn";//语言选择
var month_arry;
var week_arry;
var month_cn=new Array("一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月");//月
var month_en=new Array("January","February","March","April","May","June","July","August","September","October","November","December");//月
var week_cn=new Array("日","一","二","三","四","五","六");//星期
var week_en=new Array("Su","Mo","Tu","We","Th","Fr","Sa");//星期
var open_edit_flag = '0';//编辑按钮打开标记
var path = window.document.location.href;
var pathName = window.document.location.pathname;
var hostPath = path.substring(0, path.indexOf(pathName));
var projectName = pathName.substring(0, pathName.substr(1).indexOf("/") + 1);
function loading_calendar(id,lan){
	aim_div="#"+id;
	language=lan;
	if(lan=="cn"){
		month_arry=month_cn;
		week_arry=week_cn;
	}else{
		month_arry=month_en;
		week_arry=week_en;
	}

	//开始
	$(aim_div).fullYearPicker({
		disabledDay : '',
		value : [ /* '2016-6-25', '2016-8-26'  */],
		cellClick : function(dateStr, isDisabled) {
			/* arguments[0] */
		}
	});

}


(function() {
	systemTypeSel();
	window.onload=function(){
		window.onresize = change;
		change();

	} ;

	function change(){
		var obj=$(aim_div);
		var m_obj=$(".fullYearPicker .month-container");
		var width=obj.width();
		var class_width="month-width-";
		n=parseInt(width/200);
		if(n==5)n=4;
		if(n>6)n=6;

		if(n!=m){
			m_obj.removeClass(class_width+m);
			m_obj.addClass(class_width+n);
			m=n;
		}

	};

	function changeHandle(){
		m=0;
		change();

	}

	//设置年份菜单
	function setYearMenu(year){
		$(".year .left_first_year").text(year-1+"");
		$(".year .left_sencond_year").text(year-2+"");
		$(".year .cen_year").text(year);
		$(".year .right_first_year").text(year+1+"");
		$(".year .right_sencond_year").text(year+2+"");
	}


	//设置开始日期和结束日期
	function setDateInfo(start_date,end_date){
		$("#hd-start-date").datepicker('setValue', start_date);
		$("#hd-end-date").datepicker('setValue',end_date);
		// $("#hd-start-date").val(start_date);
		// $("#hd-end-date").val(end_date);
	}



	function tdClass(i, disabledDay, sameMonth, values, dateStr) {

		var cls = i == 0 || i == 6 ? 'weekend' : '';
		if (disabledDay && disabledDay.indexOf(i) != -1)
			cls += (cls ? ' ' : '') + 'disabled';
		if (!sameMonth){
			cls += (cls ? ' ' : '') + 'empty';
		}else{
			cls += (cls ? ' ' : '') + 'able_day';
		}
		if (sameMonth && values && cls.indexOf('disabled') == -1
			&& values.indexOf(',' + dateStr + ',') != -1)
			cls += (cls ? ' ' : '') + 'selected';
		return cls == '' ? '' : ' class="' + cls + '"';
	}
	function renderMonth(year, month, clear, disabledDay, values) {

		var d = new Date(year, month - 1, 1), s = "<div class='month-container'>"+'<table cellpadding="3" cellspacing="1" border="0"'
			+ (clear ? ' class="right"' : '')
			+ '>'
			+ '<tr><th colspan="7" class="head"  index="'+month+'">' /* + year + '年'  */
			+month_arry[month-1]
			+ '</th></tr>'
			+ '<tr><th class="weekend">'+week_arry[0]+'</th><th>'+week_arry[1]+'</th><th>'+week_arry[2]+'</th><th>'+week_arry[3]+'</th><th>'+week_arry[4]+'</th><th>'+week_arry[5]+'</th><th class="weekend">'+week_arry[6]+'</th></tr>';
		var dMonth = month - 1;
		var firstDay = d.getDay(), hit = false;
		var spe_january = 0;
		if(month==1){//1月份要特殊处理
			spe_january=1;
			//如果1号为周五或者周六，1月将会有6行数据
			if(firstDay==5||firstDay==6){
				spe_january=0;
			}
			if(spe_january==1){//1月只有5行数据
				s += '<tr>';
				var dec_cls = "able_day";
				if(i == 0 || i == 6){
					dec_cls = "weekend able_day";
				}
				for (var i = 0; i < 7; i++){
					s += '<td class="'+dec_cls+'" id="last12_'+(31-firstDay-6+i)+'" style="color: #00CED1;">'+'<span>'+(31-firstDay-6+i)+'</span></td>';
				}
				s += '</tr>';
			}
		}
		s += '<tr>';
		for (var i = 0; i < 7; i++)
			if (firstDay == i || hit) {
				s += '<td'
					+ tdClass(i, disabledDay, true, values, year
						+ '-' + month + '-' + d.getDate())
					+ '>' + d.getDate() + '</td>';
				d.setDate(d.getDate() + 1);
				hit = true;
			} else
			if(month==1){
				var dec_cls = "able_day";
				if(i == 0 || i == 6){
					dec_cls = "weekend able_day";
				}
				s += '<td class="'+dec_cls+'" id="last12_'+(32-firstDay+i)+'" style="color: #00CED1;">'+'<span>'+(32-firstDay+i)+'</span></td>';
			}else{
				s += '<td' + tdClass(i, disabledDay, false)
					+ '>&nbsp;</td>';
			}

		s += '</tr>';
		for (var i = 0; i < 5; i++) {
			if(spe_january==1&&i==4) continue;
			s += '<tr>';
			for (var j = 0; j < 7; j++) {
				s += '<td'
					+ tdClass(j, disabledDay,
						d.getMonth() == dMonth, values, year
						+ '-' + month + '-'
						+ d.getDate())
					+ '>'
					+ (d.getMonth() == dMonth ? d.getDate()
						: '&nbsp;') + '</td>';
				d.setDate(d.getDate() + 1);
			}
			s += '</tr>';
		}
		return s + '</table></div>' + (clear ? '<br>' : '');
	}
	function getDateStr(td) {
		return td.parentNode.parentNode.rows[0].cells[0].getAttribute('index')+"-"+ td.innerHTML;
	}
	function renderYear(year, el, disabledDay, value) {

		el.find('td').unbind();
		var s = '', values = ',' + value.join(',') + ',';
		for (var i = 1; i <= 12; i++)
			s += renderMonth(year, i, false, disabledDay, values);
		s+="<div class='date_clear'></div>";
		el
			.find('div.picker')
			.html(s)
			.find('td')
			.click(/*单击日期单元格*/
				/*
                function() {
                    if (!/disabled|empty/g.test(this.className))
                        $(this).toggleClass('selected');
                    if (this.className.indexOf('empty') == -1
                            && typeof el.data('config').cellClick == 'function')
                        el
                                .data('config')
                                .cellClick(
                                        getDateStr(this),
                                        this.className
                                                .indexOf('disabled') != -1);
                } */);
		changeHandle();
		day_drap_listen();
		var year=$("#cen_year").text();
		var sysId = document.getElementById("sysCalendarId").value;
		init_sun_satday(year,sysId);

	}


	//去掉特殊字符
	var excludeSpecial=function(s){
		s=s.replace(/[\-]/g,"");
		return s;
	}
	//监听日期拖在
	function day_drap_listen(){
		var is_drap=0;
		var start_date="";
		var end_date="";
		$(".fullYearPicker .picker table td").mousedown(function(event){
			/*判断是左键才触发  */
			if(event.button==0&&($(this).html()!="&nbsp;")){
				is_drap=1;
				start_date=getDateStr($(this)[0]);
			}

		});
		$(".fullYearPicker .picker table td").mouseup(function(event){
			/*判断是左键才触发  */
			if(event.button==0&&($(this).html()!="&nbsp;")){
				is_drap=0;
				end_date=getDateStr($(this)[0]);
				if(checkDate(start_date, end_date)){
					change_class(start_date, end_date);
					//	open_modal(start_date, end_date);
				}else{
					//open_modal(end_date, start_date);
				}
			}


		});
		$(".fullYearPicker .picker table td").mouseover(function(){
			var day=$(this).html();
			if(is_drap==1&&day!="&nbsp;"){
				var min_date=getDateStr($(this)[0]);
				drap_select(start_date,min_date,"selected");
			}
		});
	}

	function open_modal_stati(){
		var year=$("#cen_year").text();
		var sysId = document.getElementById("sysCalendarId").value;
		$.ajax({
			type : "POST",
			url : "noSunSatdayStati.jhtml",
			dataType:"json",
			data : {
				year : year,
				sysId : sysId
			},
			success:function(data){
				var MATTERSSTR="";
				$("#no_sun_sat_day").empty();
				for(var i=0;i<data.length;i++){
					MATTERSSTR=MATTERSSTR
						+'<li id="'+data[i].data_str +'" value="'+data[i].data_str +'" style="heigth:30px;line-height:20px;text-align: left;margin-left:10px;display:inline;float:left;">'
						+data[i].data_str
						+'</li>'
				}
				$("#no_sun_sat_day").append(MATTERSSTR);
			}
		});
		$("#calendar-modal-2").modal();
	}

	$("#calendarStati").click(function(){
		open_modal_stati();
	});
	/*根据日期判断大小 开始值小于结束值返回true  */
	function checkDate(start, end){
		var rs=false;
		var start_month=parseInt(start.split("-")[0]);
		var start_day=parseInt(start.split("-")[1]);
		var end_month=parseInt(end.split("-")[0]);
		var end_day=parseInt(end.split("-")[1]);
		/*if(start_month==end_month){
			if(start_day<end_day){
				rs=true;
			}
		}else if(start_month<end_month){
			rs=true;
		}*/
		return true;
	}
	/*窗口添加按钮*/
	$("#calendar_confirm_btn").click(function(){
		var sysId = document.getElementById("sysCalendarId").value;
		var start_date=$("#hd-start-date").val();
		start_date=start_date.split("-")[1]+"-"+start_date.split("-")[2];
		var end_date=$("#hd-end-date").val();
		end_date=end_date.split("-")[1]+"-"+end_date.split("-")[2];
		var m_type=$("#hd-type-option").val();
		if(m_type == "workday"){
			dataType = "0";
			drap_select(start_date,end_date,"work");
		}else if(m_type == "freeday"){
			dataType = "1";
			drap_select(start_date,end_date,"selected");
		}

		var year=$("#cen_year").text();
		start_date=year+excludeSpecial(start_date);
		$.ajax({
			type : "POST",
			url : "add.jhtml",
			data : {
				sysId : sysId,
				dataStr:start_date,
				//endDate:end_date,
				dataType:dataType
			},
			success : function(data){
			}
		});


		close_modal();
	});

	/*拖拽选着  */
	function drap_select(start,end,new_class){
		if(start!=end) return;//不允许拖拽选择
		var max=60;//当天数要选择到最后一天取一个大于所以月份的值
		//清除选中单元格的样式
		//$(".month-container .selected").removeClass("selected");
		var start_month=parseInt(start.split("-")[0]);
		var start_day=parseInt(start.split("-")[1]);
		var end_month=parseInt(end.split("-")[0]);
		var end_day=parseInt(end.split("-")[1]);
		if(start_month==end_month){
			if(start_day<end_day){
				select_month(start_month, start_day, end_day,new_class);
			}else{
				select_month(start_month, end_day, start_day,new_class);
			}
		}else if(start_month<end_month){
			select_month(start_month, start_day, max,new_class);
			for(var i=start_month+1;i<end_month;i++){
				select_month(i, 1, max,new_class);
			}
			select_month(end_month, 1, end_day,new_class);
		}else if(start_month>end_month){
			select_month(start_month, 1, start_day,new_class);
			for(var i=end_month+1;i<start_month;i++){
				select_month(i, 1, max,new_class);
			}
			select_month(end_month, end_day, max,new_class);
		}
	}
	/*按月加载样式*/
	function select_month(month,start,end,new_class,spe_id){
		month=month-1;
		var year = $("#cen_year").text();
		$(".fullYearPicker .picker .month-container:eq("+month+") td").each(function(){
			var num=$(this).text();
			var id=$(this).attr('id');
			if(num>=start&&num<=end){
				if(spe_id!=null&&spe_id!=''){
					if(id==undefined) return;
					if(id!=null&&id!=''&&id.indexOf(spe_id)<0) return;
				}else{
					if(id==undefined) {
					}else if(id!=null||id!=''){
						return;
					}
				}
				if($(this).hasClass("selected")) $(this).removeClass("selected");
				if($(this).hasClass("work")) $(this).removeClass("work");
				$(this).addClass(new_class);
				var temp_data = "";
				var dataType = "";
				if(month<9){
					temp_data = year + "0" + (month + 1);
				}else{
					temp_data = year + + (month + 1);
				}
				if(num<10){
					temp_data = temp_data + "0" + num;
				}else{
					temp_data = temp_data + num;
				}
				if(new_class == "workday"){
					dataType = "0";
				}else if(new_class == "freeday"){
					dataType = "1";
				}
				var sysCalendarId = document.getElementById("sysCalendarId").value;
			}
		});

	}

	/**
	 * 修改日期状态 0-工作日 1-节假日
	 */
	function updateCalendarStatus(sysCalendarId,tempData,dataType){
		$.ajax({
			type : "POST",
			url : "updateCStatus.jhtml",
			data : {
				sysCalendarId : sysCalendarId,
				tempData : tempData,
				dataType : dataType
			},
			success : function(data){
			}
		});
	}

	/*初始化事项界面*/
	function init_sun_satday(year,sysId){
		//debugger;
		$(".month-container .selected").removeClass("selected");
		$.ajax({
			type : "POST",
			url:projectName + "/app/sysCalendar/initSunSatday",
			dataType:"json",
			data : {
				year : year,
				sysId : sysId
			},
			beforeSend : function(){
				$('#div_loading1').show();
			},
			complete : function(){
				$('#div_loading1').hide();
			},
			success : function(data){
				for(var i=0;i<data.length;i++){
					var data_str=data[i].substring(4);
					var month=data_str.substring(2,0);
					var day=data_str.substring(2);
					temp_data=day.substring(1,0)
					if(temp_data==0){
						day=data_str.substring(3);
					}
					if(year==data[i].substring(0,4)){
						select_month(month,day,day,"selected");
					}
					if(year>data[i].substring(0,4)){
						select_month("1",day,day,"selected","last12_");
					}
				}
			}
		});
	}


	/*鼠标点击修改日期类型*/
	function change_class(start_date,end_date){
		if(open_edit_flag=='0') return;
		var year=$("#cen_year").text();
		start_date=year+"-"+start_date;
		end_date=year+"-"+end_date;
		var last_year_flag = 0;
		if(start_date!=null){
			if(start_date.indexOf('<span>')>=0){//上年12月数据
				start_date = start_date.replace("<span>","").replace("</span>","");
				end_date = end_date.replace("<span>","").replace("</span>","");
				last_year_flag = 1;
			}
			$("#hd-start-date").val(start_date);
			$("#hd-end-date").val(end_date);
		}
		var sysId = document.getElementById("sysCalendarId").value;
		var start_date=$("#hd-start-date").val();
		start_date=start_date.split("-")[1]+"-"+start_date.split("-")[2];
		var end_date=$("#hd-end-date").val();
		end_date=end_date.split("-")[1]+"-"+end_date.split("-")[2];
		var year=$("#cen_year").text();
		var day = $("#hd-start-date").val().split("-")[2];
		if(last_year_flag==0){
			startDate=year+"-" + start_date ;
			endDate=year+"-" + end_date;
		}else{
			startDate=(year-1)+'12'+day;
			endDate=(year-1)+'12'+day;
		}
		/*var m_type=$("#hd-type-option").val();
		if(m_type == "workday"){
			dataType = "0";
			drap_select(start_date,end_date,"work");
		}else if(m_type == "freeday"){
			dataType = "1";
			drap_select(start_date,end_date,"selected");
		}*/

		$.ajax({
			type : "POST",
			url : projectName + "/app/sysCalendar/selectDataType.jhtml",
			dataType:"json",
			data : {
				sysId : sysId,
				dataStr:startDate,
				//endDate:end_date,
				//dataType:dataType
			},
			success : function(data){
				var dataType;
				if(last_year_flag==0){
					if(data==0){//非节假日
						dataType = "1";
						drap_select(start_date,end_date,"selected");
					}else if(data==1){
						dataType = "0";
						drap_select(start_date,end_date,"work");
					}
				}else{
					if(data==0){//非节假日
						dataType = "1";
						select_month("1",day,day,"selected","last12_");
					}else if(data==1){
						dataType = "0";
						select_month("1",day,day,"work","last12_");
					}
				}

				$.ajax({
					type : "POST",
					url :  projectName + "/app/sysCalendar/add.jhtml",
					data : {
						sysId : sysId,
						dataStr:startDate,
						//endDate:end_date,
						dataType:dataType
					},
					success : function(data){
					}
				});
			}
		});
	}

	function open_modal(start_date,end_date){
		var year=$("#cen_year").text();
		start_date=year+"-"+start_date;
		end_date=year+"-"+end_date;
		if(start_date!=null){
			setDateInfo(start_date,end_date);
		}

		$("#calendar-modal-1").modal();
		//	$(".month-container .selected").removeClass("selected");
	}


	/*systemtype下拉列表*/
	function systemTypeSel1(){
		var systemtype_="systemtype_";
		$.ajax({
			type:"POST",
			url:"initSel.jhtml",
			dataType:"json",
			data:{
				dictfield:systemtype_
			},

			success:function(data){
				var MATTERSSTR="";
				$("#sysCalendarId").empty();
				for(var i=0;i<data.length;i++){
					MATTERSSTR=MATTERSSTR
						+'<option id='+data[i].code_ +' value='+data[i].code_ +' >'
						+data[i].desc_
						+'</option>'
				}
				$("#sysCalendarId").append(MATTERSSTR);
			}
		});
	}

	/*systemtype下拉列表*/
	function systemTypeSel(){
		$.ajax({
			type:"POST",
			url:projectName + "/app/sysCalendar/initSel.jhtml",
			dataType:"json",
			success:function(data){
				var MATTERSSTR="";
				$("#sysCalendarId").empty();
				for(var i=0;i<data.length;i++){
					MATTERSSTR=MATTERSSTR
						+'<option id='+data[i].code +' value='+data[i].code +' >'
						+data[i].code_desc
						+'</option>'
				}
				$("#sysCalendarId").append(MATTERSSTR);
				var sysCalendarId = $(this).children('option:selected').val();
				if(sysCalendarId == "CNAPS2" || sysCalendarId == "LCYH"){
					$("#makeDataBtn").attr("disabled",true);
					$("#sendDataBtn").attr("disabled",true);
				}else{
					$("#makeDataBtn").attr("disabled",false);
					$("#sendDataBtn").attr("disabled",false);
				}
			}
		});
	}

	/**
	 * 初始化日历
	 */
	function initCalendar(sysId,dataYear){

		$.ajax({
			type : "GET",
			url : projectName + "/app/sysCalendar/initCalendar.jhtml?sysCalendarId="+sysId+"&dataYear="+dataYear,
			beforeSend : function(){
				$('#div_loading').show();
			},
			complete : function(){
				$('#div_loading').hide();
			},
			success : function(data){
				user_conf_flag = data.user_conf_flag;
				user_conf_no = data.user_conf_no;
				init_sun_satday(dataYear,sysId);
			}
		});

	}

	//@config：配置，具体配置项目看下面
	//@param：为方法时需要传递的参数
	$.fn.fullYearPicker = function(config, param) {
		if (config === 'setDisabledDay' || config === 'setYear'
			|| config === 'getSelected'
			|| config === 'acceptChange') {//方法
			var me = $(this);
			if (config == 'setYear') {//重置年份
				me.data('config').year = param;//更新缓存数据年份
				me.find('div.year a:first').trigger('click', true);
			} else if (config == 'getSelected') {//获取当前当前年份选中的日期集合（注意不更新默认传入的值，要更新值请调用acceptChange方法）
				return me.find('td.selected').map(function() {
					return getDateStr(this);
				}).get();
			} else if (config == 'acceptChange') {//更新日历值，这样才会保存选中的值，更换其他年份后，再切换到当前年份才会自动选中上一次选中的值
				me.data('config').value = me
					.fullYearPicker('getSelected');
			} else {
				me.find('td.disabled').removeClass('disabled');
				me.data('config').disabledDay = param;//更新不可点击星期
				if (param) {
					me
						.find('table tr:gt(1)')
						.find('td')
						.each(
							function() {
								if (param
									.indexOf(this.cellIndex) != -1)
									this.className = (this.className || '')
											.replace(
												'selected',
												'')
										+ (this.className ? ' '
											: '')
										+ 'disabled';
							});
				}
			}
			return this;
		}
		//@year:显示的年份
		//@disabledDay:不允许选择的星期列，注意星期日是0，其他一样
		//@cellClick:单元格点击事件（可缺省）。事件有2个参数，第一个@dateStr：日期字符串，格式“年-月-日”，第二个@isDisabled，此单元格是否允许点击
		//@value:选中的值，注意为数组字符串，格式如['2016-6-25','2016-8-26'.......]
		config = $.extend({
			year : new Date().getFullYear()+1,
			disabledDay : '',
			value : []
		}, config);
		return this
			.addClass('fullYearPicker')
			.each(
				function() {
					var me = $(this), year = config.year|| new Date().getFullYear(), newConifg = {
						cellClick : config.cellClick,
						disabledDay : config.disabledDay,
						year : year,
						value : config.value
					};
					me.data('config', newConifg);

					me.append('<div class="year">'
						+'<table>'
						+'<th class="year-operation-btn"><a href="#"  class="am-icon-chevron-left"></a></th>'
						+'<th class="left_sencond_year year_btn">'+ ''+'</th>'
						+'<th class="left_first_year year_btn">'+ ''+'</th>'
						+'<th id="cen_year" class="cen_year year_btn">'+ year+'</th>'
						+'<th class="right_first_year year_btn">'+ ''+'</th>'
						+'<th class="right_sencond_year year_btn">'+ ''+'</th>'
						+'<th class="year-operation-btn"><a href="#" class="next am-icon-chevron-right"></a></th>'
						+'</table>'
						+'<div class="stone"></div></div><div class="picker"></div>')
						.find('.year-operation-btn')
						.click(
							function(e, setYear) {
								if (setYear)
									year = me.data('config').year;
								else
									$(this).children("a").attr("class")== 'am-icon-chevron-left' ? year--: year++;
								setYearMenu(year);
								renderYear(
									year,
									$(this).closest('div.fullYearPicker'),
									newConifg.disabledDay,
									newConifg.value);
								document.getElementById("cen_year").firstChild.data=year;
								//左右箭头切换年份时初始化日历
								var sysId = document.getElementById("sysCalendarId").value;
								initCalendar(sysId,year);
								return false;
							});
					setYearMenu(year);
					//年份选择
					$(".year .year_btn").click(function(){
						var class_name=$(this).attr("class");
						if(class_name.indexOf("cen_year")<0){
							var year=parseInt($(this).text());
							setYearMenu(year);
							renderYear(year, me, newConifg.disabledDay,newConifg.value);
							//点击年份时初始化日历
							var sysId = document.getElementById("sysCalendarId").value;
							initCalendar(sysId,year);
						}

					});
					renderYear(year, me, newConifg.disabledDay,
						newConifg.value);

				});
	};



	/**
	 * 系统选择下拉框响应事件
	 */
	$("#sysCalendarId").change(function(){
		var sysId = document.getElementById("sysCalendarId").value;
		var year = $("#cen_year").text();
		if(year.length==0){
			var mydate=new Date();
			year=mydate.getFullYear()+1;
		}else{
			year = $("#cen_year").text();
		}
		open_edit_flag = '0';
		$("#saveEditBtn").attr("disabled",true);
		initCalendar(sysId,year);
	})

})();


function close_modal(){
	$("#calendar-modal-1").modal('close');
}
/**
 * 编辑按钮单击事件
 */
function openEditControl(){
	if(user_conf_flag=='1') AOS.err("用户"+user_conf_no+"冲突");
	open_edit_flag = '1';
	$("#saveEditBtn").attr("disabled",false);
}
/**
 * 保存按钮单击事件
 */
function saveEditButton(){
	var sysId = document.getElementById("sysCalendarId").value;;
	var year = $("#cen_year").text();
	$.ajax({
		data : {
			sysId : sysId,
			year : year
		},
		type:'POST',
		url : projectName + "/app/sysCalendar/saveall.jhtml",
		success : function(data) {

			open_edit_flag = '0';
			$("#saveEditBtn").attr("disabled",true);

		}
	});
}
function sumHolidayDays(){
	if(open_edit_flag=='1') {$.messager.alert("操作提示","请先保存修改的数据","info"); return;}
	var sysId = document.getElementById("sysCalendarId").value;;
	var year = $("#cen_year").text();
	$.ajax({
		data : {
			sysId : sysId,
			year : year
		},
		url : projectName + "/app/sysCalendar/sumdays.jhtml",
		type:'POST',
		success : function(data) {
			if (data.appcode == 1) {
				open_edit_flag = '0';
				$.messager.alert("操作提示",data.appmsg);
			}else{
				$.messager.alert("操作提示",data.appmsg);
				return;
			}
		}
	});
}
/**
 * 导出按钮单击事件
 */
function exportCalendarForExcel(){
	if(open_edit_flag=='1') {AOS.err("请先保存修改的数据"); return;}
	var sysCalendarId = document.getElementById("sysCalendarId").value;
	var dataYear = $("#cen_year").text();
	window.location.href = "export.jhtml?sysCalendarId="+sysCalendarId+"&dataYear="+dataYear;
}

/**
 * 导出按钮单击事件
 */
function exportCalendarForTxt(){
	if(open_edit_flag=='1') {AOS.err("请先保存修改的数据"); return;}
	var sysCalendarId = document.getElementById("sysCalendarId").value;
	var dataYear = $("#cen_year").text();
	window.location.href = "exportTxt.jhtml?sysCalendarId="+sysCalendarId+"&dataYear="+dataYear;
}
//同步核心节假日
function synNCBSYear(){
	var sysId = document.getElementById("sysCalendarId").value;;
	var year = $("#cen_year").text();
	AOS.ajax({
		params : {
			sysId : sysId,
			year : year
		},
		url : 'synNCBSYear.jhtml',
		ok : function(data) {
			if (data.appcode === 1) {
				AOS.ok(data.appmsg);
				var days = JSON.parse(data.days);
				for(var i = 0;i<12;i++){
					$(".fullYearPicker .picker .month-container:eq("+i+") td").each(function(){
						if($(this).hasClass("selected")) $(this).removeClass("selected");
						if($(this).hasClass("work")) $(this).removeClass("work");
					});
				}
				for(var i=0;i<days.length;i++){
					var data_str=days[i].data_str.substring(4);
					var month=data_str.substring(2,0);
					var day=data_str.substring(2);
					temp_data=day.substring(1,0)
					if(temp_data==0){
						day=data_str.substring(3);
					}
					if(year==days[i].data_str.substring(0,4)){
						selectTargMonth(month,day,day,"selected");
					}
					if(year>days[i].data_str.substring(0,4)){
						$(".fullYearPicker .picker .month-container:eq("+0+") td").each(function(){
							var num=$(this).text();
							var id=$(this).attr('id');
							if(id==undefined) return;
							if(num==day&&id.indexOf("last12_")>=0){
								$(this).addClass("selected");
							}
						});
					}
				}
			}else{
				AOS.err(data.appmsg);
				return;
			}
		}
	});
}
function selectTargMonth(month,start,end,new_class){
	month=month-1;
	var year = $("#cen_year").text();
	$(".fullYearPicker .picker .month-container:eq("+month+") td").each(function(){
		var num=$(this).text();
		var id=$(this).attr('id');
		if(id==undefined){

		}else{
			if(id!=null&&id!='') return;
		}
		if(num>=start&&num<=end){
			if($(this).hasClass("selected")) $(this).removeClass("selected");
			if($(this).hasClass("work")) $(this).removeClass("work");
			$(this).addClass(new_class);
		}
	});
}

/**
 * 生成数据按钮单机事件
 */
function makeData(){
	var sysCalendarId = document.getElementById("sysCalendarId").value;
	var dataYear = $("#cen_year").text();
	var applyId = $("#applyId").val();
	var pubId = $("#pubId").val();
	AOS.ajax({
		params : {
			sysCalendarId : sysCalendarId,
			dataYear : dataYear,
			applyId : applyId,
			pubId : pubId
		},
		url : 'makeData.jhtml',
		ok : function(data) {
			if(data.success == true){
				$("#applyId").val(data.applyId);
				$("#pubId").val(data.pubId);
				AOS.ok(data.appmsg);
			}else{
				AOS.err(data.appmsg);
			}
		}
	});
}
/**
 * 发送数据按钮单机事件
 */
function sendData(){
	if(open_edit_flag=='1') {AOS.err("请先保存修改的数据"); return;}
	var sysCalendarId = document.getElementById("sysCalendarId").value;
	var dataYear = $("#cen_year").text();
	$.ajax({
		params : {
			sysCalendarId : sysCalendarId,
			dataYear : dataYear,
			button_type : '1'
		},
		url : 'sendData.jhtml',
		ok : function(data) {
			if(data.success == true){
				AOS.ok(data.appmsg);
			}else{
				AOS.err(data.appmsg);
			}
		}
	});
}
/**
 * 系统下拉框选值变化事件
 */
$("#sysCalendarId").change(function(){
	$("#applyId").val("");
	$("#pubId").val("");
	var sysCalendarId = $(this).children('option:selected').val();
	if(sysCalendarId == "CNAPS2" || sysCalendarId == "LCYH"){
		$("#makeDataBtn").attr("disabled",true);
		$("#sendDataBtn").attr("disabled",true);
	}else{
		$("#makeDataBtn").attr("disabled",false);
		$("#sendDataBtn").attr("disabled",false);
	}
	if(sysCalendarId == "NCBS"){
		$("#selsyn").attr("disabled",true);
	}else{
		$("#selsyn").attr("disabled",false);
	}
});
$("#selsyn").attr("disabled",true);
$("#saveEditBtn").attr("disabled",true);