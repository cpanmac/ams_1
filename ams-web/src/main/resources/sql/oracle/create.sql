CREATE SEQUENCE SEQ_COMM_DATA_DIC START WITH 22 INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 CACHE 20 ORDER;
-- Create table
create table COMM_DATA_DIC
(
  id             INTEGER primary key,
  data_no        VARCHAR2(20) not null,
  data_name      VARCHAR2(60) not null,
  data_type      VARCHAR2(20) not null,
  data_type_name VARCHAR2(30) not null,
  data_attr      CHAR(2) default '01' not null,
  data_logic     VARCHAR2(512)
);
-- Add comments to the table 
comment on table COMM_DATA_DIC
  is '数据字典表';
-- Add comments to the columns 
comment on column COMM_DATA_DIC.data_attr
  is '字典属性 01本地字典 02外部字典';
comment on column COMM_DATA_DIC.data_logic
  is '外部关联逻辑';

create index idx_datadic_type on comm_data_dic(data_type);

insert into comm_data_dic (ID, DATA_NO, DATA_NAME, DATA_TYPE, DATA_TYPE_NAME, DATA_ATTR, DATA_LOGIC)
values (7, '0', '0', 'USER', '用户信息', '02', 'select u.id as data_no,u.user_name as data_name from comm_user_info u');

insert into comm_data_dic (ID, DATA_NO, DATA_NAME, DATA_TYPE, DATA_TYPE_NAME, DATA_ATTR, DATA_LOGIC)
values (8, '01', '正常', '3', '服务保护状态', '01', '');

insert into comm_data_dic (ID, DATA_NO, DATA_NAME, DATA_TYPE, DATA_TYPE_NAME, DATA_ATTR, DATA_LOGIC)
values (9, '02', '降级', '3', '服务保护状态', '01', '');

insert into comm_data_dic (ID, DATA_NO, DATA_NAME, DATA_TYPE, DATA_TYPE_NAME, DATA_ATTR, DATA_LOGIC)
values (10, '03', '熔断', '3', '服务保护状态', '01', '');

insert into comm_data_dic (ID, DATA_NO, DATA_NAME, DATA_TYPE, DATA_TYPE_NAME, DATA_ATTR, DATA_LOGIC)
values (11, '04', '挡板', '3', '服务保护状态', '01', '');

insert into comm_data_dic (ID, DATA_NO, DATA_NAME, DATA_TYPE, DATA_TYPE_NAME, DATA_ATTR, DATA_LOGIC)
values (12, '01', '有效', 'STATUS', '状态', '01', '');

insert into comm_data_dic (ID, DATA_NO, DATA_NAME, DATA_TYPE, DATA_TYPE_NAME, DATA_ATTR, DATA_LOGIC)
values (13, '02', '无效', 'STATUS', '状态', '01', '');

insert into comm_data_dic (ID, DATA_NO, DATA_NAME, DATA_TYPE, DATA_TYPE_NAME, DATA_ATTR, DATA_LOGIC)
values (17, '1', '有效', '1', '系统参数状态', '01', '');

insert into comm_data_dic (ID, DATA_NO, DATA_NAME, DATA_TYPE, DATA_TYPE_NAME, DATA_ATTR, DATA_LOGIC)
values (18, '2', '无效', '1', '系统参数状态', '01', '');

insert into comm_data_dic (ID, DATA_NO, DATA_NAME, DATA_TYPE, DATA_TYPE_NAME, DATA_ATTR, DATA_LOGIC)
values (19, '0', '0', 'ORGID', '机构号', '02', 'select o.id as data_no ,o.org_no as data_name from comm_org_info o');

insert into comm_data_dic (ID, DATA_NO, DATA_NAME, DATA_TYPE, DATA_TYPE_NAME, DATA_ATTR, DATA_LOGIC)
values (20, '0', '0', 'ORG', '机构信息', '02', 'select o.id as data_no ,o.org_name as data_name from comm_org_info o');

insert into comm_data_dic (ID, DATA_NO, DATA_NAME, DATA_TYPE, DATA_TYPE_NAME, DATA_ATTR, DATA_LOGIC)
values (21, '0', '0', 'ORGNO', '机构信息', '02', 'select o.org_no as data_no ,o.org_name as data_name from comm_org_info o');


CREATE SEQUENCE SEQ_COMM_ORG_INFO START WITH 2 INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 CACHE 20 ORDER;

-- Create table
create table COMM_ORG_INFO
(
  id            INTEGER primary key,
  periods       VARCHAR2(20),
  org_no        VARCHAR2(10),
  org_name      VARCHAR2(64),
  org_grade     INTEGER,
  status        VARCHAR2(2),
  parent_id     INTEGER,
  org_type      VARCHAR2(2),
  org_alias     VARCHAR2(64),
  remark        VARCHAR2(128),
  valide_date   CHAR(8),
  invalide_date CHAR(8),
  conduct_user  INTEGER,
  conduct_time  CHAR(14)
);
-- Add comments to the table 
comment on table COMM_ORG_INFO
  is '机构信息表';
-- Add comments to the columns 
comment on column COMM_ORG_INFO.id
  is 'ID';
comment on column COMM_ORG_INFO.periods
  is '期数版本';
comment on column COMM_ORG_INFO.org_no
  is '机构号';
comment on column COMM_ORG_INFO.org_name
  is '机构名称';
comment on column COMM_ORG_INFO.org_grade
  is '机构级别';
comment on column COMM_ORG_INFO.status
  is '状态';
comment on column COMM_ORG_INFO.parent_id
  is '上级机构';
comment on column COMM_ORG_INFO.org_type
  is '机构类型';
comment on column COMM_ORG_INFO.org_alias
  is '机构别名';
comment on column COMM_ORG_INFO.remark
  is '说明';
comment on column COMM_ORG_INFO.valide_date
  is '生效日期';
comment on column COMM_ORG_INFO.invalide_date
  is '失效日期';
comment on column COMM_ORG_INFO.conduct_user
  is '操作人';
comment on column COMM_ORG_INFO.conduct_time
  is '操作时间';


insert into COMM_ORG_INFO (ID, PERIODS, ORG_NO, ORG_NAME, ORG_GRADE, STATUS, PARENT_ID, ORG_TYPE, VALIDE_DATE, INVALIDE_DATE, CONDUCT_USER, CONDUCT_TIME)
values (1, '1', '9900', '总行', 1, '01', 0, '0', '20170101', '20170101', 1, '20170101000000');


CREATE SEQUENCE SEQ_COMM_ROLE_INFO START WITH 2 INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 CACHE 20 ORDER;

-- Create table
create table COMM_ROLE_INFO
(
  id        INTEGER primary key,
  role_code VARCHAR2(32),
  role_name VARCHAR2(64),
  role_desc VARCHAR2(256),
  status    CHAR(2)
);
-- Add comments to the table 
comment on table COMM_ROLE_INFO
  is '角色信息表';
-- Add comments to the columns 
comment on column COMM_ROLE_INFO.id
  is '主键';
comment on column COMM_ROLE_INFO.role_code
  is '英文代码';
comment on column COMM_ROLE_INFO.role_name
  is '角色名称';
comment on column COMM_ROLE_INFO.role_name
  is '角色描述';
comment on column COMM_ROLE_INFO.status
  is '状态';
  
insert into comm_role_info (ID, ROLE_CODE, ROLE_NAME, ROLE_DESC, STATUS)
values (1, 'admin', '系统管理员', '超级管理员', '01');


CREATE SEQUENCE SEQ_COMM_USER_INFO START WITH 2 INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 CACHE 20 ORDER;

-- Create table
create table COMM_USER_INFO
(
  id           INTEGER primary key,
  login_name   VARCHAR2(20),
  pass_word    VARCHAR2(256),
  user_name    VARCHAR2(50),
  org_id       INTEGER,
  status       VARCHAR2(2),
  emp_no       VARCHAR2(20),
  login_time   CHAR(14),
  login_ip     VARCHAR2(20),
  user_code    VARCHAR2(10),
  email        VARCHAR2(50),
  conduct_user INTEGER,
  conduct_time CHAR(14)
);
-- Add comments to the table 
comment on table COMM_USER_INFO
  is '用户信息表';
-- Add comments to the columns 
comment on column COMM_USER_INFO.id
  is '主键';
comment on column COMM_USER_INFO.login_name
  is '登录名';
comment on column COMM_USER_INFO.pass_word
  is '登录密码';
comment on column COMM_USER_INFO.user_name
  is '用户姓名';
comment on column COMM_USER_INFO.org_id
  is '机构ID';
comment on column COMM_USER_INFO.status
  is '状态';
comment on column COMM_USER_INFO.emp_no
  is '员工号';
comment on column COMM_USER_INFO.login_time
  is '登录时间';
comment on column COMM_USER_INFO.login_ip
  is '登录IP';
comment on column COMM_USER_INFO.user_code
  is '用户代码';
comment on column COMM_USER_INFO.email
  is '邮箱';
comment on column COMM_USER_INFO.conduct_user
  is '操作人';
comment on column COMM_USER_INFO.conduct_time
  is '操作时间';

insert into COMM_USER_INFO (ID, LOGIN_NAME, PASS_WORD, USER_NAME, ORG_ID, STATUS, EMP_NO, LOGIN_TIME, LOGIN_IP, USER_CODE, CONDUCT_USER, CONDUCT_TIME)
values (1, 'admin', '{MD5}8ddcff3a80f4189ca1c9d4d902c3c909', 'admin', 1, '1', '0', '20180810142824', '0.0.0.0', null, 1, '20180712162632');


-- Create table
create table COMM_HOLIDAY_INFO
(
  id           INTEGER primary key,
  year         CHAR(4),
  holiday_str  VARCHAR2(400 CHAR) not null,
  conduct_user VARCHAR2(255 CHAR),
  conduct_time CHAR(14)
);
-- Add comments to the table 
comment on table COMM_HOLIDAY_INFO
  is '节假日信息表';
-- Add comments to the columns 
comment on column COMM_HOLIDAY_INFO.id
  is '主键';
comment on column COMM_HOLIDAY_INFO.year
  is '年份YYYYMMDD';
comment on column COMM_HOLIDAY_INFO.holiday_str
  is '节假日描述字符串';
comment on column COMM_HOLIDAY_INFO.conduct_user
  is '操作人';
comment on column COMM_HOLIDAY_INFO.conduct_time
  is '操作时间';


CREATE SEQUENCE SEQ_RESOURCE_INFO START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 CACHE 20 ORDER;

-- Create table
create table RESOURCE_INFO
(
  id              INTEGER primary key,
  resource_name   VARCHAR2(255),
  access_path     VARCHAR2(255),
  parent_id       INTEGER,
  resource_grade  INTEGER,
  resource_order  INTEGER,
  resource_attr   CHAR(2),
  resource_type   VARCHAR2(20),
  resource_image  VARCHAR2(255),
  resource_status CHAR(2),
  resource_component VARCHAR2(255),
  resource_desc   VARCHAR2(255)
);
-- Add comments to the table 
comment on table RESOURCE_INFO
  is '系统资源菜单表';
-- Add comments to the columns 
comment on column RESOURCE_INFO.id
  is '主键';
comment on column RESOURCE_INFO.resource_name
  is '资源名称';
comment on column RESOURCE_INFO.access_path
  is '访问路径';
comment on column RESOURCE_INFO.parent_id
  is '上级菜单';
comment on column RESOURCE_INFO.resource_grade
  is '菜单级别';
comment on column RESOURCE_INFO.resource_order
  is '菜单顺序';
comment on column RESOURCE_INFO.resource_attr
  is '菜单属性';
comment on column RESOURCE_INFO.resource_type
  is '菜单类型';
comment on column RESOURCE_INFO.resource_image
  is '菜单图标';
comment on column RESOURCE_INFO.resource_status
  is '菜单状态';
comment on column RESOURCE_INFO.resource_component
  is '组件信息';
comment on column RESOURCE_INFO.resource_desc
  is '描述信息';


insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (1, '首页', '', 0, 1, 1, '', 'HOME', '', '1 ', 'views/Home/index.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3100, '业务流程', '', 0, 1, 1, '', 'QITA', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3110, '待办任务', 'html/workflow/todoTask.html', 3100, 2, 1, '', 'QITA', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3120, '已办任务', 'html/workflow/doneTask.html', 3100, 2, 2, '', 'QITA', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3200, '流程设置', '', 0, 1, 2, '', 'QITA', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3210, '已部署流程', 'html/workflowManage/process/processDefinition.html', 3200, 2, 2, '', 'QITA', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3220, '待办任务处理', 'html/workflowManage/taskinfo/todoTaskAll.html', 3200, 2, 3, '', 'QITA', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3230, '已办任务查询', 'html/workflowManage/taskinfo/doneTaskAll.html', 3200, 2, 4, '', 'QITA', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3240, '流程设计器', 'html/workflowManage/process/processDesigner.html', 3200, 2, 1, '', 'QITA', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2100, '机构管理', '', 0, 1, 1, '', 'XTGL', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2110, '机构维护', 'html/systemManage/orgManage/OrgInfoConfig.html', 2100, 2, 1, '', 'XTGL', '', '1 ', 'views/OrgManage/orgManage.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2200, '角色管理', '', 0, 1, 2, '', 'XTGL', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2210, '角色维护', 'html/systemManage/roleInfoManage/roleInfo.html', 2200, 2, 1, '', 'XTGL', '', '1 ', 'views/RoleManage/roleManage.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2300, '用户管理', '', 0, 1, 3, '', 'XTGL', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2310, '用户维护', 'html/systemManage/userManage/userset.html', 2300, 2, 1, '', 'XTGL', '', '1 ', 'views/UserManage/userManage.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2311, '密码修改', 'html/systemManage/userManage/exgPwd.html', 2300, 2, 1, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2400, '权限管理', '', 0, 1, 4, '', 'XTGL', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2410, '机构权限维护', 'html/systemManage/resoManager/AuthROrgReso.html', 2400, 2, 1, '', 'XTGL', '', '1 ', 'views/AccessManage/orgAccessManage.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2420, '角色权限维护', 'html/systemManage/resoManager/AuthRRoleReso.html', 2400, 2, 1, '', 'XTGL', '', '1 ', 'views/AccessManage/roleAccessManage.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2430, '用户角色维护', 'html/systemManage/resoManager/AuthRUserRole.html', 2400, 2, 2, '', 'XTGL', '', '1 ', 'views/AccessManage/userAccessManage.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2500, '系统设置', '', 0, 1, 5, '', 'XTGL', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2510, '系统参数管理', 'html/systemManage/paramManage/paramset.html', 2500, 2, 2, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2530, '定时任务管理', 'html/systemManage/paramManage/jobManage.html', 2500, 2, 3, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2540, '节假日管理', 'html/systemManage/holidayManage/holidayDetail.html', 2500, 2, 6, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2560, '字典定义', 'html/systemManage/paramManage/dataDicConfig.html', 2500, 2, 7, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2570, '系统信息', 'html/systemManage/paramManage/sysInfo.html', 2500, 2, 1, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2580, '批量运行信息', 'html/systemManage/paramManage/sysBatchLog.html', 2500, 2, 5, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2590, '定时任务日志', 'html/systemManage/paramManage/logManage.html', 2500, 2, 4, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2700, '系统监控', '', 0, 1, 7, '', 'XTGL', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2710, '服务监控', 'html/systemManage/monitorManage/serverConsole.html', 2700, 2, 1, '', 'XTGL', '', '1 ', 'views/SystemMonitor/serviceMonitor.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2730, '缓存监控', 'html/systemManage/monitorManage/cacheMonitorManager.html', 2700, 2, 3, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2740, '系统资源监控', 'html/systemManage/monitorManage/jvmMonitor.html', 2700, 2, 4, '', 'XTGL', '', '1 ', 'views/SystemMonitor/systemResourceMonitor.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2750, '交易记录', 'html/systemManage/monitorManage/tradelogManager.html', 2700, 2, 5, '', 'XTGL', '', '1 ', 'views/SystemMonitor/transactionNoteCheck.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2770, '服务契约(原生)', 'swagger-ui.html', 2700, 2, 7, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2780, '服务契约(定制)', 'doc.html', 2700, 2, 8, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2790, '操作记录查看', 'html/businessManage/alterRecordManage.html', 2700, 2, 9, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2791, '表空间使用情况', 'html/systemManage/monitorManage/tableSpaceUse.html', 2700, 2, 10, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2792, '数据表空间占用', 'html/systemManage/monitorManage/tableSpaceTableUse.html', 2700, 2, 11, '', 'XTGL', '', '1 ', 'views/SystemMonitor/tableSpaceOccupy.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2793, '交易概要信息', '', 2700, 2, 12, '', 'XTGL', '', '1 ', 'views/TradeInfo/overviewInfo.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (1100, '请假管理', '#', 0, 1, 2, '', 'YWMK', 'nested', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (1110, '请假申请', 'html/workoff.html', 1100, 2, 1, '', 'YWMK', 'nested', '1 ', 'views/Others/HTML.vue', '');


CREATE SEQUENCE SEQ_USER_ROLE START WITH 2 INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 CACHE 20 ORDER;

-- Create table
create table AUTH_R_USER_ROLE
(
  id      INTEGER primary key,
  user_id INTEGER,
  role_id INTEGER
);
-- Add comments to the table 
comment on table AUTH_R_USER_ROLE
  is '用户角色表';
-- Add comments to the columns 
comment on column AUTH_R_USER_ROLE.id
  is '主键';
comment on column AUTH_R_USER_ROLE.user_id
  is '用户ID';
comment on column AUTH_R_USER_ROLE.role_id
  is '角色ID';

insert into auth_r_user_role (ID, USER_ID, ROLE_ID) values (1, 1, 1);


CREATE SEQUENCE SEQ_ORG_RESO START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 CACHE 20 ORDER;

-- Create table
create table AUTH_R_ORG_RESO
(
  id     INTEGER primary key,
  org_id INTEGER,
  res_id INTEGER
);
-- Add comments to the table 
comment on table AUTH_R_ORG_RESO
  is 'ID';
-- Add comments to the columns 
comment on column AUTH_R_ORG_RESO.id
  is '主键';
comment on column AUTH_R_ORG_RESO.org_id
  is '机构ID';
comment on column AUTH_R_ORG_RESO.res_id
  is '功能ID';

  
CREATE SEQUENCE SEQ_ROLE_RESO START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 CACHE 20 ORDER;

-- Create table
create table AUTH_R_ROLE_RESO
(
  id        INTEGER primary key,
  role_id   INTEGER,
  res_id    INTEGER,
  auth_code CHAR(8) default 11111111
);
-- Add comments to the table 
comment on table AUTH_R_ROLE_RESO
  is '角色功能表';
-- Add comments to the columns 
comment on column AUTH_R_ROLE_RESO.id
  is '主键';
comment on column AUTH_R_ROLE_RESO.role_id
  is '角色ID';
comment on column AUTH_R_ROLE_RESO.res_id
  is '资源ID';
comment on column AUTH_R_ROLE_RESO.auth_code
  is '权限代码';

  
  



CREATE SEQUENCE SEQ_PRODUCT_INFO START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 CACHE 20 ORDER;





CREATE SEQUENCE SEQ_COMM_REPORT_META START WITH 22 INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 CACHE 20 ORDER;

create table COMM_REPORT_META
(
    id             INTEGER primary key,
    report_no        VARCHAR2(20) not null,
    report_name      VARCHAR2(60) not null,
    view_div      VARCHAR2(500) not null,
    report_sql      VARCHAR2(500) not null,
    conduct_user  INTEGER,
    conduct_time  CHAR(14)
);
-- Add comments to the table
comment on table COMM_REPORT_META
    is '报表元数据配置';
-- Add comments to the columns
comment on column COMM_REPORT_META.report_no
    is '报表编号';
comment on column COMM_REPORT_META.report_name
    is '报表名称';




create table COMM_CALENDAR
(
  sys_id    VARCHAR2(40 CHAR) not null,
  data_type VARCHAR2(2 CHAR),
  data_year VARCHAR2(8 CHAR),
  data_str  VARCHAR2(8 CHAR) not null,
  remark    VARCHAR2(100 CHAR)
);
-- Create/Recreate primary, unique and foreign key constraints
alter table COMM_CALENDAR add constraint PK_AOS_COMM_CALENDAR primary key (SYS_ID, DATA_STR);


CREATE SEQUENCE SEQ_COMM_DOCUMENT_CONFIG START WITH 100000000 INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 CACHE 20 ORDER;

create table COMM_DOCUMENT_CONFIG
(
    id             INTEGER primary key,
    file_name      VARCHAR2(256) not null,
    file_url       VARCHAR2(256) not null,
    file_status    VARCHAR2(8) not null,
    conduct_user   INTEGER,
    conduct_time   CHAR(14)
);
-- Add comments to the table
comment on table COMM_DOCUMENT_CONFIG
    is '帮助文档配置';
-- Add comments to the columns
comment on column COMM_DOCUMENT_CONFIG.file_name
    is '文档名称';
comment on column COMM_DOCUMENT_CONFIG.file_url
    is '文档统一资源定位符';
comment on column COMM_DOCUMENT_CONFIG.file_status
    is '文档启用状态';
comment on column COMM_DOCUMENT_CONFIG.conduct_user
    is '操作人';
comment on column COMM_DOCUMENT_CONFIG.conduct_user
    is '操作时间';