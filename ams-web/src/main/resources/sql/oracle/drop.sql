-- Drop table
drop table COMM_DATA_DIC cascade constraints;
-- Drop table
drop table COMM_ORG_INFO cascade constraints;
-- Drop table
drop table COMM_ROLE_INFO cascade constraints;
-- Drop table
drop table COMM_USER_INFO cascade constraints;
-- Drop table
drop table COMM_HOLIDAY_INFO cascade constraints;
-- Drop table
drop table RESOURCE_INFO cascade constraints;
-- Drop table
drop table AUTH_R_USER_ROLE cascade constraints;
-- Drop table
drop table AUTH_R_ORG_RESO cascade constraints;
-- Drop table
drop table AUTH_R_ROLE_RESO cascade constraints;

drop table COMM_CALENDAR cascade constraints;

-- Drop table
drop table COMM_DOCUMENT_CONFIG cascade constraints;