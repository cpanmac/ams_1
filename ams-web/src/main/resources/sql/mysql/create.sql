/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2020-7-28 18:47:33                           */
/*==============================================================*/


drop table if exists AUTH_R_ORG_RESO;

drop table if exists AUTH_R_ROLE_RESO;

drop table if exists AUTH_R_USER_ROLE;

drop table if exists COMM_DATA_DIC;

drop table if exists COMM_HOLIDAY_INFO;

drop table if exists COMM_ORG_INFO;

drop table if exists COMM_REPORT_META;

drop table if exists COMM_ROLE_INFO;

drop table if exists COMM_USER_INFO;

drop table if exists RESOURCE_INFO;

/*==============================================================*/
/* Table: AUTH_R_ORG_RESO                                       */
/*==============================================================*/
create table AUTH_R_ORG_RESO
(
   ID                   int not null auto_increment comment '主键',
   ORG_ID               int comment '机构ID',
   RES_ID               int comment '功能ID',
   primary key (ID)
);

alter table AUTH_R_ORG_RESO comment 'ID';

/*==============================================================*/
/* Table: AUTH_R_ROLE_RESO                                      */
/*==============================================================*/
create table AUTH_R_ROLE_RESO
(
   ID                   int not null auto_increment comment '主键',
   ROLE_ID              int comment '角色ID',
   RES_ID               int comment '资源ID',
   AUTH_CODE            char(8) default '11111111' comment '权限代码',
   primary key (ID)
);

alter table AUTH_R_ROLE_RESO comment '角色功能表';

/*==============================================================*/
/* Table: AUTH_R_USER_ROLE                                      */
/*==============================================================*/
create table AUTH_R_USER_ROLE
(
   ID                   int not null auto_increment comment '主键',
   USER_ID              int comment '用户ID',
   ROLE_ID              int comment '角色ID',
   primary key (ID)
);

alter table AUTH_R_USER_ROLE comment '用户角色表';

/*==============================================================*/
/* Table: COMM_DATA_DIC                                         */
/*==============================================================*/
create table COMM_DATA_DIC
(
   ID                   int not null auto_increment,
   DATA_NO              varchar(20) not null,
   DATA_NAME            varchar(60) not null,
   DATA_TYPE            varchar(20) not null,
   DATA_TYPE_NAME       varchar(30) not null,
   DATA_ATTR            char(2) not null default '01' comment '字典属性 01本地字典 02外部字典',
   DATA_LOGIC           varchar(512) comment '外部关联逻辑',
   primary key (ID)
);

alter table COMM_DATA_DIC comment '数据字典表';

create index idx_datadic_type on comm_data_dic(data_type);

/*==============================================================*/
/* Table: COMM_HOLIDAY_INFO                                     */
/*==============================================================*/
create table COMM_HOLIDAY_INFO
(
   ID                   int not null auto_increment comment '主键',
   YEAR                 char(4) comment '年份YYYYMMDD',
   HOLIDAY_STR          varchar(400) not null comment '节假日描述字符串',
   CONDUCT_USER         varchar(255) comment '操作人',
   CONDUCT_TIME         char(14) comment '操作时间',
   primary key (ID)
);

alter table COMM_HOLIDAY_INFO comment '节假日信息表';

/*==============================================================*/
/* Table: COMM_ORG_INFO                                         */
/*==============================================================*/
create table COMM_ORG_INFO
(
   ID                   int not null auto_increment comment 'ID',
   PERIODS              varchar(20) comment '期数版本',
   ORG_NO               varchar(10) comment '机构号',
   ORG_NAME             varchar(50) comment '机构名称',
   ORG_GRADE            int comment '机构级别',
   STATUS               varchar(2) comment '状态',
   PARENT_ID            int comment '上级机构',
   ORG_TYPE             varchar(2) comment '机构类型',
   ORG_ALIAS            varchar(64) comment '机构别名',
   REMARK               varchar(128) comment '说明',
   VALIDE_DATE          char(8) comment '生效日期',
   INVALIDE_DATE        char(8) comment '失效日期',
   CONDUCT_USER         int comment '操作人',
   CONDUCT_TIME         char(14) comment '操作时间',
   primary key (ID)
);

alter table COMM_ORG_INFO comment '机构信息表';

insert into COMM_ORG_INFO (ID, PERIODS, ORG_NO, ORG_NAME, ORG_GRADE, STATUS, PARENT_ID, ORG_TYPE, VALIDE_DATE, INVALIDE_DATE, CONDUCT_USER, CONDUCT_TIME)
values (1, '1', '9900', '总行', 1, '01', 0, '0', '20170101', '20170101', 1, '20170101000000');


/*==============================================================*/
/* Table: COMM_REPORT_META                                      */
/*==============================================================*/
create table COMM_REPORT_META
(
   ID                   int not null auto_increment,
   REPORT_NO            varchar(20) not null comment '报表编号',
   REPORT_NAME          varchar(60) not null comment '报表名称',
   VIEW_DIV             varchar(500) not null,
   REPORT_SQL           varchar(500) not null,
   CONDUCT_USER         int,
   CONDUCT_TIME         char(14),
   primary key (ID)
);

alter table COMM_REPORT_META comment '报表元数据配置';

/*==============================================================*/
/* Table: COMM_ROLE_INFO                                        */
/*==============================================================*/
create table COMM_ROLE_INFO
(
   ID                   int not null auto_increment comment '主键',
   ROLE_CODE            varchar(32) comment '英文代码',
   ROLE_NAME            varchar(64) comment '角色描述',
   ROLE_DESC            varchar(256),
   STATUS               char(2) comment '状态',
   primary key (ID)
);
alter table COMM_ROLE_INFO comment '角色信息表';

insert into COMM_ROLE_INFO (ID, ROLE_CODE, ROLE_NAME, ROLE_DESC, STATUS)
values (1, 'admin', '系统管理员', '超级管理员', '01');
/*==============================================================*/
/* Table: COMM_USER_INFO                                        */
/*==============================================================*/
create table COMM_USER_INFO
(
   ID                   int not null auto_increment comment '主键',
   LOGIN_NAME           varchar(20) comment '登录名',
   PASS_WORD            varchar(50) comment '登录密码',
   USER_NAME            varchar(50) comment '用户姓名',
   ORG_ID               int comment '机构ID',
   STATUS               varchar(2) comment '状态',
   EMP_NO               varchar(20) comment '员工号',
   LOGIN_TIME           char(14) comment '登录时间',
   LOGIN_IP             varchar(20) comment '登录IP',
   USER_CODE            varchar(10) comment '用户代码',
   EMAIL                varchar(50) comment '邮箱',
   CONDUCT_USER         int comment '操作人',
   CONDUCT_TIME         char(14) comment '操作时间',
   primary key (ID)
);

alter table COMM_USER_INFO comment '用户信息表';

insert into COMM_USER_INFO (ID, LOGIN_NAME, PASS_WORD, USER_NAME, ORG_ID, STATUS, EMP_NO, LOGIN_TIME, LOGIN_IP, USER_CODE, CONDUCT_USER, CONDUCT_TIME)
values (1, 'admin', '{MD5}8ddcff3a80f4189ca1c9d4d902c3c909', 'admin', 1, '1', '0', '20180810142824', '0.0.0.0', null, 1, '20180712162632');



/*==============================================================*/
/* Table: RESOURCE_INFO                                         */
/*==============================================================*/
create table RESOURCE_INFO
(
   ID                   int not null auto_increment comment '主键',
   RESOURCE_NAME        varchar(255) comment '资源名称',
   ACCESS_PATH          varchar(255) comment '访问路径',
   PARENT_ID            int comment '上级菜单',
   RESOURCE_GRADE       int comment '菜单级别',
   RESOURCE_ORDER       int comment '菜单顺序',
   RESOURCE_ATTR        char(2) comment '菜单属性',
   RESOURCE_TYPE        varchar(20) comment '菜单类型',
   RESOURCE_IMAGE       varchar(255) comment '菜单图标',
   RESOURCE_STATUS      char(2) comment '菜单状态',
   RESOURCE_COMPONENT   varchar(255) comment '组件信息',
   RESOURCE_DESC        varchar(255) comment '描述信息',
   primary key (ID)
);

alter table RESOURCE_INFO comment '系统资源菜单表';

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (1, '首页', '', 0, 1, 1, '', 'HOME', '', '1 ', 'views/Home/index.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3100, '业务流程', '', 0, 1, 1, '', 'QITA', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3110, '待办任务', 'html/workflow/todoTask.html', 3100, 2, 1, '', 'QITA', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3120, '已办任务', 'html/workflow/doneTask.html', 3100, 2, 2, '', 'QITA', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3200, '流程设置', '', 0, 1, 2, '', 'QITA', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3210, '已部署流程', 'html/workflowManage/process/processDefinition.html', 3200, 2, 2, '', 'QITA', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3220, '待办任务处理', 'html/workflowManage/taskinfo/todoTaskAll.html', 3200, 2, 3, '', 'QITA', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3230, '已办任务查询', 'html/workflowManage/taskinfo/doneTaskAll.html', 3200, 2, 4, '', 'QITA', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (3240, '流程设计器', 'html/workflowManage/process/processDesigner.html', 3200, 2, 1, '', 'QITA', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2100, '机构管理', '', 0, 1, 1, '', 'XTGL', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2110, '机构维护', 'html/systemManage/orgManage/OrgInfoConfig.html', 2100, 2, 1, '', 'XTGL', '', '1 ', 'views/OrgManage/orgManage.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2200, '角色管理', '', 0, 1, 2, '', 'XTGL', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2210, '角色维护', 'html/systemManage/roleInfoManage/roleInfo.html', 2200, 2, 1, '', 'XTGL', '', '1 ', 'views/RoleManage/roleManage.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2300, '用户管理', '', 0, 1, 3, '', 'XTGL', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2310, '用户维护', 'html/systemManage/userManage/userset.html', 2300, 2, 1, '', 'XTGL', '', '1 ', 'views/UserManage/userManage.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2311, '密码修改', 'html/systemManage/userManage/exgPwd.html', 2300, 2, 1, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2400, '权限管理', '', 0, 1, 4, '', 'XTGL', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2410, '机构权限维护', 'html/systemManage/resoManager/AuthROrgReso.html', 2400, 2, 1, '', 'XTGL', '', '1 ', 'views/AccessManage/orgAccessManage.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2420, '角色权限维护', 'html/systemManage/resoManager/AuthRRoleReso.html', 2400, 2, 1, '', 'XTGL', '', '1 ', 'views/AccessManage/roleAccessManage.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2430, '用户角色维护', 'html/systemManage/resoManager/AuthRUserRole.html', 2400, 2, 2, '', 'XTGL', '', '1 ', 'views/AccessManage/userAccessManage.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2500, '系统设置', '', 0, 1, 5, '', 'XTGL', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2510, '系统参数管理', 'html/systemManage/paramManage/paramset.html', 2500, 2, 2, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2530, '定时任务管理', 'html/systemManage/paramManage/jobManage.html', 2500, 2, 3, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2540, '节假日管理', 'html/systemManage/holidayManage/holidayDetail.html', 2500, 2, 6, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2560, '字典定义', 'html/systemManage/paramManage/dataDicConfig.html', 2500, 2, 7, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2570, '系统信息', 'html/systemManage/paramManage/sysInfo.html', 2500, 2, 1, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2580, '批量运行信息', 'html/systemManage/paramManage/sysBatchLog.html', 2500, 2, 5, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2590, '定时任务日志', 'html/systemManage/paramManage/logManage.html', 2500, 2, 4, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2700, '系统监控', '', 0, 1, 7, '', 'XTGL', '', '1 ', '', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2710, '服务监控', 'html/systemManage/monitorManage/serverConsole.html', 2700, 2, 1, '', 'XTGL', '', '1 ', 'views/SystemMonitor/serviceMonitor.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2730, '缓存监控', 'html/systemManage/monitorManage/cacheMonitorManager.html', 2700, 2, 3, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2740, '系统资源监控', 'html/systemManage/monitorManage/jvmMonitor.html', 2700, 2, 4, '', 'XTGL', '', '1 ', 'views/SystemMonitor/systemResourceMonitor.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2750, '交易记录', 'html/systemManage/monitorManage/tradelogManager.html', 2700, 2, 5, '', 'XTGL', '', '1 ', 'views/SystemMonitor/transactionNoteCheck.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2770, '服务契约(原生)', 'swagger-ui.html', 2700, 2, 7, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2780, '服务契约(定制)', 'doc.html', 2700, 2, 8, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2790, '操作记录查看', 'html/businessManage/alterRecordManage.html', 2700, 2, 9, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2791, '表空间使用情况', 'html/systemManage/monitorManage/tableSpaceUse.html', 2700, 2, 10, '', 'XTGL', '', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2792, '数据表空间占用', 'html/systemManage/monitorManage/tableSpaceTableUse.html', 2700, 2, 11, '', 'XTGL', '', '1 ', 'views/SystemMonitor/tableSpaceOccupy.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (2793, '交易概要信息', '', 2700, 2, 12, '', 'XTGL', '', '1 ', 'views/TradeInfo/overviewInfo.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (1100, '请假管理', '#', 0, 1, 2, '', 'YWMK', 'nested', '1 ', 'views/Others/HTML.vue', '');

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_COMPONENT, RESOURCE_DESC)
values (1110, '请假申请', 'html/workoff.html', 1100, 2, 1, '', 'YWMK', 'nested', '1 ', 'views/Others/HTML.vue', '');



create table COMM_CALENDAR
(
  sys_id    VARCHAR2(40 CHAR) not null,
  data_type VARCHAR2(2 CHAR),
  data_year VARCHAR2(8 CHAR),
  data_str  VARCHAR2(8 CHAR) not null,
  remark    VARCHAR2(100 CHAR)
);
-- Create/Recreate primary, unique and foreign key constraints
alter table COMM_CALENDAR add constraint PK_AOS_COMM_CALENDAR primary key (SYS_ID, DATA_STR);