package com.njcb.ams.service;

import javax.annotation.Resource;

import com.njcb.ams.BaseTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.njcb.ams.portal.WebApplication;

/**
 * 数据字段管理类
 * 
 * @author LIUYANLONG
 *
 */
@ActiveProfiles("test") // 指定使用application-test.yml
public class DataDicServiceTest extends BaseTest {
	@Resource
	private DataDicService dataDicService;

	@Test
	public void testRegisterUser() {
		dataDicService.reloadDataDic();
	}
}
