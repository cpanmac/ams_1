/**
 * 描述：
 * Copyright：Copyright （c） 2017
 * Company：南京银行
 * @author xxjsb036 
 * @version 1.0 2017年3月13日
 * @see history
 * 2017年3月13日
 */

package com.njcb.ams.service;

import com.njcb.ams.BaseTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.njcb.ams.portal.WebApplication;
import com.njcb.ams.support.exception.ExceptionUtil;

/**
 * @author 刘彦龙
 *
 *         类功能描述：缓存监控
 *
 */
@ActiveProfiles("test") // 指定使用application-test.yml
public class CacheMonitorServiceTest extends BaseTest {
	@Autowired
	private CacheMonitorService service;

	@Test
	public void getCacheNames() {
		try {
			service.getCacheNames();
		} catch (Exception e) {
			ExceptionUtil.printStackTrace(e);
		}

	}

}
