package com.njcb.ams;

import com.njcb.ams.factory.comm.DataBus;
import com.njcb.ams.factory.comm.WebGlobalInfo;
import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.portal.WebApplication;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {WebApplication.class})
@ActiveProfiles("test") // 指定使用application-test.yml
public class BaseTest {
    @Autowired
    public WebApplicationContext context;

    @Before
    public void init() {
        WebGlobalInfo globalInfo = new WebGlobalInfo();
        globalInfo.setUserId(1);
        globalInfo.setUserAttr(SysBaseDefine.USER_ATTR_REAL);
        globalInfo.setLoginName("1007329");
        globalInfo.setUserName("1007329");
        globalInfo.setOrgNo("9900");
        globalInfo.setOrgName("总行");
        DataBus.setCurrentInstance(globalInfo);
        AppContext.setContext(context);
    }

    @After
    public void after() {
        System.out.println("测试结束");
    }

    @Test
    public void test() {
        System.out.println("测试示例");
    }
}
