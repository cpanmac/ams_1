package com.njcb.ams.web.controller;

import com.njcb.ams.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.portal.WebApplication;
import com.njcb.ams.store.stable.TradeConsoleService;

@ActiveProfiles("test") // 指定使用application-test.yml
public class HomePageControllerTest extends BaseTest {

	@Autowired
	private TradeConsoleService tradeConsoleService;
	
    private MockMvc mockMvc;

    @Before
    public void setupMockMvc() throws Exception {
    	tradeConsoleService.loadServerConsole();
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }
    
	@Test
	public void testLogin() throws Exception {
		MockHttpServletRequestBuilder servletRequest = MockMvcRequestBuilders.get("/");
		ResultActions rs = mockMvc.perform(servletRequest);
		rs.andExpect(MockMvcResultMatchers.status().isOk());
		rs.andDo(MockMvcResultHandlers.print());
	}


}
