package com.njcb.ams.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.njcb.ams.BaseTest;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.store.stable.TradeConsoleService;
import com.njcb.ams.support.exception.ExceptionUtil;

public class LoadMenuControllerTest extends BaseTest {

	@Autowired
	private TradeConsoleService tradeConsoleService;
	
    private MockMvc mockMvc;

    @Before
    public void setupMockMvc() throws Exception {
    	tradeConsoleService.loadServerConsole();
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }
    
//    @Test
	public void testLoadMenu() throws Exception {
		try {
			mockMvc.perform(get("/loadResoMenu.do")).andExpect(status().isOk()).andDo(print());
		} catch (Exception e) {
			ExceptionUtil.printStackTrace(e);
			throw e;
		}

	}

}
