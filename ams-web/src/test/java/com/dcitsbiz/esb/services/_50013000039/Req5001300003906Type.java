
package com.dcitsbiz.esb.services._50013000039;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.dcitsbiz.esb.metadata.ReqSysHeadType;


/**
 * <p>Req5001300003906Type complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="Req5001300003906Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReqSysHead" type="{http://esb.dcitsbiz.com/metadata}ReqSysHeadType"/&gt;
 *         &lt;element name="ReqAppHead" type="{http://esb.dcitsbiz.com/services/50013000039}ReqAppHeadType"/&gt;
 *         &lt;element name="ReqAppBody" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ydpt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="billid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="billtype" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="zb1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="zb2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="zb3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="zb4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="zb5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Req5001300003906Type", propOrder = {
    "reqSysHead",
    "reqAppHead",
    "reqAppBody"
})
public class Req5001300003906Type {

    @XmlElement(name = "ReqSysHead", required = true)
    protected ReqSysHeadType reqSysHead;
    @XmlElement(name = "ReqAppHead", required = true)
    protected ReqAppHeadType reqAppHead;
    @XmlElement(name = "ReqAppBody")
    protected Req5001300003906Type.ReqAppBody reqAppBody;

    /**
     * 获取reqSysHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ReqSysHeadType }
     *     
     */
    public ReqSysHeadType getReqSysHead() {
        return reqSysHead;
    }

    /**
     * 设置reqSysHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ReqSysHeadType }
     *     
     */
    public void setReqSysHead(ReqSysHeadType value) {
        this.reqSysHead = value;
    }

    /**
     * 获取reqAppHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ReqAppHeadType }
     *     
     */
    public ReqAppHeadType getReqAppHead() {
        return reqAppHead;
    }

    /**
     * 设置reqAppHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ReqAppHeadType }
     *     
     */
    public void setReqAppHead(ReqAppHeadType value) {
        this.reqAppHead = value;
    }

    /**
     * 获取reqAppBody属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Req5001300003906Type.ReqAppBody }
     *     
     */
    public Req5001300003906Type.ReqAppBody getReqAppBody() {
        return reqAppBody;
    }

    /**
     * 设置reqAppBody属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Req5001300003906Type.ReqAppBody }
     *     
     */
    public void setReqAppBody(Req5001300003906Type.ReqAppBody value) {
        this.reqAppBody = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ydpt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="billid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="billtype" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="zb1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="zb2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="zb3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="zb4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="zb5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ydpt",
        "billid",
        "billtype",
        "zb1",
        "zb2",
        "zb3",
        "zb4",
        "zb5"
    })
    public static class ReqAppBody {

        protected String ydpt;
        protected String billid;
        protected String billtype;
        protected String zb1;
        protected String zb2;
        protected String zb3;
        protected String zb4;
        protected String zb5;

        /**
         * 获取ydpt属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getYdpt() {
            return ydpt;
        }

        /**
         * 设置ydpt属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setYdpt(String value) {
            this.ydpt = value;
        }

        /**
         * 获取billid属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillid() {
            return billid;
        }

        /**
         * 设置billid属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillid(String value) {
            this.billid = value;
        }

        /**
         * 获取billtype属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBilltype() {
            return billtype;
        }

        /**
         * 设置billtype属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBilltype(String value) {
            this.billtype = value;
        }

        /**
         * 获取zb1属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZb1() {
            return zb1;
        }

        /**
         * 设置zb1属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZb1(String value) {
            this.zb1 = value;
        }

        /**
         * 获取zb2属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZb2() {
            return zb2;
        }

        /**
         * 设置zb2属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZb2(String value) {
            this.zb2 = value;
        }

        /**
         * 获取zb3属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZb3() {
            return zb3;
        }

        /**
         * 设置zb3属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZb3(String value) {
            this.zb3 = value;
        }

        /**
         * 获取zb4属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZb4() {
            return zb4;
        }

        /**
         * 设置zb4属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZb4(String value) {
            this.zb4 = value;
        }

        /**
         * 获取zb5属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZb5() {
            return zb5;
        }

        /**
         * 设置zb5属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZb5(String value) {
            this.zb5 = value;
        }

    }

}
