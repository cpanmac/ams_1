
package com.dcitsbiz.esb.services._50013000039;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.dcitsbiz.esb.fds.RspLocalHeadType;
import com.dcitsbiz.esb.metadata.RspSysHeadType;


/**
 * <p>Rsp5001300003905Type complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="Rsp5001300003905Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RspSysHead" type="{http://esb.dcitsbiz.com/metadata}RspSysHeadType"/&gt;
 *         &lt;element name="RspAppHead" type="{http://esb.dcitsbiz.com/services/50013000039}RspAppHeadType"/&gt;
 *         &lt;element name="RspLocalHead" type="{http://esb.dcitsbiz.com/FDS}RspLocalHeadType"/&gt;
 *         &lt;element name="RspAppBody" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="TotNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *                   &lt;element name="RetNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *                   &lt;element name="OffSet" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *                   &lt;element name="Acc_List" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="ContractNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="PrdManager" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="ClientNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="ClientName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="PrdManagerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="TransDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="TransTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="CloseDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Rsp5001300003905Type", propOrder = {
    "rspSysHead",
    "rspAppHead",
    "rspLocalHead",
    "rspAppBody"
})
public class Rsp5001300003905Type {

    @XmlElement(name = "RspSysHead", required = true)
    protected RspSysHeadType rspSysHead;
    @XmlElement(name = "RspAppHead", required = true)
    protected RspAppHeadType rspAppHead;
    @XmlElement(name = "RspLocalHead", required = true)
    protected RspLocalHeadType rspLocalHead;
    @XmlElement(name = "RspAppBody")
    protected Rsp5001300003905Type.RspAppBody rspAppBody;

    /**
     * 获取rspSysHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RspSysHeadType }
     *     
     */
    public RspSysHeadType getRspSysHead() {
        return rspSysHead;
    }

    /**
     * 设置rspSysHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RspSysHeadType }
     *     
     */
    public void setRspSysHead(RspSysHeadType value) {
        this.rspSysHead = value;
    }

    /**
     * 获取rspAppHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RspAppHeadType }
     *     
     */
    public RspAppHeadType getRspAppHead() {
        return rspAppHead;
    }

    /**
     * 设置rspAppHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RspAppHeadType }
     *     
     */
    public void setRspAppHead(RspAppHeadType value) {
        this.rspAppHead = value;
    }

    /**
     * 获取rspLocalHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RspLocalHeadType }
     *     
     */
    public RspLocalHeadType getRspLocalHead() {
        return rspLocalHead;
    }

    /**
     * 设置rspLocalHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RspLocalHeadType }
     *     
     */
    public void setRspLocalHead(RspLocalHeadType value) {
        this.rspLocalHead = value;
    }

    /**
     * 获取rspAppBody属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Rsp5001300003905Type.RspAppBody }
     *     
     */
    public Rsp5001300003905Type.RspAppBody getRspAppBody() {
        return rspAppBody;
    }

    /**
     * 设置rspAppBody属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Rsp5001300003905Type.RspAppBody }
     *     
     */
    public void setRspAppBody(Rsp5001300003905Type.RspAppBody value) {
        this.rspAppBody = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="TotNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
     *         &lt;element name="RetNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
     *         &lt;element name="OffSet" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
     *         &lt;element name="Acc_List" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="ContractNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="PrdManager" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="ClientNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="ClientName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="PrdManagerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="TransDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="TransTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="CloseDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "totNum",
        "retNum",
        "offSet",
        "accList"
    })
    public static class RspAppBody {

        @XmlElement(name = "TotNum")
        protected Integer totNum;
        @XmlElement(name = "RetNum")
        protected Integer retNum;
        @XmlElement(name = "OffSet")
        protected Integer offSet;
        @XmlElement(name = "Acc_List", required = true)
        protected List<Rsp5001300003905Type.RspAppBody.AccList> accList;

        /**
         * 获取totNum属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getTotNum() {
            return totNum;
        }

        /**
         * 设置totNum属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setTotNum(Integer value) {
            this.totNum = value;
        }

        /**
         * 获取retNum属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getRetNum() {
            return retNum;
        }

        /**
         * 设置retNum属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setRetNum(Integer value) {
            this.retNum = value;
        }

        /**
         * 获取offSet属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getOffSet() {
            return offSet;
        }

        /**
         * 设置offSet属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setOffSet(Integer value) {
            this.offSet = value;
        }

        /**
         * Gets the value of the accList property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the accList property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAccList().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Rsp5001300003905Type.RspAppBody.AccList }
         * 
         * 
         */
        public List<Rsp5001300003905Type.RspAppBody.AccList> getAccList() {
            if (accList == null) {
                accList = new ArrayList<Rsp5001300003905Type.RspAppBody.AccList>();
            }
            return this.accList;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="ContractNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="PrdManager" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="ClientNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="ClientName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="PrdManagerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="TransDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="TransTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="CloseDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "contractNo",
            "prdManager",
            "clientNo",
            "clientName",
            "prdManagerName",
            "transDate",
            "transTime",
            "closeDate",
            "status",
            "statusName"
        })
        public static class AccList {

            @XmlElement(name = "ContractNo")
            protected String contractNo;
            @XmlElement(name = "PrdManager")
            protected String prdManager;
            @XmlElement(name = "ClientNo")
            protected String clientNo;
            @XmlElement(name = "ClientName")
            protected String clientName;
            @XmlElement(name = "PrdManagerName")
            protected String prdManagerName;
            @XmlElement(name = "TransDate")
            protected String transDate;
            @XmlElement(name = "TransTime")
            protected String transTime;
            @XmlElement(name = "CloseDate")
            protected String closeDate;
            @XmlElement(name = "Status")
            protected String status;
            @XmlElement(name = "StatusName")
            protected String statusName;

            /**
             * 获取contractNo属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContractNo() {
                return contractNo;
            }

            /**
             * 设置contractNo属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContractNo(String value) {
                this.contractNo = value;
            }

            /**
             * 获取prdManager属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrdManager() {
                return prdManager;
            }

            /**
             * 设置prdManager属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrdManager(String value) {
                this.prdManager = value;
            }

            /**
             * 获取clientNo属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getClientNo() {
                return clientNo;
            }

            /**
             * 设置clientNo属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setClientNo(String value) {
                this.clientNo = value;
            }

            /**
             * 获取clientName属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getClientName() {
                return clientName;
            }

            /**
             * 设置clientName属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setClientName(String value) {
                this.clientName = value;
            }

            /**
             * 获取prdManagerName属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrdManagerName() {
                return prdManagerName;
            }

            /**
             * 设置prdManagerName属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrdManagerName(String value) {
                this.prdManagerName = value;
            }

            /**
             * 获取transDate属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTransDate() {
                return transDate;
            }

            /**
             * 设置transDate属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTransDate(String value) {
                this.transDate = value;
            }

            /**
             * 获取transTime属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTransTime() {
                return transTime;
            }

            /**
             * 设置transTime属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTransTime(String value) {
                this.transTime = value;
            }

            /**
             * 获取closeDate属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCloseDate() {
                return closeDate;
            }

            /**
             * 设置closeDate属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCloseDate(String value) {
                this.closeDate = value;
            }

            /**
             * 获取status属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatus() {
                return status;
            }

            /**
             * 设置status属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatus(String value) {
                this.status = value;
            }

            /**
             * 获取statusName属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusName() {
                return statusName;
            }

            /**
             * 设置statusName属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusName(String value) {
                this.statusName = value;
            }

        }

    }

}
