
package com.dcitsbiz.esb.services._50013000039;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.dcitsbiz.esb.metadata.ReqSysHeadType;


/**
 * <p>Req5001300003901Type complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="Req5001300003901Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReqSysHead" type="{http://esb.dcitsbiz.com/metadata}ReqSysHeadType"/&gt;
 *         &lt;element name="ReqAppHead" type="{http://esb.dcitsbiz.com/services/50013000039}ReqAppHeadType"/&gt;
 *         &lt;element name="ReqAppBody" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="conNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Req5001300003901Type", propOrder = {
    "reqSysHead",
    "reqAppHead",
    "reqAppBody"
})
public class Req5001300003901Type {

    @XmlElement(name = "ReqSysHead", required = true)
    protected ReqSysHeadType reqSysHead;
    @XmlElement(name = "ReqAppHead", required = true)
    protected ReqAppHeadType reqAppHead;
    @XmlElement(name = "ReqAppBody")
    protected Req5001300003901Type.ReqAppBody reqAppBody;

    /**
     * 获取reqSysHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ReqSysHeadType }
     *     
     */
    public ReqSysHeadType getReqSysHead() {
        return reqSysHead;
    }

    /**
     * 设置reqSysHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ReqSysHeadType }
     *     
     */
    public void setReqSysHead(ReqSysHeadType value) {
        this.reqSysHead = value;
    }

    /**
     * 获取reqAppHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ReqAppHeadType }
     *     
     */
    public ReqAppHeadType getReqAppHead() {
        return reqAppHead;
    }

    /**
     * 设置reqAppHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ReqAppHeadType }
     *     
     */
    public void setReqAppHead(ReqAppHeadType value) {
        this.reqAppHead = value;
    }

    /**
     * 获取reqAppBody属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Req5001300003901Type.ReqAppBody }
     *     
     */
    public Req5001300003901Type.ReqAppBody getReqAppBody() {
        return reqAppBody;
    }

    /**
     * 设置reqAppBody属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Req5001300003901Type.ReqAppBody }
     *     
     */
    public void setReqAppBody(Req5001300003901Type.ReqAppBody value) {
        this.reqAppBody = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="conNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "conNo"
    })
    public static class ReqAppBody {

        protected String conNo;

        /**
         * 获取conNo属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConNo() {
            return conNo;
        }

        /**
         * 设置conNo属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConNo(String value) {
            this.conNo = value;
        }

    }

}
