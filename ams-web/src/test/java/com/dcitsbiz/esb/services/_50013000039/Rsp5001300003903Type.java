
package com.dcitsbiz.esb.services._50013000039;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.dcitsbiz.esb.metadata.RspSysHeadType;


/**
 * <p>Rsp5001300003903Type complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="Rsp5001300003903Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RspSysHead" type="{http://esb.dcitsbiz.com/metadata}RspSysHeadType"/&gt;
 *         &lt;element name="RspAppHead" type="{http://esb.dcitsbiz.com/services/50013000039}RspAppHeadType"/&gt;
 *         &lt;element name="RspAppBody" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="MFCustomerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CustomerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="Attribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="Attribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="lsthetxinx" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="ContractSerialno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="SerialNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="BusinessSum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="Balance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="PutOutDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="Maturity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="TermMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="AccountNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="BaseRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="AdjustRateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="PayMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="CorpusPayMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="BaseRateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="RateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="RateFloatType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="RateFloat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="UseBalance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="Attribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="Attribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="Attribute5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Rsp5001300003903Type", propOrder = {
    "rspSysHead",
    "rspAppHead",
    "rspAppBody"
})
public class Rsp5001300003903Type {

    @XmlElement(name = "RspSysHead", required = true)
    protected RspSysHeadType rspSysHead;
    @XmlElement(name = "RspAppHead", required = true)
    protected RspAppHeadType rspAppHead;
    @XmlElement(name = "RspAppBody")
    protected Rsp5001300003903Type.RspAppBody rspAppBody;

    /**
     * 获取rspSysHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RspSysHeadType }
     *     
     */
    public RspSysHeadType getRspSysHead() {
        return rspSysHead;
    }

    /**
     * 设置rspSysHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RspSysHeadType }
     *     
     */
    public void setRspSysHead(RspSysHeadType value) {
        this.rspSysHead = value;
    }

    /**
     * 获取rspAppHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RspAppHeadType }
     *     
     */
    public RspAppHeadType getRspAppHead() {
        return rspAppHead;
    }

    /**
     * 设置rspAppHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RspAppHeadType }
     *     
     */
    public void setRspAppHead(RspAppHeadType value) {
        this.rspAppHead = value;
    }

    /**
     * 获取rspAppBody属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Rsp5001300003903Type.RspAppBody }
     *     
     */
    public Rsp5001300003903Type.RspAppBody getRspAppBody() {
        return rspAppBody;
    }

    /**
     * 设置rspAppBody属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Rsp5001300003903Type.RspAppBody }
     *     
     */
    public void setRspAppBody(Rsp5001300003903Type.RspAppBody value) {
        this.rspAppBody = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="MFCustomerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CustomerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="Attribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="Attribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="lsthetxinx" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="ContractSerialno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="SerialNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="BusinessSum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="Balance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="PutOutDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="Maturity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="TermMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="AccountNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="BaseRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="AdjustRateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="PayMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="CorpusPayMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="BaseRateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="RateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="RateFloatType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="RateFloat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="UseBalance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="Attribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="Attribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="Attribute5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mfCustomerID",
        "customerName",
        "attribute1",
        "attribute2",
        "lsthetxinx"
    })
    public static class RspAppBody {

        @XmlElement(name = "MFCustomerID")
        protected String mfCustomerID;
        @XmlElement(name = "CustomerName")
        protected String customerName;
        @XmlElement(name = "Attribute1")
        protected String attribute1;
        @XmlElement(name = "Attribute2")
        protected String attribute2;
        @XmlElement(required = true)
        protected List<Rsp5001300003903Type.RspAppBody.Lsthetxinx> lsthetxinx;

        /**
         * 获取mfCustomerID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMFCustomerID() {
            return mfCustomerID;
        }

        /**
         * 设置mfCustomerID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMFCustomerID(String value) {
            this.mfCustomerID = value;
        }

        /**
         * 获取customerName属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerName() {
            return customerName;
        }

        /**
         * 设置customerName属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerName(String value) {
            this.customerName = value;
        }

        /**
         * 获取attribute1属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAttribute1() {
            return attribute1;
        }

        /**
         * 设置attribute1属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAttribute1(String value) {
            this.attribute1 = value;
        }

        /**
         * 获取attribute2属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAttribute2() {
            return attribute2;
        }

        /**
         * 设置attribute2属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAttribute2(String value) {
            this.attribute2 = value;
        }

        /**
         * Gets the value of the lsthetxinx property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the lsthetxinx property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLsthetxinx().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Rsp5001300003903Type.RspAppBody.Lsthetxinx }
         * 
         * 
         */
        public List<Rsp5001300003903Type.RspAppBody.Lsthetxinx> getLsthetxinx() {
            if (lsthetxinx == null) {
                lsthetxinx = new ArrayList<Rsp5001300003903Type.RspAppBody.Lsthetxinx>();
            }
            return this.lsthetxinx;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="ContractSerialno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="SerialNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="BusinessSum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="Balance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="PutOutDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="Maturity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="TermMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="AccountNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="BaseRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="AdjustRateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="PayMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="CorpusPayMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="BaseRateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="RateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="RateFloatType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="RateFloat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="UseBalance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="Attribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="Attribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="Attribute5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "contractSerialno",
            "serialNo",
            "businessSum",
            "balance",
            "putOutDate",
            "maturity",
            "termMonth",
            "accountNo",
            "baseRate",
            "adjustRateType",
            "payMode",
            "corpusPayMethod",
            "baseRateType",
            "rateType",
            "rateFloatType",
            "rateFloat",
            "useBalance",
            "attribute3",
            "attribute4",
            "attribute5"
        })
        public static class Lsthetxinx {

            @XmlElement(name = "ContractSerialno")
            protected String contractSerialno;
            @XmlElement(name = "SerialNo")
            protected String serialNo;
            @XmlElement(name = "BusinessSum")
            protected String businessSum;
            @XmlElement(name = "Balance")
            protected String balance;
            @XmlElement(name = "PutOutDate")
            protected String putOutDate;
            @XmlElement(name = "Maturity")
            protected String maturity;
            @XmlElement(name = "TermMonth")
            protected String termMonth;
            @XmlElement(name = "AccountNo")
            protected String accountNo;
            @XmlElement(name = "BaseRate")
            protected String baseRate;
            @XmlElement(name = "AdjustRateType")
            protected String adjustRateType;
            @XmlElement(name = "PayMode")
            protected String payMode;
            @XmlElement(name = "CorpusPayMethod")
            protected String corpusPayMethod;
            @XmlElement(name = "BaseRateType")
            protected String baseRateType;
            @XmlElement(name = "RateType")
            protected String rateType;
            @XmlElement(name = "RateFloatType")
            protected String rateFloatType;
            @XmlElement(name = "RateFloat")
            protected String rateFloat;
            @XmlElement(name = "UseBalance")
            protected String useBalance;
            @XmlElement(name = "Attribute3")
            protected String attribute3;
            @XmlElement(name = "Attribute4")
            protected String attribute4;
            @XmlElement(name = "Attribute5")
            protected String attribute5;

            /**
             * 获取contractSerialno属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContractSerialno() {
                return contractSerialno;
            }

            /**
             * 设置contractSerialno属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContractSerialno(String value) {
                this.contractSerialno = value;
            }

            /**
             * 获取serialNo属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSerialNo() {
                return serialNo;
            }

            /**
             * 设置serialNo属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSerialNo(String value) {
                this.serialNo = value;
            }

            /**
             * 获取businessSum属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBusinessSum() {
                return businessSum;
            }

            /**
             * 设置businessSum属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBusinessSum(String value) {
                this.businessSum = value;
            }

            /**
             * 获取balance属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBalance() {
                return balance;
            }

            /**
             * 设置balance属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBalance(String value) {
                this.balance = value;
            }

            /**
             * 获取putOutDate属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPutOutDate() {
                return putOutDate;
            }

            /**
             * 设置putOutDate属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPutOutDate(String value) {
                this.putOutDate = value;
            }

            /**
             * 获取maturity属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMaturity() {
                return maturity;
            }

            /**
             * 设置maturity属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMaturity(String value) {
                this.maturity = value;
            }

            /**
             * 获取termMonth属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTermMonth() {
                return termMonth;
            }

            /**
             * 设置termMonth属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTermMonth(String value) {
                this.termMonth = value;
            }

            /**
             * 获取accountNo属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAccountNo() {
                return accountNo;
            }

            /**
             * 设置accountNo属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAccountNo(String value) {
                this.accountNo = value;
            }

            /**
             * 获取baseRate属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBaseRate() {
                return baseRate;
            }

            /**
             * 设置baseRate属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBaseRate(String value) {
                this.baseRate = value;
            }

            /**
             * 获取adjustRateType属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAdjustRateType() {
                return adjustRateType;
            }

            /**
             * 设置adjustRateType属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAdjustRateType(String value) {
                this.adjustRateType = value;
            }

            /**
             * 获取payMode属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPayMode() {
                return payMode;
            }

            /**
             * 设置payMode属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPayMode(String value) {
                this.payMode = value;
            }

            /**
             * 获取corpusPayMethod属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCorpusPayMethod() {
                return corpusPayMethod;
            }

            /**
             * 设置corpusPayMethod属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCorpusPayMethod(String value) {
                this.corpusPayMethod = value;
            }

            /**
             * 获取baseRateType属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBaseRateType() {
                return baseRateType;
            }

            /**
             * 设置baseRateType属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBaseRateType(String value) {
                this.baseRateType = value;
            }

            /**
             * 获取rateType属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRateType() {
                return rateType;
            }

            /**
             * 设置rateType属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRateType(String value) {
                this.rateType = value;
            }

            /**
             * 获取rateFloatType属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRateFloatType() {
                return rateFloatType;
            }

            /**
             * 设置rateFloatType属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRateFloatType(String value) {
                this.rateFloatType = value;
            }

            /**
             * 获取rateFloat属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRateFloat() {
                return rateFloat;
            }

            /**
             * 设置rateFloat属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRateFloat(String value) {
                this.rateFloat = value;
            }

            /**
             * 获取useBalance属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUseBalance() {
                return useBalance;
            }

            /**
             * 设置useBalance属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUseBalance(String value) {
                this.useBalance = value;
            }

            /**
             * 获取attribute3属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAttribute3() {
                return attribute3;
            }

            /**
             * 设置attribute3属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAttribute3(String value) {
                this.attribute3 = value;
            }

            /**
             * 获取attribute4属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAttribute4() {
                return attribute4;
            }

            /**
             * 设置attribute4属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAttribute4(String value) {
                this.attribute4 = value;
            }

            /**
             * 获取attribute5属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAttribute5() {
                return attribute5;
            }

            /**
             * 设置attribute5属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAttribute5(String value) {
                this.attribute5 = value;
            }

        }

    }

}
