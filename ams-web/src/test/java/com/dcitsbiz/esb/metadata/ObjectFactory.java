
package com.dcitsbiz.esb.metadata;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.dcitsbiz.esb.metadata package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.dcitsbiz.esb.metadata
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReqSysHeadType }
     * 
     */
    public ReqSysHeadType createReqSysHeadType() {
        return new ReqSysHeadType();
    }

    /**
     * Create an instance of {@link RspSysHeadType }
     * 
     */
    public RspSysHeadType createRspSysHeadType() {
        return new RspSysHeadType();
    }

}
