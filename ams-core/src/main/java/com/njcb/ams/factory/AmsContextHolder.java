package com.njcb.ams.factory;

import com.njcb.ams.factory.domain.*;
import com.njcb.ams.util.SysInfoUtils;

/**
 * 管理平台上下文
 * @author liuyanlong
 */
public class AmsContextHolder {

    /**
     * 获取系统ID
     * @return 系统ID
     */
    public static String getSysId(){
        return SysInfoUtils.getSysId();
    }

    /**
     * 获取系统名称
     * @return 系统名称
     */
    public static String getSysName(){
        return SysInfoUtils.getSysName();
    }

    /**
     * 获取当前交易日期
     * @return 当前交易日期
     */
    public static String getBusiDate(){
        return SysInfoUtils.getBusiDate();
    }

    /**
     * 获取渠道代码
     * @return 渠道代码
     */
    public static String getChannelId(){
        return SysInfoUtils.getChannelId();
    }

    /**
     * 容器上下文
     * @return 容器上下文
     */
    public static AppContext getAppContext() {
        return AppContext.getInstance();
    }
	
	 /**
     * 系统上下文
     * @return 容器上下文
     */
    public static SysContext getSysContext() {
        return SysContext.getInstance();
    }
    /**
     * 用户上下文
     * @return 用户上下文
     */
    public static UserContext getUserContext() {
        return UserContext.getInstance();
    }

    /**
     * 交易上下文
     * @return 交易上下文
     */
    public static TradeContext getTradeContext() {
        return TradeContext.getInstance();
    }

    /**
     * 组件上下文
     * @return 组件上下文
     */
    public static ComponentContext getComponentContext() {
        return ComponentContext.getInstance();
    }
}
