package com.njcb.ams.factory.domain;

import com.njcb.ams.factory.comm.DataBus;

import java.util.List;

/**
 * @author liuyanlong
 */
public class UserContext {

    private UserContext() {
    }

    private static UserContext singleton = null;

    public static synchronized UserContext getInstance() {
        if (singleton == null) {
            singleton = new UserContext();
        }
        return singleton;
    }

    /**
     * @return 用户ID
     */
    public int getUserId() {
        return DataBus.getUserId();
    }

    /**
     * @return 用户属性 0真是用户 1虚拟用户
     */
    public int getUserAttr() {
        return DataBus.getUserAttr();
    }

    /**
     * @return 用户登录名
     */
    public String getLoginName() {
        return DataBus.getLoginName();
    }

    /**
     * @return 用户显示名
     */
    public String getUserName() {
        return DataBus.getUserName();
    }

    /**
     * @return 用户角色代码
     */
    public List<String> getRoleCodes() {
        return DataBus.getRoleCodes();
    }

    /**
     * @return 用户机构号
     */
    public String getOrgNo() {
        return DataBus.getOrgNo();
    }

    /**
     * @return 用户机构名称
     */
    public String getOrgName() {
        return DataBus.getOrgName();
    }
}
