package com.njcb.ams.factory.domain;

import com.njcb.ams.factory.comm.DataBus;
import com.njcb.ams.pojo.bo.Head;
import com.njcb.ams.support.trade.TradeService;

/**
 * 交易上下文
 * @author liuyanlong
 */
public class TradeContext {

    private TradeContext() {
    }

    private static TradeContext singleton = null;

    public static synchronized TradeContext getInstance() {
        if (singleton == null) {
            singleton = new TradeContext();
        }
        return singleton;
    }

    /**
     * @return 交易码
     */
    public String getTradeCode() {
        return DataBus.getTrader().tradeCode();
    }

    /**
     * @return 交易名称
     */
    public String getTradeName() {
        return DataBus.getTrader().tradeName();
    }

    /**
     * @return 交易服务
     */
    public TradeService getTradeService(){
        return TradeService.getInstance();
    }

    /**
     * @return 交易头信息
     */
    public Head getHead(){
        //TODO 整合头信息、http交易头和ESB交易头
        return DataBus.getObject(Head.class);
    }
}
