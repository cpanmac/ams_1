package com.njcb.ams.factory.domain;

import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.util.SysInfoUtils;

/**
 * @author liuyanlong
 */
public class SysContext {
    private SysContext() {
    }

    private static SysContext singleton = null;

    public static synchronized SysContext getInstance() {
        if (singleton == null) {
            singleton = new SysContext();
        }
        return singleton;
    }

    /**
     * 数据库标识
     */
    private static String DATABASE_ID = SysBaseDefine.DatabaseId.MYSQL.getCode();

    /**
     * 获取数据库标识
     * @return
     */
    public String getDatabaseId(){
        return DATABASE_ID;
    }

    /**
     * 设置数据库标识
     * @param databaseId
     * @return
     */
    public String setDatabaseId(String databaseId){
        return DATABASE_ID = databaseId;
    }


    /**
     * 获取系统ID
     * @return 系统ID
     */
    public static String getSysId(){
        return SysInfoUtils.getSysId();
    }

    /**
     * 获取系统名称
     * @return 系统名称
     */
    public static String getSysName(){
        return SysInfoUtils.getSysName();
    }

    /**
     * 获取当前交易日期
     * @return 当前交易日期
     */
    public static String getBusiDate(){
        return SysInfoUtils.getBusiDate();
    }

    /**
     * 获取渠道代码
     * @return 渠道代码
     */
    public static String getChannelId(){
        return SysInfoUtils.getChannelId();
    }
}
