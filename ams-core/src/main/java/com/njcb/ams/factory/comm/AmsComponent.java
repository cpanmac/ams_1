package com.njcb.ams.factory.comm;

/**
 * @author LOONG
 */
public interface AmsComponent {
    /**
     * 获取组件名称
     * @return 组件名称
     */
    String getComponentName();

    /**
     * 组件说明信息
     * @return 组件说明
     */
    String getDescription();

    /**
     * 获取组件上下文
     * @return 组件上下文
     */
    String getComponentContext();
}
