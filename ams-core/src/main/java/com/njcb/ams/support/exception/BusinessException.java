package com.njcb.ams.support.exception;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 对于业务中的意外事件 也就是程序设计的一部分 需要业务处理 如 账号金额不足等 需抛出此已检查异常
 * 
 * @author liuyanlong
 */
public class BusinessException extends Exception implements SystemException {

	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = LoggerFactory.getLogger(BusinessException.class);

	public static BusinessException wrap(Throwable exception, String errorCode) {
		if (exception instanceof BusinessException) {
			BusinessException se = (BusinessException) exception;
			return se;
		} else {
			return new BusinessException(exception.getMessage(), errorCode, exception);
		}
	}

	public static BusinessException wrap(Throwable exception) {
		return wrap(exception, null);
	}

	private String errorCode;
	private final Map<String, Object> properties = new TreeMap<String, Object>();

	protected BusinessException(Throwable exception) {
		super(exception);
	}

	protected BusinessException(String errorCode) {
		this.errorCode = errorCode;
	}

	protected BusinessException(String message, String errorCode) {
		super(message);
		this.errorCode = errorCode;
		if(null == errorCode || errorCode.length() != ExceptionUtil.ERROR_CODE_LENGTH){
			logger.warn("错误码应包含[系统简称（3位）＋模块（2位）＋信息类型（1位）＋序号（4位）]");
		}
	}
	
	protected BusinessException(String message, String errorCode, Throwable cause) {
		super(message, cause);
		this.errorCode = String.valueOf(errorCode);
	}

	@Override
	public String getErrorCode() {
		return errorCode;
	}

	public BusinessException setErrorCode(String errorCode) {
		this.errorCode = errorCode;
		return this;
	}
	
	public Map<String, Object> getProperties() {
		return properties;
	}

	@SuppressWarnings("unchecked")
	public <T> T get(String name) {
		return (T) properties.get(name);
	}

	public BusinessException set(String name, Object value) {
		properties.put(name, value);
		return this;
	}

	@Override
	public void printStackTrace(PrintStream s) {
		synchronized (s) {
			printStackTrace(new PrintWriter(s));
		}
	}

	@Override
	public void printStackTrace(PrintWriter s) {
		synchronized (s) {
			s.println(this);
			s.println("\t-------------------------------");
			if (errorCode != null) {
				s.println("\terrorCode:" + errorCode + "\t" + ExceptionUtil.getUserErrorText(errorCode));
			}
			for (String key : properties.keySet()) {
				s.println("\t" + key + "=[" + properties.get(key) + "]");
			}
			s.println("\t-------------------------------");
			StackTraceElement[] trace = getStackTrace();
			for (int i = 0; i < trace.length; i++) {
				s.println("\tat " + trace[i]);
			}

			Throwable ourCause = getCause();
			if (ourCause != null) {
				ourCause.printStackTrace(s);
			}
			s.flush();
		}
	}

}
