package com.njcb.ams.support.exception;

/**
 * @author LOONG
 */
public interface ErrorCode {
	/**
	 * 返回错误代码
	 * @return
	 */
	String getCode();
}
