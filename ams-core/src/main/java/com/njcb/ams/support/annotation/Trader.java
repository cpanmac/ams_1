package com.njcb.ams.support.annotation;

import com.njcb.ams.support.annotation.enums.MonitorLevel;
import com.njcb.ams.support.annotation.enums.TradeType;
import com.njcb.ams.support.annotation.enums.TransactionManager;
import org.springframework.transaction.annotation.Propagation;

import java.lang.annotation.*;

/**
 * 标记此函数为一个交易
 * @author LOONG
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Trader {

    /* 交易码 */
    String tradeCode();

    /* 交易名称 */
    String tradeName();
    
    /* 交易分组 */
    String tradeGroup() default "";

    Propagation propagation() default Propagation.REQUIRED;

    /* 是否同步写入日志 */
    boolean synLog() default false;
    
    /* 服务监管级别 */
    MonitorLevel monitorLevel() default MonitorLevel.FULL_MONITOR;

    /* 交易类型 */
    TradeType tradeType() default TradeType.TYPE_TRANS;

    /* 事物管理器 */
    String transactionManager() default "transactionManager";

}
