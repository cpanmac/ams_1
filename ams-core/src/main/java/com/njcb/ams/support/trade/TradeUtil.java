package com.njcb.ams.support.trade;

import com.njcb.ams.factory.comm.DataBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Stack;

/**
 * @author srxhx207
 */
public class TradeUtil {
	private static final Logger logger = LoggerFactory.getLogger(TradeUtil.class);
	private static String TRADE_UTIL = "TRADE_UTIL";
	private String tradeCode;
	private String topTradeCode;
	private String tradeName;
	private String reqSys;
	private String reqSeq;
	private String reqDate;
    private String globalSeq;
	private String transSeq;
	private String busiSign;
	private String remark;
	private boolean rollbackOnly = false;
	private Stack<String> tradeStack = new Stack<String>();

	public static void setReqSys(String reqSys) {
		TradeUtil head = getInstance();
		head.reqSys = reqSys;
	}

	public static String getReqSys() {
		TradeUtil head = getInstance();
		return head.reqSys;
	}

	public static void setReqSeq(String reqSeq) {
		TradeUtil head = getInstance();
		head.reqSeq = reqSeq;
	}

	public static String getReqSeq() {
		TradeUtil head = getInstance();
		return head.reqSeq;
	}

	public static void setReqDate(String reqDate) {
		TradeUtil head = getInstance();
		head.reqDate = reqDate;
	}

	public static String getReqDate() {
		TradeUtil head = getInstance();
		return head.reqDate;
	}
	
	public static void setGlobalSeq(String globalSeq) {
		TradeUtil head = getInstance();
		head.globalSeq = globalSeq;
	}
	
	public static String getGlobalSeq() {
		TradeUtil head = getInstance();
		return head.globalSeq;
	}
	
	public static void setTransSeq(String transSeq) {
		TradeUtil head = getInstance();
		head.transSeq = transSeq;
	}
	
	public static String getTransSeq() {
		TradeUtil head = getInstance();
		return head.transSeq;
	}
	
	public static void setTradeCode(String tradeCode) {
		TradeUtil head = getInstance();
		head.tradeCode = tradeCode;
	}

	public static String getTradeCode() {
		TradeUtil head = getInstance();
		return head.tradeCode;
	}

	public static String getTopTradeCode() {
		TradeUtil head = getInstance();
		return head.topTradeCode;
	}
	
	public static void setTradeName(String tradeName) {
		TradeUtil head = getInstance();
		head.tradeName = tradeName;
	}
	
	public static String getTradeName() {
		TradeUtil head = getInstance();
		return head.tradeName;
	}

	public static void setBusiSign(String busiSign) {
		TradeUtil head = getInstance();
		head.busiSign = busiSign;
	}

	public static String getBusiSign() {
		TradeUtil head = getInstance();
		return head.busiSign;
	}

	public static void setRemark(String remark) {
		TradeUtil head = getInstance();
		head.remark = remark;
	}

	public static String getRemark() {
		TradeUtil head = getInstance();
		return head.remark;
	}

	public static void setRollbackOnly() {
		TradeUtil head = getInstance();
		head.rollbackOnly = true;
	}

	public static boolean isRollbackOnly() {
		TradeUtil head = getInstance();
		return head.rollbackOnly;
	}

	public static Stack<String> getTradeStack() {
		TradeUtil head = getInstance();
		return head.tradeStack;
	}

	public static void pushTradeCode(String tradeCode) {
		TradeUtil head = getInstance();
		if(head.tradeStack.isEmpty()){
			head.topTradeCode = tradeCode;
		}
		head.tradeStack.push(tradeCode);
	}

	public static String popTradeCode() {
		TradeUtil head = getInstance();
		if (!head.tradeStack.isEmpty()) {
			return head.tradeStack.pop();
		}else{
			logger.error("没有可出栈的交易代码，程序可能出现异常");
			return "";
		}
	}

	/**
	 * 方法功能描述：初始化TradeUtil
	 * @return
	 */
	private static TradeUtil getInstance() {
		DataBus globalInfo = DataBus.getUncheckInstance();
		if (null != globalInfo) {
			TradeUtil head = (TradeUtil) DataBus.getAttribute(TRADE_UTIL);
			if (null == head) {
				head = new TradeUtil();
			}
			DataBus.setAttribute(TRADE_UTIL, head);
			return head;
		}else{
			TradeUtil head = new TradeUtil();
			DataBus.setAttribute(TRADE_UTIL, head);
			return head;
		}
	}


	/**
	 * 清理数据
	 */
	public static void clear() {
		DataBus.setAttribute(TRADE_UTIL, null);
	}

}
