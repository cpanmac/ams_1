package com.njcb.ams.support.springmvc.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.lang.Nullable;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * 自定以Converter,处理导出Excel是的输出流
 *
 * @author liuyanlong
 */
public class AmsMappingJackson2HttpMessageConverter extends AbstractJackson2HttpMessageConverter {
	private static final Logger logger = LoggerFactory.getLogger(AmsMappingJackson2HttpMessageConverter.class);

	/**
	 * 输出json
	 */
	public static final String PARAM_TYPE_JSON = "01";
	/**
	 * 输出excel
	 */
	public static final String PARAM_TYPE_EXCEL = "02";

	/**
	 * 设定的输出类型
	 */
	public static String CONVERTER_PARAM = PARAM_TYPE_JSON;

	@SuppressWarnings("unused")
	private String jsonPrefix;


	public AmsMappingJackson2HttpMessageConverter() {
		this(Jackson2ObjectMapperBuilder.json().build());
	}

	public AmsMappingJackson2HttpMessageConverter(ObjectMapper objectMapper) {
		super(objectMapper, MediaType.APPLICATION_JSON, new MediaType("application", "*+json"));
	}


	@Override
	protected void writeInternal(Object object, @Nullable Type type, HttpOutputMessage outputMessage)
			throws IOException, HttpMessageNotWritableException {
		if (PARAM_TYPE_EXCEL.equals(CONVERTER_PARAM)) {
			//当输出为excel时不需要转换器
			if (logger.isDebugEnabled()) {
				logger.debug("json Converter is not excute");
			}
		} else {
			super.writeInternal(object, type, outputMessage);
		}
		CONVERTER_PARAM = PARAM_TYPE_JSON;
	}

}


