package com.njcb.ams.support.config;

import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.CacheConfiguration.CacheEventListenerFactoryConfiguration;
import net.sf.ehcache.config.PersistenceConfiguration;
import net.sf.ehcache.config.PersistenceConfiguration.Strategy;

/**
 * 缓存配置
 * @author liuyanlong
 *
 */
public interface AmsCacheConfiguration {
	
	/**
	 * 添加缓存
	 * @param cacheList
	 */
	default void addEhCache(List<Cache> cacheList){
		CacheConfiguration cacheConfiguration = new CacheConfiguration();
		cacheConfiguration.setMaxEntriesLocalHeap(100000);
		cacheConfiguration.setEternal(false);
		cacheConfiguration.setTimeToIdleSeconds(86400);
		cacheConfiguration.setTimeToLiveSeconds(0);
		CacheEventListenerFactoryConfiguration factory = new CacheEventListenerFactoryConfiguration();
		factory.className("net.sf.ehcache.distribution.RMICacheReplicatorFactory");
		cacheConfiguration.cacheEventListenerFactory(factory);
		cacheConfiguration.setName("TokenCache");
		PersistenceConfiguration persistenceConfiguration = new PersistenceConfiguration();
		persistenceConfiguration.strategy(Strategy.LOCALTEMPSWAP);
		cacheConfiguration.persistence(persistenceConfiguration);
		Cache tokenCacheCache = new Cache(cacheConfiguration);
		cacheList.add(tokenCacheCache);
	}
	
}
