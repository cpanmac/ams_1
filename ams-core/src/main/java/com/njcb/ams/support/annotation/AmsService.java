package com.njcb.ams.support.annotation;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 标记对象为AMS平台服务
 * @author LOONG
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface AmsService {
    /* 优先级 */
    int priority() default 99;
}

