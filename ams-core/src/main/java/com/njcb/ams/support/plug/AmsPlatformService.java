package com.njcb.ams.support.plug;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.support.annotation.AmsService;
import com.njcb.ams.support.exception.ExceptionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 交易服务类、提供交易相关操作，如获取交易代码、反射调用等。
 * @author LOONG
 */
@Service
public class AmsPlatformService {
	private static final Logger logger = LoggerFactory.getLogger(AmsPlatformService.class);

	private static Map<String,List<Object>> amsServiceMap = new HashMap<>();

	public static AmsPlatformService getInstance() {
		logger.debug("Instance TradeService");
		return AppContext.getBean(AmsPlatformService.class);
	}
	


	/**
	 * 方法功能描述：交易过滤器
	 * @param tradeFilter
	 */
	public static void addAmsService(Object amsService) {
		String serviceName = getServiceName(amsService.getClass());
		if(amsServiceMap.containsKey(serviceName)){
			List<Object> amsServiceList = amsServiceMap.get(serviceName);
		}else{
			List<Object> amsServiceList = new ArrayList<>();
			amsServiceMap.put(serviceName,amsServiceList);
		}
		List<Object> amsServiceList = amsServiceMap.get(serviceName);
		amsServiceList.add(amsService);
		amsServiceList.sort(new Comparator<Object>() {
			@Override
			public int compare(Object o1, Object o2) {
				AmsService t1 = o1.getClass().getAnnotation(AmsService.class);
				AmsService t2 = o2.getClass().getAnnotation(AmsService.class);
				if(null == t1 || null == t2){
					return 0;
				}else if(t1.priority() > t2.priority()){
					return 1;
				}else if(t1.priority() < t2.priority()){
					return -1;
				}else{
					return 0;
				}
			}
		});
	}

	/**
	 * 获取类实现的接口
	 * @param cls
	 * @return
	 */
	private static String getServiceName(Class cls){
		Class<?>[] interfaces = cls.getInterfaces();
		if(interfaces.length == 0){
			Class<?> superclass = cls.getSuperclass();
			if("java.lang.Object".equals(superclass.getName())){
				ExceptionUtil.throwAppException("被AmsService注解的类["+cls.getName()+"]没有实现接口或继承父类");
			}
			return superclass.getName();
		}else{
			return interfaces[0].getName();
		}
	}
	
	/**
	 * 方法功能描述：过去交易过滤器
	 * @return
	 */
	public static <T> List<T> getAmsService(Class<T> cls) {
		if(!amsServiceMap.containsKey(cls.getName())){
			return new ArrayList<>();
		}
		List<T> bean = (List<T>)amsServiceMap.get(cls.getName());
		return bean;
	}
}
