package com.njcb.ams.support.annotation;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.lang.annotation.*;

/**
 * 标记对象为交易管理对象
 * @author LOONG
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Validated
@Component
public @interface Interaction {
    /* 交易分组 */
    String groupName() default "";
}
