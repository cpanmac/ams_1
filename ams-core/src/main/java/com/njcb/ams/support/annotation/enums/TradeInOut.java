package com.njcb.ams.support.annotation.enums;

/**
 * 枚举类型
 * @author LIUYANLONG
 *
 */
public enum TradeInOut {
	/**
     * 查询类
     */
	INPUT,
	/**
	 *交易类
	 */
	OUTPUT;

}
