/**
 * 工程支撑包
 * 提供开发平台依赖的各种支撑类标准类功能、如异常处理、注解解析等
 * 
 * @author liuyanlong
 */
package com.njcb.ams.support;