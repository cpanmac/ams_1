package com.njcb.ams.support.codevalue;

import com.njcb.ams.util.AmsEnumUtils;

/**
 * @author liuyanlong
 */
public interface EnumCode {
    /**
     * 获取枚举代码
     * @return 枚举代码
     */
    String getCode();

    /**
     * 获取枚举代码说明
     * @return 枚举说明
     */
    String getDesc();

    /**
     * 判断美剧代码是否指定代码
     *
     * @param code
     * @return 是否此枚举
     */
    default Boolean is(String code) {
        if (code == null) {
            return Boolean.FALSE;
        }
        return this.getCode().equals(code);
    }

    /**
     * code转换码值枚举
     *
     * @param enumClass 枚举类信息
     * @param code 枚举代码
     * @param <E> 枚举类型
     * @return 枚举对象
     */
    @Deprecated
    static <E extends Enum<E>> E valueOfCode(Class<E> enumClass, String code) {
        return AmsEnumUtils.valueOfCode(enumClass,code);
    }

    /**
     * 描述转换码值枚举
     *
     * @param enumClass
     * @param desc
     * @param <E>
     * @return
     */
    @Deprecated
    static <E extends Enum<E>> E valueOfDesc(Class<E> enumClass, String desc) {
        return AmsEnumUtils.valueOfDesc(enumClass,desc);
    }

    /**
     * 获取描述文本
     * @return 枚举文本描述
     */
    default String getText() {
        return getCode() + "-" + getDesc();
    }
}
