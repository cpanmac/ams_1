package com.njcb.ams.support.trade.bean;

import java.lang.reflect.Method;

import com.njcb.ams.support.annotation.Trader;

/**
 * 应用交易定义属性
 * @author liuyanlong
 *
 */
public class TradeServiceBean {
	
	private String beanName;
	
	private Method method;
	
	private String methodName;
	
	private String tradeCode;
	
	private String tradeName;
	
	private String tradeGroup;
	
	private Trader trader;
	
	public String getBeanName() {
		return beanName;
	}
	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public String getTradeCode() {
		return tradeCode;
	}
	public void setTradeCode(String tradeCode) {
		this.tradeCode = tradeCode;
	}
	public String getTradeName() {
		return tradeName;
	}
	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}
	public String getTradeGroup() {
		return tradeGroup;
	}
	public void setTradeGroup(String tradeGroup) {
		this.tradeGroup = tradeGroup;
	}
	public Trader getTrader() {
		return trader;
	}
	public void setTrader(Trader trader) {
		this.trader = trader;
	}
	public void setMethod(Method method) {
		this.method = method;
	}
	public Method getMethod() {
		return method;
	}
	@Override
	public String toString() {
		return "TradeServiceBean [beanName=" + beanName + ", method=" + method + ", methodName=" + methodName
				+ ", tradeCode=" + tradeCode + ", tradeName=" + tradeName + ", tradeGroup=" + tradeGroup + ", trader="
				+ trader + "]";
	}
}
