package com.njcb.ams.support.annotation.enums;

import com.njcb.ams.support.codevalue.EnumCode;

/**
 * 枚举类型
 * @author LIUYANLONG
 *
 */
public enum TransactionManager implements EnumCode {
	GLOBAL("transactionManager", "主事物管理器"),
	REGION("slaveTransactionManager", "从事物管理器"),
	NONE("none", "无事物");

	private String code;
	private String desc;

	TransactionManager(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDesc() {
		return this.desc;
	}
}
