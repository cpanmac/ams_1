package com.njcb.ams.support.config;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.njcb.ams.pojo.bo.DataDicConfig;
import com.njcb.ams.support.plug.Plug;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 获取配置bean的工具类
 *
 * @author liuyanlong
 */
public class AmsConfigUtil {

    private static final Logger logger = LoggerFactory.getLogger(AmsConfigUtil.class);

    /**
     * Plug插件和插件定义的扫描注解
     */
    private static Map<Class<? extends Annotation>, Plug> scanAnnotations = new HashMap<>();

    /**
     * 处理添加了AmsConfig注解的类，将该类的父类和接口与该bean的对于管理起来，达到可以通过父类或接口查找到此bean的目的。
     * spring默认实例化的bean
     */
    private static Map<String, Object> amsConfigBeanMap = new HashMap<String, Object>();

    private static Map<String, Map<String, DataDicConfig>> dataDicMap = new HashMap<String, Map<String, DataDicConfig>>();


    /**
     * 实现AmsConfigDefault的默认配置类
     */
    private static Map<String, Object> amsConfigDefaultBeanMap = new HashMap<String, Object>();

    /**
     * 自定义实例化的bean
     */
    private static Map<String, Object> customBeanMap = new HashMap<String, Object>();

    /**
     * 获取配置类、有限使用spring实例化的配置类，如果spring还未实例化、则使用自己实例化的bean
     *
     * @param clazz
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T getBean(Class<T> clazz) {
        if (amsConfigBeanMap.containsKey(clazz.getName())) {
            logger.info("获取实例化配置类对象");
            return (T) amsConfigBeanMap.get(clazz.getName());
        } else if (amsConfigDefaultBeanMap.containsKey(clazz.getName())) {
            logger.info("获取实例化默认配置类对象");
            return (T) amsConfigDefaultBeanMap.get(clazz.getName());
        } else {
            logger.info("获取自定义实例化配置类对象");
            return (T) customBeanMap.get(clazz.getName());
        }
    }

    public static void addAmsConfigBean(String configName, Object bean) {
        amsConfigBeanMap.put(configName, bean);
    }

    public static void addAmsConfigDefaultBean(String configName, Object bean) {
        amsConfigDefaultBeanMap.put(configName, bean);
    }

    public static void addBean(String configName, Object bean) {
        customBeanMap.put(configName, bean);
    }

    public static void addAmsDataDic(String configName, DataDicConfig bean) {
        if (dataDicMap.containsKey(configName)) {
            Map<String, DataDicConfig> map = dataDicMap.get(configName);
            if (!map.containsKey(bean.getDataNo())) {
                map.put(bean.getDataNo(), bean);
            }
        } else {
            Map<String, DataDicConfig> map = new HashMap<String, DataDicConfig>();
            map.put(bean.getDataNo(), bean);
            dataDicMap.put(configName, map);
        }
    }

    public static Map<String, Map<String, DataDicConfig>> getDataDicMap() {
        return dataDicMap;
    }

    public static void addScanAnnotation(Class<? extends Annotation> annClass, Plug plug) {
        scanAnnotations.put(annClass, plug);
    }

    public static Map<Class<? extends Annotation>, Plug> getScanAnnotations() {
        return scanAnnotations;
    }
}
