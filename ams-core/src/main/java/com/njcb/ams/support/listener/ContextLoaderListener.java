package com.njcb.ams.support.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.util.SysInfoUtils;

/**
 * @author LOONG
 */
public class ContextLoaderListener implements ServletContextListener {
	private static final Logger logger = LoggerFactory.getLogger(ContextLoaderListener.class);
	
	public ContextLoaderListener() {
		super();
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		ServletContext context = event.getServletContext();
		try {
			ApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
			AppContext.setContext(ctx);
			SysBaseDefine.SYS_ID = SysInfoUtils.getSysId();
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("ApplicationContext初始化异常,系统退出,异常消息:{}",ex.getMessage());
			System.exit(0);
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}

}