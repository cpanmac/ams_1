package com.njcb.ams.assembler;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.njcb.ams.pojo.enumvalue.TradeStatusCode;
import org.springframework.stereotype.Component;

import com.njcb.ams.repository.entity.SysTradeConsole;
import com.njcb.ams.repository.entity.SysTradeLog;
import com.njcb.ams.pojo.dto.SysTradeLogDTO;
import com.njcb.ams.portal.SysBaseDefine;

/**
 * @author LOONG
 */
@Component
public class SysTradeConsoleConvertor implements Convertor<SysTradeLogDTO, SysTradeLog, SysTradeLog, Object> {

	public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.00");

	/**
	 * 交易控制信息对象拷贝
	 * @param dest 目标对象
	 * @param orig 原始对象
	 */
	public void copyConsoleBean(SysTradeConsole dest, SysTradeConsole orig) {
		if (null != orig.getId()) {
			dest.setId(orig.getId());
		}
		if (null != orig.getTradeCode()) {
			dest.setTradeCode(orig.getTradeCode());
		}
		if (null != orig.getTradeName()) {
			dest.setTradeName(orig.getTradeName());
		}
		if (null != orig.getMaxExec()) {
			dest.setMaxExec(orig.getMaxExec());
		}
		if (null != orig.getMaxRate()) {
			dest.setMaxRate(orig.getMaxRate());
		}
		if (null != orig.getCurrExec()) {
			dest.setCurrExec(orig.getCurrExec());
		}
		if (null != orig.getTimeCycle()) {
			dest.setTimeCycle(orig.getTimeCycle());
		}
		if (null != orig.getNumberCycle()) {
			dest.setNumberCycle(orig.getNumberCycle());
		}
		if (null != orig.getTimeSpeed()) {
			dest.setTimeSpeed(orig.getTimeSpeed());
		}
		if (null != orig.getTimeTakeNum()) {
			dest.setTimeTakeNum(orig.getTimeTakeNum());
		}
		if (null != orig.getTimeSuccRate()) {
			dest.setTimeSuccRate(orig.getTimeSuccRate());
		}
		if (null != orig.getNumberSuccRate()) {
			dest.setNumberSuccRate(orig.getNumberSuccRate());
		}
		if (null != orig.getTradeStatus()) {
			dest.setTradeStatus(orig.getTradeStatus());
		}
		if (null != orig.getTradeMsg()) {
			dest.setTradeMsg(orig.getTradeMsg());
		}
		if (null != orig.getIdentifyingMarker()) {
			dest.setIdentifyingMarker(orig.getIdentifyingMarker());
		}
	}


	/**
	 * 设置默认的交易控制对象
	 * @param ssc 参照对象
	 * @return 默认对象
	 */
	public SysTradeConsole defaultServerConsole(SysTradeConsole ssc) {
		if (null == ssc) {
			ssc = new SysTradeConsole();
		}
		ssc.setMaxExec(SysBaseDefine.DEFAULT_MAX_EXEC);
		ssc.setMaxRate(SysBaseDefine.DEFAULT_MAX_RATE);
		ssc.setTradeStatus(TradeStatusCode.TRADE_STATUS_01.getCode());
		ssc.setCurrExec(0);
		ssc.setTimeCycle(172800);
		ssc.setNumberCycle(100);
		ssc.setTimeSpeed(0);
		ssc.setTimeTakeNum(0);
		ssc.setTimeSuccRate(DECIMAL_FORMAT.format(new BigDecimal(100)));
		ssc.setNumberSuccRate(DECIMAL_FORMAT.format(new BigDecimal(100)));
		return ssc;
	}
	
}
