/**
 * 转换器， 用于客户端对象、域对象、数据对象之间的转换
 * 
 * 提供对象转换器标准
 * 
 * @author liuyanlong
 */
package com.njcb.ams.assembler;