package com.njcb.ams.store.cache;

import net.sf.ehcache.event.CacheEventListener;
import net.sf.ehcache.event.CacheEventListenerFactory;

import java.util.Properties;

/**
 * @author srxhx207
 */
public class MethodInvokeCacheReplicatorFactory extends CacheEventListenerFactory {
    @Override
    public CacheEventListener createCacheEventListener(Properties properties) {
        return new MethodInvokeCacheEventListener();
    }
}
