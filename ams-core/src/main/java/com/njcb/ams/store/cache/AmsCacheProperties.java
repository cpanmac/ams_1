package com.njcb.ams.store.cache;

import com.njcb.ams.factory.domain.AppContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author LOONG
 */
@Component
public class AmsCacheProperties {
	/**
	 * 缓存同步消息发送地址（如果同步到多台需要配置多台地址，多台地址用英文逗号分隔）
	 */
	@Value("${cache.providerAddresses:}")
	private String providerAddresses;

	/**
	 * 缓存同步监听端口和IP
	 */
	@Value("${cache.listenHost:localhost}")
    private String listenHost;

	@Value("${cache.listenPort:40000}")
    private String listenPort;

	public static AmsCacheProperties getInstance() {
		return AppContext.getBean(AmsCacheProperties.class);
	}

	public String getProviderAddresses() {
		return providerAddresses;
	}

	public void setProviderAddresses(String providerAddresses) {
		this.providerAddresses = providerAddresses;
	}

	public String getListenHost() {
		return listenHost;
	}

	public void setListenHost(String listenHost) {
		this.listenHost = listenHost;
	}

	public String getListenPort() {
		return listenPort;
	}

	public void setListenPort(String listenPort) {
		this.listenPort = listenPort;
	}
}
