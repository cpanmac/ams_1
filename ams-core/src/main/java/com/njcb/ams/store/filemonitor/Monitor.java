package com.njcb.ams.store.filemonitor;

public interface Monitor {
	void operate(String configName);
}
