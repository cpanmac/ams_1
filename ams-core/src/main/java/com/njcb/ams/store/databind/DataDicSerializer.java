package com.njcb.ams.store.databind;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdScalarSerializer;
import com.njcb.ams.factory.domain.AppContext;

import java.io.IOException;
import java.lang.reflect.Method;

/**
 * @author srxhx207
 */
public class DataDicSerializer extends StdScalarSerializer {
    private String dicName;
    private Method dataDicMethod;

    public DataDicSerializer(String dicName) {
        super(String.class, false);
        this.dicName = dicName;
    }

    @Override
    public void serialize(Object value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        String writeValue = null;
        if(null != value){
            writeValue = String.valueOf(value);
        }
        String returnObject = getDataNameByNo(value);
        if(null != returnObject){
            writeValue = returnObject;
        }
        gen.writeString(writeValue);
    }

    /**
     * 利于反射获取数据字典
     * @param value
     * @return
     */
    private String getDataNameByNo(Object value){
        if(null == dataDicMethod) {
            dataDicMethod = getDataDicMethod();
        }
        if(null != dataDicMethod) {
            Object returnObject = null;
            try {
                returnObject = dataDicMethod.invoke(AppContext.getBean("dataDicService"), "OrgGrade",String.valueOf(value));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(null != returnObject){
                return String.valueOf(returnObject);
            }
        }
        return null;
    }

    private Method getDataDicMethod(){
        Object dataDicService = AppContext.getBean("dataDicService");
        Method dataDicMethod = null;
        if(null != dataDicService){
            try {
                dataDicMethod = dataDicService.getClass().getMethod("getDataNameByNo",String.class,String.class);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        return dataDicMethod;
    }
}
