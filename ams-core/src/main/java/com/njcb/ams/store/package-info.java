/**
 * 业务组件包
 * 提供各类基础业务组件、如ESB组件、分页组件等
 * 
 * @author liuyanlong
 */
@com.njcb.ams.support.annotation.Information(tag="组件",desc="组件包")
package com.njcb.ams.store;