package com.njcb.ams.store.databind;

import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;

/**
 * @author srxhx207
 */
public class AmsJacksonAnnotationIntrospector extends JacksonAnnotationIntrospector {
    @Override
    public Object findSerializer(Annotated annotated) {
        DataDicFormat dataDicFormat = annotated.getAnnotation(DataDicFormat.class);
        if(null != dataDicFormat){
            return new DataDicSerializer(dataDicFormat.dicName());
        }
        return super.findSerializer(annotated);
    }
}
