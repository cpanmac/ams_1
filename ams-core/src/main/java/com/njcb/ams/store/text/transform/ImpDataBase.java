package com.njcb.ams.store.text.transform;

import java.util.Hashtable;

/**
 *
 * @author LIUYANLONG
 *
 */
public interface ImpDataBase {
	/**
	 * 检查数据
	 * @param map
	 * @return
	 */
	public int dataCheckAndConvert(Hashtable<String, String> map);

	/**
	 * 数据导入
	 * @param obj
	 * @return 总条数和有效条数
	 * @throws Exception
	 */
	public int[] doDataProcess(Object... obj) throws Exception;
}
