package com.njcb.ams.store.databind;

import java.lang.annotation.*;

/**
 * 标记为数据字典字段，返回前端时自动映射
 * @author LOONG
 *
 */
@Target({ElementType.FIELD,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataDicFormat {
    String dicName() default "";
}

