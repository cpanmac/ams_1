package com.njcb.ams.store.page;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

/**
 * 分页对象
 * 
 * @author liuyanlong
 *
 */
public class Page implements Serializable {
	@ApiModelProperty(required = true, value = "当前页数")
	private int page;
	@ApiModelProperty(required = true, value = "每页条数")
	private int rows;
	@ApiModelProperty(hidden = true)
	private int startRow;
	@ApiModelProperty(hidden = true)
	private int endRow;
	@ApiModelProperty(hidden = true)
	private long total;
	@ApiModelProperty(hidden = true)
	private int pages;
	/**
	 * 是否全局分页，开启时在endPage之前一直执行分页，关闭是只执行一次, 默认开启
	 */
	@ApiModelProperty(hidden = true)
	private Boolean global = true;
	/**
	 * 分页是否开启，默认开启
	 */
	@ApiModelProperty(hidden = true)
	private Boolean open = true;
	/**
	 * 是否设置总条数，多次分页的时候使用
	 */
	@ApiModelProperty(hidden = true)
	private Boolean setTotal = true;
	@ApiModelProperty(hidden = true)
	private List<?> result;
	/**
	 * 查询条件 ,支持执行原生SQL
	 */
	@ApiModelProperty(hidden = true)
	private String queryString;

	public Page() {
	}

	public Page(int page, int rows) {
		this.page = page;
		this.rows = rows;
		this.startRow = page > 0 ? (page - 1) * rows : 0;
		this.endRow = page * rows;
	}

	public List<?> getResult() {
		return result;
	}

	@SuppressWarnings("unchecked")
	public <T extends Object> List<T> getResult(Class<T> T) {
		return (List<T>) result;
	}

	public Page setResult(List<?> result) {
		this.result = result;
		return this;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public int getEndRow() {
		return endRow;
	}

	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

	public int getStartRow() {
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page
	 *            the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the rows
	 */
	public int getRows() {
		return rows;
	}

	/**
	 * @param rows
	 *            the rows to set
	 */
	public void setRows(int rows) {
		this.rows = rows;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public String getQueryString() {
		return queryString;
	}

	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}
	
	public Page global(Boolean global) {
		this.global = global;
		this.open = global;
		return this;
	}
	
	public Boolean isGlobal() {
		return global;
	}
	
	public Page open(Boolean open) {
		this.open = open;
		return this;
	}

	public Page setTotal(Boolean setTotal) {
		this.setTotal = setTotal;
		return this;
	}

	public Boolean isSetTotal() {
		return setTotal;
	}

	public Boolean isOpen() {
		return open;
	}

	@Override
	public String toString() {
		return "Page [page=" + page + ", rows=" + rows + ", startRow=" + startRow + ", endRow=" + endRow + ", total="
				+ total + ", pages=" + pages + ", global=" + global + ", open=" + open + ", result=" + result
				+ ", queryString=" + queryString + "]";
	}
	
}
