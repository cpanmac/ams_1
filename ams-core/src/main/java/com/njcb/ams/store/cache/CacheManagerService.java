package com.njcb.ams.store.cache;

import com.njcb.ams.pojo.enumvalue.ValidStatus;
import com.njcb.ams.repository.entity.SysParam;
import com.njcb.ams.util.AmsIpUtils;
import com.njcb.ams.util.AmsUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @author LOONG
 */
@Component
public class CacheManagerService {
    private static final Logger logger = LoggerFactory.getLogger(CacheManagerService.class);
    @Autowired
    private AmsCacheProperties amsCacheProperties;
    private List<String> localIplist = AmsIpUtils.getLocalIPList();

    /**
     * @param port
     */
    public void savePeerListenerSysParam(String port) {
        SysParam sysParam = new SysParam();
        sysParam.setType("PeerListener");
        sysParam.setSubtype(getLocaIp());
        sysParam.setName(getLocaIp());
        sysParam.setValue(port);
        sysParam.setStatus(ValidStatus.VALID.getCode());
		logger.info("集群缓存成员监听信息[" + sysParam.toString() + "]");
    }

    public void savePeerProviderSysParam(String providerAddress, String peerAddress) {
        SysParam sysParam = new SysParam();
        sysParam.setType("PeerProvider");
        sysParam.setSubtype(getLocaIp());
        sysParam.setName(providerAddress);
        sysParam.setValue(peerAddress);
        sysParam.setStatus(ValidStatus.VALID.getCode());
        logger.info("集群缓存成员通知信息[" + sysParam.toString() + "]");
    }

    /**
     * 所有通知列表
     *
     * @return
     */
    public List<String> getProviderAddressList() {
        StringTokenizer stringTokenizer = new StringTokenizer(amsCacheProperties.getProviderAddresses(), "|");
        List<String> providerAddressList = new ArrayList<>();
        while (stringTokenizer.hasMoreTokens()) {
            String providerAddress = stringTokenizer.nextToken().trim();
            providerAddressList.add(providerAddress);
        }
        return providerAddressList;
    }

    /**
     * 获取本机IP
     *
     * @return
     */
    public String getLocaIp() {
        String localHostIp = "";
        for (String providerAddress : getProviderAddressList()) {
            if (localIplist.contains(providerAddress)) {
                localHostIp = providerAddress;
                break;
            }
        }
        if (AmsUtils.isNull(localHostIp)) {
            localHostIp = "localhost";
        }
        return localHostIp;
    }
}
