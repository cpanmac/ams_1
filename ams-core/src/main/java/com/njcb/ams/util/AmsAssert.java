package com.njcb.ams.util;

import com.njcb.ams.support.exception.ErrorCode;
import com.njcb.ams.support.exception.ExceptionUtil;
import org.springframework.util.Assert;

/**
 * 断言检查
 * 
 * @author liuyanlong
 *
 */
public class AmsAssert extends Assert {

	public static void notNull(Object object) {
		notNull(object, "传入参数不能为空");
	}

	/**
	 * @param object 检测对象
	 * @param message 消息
	 */
	public static void notNull(Object object, String message) {
		if(AmsUtils.isNull(object)){
			fail(message);
		}
	}

	/**
	 * @param object 检测对象
	 * @param message 消息
	 */
	public static void notNull(Object object, String message, ErrorCode errorCode) {
		if(AmsUtils.isNull(object)){
			fail(message, errorCode);
		}
	}
	
	/**
	 * @param objects 检测对象
	 */
	public static void notNull(Object ... objects) {
		if(AmsUtils.isNull(objects)){
			fail("传入参数不能有空值");
		}
	}

	/**
	 * @param objects 检测对象
	 */
	public static void notNull(String message, Object ... objects) {
		if(AmsUtils.isNull(objects)){
			fail(message);
		}
	}
	
	public static void isNumeric(String inputStr,String message) {
		if(!AmsUtils.isNumeric(inputStr)){
			fail(message);
		}
	}
	
	public static void isInteger(String inputStr,String message) {
		if(!AmsUtils.isInteger(inputStr)){
			fail(message);
		}
	}
	
	public static void isDate(String inputStr,String message) {
		if(!AmsUtils.isDate(inputStr)){
			fail(message);
		}
	}
	
	public static void isFormatDate(String inputStr,String format,String message) {
		if(!AmsUtils.isFormatDate(inputStr,format)){
			fail(message);
		}
	}
	
	/**
	 * 断言字符串长度大于size
	 * @param text
	 * @param size
	 * @param message
	 */
	@SuppressWarnings("unchecked")
	public static void hasLength(String text, int size, String message) {
		if(AmsUtils.isNull(text) || text.length() < size){
			fail(message);
		}
	}
	
	/**
	 * @param expected 预期值
	 * @param actual 真实值
	 * @param message 消息
	 */
	public static void isEqual(String expected, String actual, String message) {
		if(!AmsUtils.isEqual(expected, actual)){
			fail(message);
		}
	}
	
	public static void isTrue(boolean expression, String message) {
		Assert.isTrue(expression, message);
	}

	/**
	 * 判断值value是否枚举enum范围内
	 * @param enumClass
	 * @param value
	 * @param message
	 */
	public static void isEnum(Class enumClass, Object value, String message) {
		if(!AmsEnumUtils.isValid(enumClass,value)){
			fail(message);
		}
	}
	
	public static void fail(String message, ErrorCode errorCode) {
		ExceptionUtil.throwAppException(message,errorCode);
    }

	public static void fail(String message) {
		ExceptionUtil.throwAppException(message,"CRM2001");
	}
}
