package com.njcb.ams.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @author srxhx207
 */
public class AmsIpUtils {
    private static final Logger logger = LoggerFactory.getLogger(AmsIpUtils.class);

    public static List<String> getLocalIPList(){
        List<String> ipList = new ArrayList<>();
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()){
                NetworkInterface networkInterface = networkInterfaces.nextElement();
                Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
                while (inetAddresses.hasMoreElements()){
                    InetAddress inetAddress = inetAddresses.nextElement();
                    if(inetAddress != null && inetAddress instanceof Inet4Address){
                        String ip = inetAddress.getHostAddress().trim();
                        ipList.add(ip);
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        logger.debug("获取本机Ip列表为{}", Arrays.toString(ipList.toArray()));
        return ipList;
    }

    /**
     * 端口是否未被用
     * @param port
     * @return
     */
    public static boolean isPortAvailable(int port) {
        try {
            String[] commond = new String[2];
            commond[0] = "netstat";
            String encoding ="gbk";
            if (new AmsSystemUtils().isWindows()) {
                commond[1] = "-aon";
            } else {
                commond[1] = "-anp";
                encoding = "utf-8";
            }
            String ret = new AmsSystemUtils().excuteCmdMultiThread(commond, encoding);
            boolean matches = Pattern.compile("(.+)("+port+"\\s+)(.*)").matcher(ret).find();
            return !matches;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 获取随机端口号
     * @param minPort
     * @param maxPort
     * @return
     */
    private static int getRandomPort(int minPort, int maxPort) {
        Random random = new Random();
        int s = random.nextInt(maxPort) % (maxPort - minPort + 1) + minPort;
        return s;
    }

    /**
     * 获取未被占用的随机端口号
     * @param minPort
     * @param maxPort
     * @return
     */
    public static int getUnAvailableRandomPort(int minPort, int maxPort) {
        int randomPort = getRandomPort(minPort, maxPort);
        if (!isPortAvailable(randomPort)) {
            return getUnAvailableRandomPort(minPort, maxPort);
        }
        return randomPort;
    }

}
