package com.njcb.ams.util;

import java.io.Closeable;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.njcb.ams.support.exception.ExceptionUtil;

/**
 * @author LOONG
 */
public class AmsIOUtils extends IOUtils{
	private static final Logger logger = LoggerFactory.getLogger(AmsIOUtils.class);

	// 关闭IO资源
	public static void close(Closeable... closeList) {
		for (Closeable closeable : closeList) {
			try {
				if (closeable != null) {
					closeable.close();
				}
			} catch (IOException e) {
				ExceptionUtil.printStackTrace(e);
				logger.error(e.toString());
			}
		}
	}
	
	// 关闭SQL资源
	public static void close(AutoCloseable... closeList) {
		for (AutoCloseable closeable : closeList) {
			try {
				if (closeable != null) {
					closeable.close();
				}
			} catch (Exception e) {
				ExceptionUtil.printStackTrace(e);
				logger.error(e.toString());
			}
		}
	}

}
