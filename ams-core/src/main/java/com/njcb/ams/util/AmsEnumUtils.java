package com.njcb.ams.util;

/**
 * @author liuyanlong
 */
import com.njcb.ams.support.codevalue.EnumCode;
import com.njcb.ams.support.exception.ExceptionUtil;

public class AmsEnumUtils {

    /**
     * 判断枚举值是否有效
     * @param enumClass
     * @param value
     * @return
     */
    public static boolean isValid(Class enumClass, Object value) {
        if (value == null) {
            return Boolean.TRUE;
        }
        if (enumClass == null) {
            return Boolean.FALSE;
        }

        if (enumClass.isEnum()) {
            Object[] objs = enumClass.getEnumConstants();
            for (int i = 0; i < objs.length; i++) {
                EnumCode enumCode = (EnumCode) objs[i];
                if (enumCode.getCode().equals(value)) {
                    return Boolean.TRUE;
                }
            }
        }
        return Boolean.FALSE;
    }

    /**
     * code转换码值枚举
     *
     * @param enumClass
     * @param code
     * @param <E>
     * @return
     */
    public static <E extends Enum<E>> E valueOfCode(Class<E> enumClass, String code) {
        if (!EnumCode.class.isAssignableFrom(enumClass)) {
            ExceptionUtil.throwAppException("[" + enumClass.getSimpleName() + "]不是派生自EnumCode");
        }
        EnumCode[] enumArr = (EnumCode[]) enumClass.getEnumConstants();
        for (EnumCode e : enumArr) {
            if (code.equals(e.getCode())) {
                return (E) e;
            }
        }
        return null;
    }

    /**
     * 描述转换码值枚举
     *
     * @param enumClass
     * @param desc
     * @param <E>
     * @return
     */
    public static <E extends Enum<E>> E valueOfDesc(Class<E> enumClass, String desc) {
        if (!EnumCode.class.isAssignableFrom(enumClass)) {
            ExceptionUtil.throwAppException("[" + enumClass.getSimpleName() + "]不是派生自EnumCode");
        }
        EnumCode[] enumArr = (EnumCode[]) enumClass.getEnumConstants();
        for (EnumCode e : enumArr) {
            if (desc.equals(e.getDesc())) {
                return (E) e;
            }
        }
        return null;
    }
}
