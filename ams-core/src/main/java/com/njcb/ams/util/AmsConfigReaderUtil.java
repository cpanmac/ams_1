package com.njcb.ams.util;

import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @author liuyanlong
 */
public class AmsConfigReaderUtil {
	private static final Logger logger = LoggerFactory.getLogger(AmsConfigReaderUtil.class);
	private static AmsConfigReaderUtil loadConf = new AmsConfigReaderUtil();
	private static ResourceBundle resourceBundle = null;

	private AmsConfigReaderUtil() {
	}

	public static AmsConfigReaderUtil getInstance() {
		return loadConf;
	}

	public void load(String url) {
		try {
			resourceBundle = ResourceBundle.getBundle(url);
		} catch (Exception e) {
			logger.debug(e.getMessage(), e);
			logger.error("加载配置文件失败");
		}
	}

	public String getProperty(String key) {
		return resourceBundle.getString(key);
	}
}
