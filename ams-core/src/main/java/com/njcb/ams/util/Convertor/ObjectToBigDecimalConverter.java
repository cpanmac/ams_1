package com.njcb.ams.util.convertor;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * @author Alan He
 * @create 2021-12-01-10:21
 */
public class ObjectToBigDecimalConverter extends BidirectionalConverter<Object, BigDecimal> {
    private static final Logger logger = LoggerFactory.getLogger(ObjectToBigDecimalConverter.class);

    @Override
    public BigDecimal convertTo(Object o, Type<BigDecimal> type, MappingContext mappingContext) {
        if(o instanceof String) {
            if(StringUtils.isBlank((String) o)) {
                logger.error("字符串为空");
                return null;
            }
            try {
                return new BigDecimal((String) o);
            } catch(Exception e) {
                e.printStackTrace();
                logger.error("字段非BigDecimal类型", e);
                return null;
            }
        }
        return null;
    }

    @Override
    public Object convertFrom(BigDecimal bigDecimal, Type<Object> type, MappingContext mappingContext) {
        try {
            return bigDecimal.toString();
        } catch(Exception e) {
            e.printStackTrace();
            logger.error("字段非BigDecimal类型", e);
            return null;
        }
    }
}
