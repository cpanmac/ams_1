package com.njcb.ams.util;

import org.jasypt.util.text.BasicTextEncryptor;

/**
 * 
 * @author alan
 * @version 1.0
 * @since 1.0
 */
public class AmsDESUtils {

	/**
	 * 加密方法
	 * @param text 明文
	 * @param password 密钥
	 * @return
	 */
	public static String encrypt(String text, String password){
		BasicTextEncryptor basicTextEncryptor = new BasicTextEncryptor();
		//设置密钥
		basicTextEncryptor.setPassword(password);
		// 加密
		return basicTextEncryptor.encrypt(text);
	}

	/**
	 * 解密方法
	 * @param text 密文
	 * @param password 密钥
	 * @return
	 */
	public static String decrypt(String text, String password){
		BasicTextEncryptor basicTextEncryptor = new BasicTextEncryptor();
		//设置密钥
		basicTextEncryptor.setPassword(password);
		return basicTextEncryptor.decrypt(text);
	}


	public static void main(String[] args) {
		// 加密
		System.out.println(encrypt("ccs","helloAlanGoodMorning"));
		//解密
		System.out.println(decrypt("vTWHVSrg+Nxl+cLP1EyaPg==","helloAlanGoodMorning"));
	}

}
