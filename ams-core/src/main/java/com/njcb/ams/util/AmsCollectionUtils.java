package com.njcb.ams.util;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.collections.CollectionUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
/**
 * @author liuyanlong
 */
public class AmsCollectionUtils extends CollectionUtils {

	
	/**
	 * 创建集合
	 * @param key
	 * @param value
	 * @return
	 */
	public static <K, V> HashMap<K, V> newHashMap(K key, V value) {
		HashMap<K, V> map = Maps.newHashMap();
		map.put(key, value);
		return map;
	}
	
	public static <K, V> HashMap<K, V> newHashMap() {
		return Maps.newHashMap();
	}
	
	
	public static <E> ArrayList<E> newArrayList() {
	    return new ArrayList<E>();
	}

	public static <E> ArrayList<E> newArrayList(@SuppressWarnings("unchecked") E... elements) {
		return Lists.newArrayList(elements);
	}
	
}
