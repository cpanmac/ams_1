package com.njcb.ams.util.convertor;

import com.njcb.ams.util.AmsDateUtils;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Alan He
 * @create 2021-12-01-10:24
 */
public class ObjectToDateConverter extends BidirectionalConverter<Object, Date> {
    private static final Logger logger = LoggerFactory.getLogger(ObjectToDateConverter.class);

    @Override
    public Date convertTo(Object o, Type<Date> type, MappingContext mappingContext) {
        if (o instanceof String) {
            try {
                return new SimpleDateFormat("yyyyMMdd").parse((String) o);
            } catch (ParseException e) {
                e.printStackTrace();
                logger.error("字段非Date类型", e);
                return null;
            }
        }
        return null;
    }

    @Override
    public Object convertFrom(Date date, Type<Object> type, MappingContext mappingContext) {
        try {
            return AmsDateUtils.format(date, "yyyyMMdd");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("字段非Date类型", e);
            return null;
        }
    }
}