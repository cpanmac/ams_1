package com.njcb.ams.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.njcb.ams.support.exception.ExceptionCode;
import com.njcb.ams.support.exception.ExceptionUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liuyanlong
 */
public class AmsJsonUtils {

    private static ObjectMapper objectMapper = new ObjectMapper();

    /**
     * json转map数组
     * @param jsonStr
     * @return
     */
    public static List<Map<String, String>> toArrayMap(String jsonStr) {
        if (StringUtils.isEmpty(jsonStr)) {
            return new ArrayList<Map<String, String>>();
        }
        List<Map<String, String>> listmap = null;
        try {
            listmap = objectMapper.readValue(jsonStr, new TypeReference<List<Map<String, String>>>() {
            });
        } catch (Exception e) {
            ExceptionUtil.printStackTrace(e);
            ExceptionUtil.throwAppException(jsonStr, ExceptionCode.JSON_PARSE_EXCEPTION);
        }
        return listmap;
    }

    /**
     * json转Object
     * @param jsonStr
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> T jsonToObject(String jsonStr, Class<T> cls) {
        if (StringUtils.isEmpty(jsonStr) || cls == null) {
            return null;
        }
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        T t = null;
        try {
            t = objectMapper.readValue(jsonStr, cls);
        } catch (Exception e) {
            ExceptionUtil.throwAppException(e.getMessage(), ExceptionCode.JSON_PARSE_EXCEPTION);
        }
        return t;
    }

    /**
     * json转对象数组
     * @param jsonStr
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> List<T> toArrayList(String jsonStr, Class<T> cls) {
        List<T> list = new ArrayList<T>();
        if (StringUtils.isEmpty(jsonStr)) {
            return list;
        }
        try {
            List<Map<String, String>> mapList = toArrayMap(jsonStr);
            for (Map<String, String> map : mapList) {
                T obj = cls.newInstance();
                BeanUtils.populate(obj, map);
                list.add(obj);
            }
        } catch (Exception e) {
            ExceptionUtil.throwAppException(e.getMessage(), ExceptionCode.JSON_PARSE_EXCEPTION);
        }
        return list;
    }

    /**
     * json转数组
     * @param jsonStr
     * @return
     */
    public static List<String> toArrayList(String jsonStr) {
        if (StringUtils.isEmpty(jsonStr)) {
            return new ArrayList<String>();
        }
        List<String> list = null;
        try {
            list = objectMapper.readValue(jsonStr, new TypeReference<List<String>>() {
            });
        } catch (Exception e) {
            ExceptionUtil.printStackTrace(e);
            ExceptionUtil.throwAppException(jsonStr, ExceptionCode.JSON_PARSE_EXCEPTION);
        }
        return list;
    }

    /**
     * map转Json字符串
     * @param jsonStr
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> jsonToMap(String jsonStr) {
        if (StringUtils.isEmpty(jsonStr)) {
            return new HashMap<>(0);
        }
        Map<String, Object> map = null;
        try {
            map = objectMapper.readValue(jsonStr, Map.class);
        } catch (Exception e) {
            ExceptionUtil.throwAppException(jsonStr + "\n" + e.getMessage(), ExceptionCode.JSON_PARSE_EXCEPTION);
        }
        return map;
    }

    /**
     * map转Json字符串
     * @param map
     * @return
     */
    public static String mapToJson(Map<String, String> map) {
        JsonNode json = objectMapper.convertValue(map, JsonNode.class);
        return json.toString();
    }

    /**
     * 对象转map
     * @param o
     * @return
     */
    public static Map<String, String> objectToMap(Object o) {
        @SuppressWarnings("unchecked")
        Map<String, String> map = objectMapper.convertValue(o, Map.class);
        return map;
    }

    /**
     * map转对象
     * @param map
     * @param toValueType
     * @param <T>
     * @return
     */
    public static <T> T map2Object(Map<String, String> map, Class<T> toValueType) {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        T o = objectMapper.convertValue(map, toValueType);
        return o;
    }

    /**
     * object转json字符串
     * @param obj
     * @return
     */
    public static String objectToJson(Object obj) {
        try {
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            ExceptionUtil.printStackTrace(e);
            ExceptionUtil.throwAppException(e.getMessage(), ExceptionCode.JSON_PARSE_EXCEPTION);
        }
        return null;
    }

    /**
     * 判断字符串是否json格式
     * @param jsonStr
     * @return
     */
    public static Boolean isJson(String jsonStr) {
        try {
            objectMapper.readValue(jsonStr, Map.class);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
