/**
 * 公共工具包
 * 提供开发平台依赖的各种基础工具类
 * 
 * @author liuyanlong
 */
package com.njcb.ams.util;