package com.njcb.ams.util;

import com.njcb.ams.support.codevalue.EnumCode;
import com.njcb.ams.support.exception.ExceptionUtil;

/**
 * 枚举字段工具类
 * @author liuyanlong
 */
@Deprecated
public class AmsEnumCodeUtils {

    /**
     * 判断枚举值是否有效
     * @param enumClass
     * @param value
     * @return
     */
    public static boolean isValid(Class enumClass, Object value) {
        return AmsEnumUtils.isValid(enumClass,value);
    }

    /**
     * code转换码值枚举
     *
     * @param enumClass
     * @param code
     * @param <E>
     * @return
     */
    public static <E extends Enum<E>> E valueOfCode(Class<E> enumClass, String code) {
        if (!EnumCode.class.isAssignableFrom(enumClass)) {
            ExceptionUtil.throwAppException("[" + enumClass.getSimpleName() + "]不是派生自EnumCode");
        }
        EnumCode[] enumArr = (EnumCode[]) enumClass.getEnumConstants();
        for (EnumCode e : enumArr) {
            if (code.equals(e.getCode())) {
                return (E) e;
            }
        }
        return null;
    }

    /**
     * 描述转换码值枚举
     *
     * @param enumClass
     * @param desc
     * @param <E>
     * @return
     */
    public static <E extends Enum<E>> E valueOfDesc(Class<E> enumClass, String desc) {
        if (!EnumCode.class.isAssignableFrom(enumClass)) {
            ExceptionUtil.throwAppException("[" + enumClass.getSimpleName() + "]不是派生自EnumCode");
        }
        EnumCode[] enumArr = (EnumCode[]) enumClass.getEnumConstants();
        for (EnumCode e : enumArr) {
            if (desc.equals(e.getDesc())) {
                return (E) e;
            }
        }
        return null;
    }
}
