package com.njcb.ams.util;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.repository.dao.SysInfoDAO;
import com.njcb.ams.repository.entity.SysInfo;

/**
 * @author LOONG
 */
public class SysInfoUtils {

    public static SysInfo getSysInfo() {
        return AppContext.getBean(SysInfoDAO.class).selectSysInfo();
    }

    public static String getSysId() {
        return getSysInfo().getSysId();
    }

    public static String getChannelId() {
        return getSysInfo().getChannelId();
    }

    public static String getSysName() {
        return getSysInfo().getSysName();
    }

    public static String getBusiDate() {
        return getSysInfo().getBusiDate();
    }

}
