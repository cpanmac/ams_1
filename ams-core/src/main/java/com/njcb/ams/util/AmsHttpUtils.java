package com.njcb.ams.util;

import com.njcb.ams.portal.SysBaseDefine;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/**
 * @author LOONG
 */
public class AmsHttpUtils {

    /**
     * ajax头标识
     */
    private static final String AJAX_HEAD = "X-Requested-With";
    private static final String AJAX_HEAD_VALUE = "XMLHttpRequest";

	/**
	 * 判断是否Ajax请求
	 * @param request
	 * @return
	 */
	public static Boolean isAjaxRequest(HttpServletRequest request) {
	    if(null == request || null == request.getHeader(AJAX_HEAD)){
	        return false;
        }
		if(AJAX_HEAD_VALUE.toUpperCase().equals(request.getHeader(AJAX_HEAD).toUpperCase())){
			return true;
		}
		return false;
	}

    /**
     * URL 解码
     */
    public static String getURLDecoderString(String str, String enc) {
        String result = "";
        if (null == str) {
            return "";
        }
        try {
            result = java.net.URLDecoder.decode(str, enc);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * URL 转码
     */
    public static String getURLEncoderString(String str, String enc) {
        String result = "";
        if (null == str) {
            return "";
        }
        try {
            result = java.net.URLEncoder.encode(str, enc);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<String,String> parseQueryString(HttpServletRequest request) {
        Map<String,String> paramMap = AmsCollectionUtils.newHashMap();
        if(null == request || null == request.getQueryString() || request.getQueryString().isEmpty()) {
            return paramMap;
        }else{
            return parseParams(request.getQueryString());
        }
    }

    public static Map parseParams(String urlQueryParam) {
        if(!urlQueryParam.startsWith("?")){
            urlQueryParam = "?" + urlQueryParam;
        }
        Map map = new HashMap();
        try {
            final String charset = "utf-8";
            urlQueryParam = URLDecoder.decode(urlQueryParam, charset);
            if (urlQueryParam.indexOf('?') != -1) {
                final String contents = urlQueryParam.substring(urlQueryParam.indexOf('?') + 1);
                String[] keyValues = contents.split("&");
                for (int i = 0; i < keyValues.length; i++) {
                    String key = keyValues[i].substring(0, keyValues[i].indexOf("="));
                    String value = keyValues[i].substring(keyValues[i].indexOf("=") + 1);
                    map.put(key, value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

}
