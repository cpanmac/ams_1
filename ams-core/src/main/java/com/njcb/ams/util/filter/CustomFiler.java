package com.njcb.ams.util.filter;

import ma.glasnost.orika.CustomFilter;
import ma.glasnost.orika.Filter;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.NullFilter;
import ma.glasnost.orika.metadata.Property;
import ma.glasnost.orika.metadata.Type;

/**
 * @author Alan He
 * @create 2021-12-01-12:35
 */
public class CustomFiler<A, B> extends NullFilter<A, B> {

    @Override
    public <S extends A, D extends B> boolean shouldMap(Type<S> type, String s, S s1, Type<D> type1, String s2, D d, MappingContext mappingContext) {
        if(s1==null || s1.equals("")){
            return false;
        }
        return true;
    }

}
