package com.njcb.ams.util.convertor;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Alan He
 * @create 2021-12-01-10:25
 */
public class ObjectToIntegerConverter extends BidirectionalConverter<Object, Integer> {
    private static final Logger logger = LoggerFactory.getLogger(ObjectToDateConverter.class);


    @Override
    public Integer convertTo(Object o, Type<Integer> type, MappingContext mappingContext) {
        if(o instanceof String) {
            try {
                return Integer.parseInt((String) ((String) o).trim());
            } catch(NumberFormatException e) {
                e.printStackTrace();
                logger.error("字段非Integer类型", e);
                return null;
            }
        }
        return null;
    }

    @Override
    public Object convertFrom(Integer integer, Type<Object> type, MappingContext mappingContext) {
        try {
            return integer;
        } catch(NumberFormatException e) {
            e.printStackTrace();
            logger.error("字段非Integer类型", e);
            return null;
        }
    }
}
