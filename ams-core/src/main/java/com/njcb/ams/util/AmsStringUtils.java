package com.njcb.ams.util;

import org.apache.commons.lang.StringUtils;

/**
 * @author srxhx207
 */
public class AmsStringUtils extends StringUtils{

	/**
	 * 返回字符串，如果为空则返回默认
	 * @param value
	 * @param defaultValue
	 * @return
	 */
	public static String getOrDefault(String value,String defaultValue) {
		if(null != value){
			return value;
		}else {
			return defaultValue;
		}
	}

    public static boolean hasLength(String str) {
    	return null != str && str.length() > 0;
    }
    
    /**
	 * 字符串填充.
	 * @param string  原始字符串
	 * @param filler  用于填充的字符
	 * @param totalLength 填充后的总长度
	 * @param atEnd  true:后填充 false:前填充
	 * @return the string
	 */
	public static String fillString(String string, char filler, int totalLength, boolean atEnd) {
		byte[] tempbyte = string.getBytes();
		int currentLength = tempbyte.length;
		int delta = totalLength - currentLength;
		for (int i = 0; i < delta; i++) {
			if (atEnd) {
				string += filler;
			} else {
				string = filler + string;
			}
		}
		return string;
	}

    public static boolean isEmptyExp(String str) {
    	return str == null || str.length() == 0 || "null".equals(str);
    }
    
    /**
     * <p>检查决策库返回结果是否为空.</p>
     *
     * <pre>
     * AmsStringUtils.isNotEmptyExp(null)      = true
     * AmsStringUtils.isNotEmptyExp("null")    = true
     * AmsStringUtils.isNotEmptyExp("")        = true
     * AmsStringUtils.isNotEmptyExp(" ")       = false
     * AmsStringUtils.isNotEmptyExp("bob")     = false
     * AmsStringUtils.isNotEmptyExp("  bob  ") = false
     * </pre>
     *
     * @param str  the String to check, may be null
     * @return <code>true</code> if the String is not empty and not null
     */
    public static boolean isNotEmptyExp(String str) {
    	return !isEmptyExp(str);
    }
    
    /**
     * <p>检查决策库返回结果是否为空.</p>
     *
     * <pre>
     * AmsStringUtils.isNotEmptyExp(null)      = false
     * AmsStringUtils.isNotEmptyExp("null")    = false
     * AmsStringUtils.isNotEmptyExp("")        = false
     * AmsStringUtils.isNotEmptyExp(" ")       = true
     * AmsStringUtils.isNotEmptyExp("bob")     = true
     * AmsStringUtils.isNotEmptyExp("  bob  ") = true
     * </pre>
     *
     * @param str  the String to check, may be null
     * @return <code>true</code> if the String is not empty and not null
     */
    public static void main(String[] args) {
		String s = "DecisionResult";
		System.out.println(isNotEmptyExp(s));
		if (isNotEmptyExp(s)) {
			System.out.println("进入if");
		}
	}
    
}
