package com.njcb.ams.util;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.service.ParamManageService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author LOONG
 */
@Component
public class AmsParamUtils {
    public static final String MAX_EXPORT = "MaxExport";

    @Resource
    private ParamManageService paramService;

    public static AmsParamUtils getInstance() {
        return AppContext.getBean(AmsParamUtils.class);
    }

    public static Integer getMaxExport() {
        Integer maxExport = SysBaseDefine.MAX_EXPORT_EXCEL_NUMBER;
        String maxExportParam = getInstance().paramService.getValueByName(MAX_EXPORT);
        if (AmsUtils.isNotNull(maxExportParam)) {
            maxExport = Integer.parseInt(maxExportParam);
        }
        return maxExport;
    }

}