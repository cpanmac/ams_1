package com.njcb.ams.util;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;

/**
 * @author LOONG
 */
public class AmsProjectUtils {

	public static String getProjectPath(Class<?> cls) {
		URL result = null;
		String path = null;
		ProtectionDomain pd = cls.getProtectionDomain();
		CodeSource cs = pd.getCodeSource();

		if (cs != null) {
			result = cs.getLocation();
		}

		if (result == null) {
			String filePath = cls.getName().replace('.', '/').concat(".class");
			ClassLoader loader = cls.getClassLoader();
			result = loader != null ? loader.getResource(filePath) : ClassLoader.getSystemResource(filePath);
		}
		try {
			path = new File(result.getFile()).getCanonicalPath();
		} catch (IOException e) {
			path = e.getMessage();
		}
		path = path.substring(0, path.indexOf("\\class"));
		return path;
	}

}
