package com.njcb.ams.util;

import com.njcb.ams.support.exception.ExceptionUtil;
import org.apache.commons.lang.time.DateFormatUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * @author liuyanlong
 */
public class AmsDateUtils {

    /**
     * 会计日期
     *
     * @return
     */
    public static String getBusiDate() {
        return SysInfoUtils.getSysInfo().getBusiDate();
    }

    /**
     * 上一会计日期
     *
     * @return
     */
    public static String getLbatDate() {
        return SysInfoUtils.getSysInfo().getLbatDate();
    }

    /**
     * 下一会计日期
     *
     * @return
     */
    public static String getNbatDate() {
        return SysInfoUtils.getSysInfo().getNbatDate();
    }

    public static String getNextDate(String currDate8) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate currDate = LocalDate.parse(currDate8, formatter);
        String nextDate = currDate.plusDays(1).format(formatter);
        return nextDate;
    }

    public static String getBeforeDate(String currDate8) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate currDate = LocalDate.parse(currDate8, formatter);
        String nextDate = currDate.plusDays(-1).format(formatter);
        return nextDate;
    }


    /**
     * 获取当前日期
     *
     * @return
     */
    public final static String getCurrentDate10() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    public final static String getCurrentDate8() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    }

    public final static String getCurrentDate6() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern("yyMMdd"));
    }


    /**
     * 获取当前时间
     *
     * @return
     */
    public final static String getCurrentTime6() {
        return LocalTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    }

    public final static String getCurrentTime14() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
    }


    /**
     * 计算两个日期相隔的天数
     *
     * @param startDate8
     * @param endDate8
     * @return
     */
    public static int getDaysBetween(String startDate8, String endDate8) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate startDate = LocalDate.parse(startDate8, formatter);
        LocalDate endDate = LocalDate.parse(endDate8, formatter);
        return (int) ChronoUnit.DAYS.between(startDate, endDate);
    }

    public static Date parseDate(String source, String pattern,String message) {
        try {
            return new SimpleDateFormat(pattern).parse(source);
        } catch (ParseException e) {
            ExceptionUtil.throwAppException(message);
        }
        return null;
    }

    public static Date parseDate(String source, String pattern) {
        try {
            return new SimpleDateFormat(pattern).parse(source);
        } catch (ParseException e) {
            ExceptionUtil.throwAppException(e.getMessage());
        }
        return null;
    }

    public static String format(Date date, String pattern) {
        if (null == date || null == pattern) {
            return "";
        }
        return DateFormatUtils.format(date, pattern);
    }

    public static String getString18(Date date) {
        if (null == date) {
            return "";
        }
        LocalDateTime ldt = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return dtf.format(ldt);
    }

    public static Date getDateFromString18(String dateTime18) {
        if (AmsUtils.isNull(dateTime18)) {
            return null;
        }
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime localDateTime = LocalDateTime.parse(dateTime18, dtf);
        Date date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        return date;
    }

}
