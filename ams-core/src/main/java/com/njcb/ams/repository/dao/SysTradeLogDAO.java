package com.njcb.ams.repository.dao;

import com.njcb.ams.bootconfig.AmsProperties;
import com.njcb.ams.factory.AmsContextHolder;
import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.SysTradeLogMapper;
import com.njcb.ams.repository.entity.SysTradeConsole;
import com.njcb.ams.repository.entity.SysTradeLog;
import com.njcb.ams.repository.entity.SysTradeLogExample;
import com.njcb.ams.util.AmsCollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author LOONG
 */
@Repository
public class SysTradeLogDAO extends BaseMyBatisDAO<SysTradeLog, SysTradeLogExample, Integer>{

	public SysTradeLogDAO() {
		this.entityClass = SysTradeLog.class;
	}

	private static final String BATCH_INSERT_ORACLE_SQL =
			"insert into SYS_TRADE_LOG (ID, TRADE_CODE, TRADE_NAME, TRADE_TYPE, TRADE_NODE, TRADE_LEVEL, USE_TIME, REQ_SEQ, GLOBAL_SEQ, TRANS_SEQ, REQ_SYS, REQ_DATE, REQ_TIME, "
					+ "RESP_SEQ, RESP_DATE, RESP_TIME, RESP_RCV_TIME, TRANS_STATUS, RET_CODE, RET_MSG, BUSI_SIGN, TIME_STAMP, APP_VERSION, REMARK) "
					+ "values (SEQ_SYS_TRADE_LOG.NEXTVAL,:tradeCode,:tradeName,:tradeType,:tradeNode,:tradeLevel,:useTime,:reqSeq,:globalSeq,:transSeq,:reqSys,:reqDate,:reqTime,"
					+ ":respSeq,:respDate,:respTime,:respRcvTime,:transStatus,:retCode,:retMsg,:busiSign,:timeStamp,:appVersion,:remark)";

	private static final String BATCH_INSERT_MYSQL_SQL =
			"insert into SYS_TRADE_LOG (TRADE_CODE, TRADE_NAME, TRADE_TYPE, TRADE_NODE, TRADE_LEVEL, USE_TIME, REQ_SEQ, GLOBAL_SEQ, TRANS_SEQ, REQ_SYS, REQ_DATE, REQ_TIME, "
					+ "RESP_SEQ, RESP_DATE, RESP_TIME, RESP_RCV_TIME, TRANS_STATUS, RET_CODE, RET_MSG, BUSI_SIGN, TIME_STAMP, APP_VERSION, REMARK) "
					+ "values (:tradeCode,:tradeName,:tradeType,:tradeNode,:tradeLevel,:useTime,:reqSeq,:globalSeq,:transSeq,:reqSys,:reqDate,:reqTime,"
					+ ":respSeq,:respDate,:respTime,:respRcvTime,:transStatus,:retCode,:retMsg,:busiSign,:timeStamp,:appVersion,:remark)";

	@Autowired
	private SysTradeLogMapper mapper;

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private AmsProperties amsProperties;

	@Override
	@SuppressWarnings("unchecked")
	public SysTradeLogMapper getMapper() {
		return mapper;
	}

	@Override
    public Integer insert(SysTradeLog tradeLog) {
		if(!amsProperties.getDbEnable()){
			return 1;
		}
		return insertBatch(AmsCollectionUtils.newArrayList(tradeLog));
	}
	
	
	@Override
	public Integer insertBatch(List<SysTradeLog> tradeLogs) {
		if(!amsProperties.getDbEnable()){
			return 1;
		}
		if(SysBaseDefine.DatabaseId.ORACLE.getCode().equals(AmsContextHolder.getSysContext().getDatabaseId())){
			return insertOracleBatch(tradeLogs);
		}else{
			return insertMysqlBatch(tradeLogs);
		}
	}


	public Integer insertOracleBatch(List<SysTradeLog> tradeLogs) {
		if (!amsProperties.getDbEnable()) {
			return 1;
		}
		NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
		SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(tradeLogs.toArray());
		int[] updateArray = namedParameterJdbcTemplate.batchUpdate(BATCH_INSERT_ORACLE_SQL, batch);
		int updateCount = 0;
		for (int val : updateArray) {
			updateCount += val;
		}
		return updateCount;
	}

	public Integer insertMysqlBatch(List<SysTradeLog> tradeLogs) {
		if (!amsProperties.getDbEnable()) {
			return 1;
		}
		NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
		SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(tradeLogs.toArray());
		int[] updateArray = namedParameterJdbcTemplate.batchUpdate(BATCH_INSERT_MYSQL_SQL, batch);
		int updateCount = 0;
		for (int val : updateArray) {
			updateCount += val;
		}
		return updateCount;
	}

	public List<?> getTradeUseTime(SysTradeConsole inBean) {
		StringBuilder sbsql = new StringBuilder(200);
		sbsql.append(" select t.trade_code, avg(t.use_time) as use_time, count(1) as trade_sum, ");
		sbsql.append(" sum(case when t.trans_status = 'S' then 1 else 0 end) as trade_succ ");
		sbsql.append("   from SYS_TRADE_LOG t ");
		sbsql.append("  where 1 = 1 ");
		if (null != inBean && null != inBean.getTimeCycle()) {
			sbsql.append("    and t.time_stamp > " + (System.currentTimeMillis() - (inBean.getTimeCycle() * 1000)));
		} else {
			sbsql.append("    and t.time_stamp > " + (System.currentTimeMillis() - 86400000));
		}
		if (null != inBean && StringUtils.isNotEmpty(inBean.getTradeCode())) {
			sbsql.append("    and t.trade_code like '%" + inBean.getTradeCode() + "%' ");
		}
		if (null != inBean && StringUtils.isNotEmpty(inBean.getTradeName())) {
			sbsql.append("    and t.trade_name like '%" + inBean.getTradeName() + "%' ");
		}
		sbsql.append("  group by t.trade_code ");
		sbsql.append("  ");
		List<Map<String, Object>> retList = jdbcTemplate.queryForList(sbsql.toString());
		return retList;
	}
	
	public SysTradeLog selectByReqDateAndSeq(String reqDate, String reqSeq) {
		SysTradeLogExample example = new SysTradeLogExample();
		example.createCriteria().andReqDateEqualTo(reqDate).andReqSeqEqualTo(reqSeq);
		return selectOneByExample(example);
	}

}
