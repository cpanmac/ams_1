package com.njcb.ams.repository.dao;

import com.njcb.ams.bootconfig.AmsProperties;
import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.SysInfoMapper;
import com.njcb.ams.repository.entity.SysInfo;
import com.njcb.ams.repository.entity.SysInfoExample;
import com.njcb.ams.util.AmsDateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author
 */
@Repository
public class SysInfoDAO extends BaseMyBatisDAO<SysInfo, SysInfoExample, Integer> {
	private static final Logger logger = LoggerFactory.getLogger(SysInfoDAO.class);

	public SysInfoDAO() {
		this.entityClass = SysInfo.class;
	}

	@Autowired
	private AmsProperties amsProperties;
	
	@Autowired
	private SysInfoMapper mapper;

	@Override
	@SuppressWarnings("unchecked")
	public SysInfoMapper getMapper() {
		return mapper;
	}

	@Cacheable(value = "SysParamCache", key = "'SysInfo'")
	public SysInfo selectSysInfo() {
		logger.debug("获取交易日期查缓存未命中，查询数据库");
		if(amsProperties.getDbEnable()){
			return getSysInfoByInspType();
		}else{
			SysInfo sysInfo = new SysInfo();
			sysInfo.setId(1);
			sysInfo.setSysId(amsProperties.getSysId());
			sysInfo.setSysShort(amsProperties.getSysShort());
			sysInfo.setSysName(amsProperties.getSysName());
			sysInfo.setChannelId(amsProperties.getChannelId());
			sysInfo.setBusiDate(AmsDateUtils.getCurrentDate8());
    		sysInfo.setLbatDate(AmsDateUtils.getBeforeDate(sysInfo.getBusiDate()));
    		sysInfo.setNbatDate(AmsDateUtils.getNextDate(sysInfo.getBusiDate()));
			return sysInfo;
		}
		
	}

	public List<SysInfo> selectAll() {
		return mapper.selectByExample(new SysInfoExample());
	}
	
	@Override
	@CacheEvict(value = "SysParamCache", key = "'SysInfo'")
	public Integer updateByPrimaryKey(SysInfo sysInfo) {
		Integer count = mapper.updateByPrimaryKey(sysInfo);
		return count;
	}

	@Override
	@CacheEvict(value = "SysParamCache", key = "'SysInfo'")
	public Integer updateByPrimaryKeySelective(SysInfo sysInfo) {
		Integer count = mapper.updateByPrimaryKeySelective(sysInfo);
		return count;
	}

	@CacheEvict(value = "SysParamCache", key = "'SysInfo'")
	public void updateCacheSysInfo() {
		SysInfo sysInfo = getSysInfoByInspType();
		SysBaseDefine.SYS_ID = sysInfo.getSysId();
		SysBaseDefine.SYS_NAME = sysInfo.getSysName();
		SysBaseDefine.BUSI_DATE = sysInfo.getBusiDate();
	}

	/**
	 * 根据系统日期取值方式获取系统信息
	 * @return
	 */
	private SysInfo getSysInfoByInspType(){
		SysInfo sysInfo = mapper.selectByPrimaryKey(1);
		/**
		 * 如果sysinfo未配置、或配置类型为检查物理日期，则直接取物理日期
		 */
		if (SysInfo.INSP_TYPE_1.equals(sysInfo.getInspType())) {
			sysInfo.setBusiDate(AmsDateUtils.getCurrentDate8());
			sysInfo.setLbatDate(AmsDateUtils.getBeforeDate(sysInfo.getBusiDate()));
			sysInfo.setNbatDate(AmsDateUtils.getNextDate(sysInfo.getBusiDate()));
		}
		return sysInfo;
	}

}
