package com.njcb.ams.repository.dao.mapper;

import java.util.List;

import com.njcb.ams.repository.entity.SysTradeLog;
import com.njcb.ams.repository.entity.SysTradeLogExample;
import org.springframework.stereotype.Component;

@Component
public interface SysTradeLogMapper extends BaseMapper<SysTradeLog, SysTradeLogExample, Integer> {
	List<SysTradeLog> queryCountOfBusinessTop(SysTradeLog record);
	List<SysTradeLog> queryTimeOfBusinessTop(SysTradeLog record);
	List<SysTradeLog> querySuccRateOfBusinessTop(SysTradeLog record);
}