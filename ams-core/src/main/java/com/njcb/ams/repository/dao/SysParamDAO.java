package com.njcb.ams.repository.dao;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.SysParamMapper;
import com.njcb.ams.repository.entity.SysParam;
import com.njcb.ams.repository.entity.SysParamExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SysParamDAO extends BaseMyBatisDAO<SysParam, SysParamExample, Integer> {

	public SysParamDAO() {
		this.entityClass = SysParam.class;
	}

	@Autowired
	private SysParamMapper mapper;

	@Override
	@SuppressWarnings("unchecked")
	public SysParamMapper getMapper() {
		return mapper;
	}

	public List<SysParam> selectAll() {
		return mapper.selectByExample(new SysParamExample());
	}

	public SysParam getSysParamByName(String name) {
		SysParamExample example = new SysParamExample();
		SysParamExample.Criteria c = example.createCriteria();
		c.andNameEqualTo(name);
		SysParam sysParam = selectOneByExample(example);
		return sysParam;
	}

	/**
	 * 根据名称获取对应值
	 * @param name
	 * @return
	 */
	public String getValueByName(String name) {
		SysParamExample example = new SysParamExample();
		SysParamExample.Criteria c = example.createCriteria();
		c.andNameEqualTo(name);
		SysParam sysParam = selectOneByExample(example);
		// 无对应记录返回空
		if (sysParam == null) {
			return null;
		}
		return sysParam.getValue();
	}

}
