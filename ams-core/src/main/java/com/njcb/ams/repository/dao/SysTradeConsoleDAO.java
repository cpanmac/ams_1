package com.njcb.ams.repository.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.njcb.ams.bootconfig.AmsProperties;
import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.SysTradeConsoleMapper;
import com.njcb.ams.repository.entity.SysTradeConsole;
import com.njcb.ams.repository.entity.SysTradeConsoleExample;

@Repository
public class SysTradeConsoleDAO extends BaseMyBatisDAO<SysTradeConsole, SysTradeConsoleExample, Integer> {

	public SysTradeConsoleDAO() {
		this.entityClass = SysTradeConsole.class;
	}

	@Autowired
	private SysTradeConsoleMapper mapper;
	
	@Autowired
	private AmsProperties amsProperties;

	@Override
	@SuppressWarnings("unchecked")
	public SysTradeConsoleMapper getMapper() {
		return mapper;
	}

	public List<SysTradeConsole> selectAll() {
		if(amsProperties.getDbEnable()){
			return mapper.selectByExample(new SysTradeConsoleExample());
		}else{
			return new ArrayList<SysTradeConsole>();
		}
	}
	
	@Override
	public Integer insert(SysTradeConsole record) {
		if(amsProperties.getDbEnable()){
			return getMapper().insert(record);
		}else{
			return 1;
		}
	}
	
	@Override
	public Integer deleteByPrimaryKey(Integer id) {
		if(amsProperties.getDbEnable()){
			return getMapper().deleteByPrimaryKey(id);
		}else{
			return 1;
		}
	}
	
	@Override
	public Integer updateByPrimaryKeySelective(SysTradeConsole record) {
		if(amsProperties.getDbEnable()){
			return getMapper().updateByPrimaryKeySelective(record);
		}else{
			return 1;
		}
		
	}

}
