package com.njcb.ams.repository.entity;

import java.util.ArrayList;
import java.util.List;

public class SysTradeLogExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SysTradeLogExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andTradeCodeIsNull() {
            addCriterion("TRADE_CODE is null");
            return (Criteria) this;
        }

        public Criteria andTradeCodeIsNotNull() {
            addCriterion("TRADE_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andTradeCodeEqualTo(String value) {
            addCriterion("TRADE_CODE =", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeNotEqualTo(String value) {
            addCriterion("TRADE_CODE <>", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeGreaterThan(String value) {
            addCriterion("TRADE_CODE >", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeGreaterThanOrEqualTo(String value) {
            addCriterion("TRADE_CODE >=", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeLessThan(String value) {
            addCriterion("TRADE_CODE <", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeLessThanOrEqualTo(String value) {
            addCriterion("TRADE_CODE <=", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeLike(String value) {
            addCriterion("TRADE_CODE like", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeNotLike(String value) {
            addCriterion("TRADE_CODE not like", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeIn(List<String> values) {
            addCriterion("TRADE_CODE in", values, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeNotIn(List<String> values) {
            addCriterion("TRADE_CODE not in", values, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeBetween(String value1, String value2) {
            addCriterion("TRADE_CODE between", value1, value2, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeNotBetween(String value1, String value2) {
            addCriterion("TRADE_CODE not between", value1, value2, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeNameIsNull() {
            addCriterion("TRADE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andTradeNameIsNotNull() {
            addCriterion("TRADE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andTradeNameEqualTo(String value) {
            addCriterion("TRADE_NAME =", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameNotEqualTo(String value) {
            addCriterion("TRADE_NAME <>", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameGreaterThan(String value) {
            addCriterion("TRADE_NAME >", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameGreaterThanOrEqualTo(String value) {
            addCriterion("TRADE_NAME >=", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameLessThan(String value) {
            addCriterion("TRADE_NAME <", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameLessThanOrEqualTo(String value) {
            addCriterion("TRADE_NAME <=", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameLike(String value) {
            addCriterion("TRADE_NAME like", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameNotLike(String value) {
            addCriterion("TRADE_NAME not like", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameIn(List<String> values) {
            addCriterion("TRADE_NAME in", values, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameNotIn(List<String> values) {
            addCriterion("TRADE_NAME not in", values, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameBetween(String value1, String value2) {
            addCriterion("TRADE_NAME between", value1, value2, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameNotBetween(String value1, String value2) {
            addCriterion("TRADE_NAME not between", value1, value2, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeTypeIsNull() {
            addCriterion("TRADE_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andTradeTypeIsNotNull() {
            addCriterion("TRADE_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andTradeTypeEqualTo(String value) {
            addCriterion("TRADE_TYPE =", value, "tradeType");
            return (Criteria) this;
        }

        public Criteria andTradeTypeNotEqualTo(String value) {
            addCriterion("TRADE_TYPE <>", value, "tradeType");
            return (Criteria) this;
        }

        public Criteria andTradeTypeGreaterThan(String value) {
            addCriterion("TRADE_TYPE >", value, "tradeType");
            return (Criteria) this;
        }

        public Criteria andTradeTypeGreaterThanOrEqualTo(String value) {
            addCriterion("TRADE_TYPE >=", value, "tradeType");
            return (Criteria) this;
        }

        public Criteria andTradeTypeLessThan(String value) {
            addCriterion("TRADE_TYPE <", value, "tradeType");
            return (Criteria) this;
        }

        public Criteria andTradeTypeLessThanOrEqualTo(String value) {
            addCriterion("TRADE_TYPE <=", value, "tradeType");
            return (Criteria) this;
        }

        public Criteria andTradeTypeLike(String value) {
            addCriterion("TRADE_TYPE like", value, "tradeType");
            return (Criteria) this;
        }

        public Criteria andTradeTypeNotLike(String value) {
            addCriterion("TRADE_TYPE not like", value, "tradeType");
            return (Criteria) this;
        }

        public Criteria andTradeTypeIn(List<String> values) {
            addCriterion("TRADE_TYPE in", values, "tradeType");
            return (Criteria) this;
        }

        public Criteria andTradeTypeNotIn(List<String> values) {
            addCriterion("TRADE_TYPE not in", values, "tradeType");
            return (Criteria) this;
        }

        public Criteria andTradeTypeBetween(String value1, String value2) {
            addCriterion("TRADE_TYPE between", value1, value2, "tradeType");
            return (Criteria) this;
        }

        public Criteria andTradeTypeNotBetween(String value1, String value2) {
            addCriterion("TRADE_TYPE not between", value1, value2, "tradeType");
            return (Criteria) this;
        }

        public Criteria andTradeNodeIsNull() {
            addCriterion("TRADE_NODE is null");
            return (Criteria) this;
        }

        public Criteria andTradeNodeIsNotNull() {
            addCriterion("TRADE_NODE is not null");
            return (Criteria) this;
        }

        public Criteria andTradeNodeEqualTo(String value) {
            addCriterion("TRADE_NODE =", value, "tradeNode");
            return (Criteria) this;
        }

        public Criteria andTradeNodeNotEqualTo(String value) {
            addCriterion("TRADE_NODE <>", value, "tradeNode");
            return (Criteria) this;
        }

        public Criteria andTradeNodeGreaterThan(String value) {
            addCriterion("TRADE_NODE >", value, "tradeNode");
            return (Criteria) this;
        }

        public Criteria andTradeNodeGreaterThanOrEqualTo(String value) {
            addCriterion("TRADE_NODE >=", value, "tradeNode");
            return (Criteria) this;
        }

        public Criteria andTradeNodeLessThan(String value) {
            addCriterion("TRADE_NODE <", value, "tradeNode");
            return (Criteria) this;
        }

        public Criteria andTradeNodeLessThanOrEqualTo(String value) {
            addCriterion("TRADE_NODE <=", value, "tradeNode");
            return (Criteria) this;
        }

        public Criteria andTradeNodeLike(String value) {
            addCriterion("TRADE_NODE like", value, "tradeNode");
            return (Criteria) this;
        }

        public Criteria andTradeNodeNotLike(String value) {
            addCriterion("TRADE_NODE not like", value, "tradeNode");
            return (Criteria) this;
        }

        public Criteria andTradeNodeIn(List<String> values) {
            addCriterion("TRADE_NODE in", values, "tradeNode");
            return (Criteria) this;
        }

        public Criteria andTradeNodeNotIn(List<String> values) {
            addCriterion("TRADE_NODE not in", values, "tradeNode");
            return (Criteria) this;
        }

        public Criteria andTradeNodeBetween(String value1, String value2) {
            addCriterion("TRADE_NODE between", value1, value2, "tradeNode");
            return (Criteria) this;
        }

        public Criteria andTradeNodeNotBetween(String value1, String value2) {
            addCriterion("TRADE_NODE not between", value1, value2, "tradeNode");
            return (Criteria) this;
        }

        public Criteria andTradeLevelIsNull() {
            addCriterion("TRADE_LEVEL is null");
            return (Criteria) this;
        }

        public Criteria andTradeLevelIsNotNull() {
            addCriterion("TRADE_LEVEL is not null");
            return (Criteria) this;
        }

        public Criteria andTradeLevelEqualTo(String value) {
            addCriterion("TRADE_LEVEL =", value, "tradeLevel");
            return (Criteria) this;
        }

        public Criteria andTradeLevelNotEqualTo(String value) {
            addCriterion("TRADE_LEVEL <>", value, "tradeLevel");
            return (Criteria) this;
        }

        public Criteria andTradeLevelGreaterThan(String value) {
            addCriterion("TRADE_LEVEL >", value, "tradeLevel");
            return (Criteria) this;
        }

        public Criteria andTradeLevelGreaterThanOrEqualTo(String value) {
            addCriterion("TRADE_LEVEL >=", value, "tradeLevel");
            return (Criteria) this;
        }

        public Criteria andTradeLevelLessThan(String value) {
            addCriterion("TRADE_LEVEL <", value, "tradeLevel");
            return (Criteria) this;
        }

        public Criteria andTradeLevelLessThanOrEqualTo(String value) {
            addCriterion("TRADE_LEVEL <=", value, "tradeLevel");
            return (Criteria) this;
        }

        public Criteria andTradeLevelLike(String value) {
            addCriterion("TRADE_LEVEL like", value, "tradeLevel");
            return (Criteria) this;
        }

        public Criteria andTradeLevelNotLike(String value) {
            addCriterion("TRADE_LEVEL not like", value, "tradeLevel");
            return (Criteria) this;
        }

        public Criteria andTradeLevelIn(List<String> values) {
            addCriterion("TRADE_LEVEL in", values, "tradeLevel");
            return (Criteria) this;
        }

        public Criteria andTradeLevelNotIn(List<String> values) {
            addCriterion("TRADE_LEVEL not in", values, "tradeLevel");
            return (Criteria) this;
        }

        public Criteria andTradeLevelBetween(String value1, String value2) {
            addCriterion("TRADE_LEVEL between", value1, value2, "tradeLevel");
            return (Criteria) this;
        }

        public Criteria andTradeLevelNotBetween(String value1, String value2) {
            addCriterion("TRADE_LEVEL not between", value1, value2, "tradeLevel");
            return (Criteria) this;
        }

        public Criteria andUseTimeIsNull() {
            addCriterion("USE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andUseTimeIsNotNull() {
            addCriterion("USE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andUseTimeEqualTo(long value) {
            addCriterion("USE_TIME =", value, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeNotEqualTo(long value) {
            addCriterion("USE_TIME <>", value, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeGreaterThan(long value) {
            addCriterion("USE_TIME >", value, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeGreaterThanOrEqualTo(long value) {
            addCriterion("USE_TIME >=", value, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeLessThan(long value) {
            addCriterion("USE_TIME <", value, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeLessThanOrEqualTo(long value) {
            addCriterion("USE_TIME <=", value, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeIn(List<Long> values) {
            addCriterion("USE_TIME in", values, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeNotIn(List<Long> values) {
            addCriterion("USE_TIME not in", values, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeBetween(long value1, long value2) {
            addCriterion("USE_TIME between", value1, value2, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeNotBetween(long value1, long value2) {
            addCriterion("USE_TIME not between", value1, value2, "useTime");
            return (Criteria) this;
        }

        public Criteria andReqSeqIsNull() {
            addCriterion("REQ_SEQ is null");
            return (Criteria) this;
        }

        public Criteria andReqSeqIsNotNull() {
            addCriterion("REQ_SEQ is not null");
            return (Criteria) this;
        }

        public Criteria andReqSeqEqualTo(String value) {
            addCriterion("REQ_SEQ =", value, "reqSeq");
            return (Criteria) this;
        }

        public Criteria andReqSeqNotEqualTo(String value) {
            addCriterion("REQ_SEQ <>", value, "reqSeq");
            return (Criteria) this;
        }

        public Criteria andReqSeqGreaterThan(String value) {
            addCriterion("REQ_SEQ >", value, "reqSeq");
            return (Criteria) this;
        }

        public Criteria andReqSeqGreaterThanOrEqualTo(String value) {
            addCriterion("REQ_SEQ >=", value, "reqSeq");
            return (Criteria) this;
        }

        public Criteria andReqSeqLessThan(String value) {
            addCriterion("REQ_SEQ <", value, "reqSeq");
            return (Criteria) this;
        }

        public Criteria andReqSeqLessThanOrEqualTo(String value) {
            addCriterion("REQ_SEQ <=", value, "reqSeq");
            return (Criteria) this;
        }

        public Criteria andReqSeqLike(String value) {
            addCriterion("REQ_SEQ like", value, "reqSeq");
            return (Criteria) this;
        }

        public Criteria andReqSeqNotLike(String value) {
            addCriterion("REQ_SEQ not like", value, "reqSeq");
            return (Criteria) this;
        }

        public Criteria andReqSeqIn(List<String> values) {
            addCriterion("REQ_SEQ in", values, "reqSeq");
            return (Criteria) this;
        }

        public Criteria andReqSeqNotIn(List<String> values) {
            addCriterion("REQ_SEQ not in", values, "reqSeq");
            return (Criteria) this;
        }

        public Criteria andReqSeqBetween(String value1, String value2) {
            addCriterion("REQ_SEQ between", value1, value2, "reqSeq");
            return (Criteria) this;
        }

        public Criteria andReqSeqNotBetween(String value1, String value2) {
            addCriterion("REQ_SEQ not between", value1, value2, "reqSeq");
            return (Criteria) this;
        }

        public Criteria andGlobalSeqIsNull() {
            addCriterion("GLOBAL_SEQ is null");
            return (Criteria) this;
        }

        public Criteria andGlobalSeqIsNotNull() {
            addCriterion("GLOBAL_SEQ is not null");
            return (Criteria) this;
        }

        public Criteria andGlobalSeqEqualTo(String value) {
            addCriterion("GLOBAL_SEQ =", value, "globalSeq");
            return (Criteria) this;
        }

        public Criteria andGlobalSeqNotEqualTo(String value) {
            addCriterion("GLOBAL_SEQ <>", value, "globalSeq");
            return (Criteria) this;
        }

        public Criteria andGlobalSeqGreaterThan(String value) {
            addCriterion("GLOBAL_SEQ >", value, "globalSeq");
            return (Criteria) this;
        }

        public Criteria andGlobalSeqGreaterThanOrEqualTo(String value) {
            addCriterion("GLOBAL_SEQ >=", value, "globalSeq");
            return (Criteria) this;
        }

        public Criteria andGlobalSeqLessThan(String value) {
            addCriterion("GLOBAL_SEQ <", value, "globalSeq");
            return (Criteria) this;
        }

        public Criteria andGlobalSeqLessThanOrEqualTo(String value) {
            addCriterion("GLOBAL_SEQ <=", value, "globalSeq");
            return (Criteria) this;
        }

        public Criteria andGlobalSeqLike(String value) {
            addCriterion("GLOBAL_SEQ like", value, "globalSeq");
            return (Criteria) this;
        }

        public Criteria andGlobalSeqNotLike(String value) {
            addCriterion("GLOBAL_SEQ not like", value, "globalSeq");
            return (Criteria) this;
        }

        public Criteria andGlobalSeqIn(List<String> values) {
            addCriterion("GLOBAL_SEQ in", values, "globalSeq");
            return (Criteria) this;
        }

        public Criteria andGlobalSeqNotIn(List<String> values) {
            addCriterion("GLOBAL_SEQ not in", values, "globalSeq");
            return (Criteria) this;
        }

        public Criteria andGlobalSeqBetween(String value1, String value2) {
            addCriterion("GLOBAL_SEQ between", value1, value2, "globalSeq");
            return (Criteria) this;
        }

        public Criteria andGlobalSeqNotBetween(String value1, String value2) {
            addCriterion("GLOBAL_SEQ not between", value1, value2, "globalSeq");
            return (Criteria) this;
        }

        public Criteria andTransSeqIsNull() {
            addCriterion("TRANS_SEQ is null");
            return (Criteria) this;
        }

        public Criteria andTransSeqIsNotNull() {
            addCriterion("TRANS_SEQ is not null");
            return (Criteria) this;
        }

        public Criteria andTransSeqEqualTo(String value) {
            addCriterion("TRANS_SEQ =", value, "transSeq");
            return (Criteria) this;
        }

        public Criteria andTransSeqNotEqualTo(String value) {
            addCriterion("TRANS_SEQ <>", value, "transSeq");
            return (Criteria) this;
        }

        public Criteria andTransSeqGreaterThan(String value) {
            addCriterion("TRANS_SEQ >", value, "transSeq");
            return (Criteria) this;
        }

        public Criteria andTransSeqGreaterThanOrEqualTo(String value) {
            addCriterion("TRANS_SEQ >=", value, "transSeq");
            return (Criteria) this;
        }

        public Criteria andTransSeqLessThan(String value) {
            addCriterion("TRANS_SEQ <", value, "transSeq");
            return (Criteria) this;
        }

        public Criteria andTransSeqLessThanOrEqualTo(String value) {
            addCriterion("TRANS_SEQ <=", value, "transSeq");
            return (Criteria) this;
        }

        public Criteria andTransSeqLike(String value) {
            addCriterion("TRANS_SEQ like", value, "transSeq");
            return (Criteria) this;
        }

        public Criteria andTransSeqNotLike(String value) {
            addCriterion("TRANS_SEQ not like", value, "transSeq");
            return (Criteria) this;
        }

        public Criteria andTransSeqIn(List<String> values) {
            addCriterion("TRANS_SEQ in", values, "transSeq");
            return (Criteria) this;
        }

        public Criteria andTransSeqNotIn(List<String> values) {
            addCriterion("TRANS_SEQ not in", values, "transSeq");
            return (Criteria) this;
        }

        public Criteria andTransSeqBetween(String value1, String value2) {
            addCriterion("TRANS_SEQ between", value1, value2, "transSeq");
            return (Criteria) this;
        }

        public Criteria andTransSeqNotBetween(String value1, String value2) {
            addCriterion("TRANS_SEQ not between", value1, value2, "transSeq");
            return (Criteria) this;
        }

        public Criteria andReqSysIsNull() {
            addCriterion("REQ_SYS is null");
            return (Criteria) this;
        }

        public Criteria andReqSysIsNotNull() {
            addCriterion("REQ_SYS is not null");
            return (Criteria) this;
        }

        public Criteria andReqSysEqualTo(String value) {
            addCriterion("REQ_SYS =", value, "reqSys");
            return (Criteria) this;
        }

        public Criteria andReqSysNotEqualTo(String value) {
            addCriterion("REQ_SYS <>", value, "reqSys");
            return (Criteria) this;
        }

        public Criteria andReqSysGreaterThan(String value) {
            addCriterion("REQ_SYS >", value, "reqSys");
            return (Criteria) this;
        }

        public Criteria andReqSysGreaterThanOrEqualTo(String value) {
            addCriterion("REQ_SYS >=", value, "reqSys");
            return (Criteria) this;
        }

        public Criteria andReqSysLessThan(String value) {
            addCriterion("REQ_SYS <", value, "reqSys");
            return (Criteria) this;
        }

        public Criteria andReqSysLessThanOrEqualTo(String value) {
            addCriterion("REQ_SYS <=", value, "reqSys");
            return (Criteria) this;
        }

        public Criteria andReqSysLike(String value) {
            addCriterion("REQ_SYS like", value, "reqSys");
            return (Criteria) this;
        }

        public Criteria andReqSysNotLike(String value) {
            addCriterion("REQ_SYS not like", value, "reqSys");
            return (Criteria) this;
        }

        public Criteria andReqSysIn(List<String> values) {
            addCriterion("REQ_SYS in", values, "reqSys");
            return (Criteria) this;
        }

        public Criteria andReqSysNotIn(List<String> values) {
            addCriterion("REQ_SYS not in", values, "reqSys");
            return (Criteria) this;
        }

        public Criteria andReqSysBetween(String value1, String value2) {
            addCriterion("REQ_SYS between", value1, value2, "reqSys");
            return (Criteria) this;
        }

        public Criteria andReqSysNotBetween(String value1, String value2) {
            addCriterion("REQ_SYS not between", value1, value2, "reqSys");
            return (Criteria) this;
        }

        public Criteria andReqDateIsNull() {
            addCriterion("REQ_DATE is null");
            return (Criteria) this;
        }

        public Criteria andReqDateIsNotNull() {
            addCriterion("REQ_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andReqDateEqualTo(String value) {
            addCriterion("REQ_DATE =", value, "reqDate");
            return (Criteria) this;
        }

        public Criteria andReqDateNotEqualTo(String value) {
            addCriterion("REQ_DATE <>", value, "reqDate");
            return (Criteria) this;
        }

        public Criteria andReqDateGreaterThan(String value) {
            addCriterion("REQ_DATE >", value, "reqDate");
            return (Criteria) this;
        }

        public Criteria andReqDateGreaterThanOrEqualTo(String value) {
            addCriterion("REQ_DATE >=", value, "reqDate");
            return (Criteria) this;
        }

        public Criteria andReqDateLessThan(String value) {
            addCriterion("REQ_DATE <", value, "reqDate");
            return (Criteria) this;
        }

        public Criteria andReqDateLessThanOrEqualTo(String value) {
            addCriterion("REQ_DATE <=", value, "reqDate");
            return (Criteria) this;
        }

        public Criteria andReqDateLike(String value) {
            addCriterion("REQ_DATE like", value, "reqDate");
            return (Criteria) this;
        }

        public Criteria andReqDateNotLike(String value) {
            addCriterion("REQ_DATE not like", value, "reqDate");
            return (Criteria) this;
        }

        public Criteria andReqDateIn(List<String> values) {
            addCriterion("REQ_DATE in", values, "reqDate");
            return (Criteria) this;
        }

        public Criteria andReqDateNotIn(List<String> values) {
            addCriterion("REQ_DATE not in", values, "reqDate");
            return (Criteria) this;
        }

        public Criteria andReqDateBetween(String value1, String value2) {
            addCriterion("REQ_DATE between", value1, value2, "reqDate");
            return (Criteria) this;
        }

        public Criteria andReqDateNotBetween(String value1, String value2) {
            addCriterion("REQ_DATE not between", value1, value2, "reqDate");
            return (Criteria) this;
        }

        public Criteria andReqTimeIsNull() {
            addCriterion("REQ_TIME is null");
            return (Criteria) this;
        }

        public Criteria andReqTimeIsNotNull() {
            addCriterion("REQ_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andReqTimeEqualTo(String value) {
            addCriterion("REQ_TIME =", value, "reqTime");
            return (Criteria) this;
        }

        public Criteria andReqTimeNotEqualTo(String value) {
            addCriterion("REQ_TIME <>", value, "reqTime");
            return (Criteria) this;
        }

        public Criteria andReqTimeGreaterThan(String value) {
            addCriterion("REQ_TIME >", value, "reqTime");
            return (Criteria) this;
        }

        public Criteria andReqTimeGreaterThanOrEqualTo(String value) {
            addCriterion("REQ_TIME >=", value, "reqTime");
            return (Criteria) this;
        }

        public Criteria andReqTimeLessThan(String value) {
            addCriterion("REQ_TIME <", value, "reqTime");
            return (Criteria) this;
        }

        public Criteria andReqTimeLessThanOrEqualTo(String value) {
            addCriterion("REQ_TIME <=", value, "reqTime");
            return (Criteria) this;
        }

        public Criteria andReqTimeLike(String value) {
            addCriterion("REQ_TIME like", value, "reqTime");
            return (Criteria) this;
        }

        public Criteria andReqTimeNotLike(String value) {
            addCriterion("REQ_TIME not like", value, "reqTime");
            return (Criteria) this;
        }

        public Criteria andReqTimeIn(List<String> values) {
            addCriterion("REQ_TIME in", values, "reqTime");
            return (Criteria) this;
        }

        public Criteria andReqTimeNotIn(List<String> values) {
            addCriterion("REQ_TIME not in", values, "reqTime");
            return (Criteria) this;
        }

        public Criteria andReqTimeBetween(String value1, String value2) {
            addCriterion("REQ_TIME between", value1, value2, "reqTime");
            return (Criteria) this;
        }

        public Criteria andReqTimeNotBetween(String value1, String value2) {
            addCriterion("REQ_TIME not between", value1, value2, "reqTime");
            return (Criteria) this;
        }

        public Criteria andRespSeqIsNull() {
            addCriterion("RESP_SEQ is null");
            return (Criteria) this;
        }

        public Criteria andRespSeqIsNotNull() {
            addCriterion("RESP_SEQ is not null");
            return (Criteria) this;
        }

        public Criteria andRespSeqEqualTo(String value) {
            addCriterion("RESP_SEQ =", value, "respSeq");
            return (Criteria) this;
        }

        public Criteria andRespSeqNotEqualTo(String value) {
            addCriterion("RESP_SEQ <>", value, "respSeq");
            return (Criteria) this;
        }

        public Criteria andRespSeqGreaterThan(String value) {
            addCriterion("RESP_SEQ >", value, "respSeq");
            return (Criteria) this;
        }

        public Criteria andRespSeqGreaterThanOrEqualTo(String value) {
            addCriterion("RESP_SEQ >=", value, "respSeq");
            return (Criteria) this;
        }

        public Criteria andRespSeqLessThan(String value) {
            addCriterion("RESP_SEQ <", value, "respSeq");
            return (Criteria) this;
        }

        public Criteria andRespSeqLessThanOrEqualTo(String value) {
            addCriterion("RESP_SEQ <=", value, "respSeq");
            return (Criteria) this;
        }

        public Criteria andRespSeqLike(String value) {
            addCriterion("RESP_SEQ like", value, "respSeq");
            return (Criteria) this;
        }

        public Criteria andRespSeqNotLike(String value) {
            addCriterion("RESP_SEQ not like", value, "respSeq");
            return (Criteria) this;
        }

        public Criteria andRespSeqIn(List<String> values) {
            addCriterion("RESP_SEQ in", values, "respSeq");
            return (Criteria) this;
        }

        public Criteria andRespSeqNotIn(List<String> values) {
            addCriterion("RESP_SEQ not in", values, "respSeq");
            return (Criteria) this;
        }

        public Criteria andRespSeqBetween(String value1, String value2) {
            addCriterion("RESP_SEQ between", value1, value2, "respSeq");
            return (Criteria) this;
        }

        public Criteria andRespSeqNotBetween(String value1, String value2) {
            addCriterion("RESP_SEQ not between", value1, value2, "respSeq");
            return (Criteria) this;
        }

        public Criteria andRespDateIsNull() {
            addCriterion("RESP_DATE is null");
            return (Criteria) this;
        }

        public Criteria andRespDateIsNotNull() {
            addCriterion("RESP_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andRespDateEqualTo(String value) {
            addCriterion("RESP_DATE =", value, "respDate");
            return (Criteria) this;
        }

        public Criteria andRespDateNotEqualTo(String value) {
            addCriterion("RESP_DATE <>", value, "respDate");
            return (Criteria) this;
        }

        public Criteria andRespDateGreaterThan(String value) {
            addCriterion("RESP_DATE >", value, "respDate");
            return (Criteria) this;
        }

        public Criteria andRespDateGreaterThanOrEqualTo(String value) {
            addCriterion("RESP_DATE >=", value, "respDate");
            return (Criteria) this;
        }

        public Criteria andRespDateLessThan(String value) {
            addCriterion("RESP_DATE <", value, "respDate");
            return (Criteria) this;
        }

        public Criteria andRespDateLessThanOrEqualTo(String value) {
            addCriterion("RESP_DATE <=", value, "respDate");
            return (Criteria) this;
        }

        public Criteria andRespDateLike(String value) {
            addCriterion("RESP_DATE like", value, "respDate");
            return (Criteria) this;
        }

        public Criteria andRespDateNotLike(String value) {
            addCriterion("RESP_DATE not like", value, "respDate");
            return (Criteria) this;
        }

        public Criteria andRespDateIn(List<String> values) {
            addCriterion("RESP_DATE in", values, "respDate");
            return (Criteria) this;
        }

        public Criteria andRespDateNotIn(List<String> values) {
            addCriterion("RESP_DATE not in", values, "respDate");
            return (Criteria) this;
        }

        public Criteria andRespDateBetween(String value1, String value2) {
            addCriterion("RESP_DATE between", value1, value2, "respDate");
            return (Criteria) this;
        }

        public Criteria andRespDateNotBetween(String value1, String value2) {
            addCriterion("RESP_DATE not between", value1, value2, "respDate");
            return (Criteria) this;
        }

        public Criteria andRespTimeIsNull() {
            addCriterion("RESP_TIME is null");
            return (Criteria) this;
        }

        public Criteria andRespTimeIsNotNull() {
            addCriterion("RESP_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andRespTimeEqualTo(String value) {
            addCriterion("RESP_TIME =", value, "respTime");
            return (Criteria) this;
        }

        public Criteria andRespTimeNotEqualTo(String value) {
            addCriterion("RESP_TIME <>", value, "respTime");
            return (Criteria) this;
        }

        public Criteria andRespTimeGreaterThan(String value) {
            addCriterion("RESP_TIME >", value, "respTime");
            return (Criteria) this;
        }

        public Criteria andRespTimeGreaterThanOrEqualTo(String value) {
            addCriterion("RESP_TIME >=", value, "respTime");
            return (Criteria) this;
        }

        public Criteria andRespTimeLessThan(String value) {
            addCriterion("RESP_TIME <", value, "respTime");
            return (Criteria) this;
        }

        public Criteria andRespTimeLessThanOrEqualTo(String value) {
            addCriterion("RESP_TIME <=", value, "respTime");
            return (Criteria) this;
        }

        public Criteria andRespTimeLike(String value) {
            addCriterion("RESP_TIME like", value, "respTime");
            return (Criteria) this;
        }

        public Criteria andRespTimeNotLike(String value) {
            addCriterion("RESP_TIME not like", value, "respTime");
            return (Criteria) this;
        }

        public Criteria andRespTimeIn(List<String> values) {
            addCriterion("RESP_TIME in", values, "respTime");
            return (Criteria) this;
        }

        public Criteria andRespTimeNotIn(List<String> values) {
            addCriterion("RESP_TIME not in", values, "respTime");
            return (Criteria) this;
        }

        public Criteria andRespTimeBetween(String value1, String value2) {
            addCriterion("RESP_TIME between", value1, value2, "respTime");
            return (Criteria) this;
        }

        public Criteria andRespTimeNotBetween(String value1, String value2) {
            addCriterion("RESP_TIME not between", value1, value2, "respTime");
            return (Criteria) this;
        }

        public Criteria andRespRcvTimeIsNull() {
            addCriterion("RESP_RCV_TIME is null");
            return (Criteria) this;
        }

        public Criteria andRespRcvTimeIsNotNull() {
            addCriterion("RESP_RCV_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andRespRcvTimeEqualTo(String value) {
            addCriterion("RESP_RCV_TIME =", value, "respRcvTime");
            return (Criteria) this;
        }

        public Criteria andRespRcvTimeNotEqualTo(String value) {
            addCriterion("RESP_RCV_TIME <>", value, "respRcvTime");
            return (Criteria) this;
        }

        public Criteria andRespRcvTimeGreaterThan(String value) {
            addCriterion("RESP_RCV_TIME >", value, "respRcvTime");
            return (Criteria) this;
        }

        public Criteria andRespRcvTimeGreaterThanOrEqualTo(String value) {
            addCriterion("RESP_RCV_TIME >=", value, "respRcvTime");
            return (Criteria) this;
        }

        public Criteria andRespRcvTimeLessThan(String value) {
            addCriterion("RESP_RCV_TIME <", value, "respRcvTime");
            return (Criteria) this;
        }

        public Criteria andRespRcvTimeLessThanOrEqualTo(String value) {
            addCriterion("RESP_RCV_TIME <=", value, "respRcvTime");
            return (Criteria) this;
        }

        public Criteria andRespRcvTimeLike(String value) {
            addCriterion("RESP_RCV_TIME like", value, "respRcvTime");
            return (Criteria) this;
        }

        public Criteria andRespRcvTimeNotLike(String value) {
            addCriterion("RESP_RCV_TIME not like", value, "respRcvTime");
            return (Criteria) this;
        }

        public Criteria andRespRcvTimeIn(List<String> values) {
            addCriterion("RESP_RCV_TIME in", values, "respRcvTime");
            return (Criteria) this;
        }

        public Criteria andRespRcvTimeNotIn(List<String> values) {
            addCriterion("RESP_RCV_TIME not in", values, "respRcvTime");
            return (Criteria) this;
        }

        public Criteria andRespRcvTimeBetween(String value1, String value2) {
            addCriterion("RESP_RCV_TIME between", value1, value2, "respRcvTime");
            return (Criteria) this;
        }

        public Criteria andRespRcvTimeNotBetween(String value1, String value2) {
            addCriterion("RESP_RCV_TIME not between", value1, value2, "respRcvTime");
            return (Criteria) this;
        }

        public Criteria andTransStatusIsNull() {
            addCriterion("TRANS_STATUS is null");
            return (Criteria) this;
        }

        public Criteria andTransStatusIsNotNull() {
            addCriterion("TRANS_STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andTransStatusEqualTo(String value) {
            addCriterion("TRANS_STATUS =", value, "transStatus");
            return (Criteria) this;
        }

        public Criteria andTransStatusNotEqualTo(String value) {
            addCriterion("TRANS_STATUS <>", value, "transStatus");
            return (Criteria) this;
        }

        public Criteria andTransStatusGreaterThan(String value) {
            addCriterion("TRANS_STATUS >", value, "transStatus");
            return (Criteria) this;
        }

        public Criteria andTransStatusGreaterThanOrEqualTo(String value) {
            addCriterion("TRANS_STATUS >=", value, "transStatus");
            return (Criteria) this;
        }

        public Criteria andTransStatusLessThan(String value) {
            addCriterion("TRANS_STATUS <", value, "transStatus");
            return (Criteria) this;
        }

        public Criteria andTransStatusLessThanOrEqualTo(String value) {
            addCriterion("TRANS_STATUS <=", value, "transStatus");
            return (Criteria) this;
        }

        public Criteria andTransStatusLike(String value) {
            addCriterion("TRANS_STATUS like", value, "transStatus");
            return (Criteria) this;
        }

        public Criteria andTransStatusNotLike(String value) {
            addCriterion("TRANS_STATUS not like", value, "transStatus");
            return (Criteria) this;
        }

        public Criteria andTransStatusIn(List<String> values) {
            addCriterion("TRANS_STATUS in", values, "transStatus");
            return (Criteria) this;
        }

        public Criteria andTransStatusNotIn(List<String> values) {
            addCriterion("TRANS_STATUS not in", values, "transStatus");
            return (Criteria) this;
        }

        public Criteria andTransStatusBetween(String value1, String value2) {
            addCriterion("TRANS_STATUS between", value1, value2, "transStatus");
            return (Criteria) this;
        }

        public Criteria andTransStatusNotBetween(String value1, String value2) {
            addCriterion("TRANS_STATUS not between", value1, value2, "transStatus");
            return (Criteria) this;
        }

        public Criteria andRetCodeIsNull() {
            addCriterion("RET_CODE is null");
            return (Criteria) this;
        }

        public Criteria andRetCodeIsNotNull() {
            addCriterion("RET_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andRetCodeEqualTo(String value) {
            addCriterion("RET_CODE =", value, "retCode");
            return (Criteria) this;
        }

        public Criteria andRetCodeNotEqualTo(String value) {
            addCriterion("RET_CODE <>", value, "retCode");
            return (Criteria) this;
        }

        public Criteria andRetCodeGreaterThan(String value) {
            addCriterion("RET_CODE >", value, "retCode");
            return (Criteria) this;
        }

        public Criteria andRetCodeGreaterThanOrEqualTo(String value) {
            addCriterion("RET_CODE >=", value, "retCode");
            return (Criteria) this;
        }

        public Criteria andRetCodeLessThan(String value) {
            addCriterion("RET_CODE <", value, "retCode");
            return (Criteria) this;
        }

        public Criteria andRetCodeLessThanOrEqualTo(String value) {
            addCriterion("RET_CODE <=", value, "retCode");
            return (Criteria) this;
        }

        public Criteria andRetCodeLike(String value) {
            addCriterion("RET_CODE like", value, "retCode");
            return (Criteria) this;
        }

        public Criteria andRetCodeNotLike(String value) {
            addCriterion("RET_CODE not like", value, "retCode");
            return (Criteria) this;
        }

        public Criteria andRetCodeIn(List<String> values) {
            addCriterion("RET_CODE in", values, "retCode");
            return (Criteria) this;
        }

        public Criteria andRetCodeNotIn(List<String> values) {
            addCriterion("RET_CODE not in", values, "retCode");
            return (Criteria) this;
        }

        public Criteria andRetCodeBetween(String value1, String value2) {
            addCriterion("RET_CODE between", value1, value2, "retCode");
            return (Criteria) this;
        }

        public Criteria andRetCodeNotBetween(String value1, String value2) {
            addCriterion("RET_CODE not between", value1, value2, "retCode");
            return (Criteria) this;
        }

        public Criteria andRetMsgIsNull() {
            addCriterion("RET_MSG is null");
            return (Criteria) this;
        }

        public Criteria andRetMsgIsNotNull() {
            addCriterion("RET_MSG is not null");
            return (Criteria) this;
        }

        public Criteria andRetMsgEqualTo(String value) {
            addCriterion("RET_MSG =", value, "retMsg");
            return (Criteria) this;
        }

        public Criteria andRetMsgNotEqualTo(String value) {
            addCriterion("RET_MSG <>", value, "retMsg");
            return (Criteria) this;
        }

        public Criteria andRetMsgGreaterThan(String value) {
            addCriterion("RET_MSG >", value, "retMsg");
            return (Criteria) this;
        }

        public Criteria andRetMsgGreaterThanOrEqualTo(String value) {
            addCriterion("RET_MSG >=", value, "retMsg");
            return (Criteria) this;
        }

        public Criteria andRetMsgLessThan(String value) {
            addCriterion("RET_MSG <", value, "retMsg");
            return (Criteria) this;
        }

        public Criteria andRetMsgLessThanOrEqualTo(String value) {
            addCriterion("RET_MSG <=", value, "retMsg");
            return (Criteria) this;
        }

        public Criteria andRetMsgLike(String value) {
            addCriterion("RET_MSG like", value, "retMsg");
            return (Criteria) this;
        }

        public Criteria andRetMsgNotLike(String value) {
            addCriterion("RET_MSG not like", value, "retMsg");
            return (Criteria) this;
        }

        public Criteria andRetMsgIn(List<String> values) {
            addCriterion("RET_MSG in", values, "retMsg");
            return (Criteria) this;
        }

        public Criteria andRetMsgNotIn(List<String> values) {
            addCriterion("RET_MSG not in", values, "retMsg");
            return (Criteria) this;
        }

        public Criteria andRetMsgBetween(String value1, String value2) {
            addCriterion("RET_MSG between", value1, value2, "retMsg");
            return (Criteria) this;
        }

        public Criteria andRetMsgNotBetween(String value1, String value2) {
            addCriterion("RET_MSG not between", value1, value2, "retMsg");
            return (Criteria) this;
        }

        public Criteria andBusiSignIsNull() {
            addCriterion("BUSI_SIGN is null");
            return (Criteria) this;
        }

        public Criteria andBusiSignIsNotNull() {
            addCriterion("BUSI_SIGN is not null");
            return (Criteria) this;
        }

        public Criteria andBusiSignEqualTo(String value) {
            addCriterion("BUSI_SIGN =", value, "busiSign");
            return (Criteria) this;
        }

        public Criteria andBusiSignNotEqualTo(String value) {
            addCriterion("BUSI_SIGN <>", value, "busiSign");
            return (Criteria) this;
        }

        public Criteria andBusiSignGreaterThan(String value) {
            addCriterion("BUSI_SIGN >", value, "busiSign");
            return (Criteria) this;
        }

        public Criteria andBusiSignGreaterThanOrEqualTo(String value) {
            addCriterion("BUSI_SIGN >=", value, "busiSign");
            return (Criteria) this;
        }

        public Criteria andBusiSignLessThan(String value) {
            addCriterion("BUSI_SIGN <", value, "busiSign");
            return (Criteria) this;
        }

        public Criteria andBusiSignLessThanOrEqualTo(String value) {
            addCriterion("BUSI_SIGN <=", value, "busiSign");
            return (Criteria) this;
        }

        public Criteria andBusiSignLike(String value) {
            addCriterion("BUSI_SIGN like", value, "busiSign");
            return (Criteria) this;
        }

        public Criteria andBusiSignNotLike(String value) {
            addCriterion("BUSI_SIGN not like", value, "busiSign");
            return (Criteria) this;
        }

        public Criteria andBusiSignIn(List<String> values) {
            addCriterion("BUSI_SIGN in", values, "busiSign");
            return (Criteria) this;
        }

        public Criteria andBusiSignNotIn(List<String> values) {
            addCriterion("BUSI_SIGN not in", values, "busiSign");
            return (Criteria) this;
        }

        public Criteria andBusiSignBetween(String value1, String value2) {
            addCriterion("BUSI_SIGN between", value1, value2, "busiSign");
            return (Criteria) this;
        }

        public Criteria andBusiSignNotBetween(String value1, String value2) {
            addCriterion("BUSI_SIGN not between", value1, value2, "busiSign");
            return (Criteria) this;
        }

        public Criteria andTimeStampIsNull() {
            addCriterion("TIME_STAMP is null");
            return (Criteria) this;
        }

        public Criteria andTimeStampIsNotNull() {
            addCriterion("TIME_STAMP is not null");
            return (Criteria) this;
        }

        public Criteria andTimeStampEqualTo(long value) {
            addCriterion("TIME_STAMP =", value, "timeStamp");
            return (Criteria) this;
        }

        public Criteria andTimeStampNotEqualTo(long value) {
            addCriterion("TIME_STAMP <>", value, "timeStamp");
            return (Criteria) this;
        }

        public Criteria andTimeStampGreaterThan(long value) {
            addCriterion("TIME_STAMP >", value, "timeStamp");
            return (Criteria) this;
        }

        public Criteria andTimeStampGreaterThanOrEqualTo(long value) {
            addCriterion("TIME_STAMP >=", value, "timeStamp");
            return (Criteria) this;
        }

        public Criteria andTimeStampLessThan(long value) {
            addCriterion("TIME_STAMP <", value, "timeStamp");
            return (Criteria) this;
        }

        public Criteria andTimeStampLessThanOrEqualTo(long value) {
            addCriterion("TIME_STAMP <=", value, "timeStamp");
            return (Criteria) this;
        }

        public Criteria andTimeStampIn(List<Long> values) {
            addCriterion("TIME_STAMP in", values, "timeStamp");
            return (Criteria) this;
        }

        public Criteria andTimeStampNotIn(List<Long> values) {
            addCriterion("TIME_STAMP not in", values, "timeStamp");
            return (Criteria) this;
        }

        public Criteria andTimeStampBetween(long value1, long value2) {
            addCriterion("TIME_STAMP between", value1, value2, "timeStamp");
            return (Criteria) this;
        }

        public Criteria andTimeStampNotBetween(long value1, long value2) {
            addCriterion("TIME_STAMP not between", value1, value2, "timeStamp");
            return (Criteria) this;
        }

        public Criteria andAppVersionIsNull() {
            addCriterion("APP_VERSION is null");
            return (Criteria) this;
        }

        public Criteria andAppVersionIsNotNull() {
            addCriterion("APP_VERSION is not null");
            return (Criteria) this;
        }

        public Criteria andAppVersionEqualTo(String value) {
            addCriterion("APP_VERSION =", value, "appVersion");
            return (Criteria) this;
        }

        public Criteria andAppVersionNotEqualTo(String value) {
            addCriterion("APP_VERSION <>", value, "appVersion");
            return (Criteria) this;
        }

        public Criteria andAppVersionGreaterThan(String value) {
            addCriterion("APP_VERSION >", value, "appVersion");
            return (Criteria) this;
        }

        public Criteria andAppVersionGreaterThanOrEqualTo(String value) {
            addCriterion("APP_VERSION >=", value, "appVersion");
            return (Criteria) this;
        }

        public Criteria andAppVersionLessThan(String value) {
            addCriterion("APP_VERSION <", value, "appVersion");
            return (Criteria) this;
        }

        public Criteria andAppVersionLessThanOrEqualTo(String value) {
            addCriterion("APP_VERSION <=", value, "appVersion");
            return (Criteria) this;
        }

        public Criteria andAppVersionLike(String value) {
            addCriterion("APP_VERSION like", value, "appVersion");
            return (Criteria) this;
        }

        public Criteria andAppVersionNotLike(String value) {
            addCriterion("APP_VERSION not like", value, "appVersion");
            return (Criteria) this;
        }

        public Criteria andAppVersionIn(List<String> values) {
            addCriterion("APP_VERSION in", values, "appVersion");
            return (Criteria) this;
        }

        public Criteria andAppVersionNotIn(List<String> values) {
            addCriterion("APP_VERSION not in", values, "appVersion");
            return (Criteria) this;
        }

        public Criteria andAppVersionBetween(String value1, String value2) {
            addCriterion("APP_VERSION between", value1, value2, "appVersion");
            return (Criteria) this;
        }

        public Criteria andAppVersionNotBetween(String value1, String value2) {
            addCriterion("APP_VERSION not between", value1, value2, "appVersion");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}