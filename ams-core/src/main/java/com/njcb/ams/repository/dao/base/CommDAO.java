package com.njcb.ams.repository.dao.base;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.njcb.ams.factory.domain.AppContext;

/**
 * 数据库访问类
 * 
 * @author LIUYANLONG
 */
@Repository
public class CommDAO {
	private static final Logger logger = LoggerFactory.getLogger(CommDAO.class);

	public static CommDAO getInstance() {
		return AppContext.getBean(CommDAO.class);
	}

	public static Connection getConnection() throws SQLException {
		logger.debug("获取数据库连接");
		DataSource dataSource = (DataSource) AppContext.getBean("dataSource");
		Connection conn = dataSource.getConnection();
		return conn;
	}

	/**
	 * 根据输入条件查询
	 * @param sql SQL查询语句
	 * @return Iterator 返回数组集合
	 * @throws Exception sql异常
	 */
	public List<Map<String, Object>> queryBySQL(final String sql) {
		logger.debug("执行sql:{}", sql);
		DataSource dataSource = (DataSource) AppContext.getBean("dataSource");
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		return jdbcTemplate.queryForList(sql);
	}

}
