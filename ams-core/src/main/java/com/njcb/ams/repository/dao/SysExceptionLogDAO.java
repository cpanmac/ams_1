package com.njcb.ams.repository.dao;

import com.njcb.ams.bootconfig.AmsProperties;
import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.SysTradeLogMapper;
import com.njcb.ams.repository.entity.ExceptionLog;
import com.njcb.ams.repository.entity.SysTradeConsole;
import com.njcb.ams.repository.entity.SysTradeLog;
import com.njcb.ams.repository.entity.SysTradeLogExample;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author LOONG
 */
@Repository
public class SysExceptionLogDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AmsProperties amsProperties;

    public Integer insert(ExceptionLog exceptionLog) {
        if (!amsProperties.getDbEnable()) {
            return 1;
        }
        return jdbcTemplate.update(
                "insert into SYS_EXCEPTION_LOG (TRANS_SEQ, ERROR_CODE, ERROR_MSG, ERROR_DATE, ERROR_TIME, USER_INFO, STACK_TRACE)"
                        + " values (?,?,?,?,?,?,?)",
                new PreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps) throws SQLException {
                        ps.setString(1, exceptionLog.getTransSeq());
                        ps.setString(2, exceptionLog.getErrorCode());
                        ps.setString(3, exceptionLog.getErrorMsg());
                        ps.setString(4, exceptionLog.getErrorDate());
                        ps.setString(5, exceptionLog.getErrorTime());
                        ps.setString(6, exceptionLog.getUserInfo());
                        ps.setString(7, exceptionLog.getStackTrace());
                    }
                });
    }


    public List<ExceptionLog> getExceptionLog(ExceptionLog exceptionLog) {
        StringBuilder sbsql = new StringBuilder(200);
        sbsql.append(" SELECT ERROR_CODE, ERROR_MSG, ERROR_DATE,ERROR_TIME,USER_INFO,STACK_TRACE ");
        sbsql.append("   FROM SYS_EXCEPTION_LOG ");
        sbsql.append("  WHERE 1 = 1 ");
        if (null != exceptionLog && null != exceptionLog.getErrorDate()) {
            sbsql.append("    AND ERROR_DATE = " + exceptionLog.getErrorDate());
        }
        sbsql.append("  group by ERROR_DATE, ERROR_TIME");
        sbsql.append("  ");
        List<ExceptionLog> retList = jdbcTemplate.queryForList(sbsql.toString(), ExceptionLog.class);
        return retList;
    }

}