/*
 * SysAlterRecord.java
 * Copyright(C) trythis.cn
 * 2020年06月04日 13时58分49秒Created
 */
package com.njcb.ams.repository.dao;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.SysAlterRecordMapper;
import com.njcb.ams.repository.entity.SysAlterRecord;
import com.njcb.ams.repository.entity.SysAlterRecordExample;
import com.njcb.ams.store.page.Page;
import com.njcb.ams.store.page.PageHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SysAlterRecordDAO extends BaseMyBatisDAO<SysAlterRecord, SysAlterRecordExample, Integer> {
    @Autowired
    private SysAlterRecordMapper mapper;

    public SysAlterRecordDAO() {
        this.entityClass = SysAlterRecord.class;
    }

	@Override
	@SuppressWarnings("unchecked")
	public SysAlterRecordMapper getMapper() {
		return mapper;
	}

	public static SysAlterRecordDAO getInstance() {
		return AppContext.getBean(SysAlterRecordDAO.class);
	}

	public void saveRecord(Object dataBean) {
		List<SysAlterRecord> objList = new ArrayList<SysAlterRecord>();
		try {
			String className = dataBean.getClass().getSimpleName();
			Integer id = 0;
			String periods = "none";
			try {
				Field idField = dataBean.getClass().getDeclaredField("id");
				idField.setAccessible(true);
				id = (Integer) idField.get(dataBean);
			} catch (NoSuchFieldException e) {
				logger.warn("{}没有属性id", className);
			}
			try {
				Field periodsField = dataBean.getClass().getDeclaredField("periods");
				periodsField.setAccessible(true);
				periods = String.valueOf(periodsField.get(dataBean));
			} catch (NoSuchFieldException e) {
				logger.warn("{}没有属性periods", className);
			}
			String timeStamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
			Field[] fields = dataBean.getClass().getDeclaredFields();
			for (int i = 0; i < fields.length; i++) {
				Field fi = fields[i];
				fi.setAccessible(true);
				SysAlterRecord ar = new SysAlterRecord();
				ar.setObjectName(className);
				ar.setId(null == id ? 0 : id);
				ar.setPeriods(periods);
				ar.setTimeStamp(timeStamp);
				ar.setFieldName(fi.getName());
				ar.setFieldValue(String.valueOf(fi.get(dataBean)));
				objList.add(ar);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
		mapper.insertBatch(objList);
	}

	public List<SysAlterRecord> selectByPivod(SysAlterRecord record) {
		PageHandle.startPage(record);
		List<SysAlterRecord> rtList = mapper.selectByPivod(record);
		Page page = PageHandle.endPage();
		return rtList;
	}

	/**
	 * 查询最后两次修改的完整记录
	 * @param record 操作记录查询对象
	 * @return 操作记录集合
	 */
	public List<Map<String, Object>> selectLastTwoRecord(SysAlterRecord record){
		List<SysAlterRecord> lastTwo = mapper.selectLastTwo(record);
		List<Map<String, Object>> rtMap = new ArrayList<Map<String, Object>>();
		for (SysAlterRecord lastRecord : lastTwo) {
			List<SysAlterRecord> subList = selectRecord(lastRecord);
			Map<String, Object> map = new HashMap<String, Object>();
			for (SysAlterRecord sub : subList) {
				map.put("objectName", sub.getObjectName());
				map.put("periods", sub.getPeriods());
				map.put(sub.getFieldName(), sub.getFieldValue());
			}
			rtMap.add(map);
		}
		return rtMap;
	}

	/**
	 * 查询单个对象的List<SysAlterRecord>
	 * @param record 操作记录查询对象
	 * @return 操作记录集合
	 */
	public List<SysAlterRecord> selectRecord(SysAlterRecord record){
		SysAlterRecordExample example = new SysAlterRecordExample();
		SysAlterRecordExample.Criteria criteria = example.createCriteria();
		if (null != record.getObjectName()) {
			criteria.andObjectNameEqualTo(record.getObjectName());
		}
		if (null != record.getId()) {
			criteria.andIdEqualTo(record.getId());
		}
		if (null != record.getPeriods()) {
			criteria.andPeriodsEqualTo(record.getPeriods());
		}
		if (null != record.getTimeStamp()) {
			criteria.andTimeStampEqualTo(record.getTimeStamp());
		}
		List<SysAlterRecord> subList = selectByExample(example);
		return  subList;
	}

}
