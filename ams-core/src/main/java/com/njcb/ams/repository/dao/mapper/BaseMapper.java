package com.njcb.ams.repository.dao.mapper;

import java.io.Serializable;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface BaseMapper<Obj, ObjExample, PK extends Serializable> {

	/**
	 * 根据指定主键获取一条数据库记录
	 * @param id 主键
	 * @return 实体对象
	 */
	Obj selectByPrimaryKey(PK id);

	/**
	 * 根据指定主键锁定获取一条数据库记录
	 * @param id 主键
	 * @return 实体对象
	 */
	Obj selectByPrimaryKeyForUpdate(PK id);

	/**
	 * 根据主键来更新符合条件的数据库记录
	 * @param record 实体
	 * @return 实体主键
	 */
	Integer updateByPrimaryKey(Obj record);

	/**
	 * 动态字段,根据主键来更新符合条件的数据库记录
	 * @param record 实体
	 * @return 成功条数
	 */
	Integer updateByPrimaryKeySelective(Obj record);

	/**
	 * 根据主键删除数据库的记录
	 * @param id 主键
	 * @return 成功条数
	 */
	Integer deleteByPrimaryKey(PK id);

	/**
	 * 根据指定的条件获取数据库记录数
	 * @param example 表达式
	 * @return 总数
	 */
	Long countByExample(ObjExample example);

	/**
	 * 根据指定的条件删除数据库符合条件的记录
	 * @param example 表达式
	 * @return 成功条数
	 */
	Integer deleteByExample(ObjExample example);

	/**
	 * 新写入数据库记录
	 * @param record 实体
	 * @return 成功条数
	 */
	Integer insert(Obj record);

	/**
	 * 批量写入数据库记录
	 * @param batchList 实体集合
	 * @return 成功条数
	 */
	Integer insertBatch(List<Obj> batchList);

	/**
	 * 批量分组写入数据库记录，自动按照500一批插入
	 * @param batchList 实体集合
	 * @return 成功条数
	 */
	Integer insertBatchGroup(List<Obj> batchList);

	/**
	 * 动态字段,写入数据库记录
	 * @param record 实体
	 * @return 成功条数
	 */
	Integer insertSelective(Obj record);

	/**
	 * 根据指定的条件查询符合条件的数据库记录
	 * @param example 表达式
	 * @return 实体集合
	 */
	List<Obj> selectByExample(ObjExample example);

	/**
	 * 根据指定的动态字段查询符合条件的数据库记录
	 * @param record 条件对象
	 * @return  实体集合
	 */
	List<Obj> selectBySelective(Obj record);

	/**
	 * 动态根据指定的条件来更新符合条件的数据库记录
	 * @param record 实体值对象
	 * @param example 表达式
	 * @return 成功条数
	 */
	Integer updateByExampleSelective(@Param("record") Obj record, @Param("example") ObjExample example);

	/**
	 * 根据指定的条件来更新符合条件的数据库记录
	 * @param record 实体值对象
	 * @param example 表达式
	 * @return 成功条数
	 */
	Integer updateByExample(@Param("record") Obj record, @Param("example") ObjExample example);

}
