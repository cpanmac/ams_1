/*
 * SysAlterRecordMapper.java
 * Copyright(C) trythis.cn
 * 2020年06月04日 13时58分49秒Created
 */
package com.njcb.ams.repository.dao.mapper;

import com.njcb.ams.repository.entity.SysAlterRecord;
import com.njcb.ams.repository.entity.SysAlterRecordExample;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author LOONG
 */
@Component
public interface SysAlterRecordMapper extends BaseMapper<SysAlterRecord, SysAlterRecordExample, Integer> {

	/**
	 * 查到对象修改记录
	 * @param record 操作记录查询对象
	 * @return 操作记录集合
	 */
	List<SysAlterRecord> selectByPivod(SysAlterRecord record);

	/**
	 * 查询最后两次记录
	 * @param record 操作记录查询对象
	 * @return 操作记录集合
	 */
	List<SysAlterRecord> selectLastTwo(SysAlterRecord record);
}