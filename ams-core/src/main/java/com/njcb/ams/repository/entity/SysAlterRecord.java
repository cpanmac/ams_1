/*
 * SysAlterRecord.java
 * Copyright(C) trythis.cn
 * 2020年06月04日 13时58分49秒Created
 */
package com.njcb.ams.repository.entity;

import com.njcb.ams.store.page.Page;

/**
 * @author srxhx207
 */
public class SysAlterRecord extends Page {
    private Integer id;

    private String periods;

    private String timeStamp;

    private String objectName;

    private String fieldName;

    private String fieldValue;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPeriods() {
        return periods;
    }

    public void setPeriods(String periods) {
        this.periods = periods;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }
}