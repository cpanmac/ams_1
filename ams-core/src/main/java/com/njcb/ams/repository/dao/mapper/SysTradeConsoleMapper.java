package com.njcb.ams.repository.dao.mapper;

import com.njcb.ams.repository.entity.SysTradeConsole;
import com.njcb.ams.repository.entity.SysTradeConsoleExample;
import org.springframework.stereotype.Component;

/**
 * @author LOONG
 */
@Component
public interface SysTradeConsoleMapper extends BaseMapper<SysTradeConsole, SysTradeConsoleExample, Integer> {
}