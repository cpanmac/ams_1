package com.njcb.ams.repository.entity;

import org.apache.commons.lang.StringUtils;

import com.njcb.ams.store.page.Page;

import javax.validation.constraints.NotNull;

/**
 * @author LOONG
 */
public class SysTradeConsole extends Page {
    /**
     * 更新的时候校验分组
     */
    public interface Update {
    }

    private Integer id;
    /**
     * 交易码
     */
    @NotNull(groups = Update.class)
    private String tradeCode;
    /**
     * 交易名称
     */
    private String tradeName;
    /**
     * 交易组
     */
    private String tradeGroup;
    /**
     * 允许最大并发数
     */
    private Integer maxExec;
    /**
     * 允许最大流量
     */
    private Integer maxRate;
    /**
     * 当前并发数
     */
    private Integer currExec;

    /**
     * 当前流量
     */
    private Double currRate;

    /**
     * 时间周期基数
     */
    private Integer timeCycle;
    /**
     * 时间周期基数名称
     */
    private String timeCycleName;
    /**
     * 数量周期基数
     */
    private Integer numberCycle;
    /**
     * 时间内平均响应时间 单位秒
     */
    private Integer timeSpeed;
    /**
     * 时间内交易笔数
     */
    private Integer timeTakeNum;
    /**
     * 时间内交易成功率
     */
    private String timeSuccRate;
    /**
     * 数量内交易成功率
     */
    private String numberSuccRate;
    /**
     * 当前服务响应级别 ：正常模式，降级模式，断路模式
     */
    private String tradeStatus;
    /**
     * 交易消息
     */
    private String tradeMsg;
    /**
     * 交易实现类
     */
    private String implClass;

    private String databaseChain;

    private String invokingChain;

    /**
     * 识别标记，json格式用于识别具体服务
     */
    private String identifyingMarker;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTradeCode() {
        return tradeCode;
    }

    public void setTradeCode(String tradeCode) {
        this.tradeCode = tradeCode;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getTradeGroup() {
        return tradeGroup;
    }

    public void setTradeGroup(String tradeGroup) {
        this.tradeGroup = tradeGroup;
    }

    public Integer getMaxExec() {
        return maxExec;
    }

    public void setMaxExec(Integer maxExec) {
        this.maxExec = maxExec;
    }

    public void setMaxRate(Integer maxRate) {
        this.maxRate = maxRate;
    }

    public Integer getMaxRate() {
        return maxRate;
    }

    public Integer getCurrExec() {
        return currExec;
    }

    public void setCurrExec(Integer currExec) {
        this.currExec = currExec;
    }

    public Double getCurrRate() {
        return currRate;
    }

    public void setCurrRate(Double currRate) {
        this.currRate = currRate;
    }

    public Integer getTimeCycle() {
        return timeCycle;
    }

    public void setTimeCycle(Integer timeCycle) {
        this.timeCycle = timeCycle;
    }

    public void setImplClass(String implClass) {
        this.implClass = implClass;
    }

    public String getImplClass() {
        return implClass;
    }

    public String getIdentifyingMarker() {
        return identifyingMarker;
    }

    public void setIdentifyingMarker(String identifyingMarker) {
        this.identifyingMarker = identifyingMarker;
    }

    public String getTimeCycleName() {
        if (null == this.timeCycle) {
            return "";
        } else if (this.timeCycle < 60) {
            return String.valueOf(timeCycle) + "秒";
        } else if (this.timeCycle < 3600) {
            return timeCycle / 60 + "分" + timeCycle % 60 + "秒";
        } else if (this.timeCycle < 86400) {
            return timeCycle / 3600 + "时" + timeCycle % 3600 / 60 + "分" + timeCycle % 60 + "秒";
        } else {
            return timeCycle / 86400 + "天" + timeCycle % 86400 / 3600 + "时" + timeCycle % 3600 / 60 + "分";
        }
    }

    public void setTimeCycleName(String timeCycleName) {
        if (StringUtils.isEmpty(timeCycleName)) {
            timeCycleName = "86400";
        }
        timeCycleName = timeCycleName.toUpperCase();
        String shorttimeCycle = "";
        if (!StringUtils.isNumeric(timeCycleName)) {
            shorttimeCycle = timeCycleName.trim().substring(0, timeCycleName.trim().length() - 1);
            if (StringUtils.isEmpty(shorttimeCycle) || !StringUtils.isNumeric(shorttimeCycle)) {
                timeCycleName = "86400";
            }
        }
        if (null == timeCycleName) {
            this.timeCycle = 86400;
        } else if (StringUtils.isNumeric(timeCycleName)) {
            this.timeCycle = Integer.parseInt(timeCycleName);
        } else if (timeCycleName.endsWith("S")) {
            this.timeCycle = Integer.parseInt(shorttimeCycle);
        } else if (timeCycleName.endsWith("M")) {
            this.timeCycle = Integer.parseInt(shorttimeCycle) * 60;
        } else if (timeCycleName.endsWith("H")) {
            this.timeCycle = Integer.parseInt(shorttimeCycle) * 3600;
        } else if (timeCycleName.endsWith("D")) {
            this.timeCycle = Integer.parseInt(shorttimeCycle) * 86400;
        } else {
            this.timeCycle = 86400;
        }
        this.timeCycleName = timeCycleName;
    }

    public Integer getNumberCycle() {
        return numberCycle;
    }

    public void setNumberCycle(Integer numberCycle) {
        this.numberCycle = numberCycle;
    }

    public Integer getTimeSpeed() {
        return timeSpeed;
    }

    public void setTimeSpeed(Integer timeSpeed) {
        this.timeSpeed = timeSpeed;
    }

    public Integer getTimeTakeNum() {
        return timeTakeNum;
    }

    public void setTimeTakeNum(Integer timeTakeNum) {
        this.timeTakeNum = timeTakeNum;
    }

    public String getTimeSuccRate() {
        return timeSuccRate;
    }

    public void setTimeSuccRate(String timeSuccRate) {
        this.timeSuccRate = timeSuccRate;
    }

    public String getNumberSuccRate() {
        return numberSuccRate;
    }

    public void setNumberSuccRate(String numberSuccRate) {
        this.numberSuccRate = numberSuccRate;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public void setTradeMsg(String tradeMsg) {
        this.tradeMsg = tradeMsg;
    }

    public String getTradeMsg() {
        return tradeMsg;
    }

    /**
     * @return the databaseChain
     */
    public String getDatabaseChain() {
        return databaseChain;
    }

    /**
     * @param databaseChain the databaseChain to set
     */
    public void setDatabaseChain(String databaseChain) {
        this.databaseChain = databaseChain;
    }

    /**
     * @return the invokingChain
     */
    public String getInvokingChain() {
        return invokingChain;
    }

    /**
     * @param invokingChain the invokingChain to set
     */
    public void setInvokingChain(String invokingChain) {
        this.invokingChain = invokingChain;
    }

    @Override
    public String toString() {
        return "SysServerConsole [id=" + id + ", tradeCode=" + tradeCode + ", tradeName=" + tradeName + ", maxExec=" + maxExec + ", maxRate=" + maxRate + ", currExec=" + currExec + ", timeCycle="
                + timeCycle + ", timeCycleName=" + timeCycleName + ", numberCycle=" + numberCycle + ", timeSpeed=" + timeSpeed + ", timeTakeNum=" + timeTakeNum + ", timeSuccRate=" + timeSuccRate
                + ", numberSuccRate=" + numberSuccRate + ", tradeStatus=" + tradeStatus + ", tradeMsg=" + tradeMsg + "]";
    }
}