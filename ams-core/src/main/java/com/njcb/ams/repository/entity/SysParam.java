package com.njcb.ams.repository.entity;

import com.njcb.ams.store.page.Page;
import com.njcb.ams.support.annotation.OperRecord;

import java.io.Serializable;

/**
 * @author srxhx207
 */
@OperRecord
public class SysParam extends Page implements Serializable {
    private Integer id;

    private String name;

    private String value;

    private String type;

    private String subtype;

    private String status;

    private String remark;

    private Integer conductUser;

    private String conductTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getConductUser() {
        return conductUser;
    }

    public void setConductUser(Integer conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }

    @Override
    public String toString() {
        return "SysParam{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", type='" + type + '\'' +
                ", subtype='" + subtype + '\'' +
                ", status='" + status + '\'' +
                ", remark='" + remark + '\'' +
                ", conductUser=" + conductUser +
                ", conductTime='" + conductTime + '\'' +
                "} ";
    }
}