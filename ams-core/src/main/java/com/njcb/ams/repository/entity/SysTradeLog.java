package com.njcb.ams.repository.entity;

import com.njcb.ams.support.codevalue.EnumCode;

/**
 * @author LOONG
 */
public class SysTradeLog {
    public enum TradeType implements EnumCode {
        SERVER("10", "服务端"), CLIENT("20", "客户端");
        private String code;
        private String desc;

        TradeType(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        @Override
        public String getCode() {
            return this.code;
        }

        @Override
        public String getDesc() {
            return this.desc;
        }
    }

    private Integer id;

    private String tradeCode;

    private String tradeName;

    /**
     * 交易类型 10:受理交易 20:发起交易
     */
    private String tradeType;

    /**
     * 执行节点IP
     */
    private String tradeNode;

    /**
     * 交易层级 1为顶层
     */
    private String tradeLevel;

    private Long useTime;

    private String reqSeq;

    private String globalSeq;

    private String transSeq;

    private String reqSys;

    private String reqDate;

    private String reqTime;

    private String respSeq;

    private String respDate;

    private String respTime;

    /**
     * 返回物理时间
     */
    private String respRcvTime;

    private String transStatus;

    private String retCode;

    private String retMsg;
    /**
     * 业务标识号
     */
    private String busiSign;

    private Long timeStamp;

    private String appVersion;

    private String remark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTradeCode() {
        return tradeCode;
    }

    public void setTradeCode(String tradeCode) {
        this.tradeCode = tradeCode;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getTradeNode() {
        return tradeNode;
    }

    public void setTradeNode(String tradeNode) {
        this.tradeNode = tradeNode;
    }

    public String getTradeLevel() {
        return tradeLevel;
    }

    public void setTradeLevel(String tradeLevel) {
        this.tradeLevel = tradeLevel;
    }

    public Long getUseTime() {
        return useTime;
    }

    public void setUseTime(Long useTime) {
        this.useTime = useTime;
    }

    public String getReqSeq() {
        return reqSeq;
    }

    public void setReqSeq(String reqSeq) {
        this.reqSeq = reqSeq;
    }

    public String getGlobalSeq() {
        return globalSeq;
    }

    public void setGlobalSeq(String globalSeq) {
        this.globalSeq = globalSeq;
    }

    public String getTransSeq() {
        return transSeq;
    }

    public void setTransSeq(String transSeq) {
        this.transSeq = transSeq;
    }

    public String getReqDate() {
        return reqDate;
    }

    public void setReqDate(String reqDate) {
        this.reqDate = reqDate;
    }

    public String getReqSys() {
        return reqSys;
    }

    public void setReqSys(String reqSys) {
        this.reqSys = reqSys;
    }

    public String getReqTime() {
        return reqTime;
    }

    public void setReqTime(String reqTime) {
        this.reqTime = reqTime;
    }

    public String getRespSeq() {
        return respSeq;
    }

    public void setRespSeq(String respSeq) {
        this.respSeq = respSeq;
    }

    public String getRespDate() {
        return respDate;
    }

    public void setRespDate(String respDate) {
        this.respDate = respDate;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getRespRcvTime() {
        return respRcvTime;
    }

    public void setRespRcvTime(String respRcvTime) {
        this.respRcvTime = respRcvTime;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }

    public String getRetCode() {
        return retCode;
    }

    public void setRetCode(String retCode) {
        this.retCode = retCode;
    }

    public String getRetMsg() {
        return retMsg;
    }

    public void setRetMsg(String retMsg) {
        this.retMsg = retMsg;
    }

    public String getBusiSign() {
        return busiSign;
    }

    public void setBusiSign(String busiSign) {
        this.busiSign = busiSign;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "SysTradeLog{" +
                "tradeCode='" + tradeCode + '\'' +
                ", tradeName='" + tradeName + '\'' +
                ", tradeType='" + tradeType + '\'' +
                ", tradeNode='" + tradeNode + '\'' +
                ", useTime=" + useTime +
                ", reqSeq='" + reqSeq + '\'' +
                ", globalSeq='" + globalSeq + '\'' +
                ", transSeq='" + transSeq + '\'' +
                ", reqDate='" + reqDate + '\'' +
                ", reqTime='" + reqTime + '\'' +
                ", respSeq='" + respSeq + '\'' +
                ", respDate='" + respDate + '\'' +
                ", respTime='" + respTime + '\'' +
                ", respRcvTime='" + respRcvTime + '\'' +
                ", transStatus='" + transStatus + '\'' +
                ", retCode='" + retCode + '\'' +
                ", retMsg='" + retMsg + '\'' +
                ", busiSign='" + busiSign + '\'' +
                ", timeStamp=" + timeStamp +
                ", appVersion='" + appVersion + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}