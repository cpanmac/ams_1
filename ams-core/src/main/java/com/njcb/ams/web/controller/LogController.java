package com.njcb.ams.web.controller;

import ch.qos.logback.classic.Level;
import com.njcb.ams.application.BusinessOPSManager;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.pojo.dto.standard.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author srxhx207
 */
@RestController
@Lazy
public class LogController {
	private static final Logger logger = LoggerFactory.getLogger(LogController.class);

	@Autowired
	private BusinessOPSManager businessManager;

	@RequestMapping(value = "/open/loglevel/{package}/change/{level}")
	@ResponseBody
	public Response changeLogLevel(@PathVariable("package") String packageName , @PathVariable("level") String levelName) {
		try {
			Level newLevel = Level.valueOf(levelName);
			businessManager.changeLogLevel(packageName,newLevel);
			return Response.buildSucc("包名["+packageName+"]的日志级别已变更为["+newLevel+"]");
		} catch (Exception e) {
			logger.error("动态修改日志级别出错", e);
			return Response.buildFail(e.getMessage());
		}
	}


	@RequestMapping(value = "/open/loglevel/list")
	@ResponseBody
	public PageResponse<Map<String, String>> getLogLevel() {
		return PageResponse.build(businessManager.getLoggerList());
	}
	
}
