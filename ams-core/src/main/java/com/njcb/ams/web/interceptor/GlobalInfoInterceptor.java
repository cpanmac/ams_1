package com.njcb.ams.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.njcb.ams.factory.comm.DataBus;
import com.njcb.ams.factory.comm.WebGlobalInfo;

/**
 * web Action拦截器
 * 
 * @author LIUYANLONG
 *
 */
public class GlobalInfoInterceptor implements HandlerInterceptor {

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handle, Exception ex)
			throws Exception {
		// 清除分页对象
		// PageHandle.removePage();
		// 清除GlobalInfo对象
		DataBus.remove();
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handle, ModelAndView mav)
			throws Exception {
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handle) throws Exception {
		WebGlobalInfo.getFromRequest(request.getSession());
		return true;
	}

}
