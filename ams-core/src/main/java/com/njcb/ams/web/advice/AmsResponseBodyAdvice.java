package com.njcb.ams.web.advice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.njcb.ams.util.AmsJsonUtils;

/**
 * @author LOONG
 */
@Order(0)
@ControllerAdvice
public class AmsResponseBodyAdvice implements ResponseBodyAdvice<Object> {
	private static final Logger logger = LoggerFactory.getLogger(AmsResponseBodyAdvice.class);

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType contentType, Class<? extends HttpMessageConverter<?>> converterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		logger.info("http response: {} \n {}",request.getURI(), AmsJsonUtils.objectToJson(body));
		return body;
	}

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		// 判断Controller返回值是否Map，如果是则返回true执行此处理，否则不处理
		return true;
	}

}
