package com.njcb.ams.bootconfig;

import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.repository.dao.SysInfoDAO;
import com.njcb.ams.repository.entity.SysInfo;
import com.njcb.ams.service.LogManageService;
import com.njcb.ams.store.filemonitor.FileMonitor;
import com.njcb.ams.store.filemonitor.Monitor;
import com.njcb.ams.support.config.AmsConfigUtil;
import com.njcb.ams.support.config.AppConfiguration;
import com.njcb.ams.util.AmsConfigReaderUtil;
import com.njcb.ams.util.AmsIpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.util.Arrays;

/**
 * @author liuyanlong
 */
@Component
@Lazy(false)
public class AmsCoreInit{
	private static final Logger logger = LoggerFactory.getLogger(AmsCoreInit.class);

	@Autowired
	private SysInfoDAO sysInfoDAO;
	
	@PostConstruct
	public void init() {
		initAppConfig();
		initSysInfo();
//		initFileMonitor();
		initLogManage();
	}

	public void initAppConfig() {
		try {
			logger.info("-------------------应用初始化开始-------------------");
			AppConfiguration systemInfoService = AmsConfigUtil.getBean(AppConfiguration.class);
			if (null != systemInfoService) {
				systemInfoService.init();
				SysBaseDefine.APP_VERSION = systemInfoService.getAppVersion();
				logger.info("版本初始化为:"+SysBaseDefine.APP_VERSION);
			}
			logger.info("-------------------应用初始化完成-------------------");
		} catch(NoSuchBeanDefinitionException e){
			logger.warn("应用程序未实现appConfiguration服务:{}",e.getMessage());
		} catch (Exception e) {
			logger.info("-------------------应用初始化错误-------------------");
			logger.error(e.getMessage(), e);
		}
	}
	
	private void initLogManage() {
		logger.info("-----------------初始化日志处理线程开始----------------");
		LogManageService.init();
		logger.info("-----------------初始化日志处理线程完成----------------");
	}
	
	private void initSysInfo() {
		try {
			logger.info("本机IP为[{}]", Arrays.toString(AmsIpUtils.getLocalIPList().toArray()));
			logger.info("------------------初始化系统信息开始-------------------");
			SysBaseDefine.TRADE_NODE = InetAddress.getLocalHost().getHostAddress();
			SysInfo sysInfo = sysInfoDAO.selectSysInfo();
			SysBaseDefine.SYS_NAME = sysInfo.getSysName();
			logger.info("------------------初始化系统信息完成-------------------");
		} catch (Throwable e) {
			logger.error("------------------初始化系统信息失败-------------------", e);
			System.exit(0);
		}
	}
	
	@SuppressWarnings("unused")
	private void initFileMonitor() {
		logger.debug("-----------------开始定时任务----------------");
		try {
			FileMonitor fileMonitor = FileMonitor.getInstance();
			Monitor cm = new Monitor() {
				@Override
				public void operate(String configName) {
					logger.info("------------配置文件重新加载:{}-----------", configName);
					AmsConfigReaderUtil.getInstance().load(SysBaseDefine.PROPERTY_FILE_PATH);
				}
			};
			fileMonitor.add(SysBaseDefine.PROPERTY_FILE_PATH, cm);
			fileMonitor.start();
		} catch (Throwable e) {
			logger.error("------------------开始定时任务失败-------------------", e);
			System.exit(0);
		}
	}


}
