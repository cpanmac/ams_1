package com.njcb.ams.bootconfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.njcb.ams.factory.domain.AppContext;

/**
 * @author LOONG
 */
@Component
public class AmsProperties {
	/**
	 * 是否启用数据库,默认true
	 */
	@Value("${ams.db.enable:true}")
	private Boolean dbEnable = true;
	
	@Value("${ams.info.sysId:}")
	private String sysId;

	@Value("${ams.info.sysShort:}")
    private String sysShort;

	@Value("${ams.info.sysName:}")
    private String sysName;

	@Value("${ams.info.channelId:}")
    private String channelId;

	@Value("${spring.cache.type:}")
	private String cacheType;
	
    public static AmsProperties getInstance() {
		return AppContext.getBean(AmsProperties.class);
	}
    
	public void setDbEnable(Boolean dbEnable) {
		this.dbEnable = dbEnable;
	}
	
	public Boolean getDbEnable() {
		return dbEnable;
	}

	public String getSysId() {
		return sysId;
	}

	public void setSysId(String sysId) {
		this.sysId = sysId;
	}

	public String getSysShort() {
		return sysShort;
	}

	public void setSysShort(String sysShort) {
		this.sysShort = sysShort;
	}

	public String getSysName() {
		return sysName;
	}

	public void setSysName(String sysName) {
		this.sysName = sysName;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getCacheType() {
		return cacheType;
	}

	public void setCacheType(String cacheType) {
		this.cacheType = cacheType;
	}
}
