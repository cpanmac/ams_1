package com.njcb.ams.bootconfig;

import com.alibaba.ttl.threadpool.TtlExecutors;
import com.njcb.ams.support.custom.AmsAsyncExceptionHandler;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * 用于spring支持异步处理注解@Async
 * @author LOONG
 *
 */
@Configuration
@EnableAsync(proxyTargetClass = true)
public class SpringAsyncConfig implements AsyncConfigurer {

	/**
	 * 构建异步处理线程池
	 */
	@Override
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		//设置线程名称
		taskExecutor.setThreadNamePrefix("AMS-AsyncThreadPool-");
		taskExecutor.setCorePoolSize(5);
		taskExecutor.setMaxPoolSize(30);
		taskExecutor.setQueueCapacity(100000);
		taskExecutor.initialize();
		return TtlExecutors.getTtlExecutor(taskExecutor);
	}

	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return new AmsAsyncExceptionHandler();
	}

}
