package com.njcb.ams.bootconfig;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.store.databind.AmsJacksonAnnotationIntrospector;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.text.SimpleDateFormat;

/**
 * 应用级bean初始化
 * @author LOONG
 *
 */
@Configuration
@Order(Integer.MIN_VALUE)
public class AmsInitCoreAppBean {

	AmsInitCoreAppBean(ApplicationContext context){
		AppContext.setContext(context);
	}

	/**
	 * json映射对象
	 * @return 映射器
	 */
	@Bean(name = "objectMapper")
	public ObjectMapper objectMapper() {
		ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json().build();
		objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		objectMapper.setAnnotationIntrospector(new AmsJacksonAnnotationIntrospector());
		return objectMapper;
	}


	/**
	 * 对象转换映射工厂
	 * @return 映射器工厂
	 */
	@Bean(name = "mapperFactory")
	public MapperFactory mapperFactory() {
		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		return mapperFactory;
	}
}
