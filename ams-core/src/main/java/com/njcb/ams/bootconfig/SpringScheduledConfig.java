package com.njcb.ams.bootconfig;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.repository.dao.SysInfoDAO;
import com.njcb.ams.util.AmsParamUtils;
import com.njcb.ams.util.SysInfoUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @author liuyanlong
 */
@Configuration
@EnableScheduling
public class SpringScheduledConfig {

    @Scheduled(initialDelay = 60000, fixedRate = 300000)
    public void updateCacheSysInfo() {
        AppContext.getBean(SysInfoDAO.class).updateCacheSysInfo();
    }

    @Scheduled(initialDelay = 60000, fixedRate = 300000)
    public void updateCacheSysParam() {
        SysBaseDefine.MAX_EXPORT_EXCEL_NUMBER = AmsParamUtils.getMaxExport();
    }

}
