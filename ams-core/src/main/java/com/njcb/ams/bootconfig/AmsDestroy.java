package com.njcb.ams.bootconfig;

import com.njcb.ams.support.config.AmsConfigUtil;
import com.njcb.ams.support.config.AppConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * @author liuyanlong
 */
@Component
@Lazy(false)
public class AmsDestroy implements DisposableBean{
	private static final Logger logger = LoggerFactory.getLogger(AmsDestroy.class);

	@Override
	public void destroy() {
		AppConfiguration systemInfoService = AmsConfigUtil.getBean(AppConfiguration.class);
		if(null == systemInfoService){
			logger.info("应用销毁时没有找到实现AppConfiguration的配置类");
			return;
		}
		logger.info("---------------执行应用销毁程序-----------------");
		systemInfoService.destroy();;
		logger.info("---------------执行应用销毁完成-----------------");
	}
}
