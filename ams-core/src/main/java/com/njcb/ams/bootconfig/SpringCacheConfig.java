package com.njcb.ams.bootconfig;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.store.cache.AmsCacheProperties;
import com.njcb.ams.store.cache.AmsEhCacheManagerFactoryBean;
import com.njcb.ams.store.cache.CacheManagerService;
import com.njcb.ams.support.config.AmsCacheConfiguration;
import com.njcb.ams.support.config.AmsConfigUtil;
import com.njcb.ams.support.config.defaultconfig.AmsDefaultCacheConfig;
import com.njcb.ams.util.AmsCacheUtils;
import com.njcb.ams.util.AmsIpUtils;
import com.njcb.ams.util.AmsUtils;
import net.sf.ehcache.Cache;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.CacheConfiguration.CacheEventListenerFactoryConfiguration;
import net.sf.ehcache.config.PersistenceConfiguration;
import net.sf.ehcache.config.PersistenceConfiguration.Strategy;
import net.sf.ehcache.distribution.CacheManagerPeerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于构建缓存ehcache
 *
 * @author liuyanlong
 */
@Configuration
@EnableCaching
public class SpringCacheConfig {
    private static final Logger logger = LoggerFactory.getLogger(SpringCacheConfig.class);

    @Autowired
    private AmsCacheProperties amsCacheProperties;

    private List<String> localIplist = AmsIpUtils.getLocalIPList();

    SpringCacheConfig(ApplicationContext context) {
        AppContext.setContext(context);
    }

    /**
     * ehcache 主要的管理器
     * @return 缓存管理器
     */
    @Bean(name = "cacheManager")
    public CacheManager cacheManager() {
        AmsEhCacheManagerFactoryBean bean = ehCacheManager();
        net.sf.ehcache.CacheManager cacheManager = bean.getObject();
        configCache(cacheManager);
        if(AmsUtils.isNotNull(amsCacheProperties.getProviderAddresses())){
            configPeerProvider(cacheManager);
            configPeerListener(cacheManager);
        }
        return new EhCacheCacheManager(cacheManager);
    }

    /**
     * 据shared与否的设置, Spring分别通过CacheManager.create() 或new
     * CacheManager()方式来创建一个ehcache基地.
     * 也说是说通过这个来设置cache的基地是这里的Spring独用,还是跟别的(如hibernate的Ehcache共享)
     *
     * @return 缓存管理器工厂
     */
    @Bean(name = "ehcacheManager")
    public AmsEhCacheManagerFactoryBean ehCacheManager() {
        AmsEhCacheManagerFactoryBean cacheManagerFactoryBean = new AmsEhCacheManagerFactoryBean(amsCacheProperties);
        cacheManagerFactoryBean.setConfigLocation(new ClassPathResource("ams-ehcache.xml"));
        cacheManagerFactoryBean.setShared(false);
        return cacheManagerFactoryBean;
    }

    /**
     * 集群同步
     *
     * @return
     */
    private CacheConfiguration defaultCacheConfiguration(String name) {
        return getCacheConfiguration(name);
    }

    private CacheConfiguration getCacheConfiguration(String name) {
        CacheConfiguration cacheConfiguration = new CacheConfiguration();
        cacheConfiguration.setMaxEntriesLocalHeap(100000);
        cacheConfiguration.setEternal(false);
        cacheConfiguration.setTimeToIdleSeconds(3600);
        cacheConfiguration.setTimeToLiveSeconds(60);
        if (AmsUtils.isNotNull(amsCacheProperties.getProviderAddresses())) {
            CacheEventListenerFactoryConfiguration factory = new CacheEventListenerFactoryConfiguration();
            factory.className("net.sf.ehcache.distribution.RMICacheReplicatorFactory");
//          同步复制
//          factory.setProperties("replicateAsynchronously=false");
            cacheConfiguration.cacheEventListenerFactory(factory);
        }
        cacheConfiguration.setName(name);
        PersistenceConfiguration persistenceConfiguration = new PersistenceConfiguration();
        persistenceConfiguration.strategy(Strategy.LOCALTEMPSWAP);
        cacheConfiguration.persistence(persistenceConfiguration);
        return cacheConfiguration;
    }


    private CacheConfiguration getMethodInvokeConfiguration(String name) {
        CacheConfiguration cacheConfiguration = new CacheConfiguration();
        cacheConfiguration.setMaxEntriesLocalHeap(100000);
        cacheConfiguration.setEternal(false);
        cacheConfiguration.setTimeToIdleSeconds(3600);
        cacheConfiguration.setTimeToLiveSeconds(60);
        if (AmsUtils.isNotNull(amsCacheProperties.getProviderAddresses())) {
            CacheEventListenerFactoryConfiguration factory = new CacheEventListenerFactoryConfiguration();
            factory.className("com.njcb.ams.store.cache.MethodInvokeCacheReplicatorFactory");
//          同步复制
            factory.setProperties("replicateAsynchronously=false");
            cacheConfiguration.cacheEventListenerFactory(factory);
        }
        cacheConfiguration.setName(name);
        PersistenceConfiguration persistenceConfiguration = new PersistenceConfiguration();
        persistenceConfiguration.strategy(Strategy.LOCALTEMPSWAP);
        cacheConfiguration.persistence(persistenceConfiguration);
        return cacheConfiguration;
    }

    /**
     * 单机缓存
     *
     * @return
     */
    private CacheConfiguration aloneCacheConfiguration(String name) {
        return getCacheConfiguration(name);
    }

    /**
     * 缓存配置
     *
     * @param cacheManager
     */
    private void configCache(net.sf.ehcache.CacheManager cacheManager) {
        AmsCacheConfiguration cacheConfiguration = AmsConfigUtil.getBean(AmsCacheConfiguration.class);
        if (null == cacheConfiguration) {
            cacheConfiguration = new AmsDefaultCacheConfig();
        }
        List<Cache> cacheList = new ArrayList<Cache>();
        cacheConfiguration.addEhCache(cacheList);
//        cacheList.add(new Cache(getMethodInvokeConfiguration("MethodInvokeCache")));
        cacheList.add(new Cache(defaultCacheConfiguration(AmsCacheUtils.SYS_PARAM_CACHE)));
        cacheList.add(new Cache(defaultCacheConfiguration(AmsCacheUtils.USER_TEMP_PARAM_CACHE)));
        cacheList.add(new Cache(defaultCacheConfiguration(AmsCacheUtils.USER_DYNAMIC_CACHE)));
        cacheList.add(new Cache(aloneCacheConfiguration(AmsCacheUtils.USER_STATIC_CACHE)));
        for (Cache cache : cacheList) {
            cacheManager.addCache(cache);
        }

    }

    /**
     * 配置分布式缓存成员发布
     */
    private void configPeerProvider(net.sf.ehcache.CacheManager cacheManager) {
        String[] cacheNames = cacheManager.getCacheNames();
        CacheManagerPeerProvider pro = cacheManager.getCacheManagerPeerProviders().get("RMI");
        List<String> providerAddressList = AppContext.getBean(CacheManagerService.class).getProviderAddressList();
        for (String providerAddress : providerAddressList) {
            if (localIplist.contains(providerAddress)) {
                continue;
            }
            for (String cacheName : cacheNames) {
                String peerAddress = "//" + providerAddress + ":" + amsCacheProperties.getListenPort() + "/" + cacheName;
                pro.registerPeer(peerAddress);
                AppContext.getBean(CacheManagerService.class).savePeerProviderSysParam(providerAddress, peerAddress);
                logger.info("注册缓存成员发布,地址为[{}]", peerAddress);
            }
        }
    }


    /**
     * 配置分布式缓存监听
     */
    private void configPeerListener(net.sf.ehcache.CacheManager cacheManager) {
    }
}
