/**
 * 服务提供包
 * 提供开发平台各类业务服务或领域服务
 * 
 * @author liuyanlong
 */
package com.njcb.ams.service;