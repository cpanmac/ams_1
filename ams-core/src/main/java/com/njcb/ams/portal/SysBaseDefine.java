package com.njcb.ams.portal;

import com.njcb.ams.support.codevalue.EnumCode;

import java.io.File;

/**
 * 常量定义
 *
 * @author liuyanlong
 */
public class SysBaseDefine {

    /**
     * 开发平台版本号
     */
    public static final String PLATFORM_VERSION = "V2.3.5";
    /**
     * 开发平台网站
     */
    public static final String PLATFORM_WEBSITE = "http://www.trythis.cn/";
    /**
     * 开发联系人
     */
    public static final String PLATFORM_CONTACT_AUTHOR = "刘彦龙";
    /**
     * 开发联系人邮箱
     */
    public static final String PLATFORM_CONTACT_MAIL = "23562656@qq.com";
    /**
     * 开发联系人手机
     */
    public static final String PLATFORM_CONTACT_PHONE = "18705186042";


    /**
     * 系统ID
     */
    public static String SYS_ID = "NNN";
    /**
     * 系统名称
     */
    public static String SYS_NAME = "系统";
    /**
     * 交易日期
     */
    public static String BUSI_DATE = "20991231";
    /**
     * 节点号
     */
    public static String TRADE_NODE = "";
    /**
     * 应用版本号
     */
    public static String APP_VERSION = PLATFORM_VERSION;

    /**
     * 重定向参数
     */
    public static final String REDIRECT = "redirect";

    /**
     * 登录URL参数
     */
    public static final String LOGIN_URL = "loginUrl";

    /**
     * 交易控制器默认配置
     * 默认最大并发数
     */
    public static final Integer DEFAULT_MAX_EXEC = 100;
    /**
     * 交易控制器默认配置
     * 默认最大流量
     */
    public static final Integer DEFAULT_MAX_RATE = 10000;

    /**
     * 错误信息是否包含交易信息
     */
    public static boolean ERROR_MESSAGE_TRADE_ENABLE = true;

    /**
     * GLOBALINFO存储信息主键
     */
    public static final String GLOBALINFO_SYSTRADELOG = "SYSTRADELOG";


    /**
     * 1 具有所有权限
     * 2 查询权限
     * 3-8 待定
     */
    public static final String RULE_AUTH_CODE_DEFAULT = "10000000";

    /**
     * Excel最大导出条数
     */
    public static int MAX_EXPORT_EXCEL_NUMBER = 5000;
    public static final String QUERY_MODEL_EXPALL = "expall";
    public static final String QUERY_MODEL_NORMAL = "normal";

    /**
     * REQ_PARAM_DICLIST 数据字典列表(需加载的)
     * REQ_PARAM_DICMAP  数据字典映射数据(需加载的)
     */
    public static final String REQ_PARAM_DICLIST = "dataDicList";
    public static final String REQ_PARAM_DICMAP = "dataDicMap";

    /**
     * 配置文件名
     */
    public static final String PROPERTY_FILE_PATH = "ams";

    /**
     * 允许的登录认证尝试次数
     */
    public static final int MAX_ATTEMPT_LIMIT = 5;
    /**
     * 系统用户默认密码
     */
    public static final String DEFAULT_PASSWORD = "88888888";
    /**
     * 登录是否验证码配置
     */
    public static final boolean IS_CHECK_CODE_VALUE = false;

    /**
     * MsgSession : 存放登录错误消息session
     * CodeRandSession : 存放验证码session
     */
    public static final String VALID_CODE_TEMP_SESSION = "CodeRandSession";

    /**
     * 用户状态 1正常,2锁定
     */
    public static final String USER_STATUS_NORMAL = "1";
    public static final String USER_STATUS_LOCK = "2";

    /**
     * 用户属性 0真实用户，1虚拟用户
     */
    public static final int USER_ATTR_REAL = 0;
    public static final int USER_ATTR_VIRT = 1;

    /**
     * 状态01有效 02无效
     */
    public static final String STATUS_VALID = "01";
    public static final String STATUS_INVALID = "02";

    /**
     * 数据期数序列 用于控制获取序号
     */
    public static final String SEQ_VALUENO_PERIODS = "PERIODS";

    /**
     * 码值类型  01:本地码值 02:外部码值
     */
    public static final String DATADIC_TYPE_LOCAL_01 = "01";
    public static final String DATADIC_TYPE_EXTER_02 = "02";

    /**
     * 默认返回码
     */
    public static final String TRANS_RET_CODE_DEFAULT = "000000";
    /**
     * 默认返回消息
     */
    public static final String TRANS_RET_MSG_DEFAULT = "交易成功";

    /**
     * 系统临时文件目录
     */
    public static final String SYS_FILE_TMP_PATH = System.getProperty("user.home") + File.separator + "amstemp" + File.separator;


    /**
     * 字符集
     */
    public enum CharSet {
        DEFAULT("UTF-8"), UTF8("UTF-8"), GBK("GBK"), GB2312("GB2312"), ASCII("ASCII");

        public final String value;

        private CharSet(String value) {
            this.value = value;
        }
    }

    /**
     * DatabaseId
     */
    public enum DatabaseId implements EnumCode {
        ORACLE("oracle", "oracle"), MYSQL("mysql", "mysql");
        private String code;
        private String desc;

        DatabaseId(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        @Override
        public String getCode() {
            return this.code;
        }

        @Override
        public String getDesc() {
            return this.desc;
        }
    }

    public static void main(String[] args) {
        System.out.println(CharSet.DEFAULT.value);
    }

}
