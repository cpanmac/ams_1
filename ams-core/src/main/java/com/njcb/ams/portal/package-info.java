/**
 * 门户入口包
 * 提供开发平台初始化及全局上下文
 * 
 * @author liuyanlong
 */
package com.njcb.ams.portal;