package com.njcb.ams.application;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.njcb.ams.annotation.TradeEnum;
import com.njcb.ams.repository.dao.SysInfoDAO;
import com.njcb.ams.repository.entity.SysInfo;
import com.njcb.ams.pojo.dto.ComponentInfoDTO;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.support.annotation.*;
import com.njcb.ams.support.annotation.enums.TradeType;
import com.njcb.ams.util.AmsCollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 平台维护功能
 * 
 * @author liuyanlong
 *
 */
@TradeEnum
@Interaction(groupName = "ZZ.AMS.CORE.OPS")
public class BusinessOPSManager {
	private static final Logger logger = LoggerFactory.getLogger(BusinessOPSManager.class);
	@Autowired
	private SysInfoDAO sysInfoDAO;

	@Uncheck
	public static BusinessOPSManager getInstance() {
		logger.debug("Instance BusinessManager");
		return AppContext.getBean(BusinessOPSManager.class);
	}

	@Trader(tradeCode = "OPS001", tradeName = "查询应用信息", tradeType = TradeType.TYPE_QUERY)
	public SysInfo appInfo() {
		return sysInfoDAO.selectSysInfo();
	}

	@Trader(tradeCode = "OPS002", tradeName = "获取组件列表", tradeType = TradeType.TYPE_QUERY)
	public PageResponse<ComponentInfoDTO> componentList() {
		Package pkg = Package.getPackage("com.njcb.ams.store");
		if(null == pkg){
			ComponentInfoDTO component = new ComponentInfoDTO();
			component.setPkgName("com.njcb.ams.store");
			component.setImplTitle("none");
			component.setImplVersion("none");
			List<ComponentInfoDTO> pkgList = new ArrayList<ComponentInfoDTO>();
			pkgList.add(component);
			return PageResponse.build(pkgList, 1);
		}
		Information info = pkg.getAnnotation(Information.class);
		ComponentInfoDTO component = new ComponentInfoDTO();
		component.setPkgName(pkg.getName());
		component.setImplTitle(pkg.getImplementationTitle());
		component.setImplVersion(pkg.getImplementationVersion());
		if (null != info) {
			component.setComponentName(info.tag());
			component.setComponentInfo(info.desc());
		}
		List<ComponentInfoDTO> pkgList = new ArrayList<ComponentInfoDTO>();
		pkgList.add(component);
		return PageResponse.build(pkgList, 1);
	}

	@Trader(tradeCode = "OPS003", tradeName = "更改日志级别", tradeType = TradeType.TYPE_QUERY)
	public void changeLogLevel(String packageName , Level newLevel) {
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		loggerContext.getLogger(packageName).setLevel(newLevel);
	}

	@Trader(tradeCode = "OPS004", tradeName = "查看日志级别", tradeType = TradeType.TYPE_QUERY)
	public List<Map<String,String>> getLoggerList() {
		List<Map<String,String>> logList = new ArrayList<Map<String,String>>();
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		List<ch.qos.logback.classic.Logger> loggerList = loggerContext.getLoggerList();
		for (ch.qos.logback.classic.Logger logger:loggerList) {
			if(null != logger.getLevel()){
				logList.add(AmsCollectionUtils.newHashMap(logger.getName(), String.valueOf(logger.getLevel())));
			}
		}
		return logList;
	}

}
