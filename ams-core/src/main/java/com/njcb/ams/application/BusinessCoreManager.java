package com.njcb.ams.application;

import com.njcb.ams.assembler.SysTradeLogConvertor;
import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.pojo.bo.SysAlterRecordBO;
import com.njcb.ams.pojo.dto.SysTradeLogDTO;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.repository.dao.SysAlterRecordDAO;
import com.njcb.ams.repository.dao.SysTradeLogDAO;
import com.njcb.ams.repository.entity.SysAlterRecord;
import com.njcb.ams.repository.entity.SysTradeConsole;
import com.njcb.ams.repository.entity.SysTradeLog;
import com.njcb.ams.store.page.Page;
import com.njcb.ams.store.page.PageHandle;
import com.njcb.ams.store.stable.TradeConsoleService;
import com.njcb.ams.support.annotation.Interaction;
import com.njcb.ams.support.annotation.Trader;
import com.njcb.ams.support.annotation.Uncheck;
import com.njcb.ams.support.annotation.enums.TradeType;
import com.njcb.ams.support.exception.ExceptionUtil;
import com.njcb.ams.util.AmsJsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 系统基础功能
 *
 * @author liuyanlong
 */
@Interaction(groupName = "ZZ.AMS.CORE.TRADE")
public class BusinessCoreManager {
    private static final Logger logger = LoggerFactory.getLogger(BusinessCoreManager.class);
    @Autowired
    private TradeConsoleService tradeConsoleService;
    @Autowired
    private SysTradeLogDAO tradeLogDAO;
    @Autowired
    private SysAlterRecordDAO alterRecordDAO;
    @Autowired
    private SysTradeLogConvertor sysTradeLogConvertor;

    @Uncheck
    public static BusinessCoreManager getInstance() {
        logger.debug("Instance BusinessManager");
        return AppContext.getBean(BusinessCoreManager.class);
    }

    @Trader(tradeCode = "SC1001", tradeName = "服务控制查询", tradeType = TradeType.TYPE_QUERY)
    public List<SysTradeConsole> getServerConsole(SysTradeConsole inBean) {
        return tradeConsoleService.getTradeConsoleInfo(inBean);
    }

    @Trader(tradeCode = "SC1002", tradeName = "服务控制修改")
    public int serverConsoleSave(@Validated(SysTradeConsole.Update.class) SysTradeConsole inBean) {
        if(!AmsJsonUtils.isJson(inBean.getIdentifyingMarker())){
            ExceptionUtil.throwAppException("识别标记字段仅支持json格式字符串");
        }
        return tradeConsoleService.saveServerConsole(inBean);
    }

    @Trader(tradeCode = "SC2001", tradeName = "交易记录查询", tradeType = TradeType.TYPE_QUERY)
    public PageResponse<SysTradeLog> tradeLogQuery(SysTradeLogDTO tradeLogDTO) {
        SysTradeLog tradeLog = sysTradeLogConvertor.dtoToEntity(tradeLogDTO);
        PageHandle.startPage(tradeLogDTO.getPage(), tradeLogDTO.getRows());
        List<SysTradeLog> list = tradeLogDAO.selectBySelective(tradeLog);
        Page page = PageHandle.endPage();
        PageResponse<SysTradeLog> pageResponse = PageResponse.build(list, page.getTotal());
        return pageResponse;
    }

    @Trader(tradeCode = "AL1001", tradeName = "操作记录查询", tradeType = TradeType.TYPE_QUERY)
    public List<SysAlterRecordBO> alterRecordQuery(SysAlterRecord inBean) {
        List<SysAlterRecordBO> boList = new ArrayList<>();
        List<SysAlterRecord> rtList = alterRecordDAO.selectByPivod(inBean);
        for (SysAlterRecord rec : rtList) {
            SysAlterRecordBO bo = new SysAlterRecordBO();
            bo.setId(rec.getId());
            bo.setObjectName(rec.getObjectName());
            bo.setPeriods(rec.getPeriods());
            bo.setTimeStamp(rec.getTimeStamp());
            List<Map<String, Object>> mapList = alterRecordDAO.selectLastTwoRecord(rec);
            if (mapList.size() > 0) {
                Object conductUser = mapList.get(0).get("conductUser");
                if (null != conductUser) {
                    bo.setConductUser(String.valueOf(conductUser));
                }
                Object conductTime = mapList.get(0).get("conductTime");
                if (null != conductTime) {
                    bo.setConductTime(String.valueOf(conductTime));
                }
            }
            bo.setLastTwoRecord(mapList);
            boList.add(bo);
        }
        return boList;
    }

}
