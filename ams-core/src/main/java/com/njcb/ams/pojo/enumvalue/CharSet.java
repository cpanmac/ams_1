package com.njcb.ams.pojo.enumvalue;

import com.njcb.ams.support.codevalue.EnumCode;

/**
 * 字符集
 * @author LOONG
 */
public enum CharSet implements EnumCode {
    DEFAULT("UTF-8","UTF-8"), UTF8("UTF-8","UTF-8"), GBK("GBK","GBK"), GB2312("GB2312","GB2312"), ASCII("ASCII","ASCII");

    private String code;
    private String desc;

    CharSet(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}