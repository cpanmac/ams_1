package com.njcb.ams.pojo.bo;

import com.njcb.ams.store.page.Page;

/**
 * @author LOONG
 */
public class DataDicConfig {

    private String dataNo;

    private String dataName;

    private String dataType;

    private String dataTypeName;

    public DataDicConfig(String dataNo, String dataName, String dataType, String dataTypeName){
        this.dataNo = dataNo;
        this.dataName = dataName;
        this.dataType = dataType;
        this.dataTypeName = dataTypeName;
    }

    public String getDataNo() {
        return dataNo;
    }

    public void setDataNo(String dataNo) {
        this.dataNo = dataNo;
    }

    public String getDataName() {
        return dataName;
    }

    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataTypeName() {
        return dataTypeName;
    }

    public void setDataTypeName(String dataTypeName) {
        this.dataTypeName = dataTypeName;
    }

}