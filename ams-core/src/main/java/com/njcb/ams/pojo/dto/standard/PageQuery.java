package com.njcb.ams.pojo.dto.standard;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import io.swagger.annotations.ApiModelProperty;

/**
 * 分页查询
 * Created by liuyanlong
 */
public abstract class PageQuery extends CommandRequest {
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(required = true, value = "当前页数")
	private int page;// 当前页数
	@ApiModelProperty(required = true, value = "每页条数")
	private int rows;// 每页条数
	@ApiModelProperty(hidden = true)
	private long total;
	@ApiModelProperty(value = "排序描述")
	private List<OrderDesc> orderDescs;
	
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}
	
	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	
    public List<OrderDesc> getOrderDescs() {
        return orderDescs;
    }

    public void addOrderDesc(OrderDesc orderDesc) {
    	orderDescs = Optional.ofNullable(orderDescs).orElse(new ArrayList<>());
        orderDescs.add(orderDesc);
    }

}
