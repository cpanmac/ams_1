package com.njcb.ams.pojo.dto.standard;

import java.util.Collection;

import io.swagger.annotations.ApiModelProperty;

/**
 * 多条记录返回
 * 通常用于页面查询或条件查询
 * @author liuyanlong
 * @param <T> 分页对象
 */
public class PageResponse<T> extends Response {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "总数据条数")
	private long total;

	@ApiModelProperty(value = "数组数据")
    private Collection<T> rows;

    public static <T> PageResponse<T> build(Collection<T> result) {
        if(null == result){
            return build(result,0);
        }
        return build(result,result.size());
    }
    
    public static <T> PageResponse<T> build(Collection<T> result, long total) {
    	PageResponse<T> multiResponse = new PageResponse<>();
        multiResponse.setSuccess(true);
        multiResponse.setResult(result);
        multiResponse.setTotal(total);
        return multiResponse;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public Collection<T> getRows() {
        return rows;
    }

    
    @SuppressWarnings("unchecked")
	public void setResult(@SuppressWarnings("rawtypes") Collection rows) {
        this.rows = rows;
    }

    public static PageResponse<?> buildFail(String code, String message) {
    	@SuppressWarnings("rawtypes")
		PageResponse<?> response = new PageResponse();
        response.setSuccess(false);
        response.setCode(code);
        response.setMessage(message);
        return response;
    }
    
    public static PageResponse<?> buildFail(String message) {
    	@SuppressWarnings("rawtypes")
		PageResponse<?> response = new PageResponse();
        response.setSuccess(false);
        response.setMessage(message);
        return response;
    }

    @SuppressWarnings("rawtypes")
	public static PageResponse buildSucc(){
    	PageResponse response = new PageResponse();
        response.setSuccess(true);
        return response;
    }

}
