package com.njcb.ams.pojo.dto;

public class ComponentInfoDTO {
	private String pkgName;
	private String implTitle;
	private String implVersion;
	private String componentName;
	private String componentInfo;
	public String getPkgName() {
		return pkgName;
	}
	public void setPkgName(String pkgName) {
		this.pkgName = pkgName;
	}
	public String getImplTitle() {
		return implTitle;
	}
	public void setImplTitle(String implTitle) {
		this.implTitle = implTitle;
	}
	public String getImplVersion() {
		return implVersion;
	}
	public void setImplVersion(String implVersion) {
		this.implVersion = implVersion;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public String getComponentInfo() {
		return componentInfo;
	}
	public void setComponentInfo(String componentInfo) {
		this.componentInfo = componentInfo;
	}
	@Override
	public String toString() {
		return "ComponentInfoDTO [pkgName=" + pkgName + ", implTitle=" + implTitle + ", implVersion=" + implVersion
				+ ", componentName=" + componentName + ", componentInfo=" + componentInfo + "]";
	}
	
}
