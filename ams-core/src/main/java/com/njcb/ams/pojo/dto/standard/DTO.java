package com.njcb.ams.pojo.dto.standard;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.ApiModelProperty;

/**
 * Data Transfer object 数据传输对象, 包含 命令、查询 和响应, 
 * 
 * CQRS 命令查询模式标准
 * 
 * @author 刘彦龙
 */
public class DTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(hidden = true)
	private Map<String, Object> extValues = new HashMap<String, Object>();

    public Object getExtField(String key){
        if(extValues != null){
            return extValues.get(key);
        }
        return null;
    }

    public void putExtField(String fieldName, Object value){
        this.extValues.put(fieldName, value);
    }
    
    public void putAllExtField(Map<String, Object> extFields){
        this.extValues.putAll(extFields);
    }

    public Map<String, Object> getExtValues() {
        return extValues;
    }

    public void setExtValues(Map<String, Object> extValues) {
        this.extValues = extValues;
    }

    @Override
    public String toString() {
        return "DTO{" +
                "extValues=" + extValues +
                '}';
    }
}
