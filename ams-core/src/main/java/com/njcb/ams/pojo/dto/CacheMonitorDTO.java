package com.njcb.ams.pojo.dto;

import com.njcb.ams.pojo.dto.standard.PageQuery;

/**
 * @author liuyanlong
 *
 */
public class CacheMonitorDTO extends PageQuery {
	private static final long serialVersionUID = 1L;
	// 缓存管理器名称
	private String cacheName;
	// 缓存主键
	private String cacheKey;
	// 缓存主键
	private String cacheValType;
	// 缓存值
	private String cacheValue;

	/**
	 * @return the cacheName
	 */
	public String getCacheName() {
		return cacheName;
	}

	/**
	 * @param cacheName
	 *            the cacheName to set
	 */
	public void setCacheName(String cacheName) {
		this.cacheName = cacheName;
	}

	/**
	 * @return the cacheKey
	 */
	public String getCacheKey() {
		return cacheKey;
	}

	/**
	 * @param cacheKey
	 *            the cacheKey to set
	 */
	public void setCacheKey(String cacheKey) {
		this.cacheKey = cacheKey;
	}

	/**
	 * @return the cacheValue
	 */
	public String getCacheValue() {
		return cacheValue;
	}

	/**
	 * @param cacheValue
	 *            the cacheValue to set
	 */
	public void setCacheValue(String cacheValue) {
		this.cacheValue = cacheValue;
	}

	/**
	 * @param cacheValType
	 *            the cacheValType to set
	 */
	public void setCacheValType(String cacheValType) {
		this.cacheValType = cacheValType;
	}

	/**
	 * @return the cacheValType
	 */
	public String getCacheValType() {
		return cacheValType;
	}

	@Override
	public String toString() {
		return "CacheMonitorDTO{" +
				"cacheName='" + cacheName + '\'' +
				", cacheKey='" + cacheKey + '\'' +
				", cacheValType='" + cacheValType + '\'' +
				", cacheValue='" + cacheValue + '\'' +
				'}';
	}
}
