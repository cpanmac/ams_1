package com.njcb.ams.pojo.dto.standard.event;

import com.njcb.ams.pojo.dto.standard.DTO;

/**
 * 事件消息基类
 * @author liuyanlong
 */
public class Event extends DTO{
    private static final long serialVersionUID = 1L;
    protected String eventId;
    protected String eventType;

    /**
     * event type,for example "CREATE"/"UPDATE"...
     * @return 事件类型
     */
    public String getEventType(){
        return eventType;
    }

    public String getEventId(){
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
}
