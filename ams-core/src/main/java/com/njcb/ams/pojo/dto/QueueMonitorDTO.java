package com.njcb.ams.pojo.dto;

import com.njcb.ams.pojo.dto.standard.PageQuery;

/**
 * @author liuyanlong
 *
 */
public class QueueMonitorDTO extends PageQuery {
	private static final long serialVersionUID = 1L;
	// 队列管理器
	private String queueManage;
	// 队列类型
	private String queueType;
	// 队列名称
	private String queueName;
	// 队列深度
	private String queueDepth;

	/**
	 * @return the queueManage
	 */
	public String getQueueManage() {
		return queueManage;
	}

	/**
	 * @param queueManage
	 *            the queueManage to set
	 */
	public void setQueueManage(String queueManage) {
		this.queueManage = queueManage;
	}

	/**
	 * @return the queueType
	 */
	public String getQueueType() {
		return queueType;
	}

	/**
	 * @param queueType
	 *            the queueType to set
	 */
	public void setQueueType(String queueType) {
		this.queueType = queueType;
	}

	/**
	 * @return the queueName
	 */
	public String getQueueName() {
		return queueName;
	}

	/**
	 * @param queueName
	 *            the queueName to set
	 */
	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	/**
	 * @return the queueDepth
	 */
	public String getQueueDepth() {
		return queueDepth;
	}

	/**
	 * @param queueDepth
	 *            the queueDepth to set
	 */
	public void setQueueDepth(String queueDepth) {
		this.queueDepth = queueDepth;
	}

	@Override
	public String toString() {
		return "QueueMonitorDTO{" +
				"queueManage='" + queueManage + '\'' +
				", queueType='" + queueType + '\'' +
				", queueName='" + queueName + '\'' +
				", queueDepth='" + queueDepth + '\'' +
				'}';
	}
}
