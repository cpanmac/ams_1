package com.njcb.ams.pojo.enumvalue;

import com.njcb.ams.support.annotation.DataDic;
import com.njcb.ams.support.codevalue.EnumCode;

/**
 * 交易响应状态码
 *
 * @author LOONG
 */
@DataDic(dataType = "TradeStatus", dataTypeName = "交易状态代码")
public enum TradeStatusCode implements EnumCode {

    TRADE_STATUS_01("01", "正常"), TRADE_STATUS_02("02", "降级"), TRADE_STATUS_03("03", "熔断"), TRADE_STATUS_04("04", "模拟");

    private String code;
    private String desc;

    TradeStatusCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
