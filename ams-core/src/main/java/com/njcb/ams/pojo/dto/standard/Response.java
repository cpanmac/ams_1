package com.njcb.ams.pojo.dto.standard;

import io.swagger.annotations.ApiModelProperty;

/**
 * 调用响应
 * 
 * @author liuyanlong
 */
public class Response extends DTO{

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "成功标志")
    private boolean success;
    
    @ApiModelProperty(value = "返回码")
    private String code = "0000";
    
    @ApiModelProperty(value = "返回消息")
    private String message = "成功";
    
    public void setSuccess(boolean success) {
		this.success = success;
	}
    
	public boolean isSuccess() {
		return success;
	}

	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public static Response buildFail(String code, String message) {
        Response response = new Response();
        response.setSuccess(false);
        response.setCode(code);
        response.setMessage(message);
        return response;
    }
    
    public static Response buildFail(String message) {
        Response response = new Response();
        response.setSuccess(false);
        response.setMessage(message);
        return response;
    }

    public static Response buildSucc(){
        Response response = new Response();
        response.setSuccess(true);
        return response;
    }
    
    public static Response buildSucc(String message){
        Response response = new Response();
        response.setSuccess(true);
        response.setMessage(message);
        return response;
    }
    
    @Override
    public String toString() {
        return "Response [success=" + success + ", code=" + code + ", message=" + message + "]";
    }
}
