package com.njcb.ams.pojo.enumvalue;

import com.njcb.ams.support.annotation.DataDic;
import com.njcb.ams.support.codevalue.EnumCode;

/**
 * 有效标识
 * 全局通用，适用所有的有效/无效判断
 *
 * @author LOONG
 */
@DataDic(dataType = "ValidStatus", dataTypeName = "有效/无效标识")
public enum ValidStatus implements EnumCode {

    VALID("1", "有效"),
    INVALID("0", "无效");

    private String code;
    private String desc;

    ValidStatus(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
