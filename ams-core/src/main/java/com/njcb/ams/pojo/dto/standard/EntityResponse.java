package com.njcb.ams.pojo.dto.standard;

import io.swagger.annotations.ApiModelProperty;

/**
 * 返回响应实体
 * @author liuyanlong
 */
public class EntityResponse<T> extends Response {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "数据实体")
    private T entity;

    public static <T> EntityResponse<T> build(T entity) {
    	EntityResponse<T> multiResponse = new EntityResponse<>();
        multiResponse.setSuccess(true);
        multiResponse.setEntity(entity);
        return multiResponse;
    }
    
    public void setEntity(T entity) {
		this.entity = entity;
	}
    
    public T getEntity() {
		return entity;
	}

    public static  EntityResponse buildFail(String message) {
        EntityResponse response = new EntityResponse<>();
        response.setSuccess(false);
        response.setMessage(message);
        return response;
    }

    @SuppressWarnings("rawtypes")
    public static EntityResponse buildFail(String code, String message) {
		EntityResponse response = new EntityResponse();
        response.setSuccess(false);
        response.setCode(code);
        response.setMessage(message);
        return response;
    }

	public static <T> EntityResponse<T> buildFail(T entity, String message) {
		EntityResponse<T> response = new EntityResponse<>();
		response.setSuccess(false);
		response.setMessage(message);
		return response;
	}

    public static <T> EntityResponse<T> buildFail(T entity, String code, String message) {
        EntityResponse response = new EntityResponse();
        response.setSuccess(false);
        response.setCode(code);
        response.setMessage(message);
        response.setEntity(entity);
        return response;
    }

	public static EntityResponse buildSucc(){
    	EntityResponse response = new EntityResponse();
        response.setSuccess(true);
        return response;
    }

    public static <T> EntityResponse<T> buildSucc(T entity, String code, String message){
        EntityResponse response = new EntityResponse();
        response.setSuccess(true);
        response.setCode(code);
        response.setMessage(message);
        response.setEntity(entity);
        return response;
    }

}
