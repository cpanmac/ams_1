package com.njcb.ams.pojo.enumvalue;

import com.njcb.ams.support.annotation.DataDic;
import com.njcb.ams.support.codevalue.EnumCode;

/**
 * 交易响应状态码
 * @author LOONG
 */
@DataDic(dataType = "TransStatus", dataTypeName = "ESB交易返回码")
public enum TransStatusCode implements EnumCode {

    SUCC("S", "成功"), FAIL("F", "失败"), EROR("E", "错误"), BEING("I", "处理中");

    private String code;
    private String desc;

    TransStatusCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
