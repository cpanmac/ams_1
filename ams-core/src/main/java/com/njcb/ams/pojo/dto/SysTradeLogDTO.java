package com.njcb.ams.pojo.dto;

import com.njcb.ams.pojo.dto.standard.PageQuery;

public class SysTradeLogDTO extends PageQuery {
    private static final long serialVersionUID = 1L;

    private String tradeCode;

    private String tradeName;

    private String tradeType;

    private String tradeNode;

    private Long useTime;

    private String reqSeq;

    private String globalSeq;

    private String transSeq;

    private String reqDate;

    private String reqTime;

    private String serviceId;

    private String svcScn;

    private String respSeq;

    private String respDate;

    private String respTime;

    private String respRcvTime;

    private String transStatus;

    private String retCode;

    private String retMsg;
    //业务标识号
    private String busiSign;

    private Long timeStamp;

    private String appVersion;

    private String remark;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getTradeCode() {
        return tradeCode;
    }

    public void setTradeCode(String tradeCode) {
        this.tradeCode = tradeCode;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getTradeNode() {
        return tradeNode;
    }

    public void setTradeNode(String tradeNode) {
        this.tradeNode = tradeNode;
    }

    public Long getUseTime() {
        return useTime;
    }

    public void setUseTime(Long useTime) {
        this.useTime = useTime;
    }

    public String getReqSeq() {
        return reqSeq;
    }

    public void setReqSeq(String reqSeq) {
        this.reqSeq = reqSeq;
    }

    public String getGlobalSeq() {
        return globalSeq;
    }

    public void setGlobalSeq(String globalSeq) {
        this.globalSeq = globalSeq;
    }

    public String getTransSeq() {
        return transSeq;
    }

    public void setTransSeq(String transSeq) {
        this.transSeq = transSeq;
    }

    public String getReqDate() {
        return reqDate;
    }

    public void setReqDate(String reqDate) {
        this.reqDate = reqDate;
    }

    public String getReqTime() {
        return reqTime;
    }

    public void setReqTime(String reqTime) {
        this.reqTime = reqTime;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getSvcScn() {
        return svcScn;
    }

    public void setSvcScn(String svcScn) {
        this.svcScn = svcScn;
    }

    public String getRespSeq() {
        return respSeq;
    }

    public void setRespSeq(String respSeq) {
        this.respSeq = respSeq;
    }

    public String getRespDate() {
        return respDate;
    }

    public void setRespDate(String respDate) {
        this.respDate = respDate;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getRespRcvTime() {
        return respRcvTime;
    }

    public void setRespRcvTime(String respRcvTime) {
        this.respRcvTime = respRcvTime;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }

    public String getRetCode() {
        return retCode;
    }

    public void setRetCode(String retCode) {
        this.retCode = retCode;
    }

    public String getRetMsg() {
        return retMsg;
    }

    public void setRetMsg(String retMsg) {
        this.retMsg = retMsg;
    }

    public String getBusiSign() {
        return busiSign;
    }

    public void setBusiSign(String busiSign) {
        this.busiSign = busiSign;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "SysTradeLogDTO{" +
                "tradeCode='" + tradeCode + '\'' +
                ", tradeName='" + tradeName + '\'' +
                ", tradeType='" + tradeType + '\'' +
                ", tradeNode='" + tradeNode + '\'' +
                ", useTime=" + useTime +
                ", reqSeq='" + reqSeq + '\'' +
                ", globalSeq='" + globalSeq + '\'' +
                ", transSeq='" + transSeq + '\'' +
                ", reqDate='" + reqDate + '\'' +
                ", reqTime='" + reqTime + '\'' +
                ", serviceId='" + serviceId + '\'' +
                ", svcScn='" + svcScn + '\'' +
                ", respSeq='" + respSeq + '\'' +
                ", respDate='" + respDate + '\'' +
                ", respTime='" + respTime + '\'' +
                ", respRcvTime='" + respRcvTime + '\'' +
                ", transStatus='" + transStatus + '\'' +
                ", retCode='" + retCode + '\'' +
                ", retMsg='" + retMsg + '\'' +
                ", busiSign='" + busiSign + '\'' +
                ", timeStamp=" + timeStamp +
                ", appVersion='" + appVersion + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}