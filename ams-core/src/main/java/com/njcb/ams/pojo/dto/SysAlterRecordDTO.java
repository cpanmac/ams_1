package com.njcb.ams.pojo.dto;

import com.njcb.ams.pojo.dto.standard.PageQuery;

public class SysAlterRecordDTO extends PageQuery {
	private static final long serialVersionUID = 1L;

	private Integer id;

    private String periods;

    private String timeStamp;

    private String className;

    private String fieldName;

    private String fieldValue;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPeriods() {
        return periods;
    }

    public void setPeriods(String periods) {
        this.periods = periods;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    @Override
    public String toString() {
        return "SysAlterRecordDTO{" +
                "id=" + id +
                ", periods='" + periods + '\'' +
                ", timeStamp='" + timeStamp + '\'' +
                ", className='" + className + '\'' +
                ", fieldName='" + fieldName + '\'' +
                ", fieldValue='" + fieldValue + '\'' +
                '}';
    }
}