package com.njcb.ams.pojo.bo;

import com.njcb.ams.store.page.Page;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Map;

public class SysAlterRecordBO {
    private Integer id;

    private String periods;

    private String timeStamp;

    private String objectName;

    private String fieldName;

    private String fieldValue;

    private String conductUser;

    private String conductTime;

    private List<Map<String, Object>> lastList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPeriods() {
        return periods;
    }

    public void setPeriods(String periods) {
        this.periods = periods;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getConductUser() {
        return conductUser;
    }

    public void setConductUser(String conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }

    public void setLastTwoRecord(List<Map<String, Object>> lastList) {
        this.lastList = lastList;
    }

    public List<Map<String, Object>> getLastList() {
        return lastList;
    }

    @Override
    public String toString() {
        return "SysAlterRecordBO{" +
                "id=" + id +
                ", periods='" + periods + '\'' +
                ", timeStamp='" + timeStamp + '\'' +
                ", objectName='" + objectName + '\'' +
                ", fieldName='" + fieldName + '\'' +
                ", fieldValue='" + fieldValue + '\'' +
                ", conductUser='" + conductUser + '\'' +
                ", conductTime='" + conductTime + '\'' +
                ", lastList=" + lastList +
                '}';
    }
}