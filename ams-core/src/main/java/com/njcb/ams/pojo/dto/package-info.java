/**
 * @author liuyanlong
 * @version 1.0 2018年5月14日
 *      2018年5月14日
 * Data Transfer Object 数据传输对象
 * 用于展示层与服务层之间是数据传输对象
 * 包括UI到后端，webservice到后端
 *
 */
package com.njcb.ams.pojo.dto;