-- Create table
create table SYS_INFO
(
  id         INTEGER primary key,
  sys_id     VARCHAR2(8),
  sys_short  VARCHAR2(8),
  sys_name   VARCHAR2(50),
  channel_id VARCHAR2(8),
  busi_date  VARCHAR2(8),
  lbat_date  VARCHAR2(8),
  nbat_date  VARCHAR2(8),
  insp_type   VARCHAR2(2),
  status     VARCHAR2(2)
);
-- Add comments to the table 
comment on table SYS_INFO
  is '系统信息表';
-- Add comments to the columns 
comment on column SYS_INFO.sys_short
  is '系统简称';
comment on column SYS_INFO.sys_name
  is '系统名称';
comment on column SYS_INFO.busi_date
  is '交易日期';
comment on column SYS_INFO.lbat_date
  is '上一日期';
comment on column SYS_INFO.nbat_date
  is '下一日期';
comment on column SYS_INFO.insp_type
  is '系统日期取值方式 1:物理日期 2:逻辑日期';
comment on column SYS_INFO.status
  is '系统状态';
;

INSERT INTO SYS_INFO (ID, SYS_ID, SYS_SHORT, SYS_NAME, CHANNEL_ID, BUSI_DATE, LBAT_DATE, NBAT_DATE, INSP_TYPE, STATUS) VALUES (1, 'AAA', 'AMS', '统一开发平台', 'AMS', '20200101', '20200101', '20200101', '1', '2');

CREATE SEQUENCE SEQ_SYS_TRADE_LOG START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 CACHE 20 ORDER;
-- Create table
create table SYS_TRADE_LOG
(
  id            INTEGER primary key,
  trade_code    VARCHAR2(30),
  trade_name    VARCHAR2(30),
  trade_type    VARCHAR2(30),
  trade_node    VARCHAR2(30),
  trade_level    VARCHAR2(2),
  use_time      INTEGER,
  req_seq       VARCHAR2(32),
  global_seq    VARCHAR2(32),
  trans_seq     VARCHAR2(32),
  req_sys       VARCHAR2(8),
  req_date      VARCHAR2(8),
  req_time      VARCHAR2(6),
  resp_seq      VARCHAR2(32),
  resp_date     VARCHAR2(8),
  resp_time     VARCHAR2(6),
  resp_rcv_time VARCHAR2(14),
  trans_status  VARCHAR2(8),
  ret_code      VARCHAR2(32),
  ret_msg       VARCHAR2(2048),
  busi_sign     VARCHAR2(64),
  time_stamp    NUMBER,
  app_version   VARCHAR2(32),
  remark        VARCHAR2(512)
);
-- Add comments to the table 
comment on table SYS_TRADE_LOG
  is '交易日志表';
-- Add comments to the columns 
comment on column SYS_TRADE_LOG.trade_code
  is '交易代码';
comment on column SYS_TRADE_LOG.trade_name
  is '交易名称';
comment on column SYS_TRADE_LOG.trade_type
  is '交易类型 10:受理交易 20:发起交易';
comment on column SYS_TRADE_LOG.trade_node
  is '执行节点IP';
comment on column SYS_TRADE_LOG.trade_level
    is '交易层级 1为顶层';
comment on column SYS_TRADE_LOG.use_time
  is '执行耗时';
comment on column SYS_TRADE_LOG.req_seq
  is '请求流水';
comment on column SYS_TRADE_LOG.global_seq
  is '全局流水';
comment on column SYS_TRADE_LOG.trans_seq
  is '内部交易流水';
comment on column SYS_TRADE_LOG.req_sys
  is '请求系统';
comment on column SYS_TRADE_LOG.req_date
  is '请求日期';
comment on column SYS_TRADE_LOG.req_time
  is '请求时间';
comment on column SYS_TRADE_LOG.resp_seq
  is '返回流水';
comment on column SYS_TRADE_LOG.resp_date
  is '返回日期';
comment on column SYS_TRADE_LOG.resp_time
  is '返回时间';
comment on column SYS_TRADE_LOG.resp_rcv_time
  is '返回物理时间';
comment on column SYS_TRADE_LOG.trans_status
  is '交易状态';
comment on column SYS_TRADE_LOG.ret_code
  is '返回码';
comment on column SYS_TRADE_LOG.ret_msg
  is '返回消息';
comment on column SYS_TRADE_LOG.busi_sign
  is '业务标识';
comment on column SYS_TRADE_LOG.time_stamp
  is '处理时间戳';
comment on column SYS_TRADE_LOG.app_version
  is '应用版本';
comment on column SYS_TRADE_LOG.remark
  is '备注';

create index idx_date on sys_trade_log(req_date);

create index idx_dateseq on sys_trade_log(req_date,trans_seq);

-- Create table
create table SYS_EXCEPTION_LOG
(
  TRANS_SEQ     VARCHAR2(30),
  ERROR_CODE    VARCHAR2(32),
  ERROR_MSG    VARCHAR2(1024),
  ERROR_DATE    VARCHAR2(8),
  ERROR_TIME    VARCHAR2(6),
  USER_INFO    VARCHAR2(32),
  STACK_TRACE   CLOB
);
-- Add comments to the table
comment on table SYS_EXCEPTION_LOG
  is '异常日志表';
-- Add comments to the columns
comment on column SYS_EXCEPTION_LOG.ERROR_CODE
  is '错误代码';
comment on column SYS_EXCEPTION_LOG.ERROR_MSG
  is '错误消息';
comment on column SYS_EXCEPTION_LOG.ERROR_DATE
  is '发生日期';
comment on column SYS_EXCEPTION_LOG.ERROR_TIME
  is '发生时间';
comment on column SYS_EXCEPTION_LOG.USER_INFO
  is '用户信息';
comment on column SYS_EXCEPTION_LOG.STACK_TRACE
  is '异常堆栈';


CREATE SEQUENCE SEQ_SYS_TRADE_CONSOLE START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 CACHE 20 ORDER;

-- Create table
create table SYS_TRADE_CONSOLE
(
  ID               INTEGER primary key,
  TRADE_CODE       VARCHAR2(30),
  TRADE_NAME       VARCHAR2(30),
  TRADE_GROUP      VARCHAR2(30),
  MAX_EXEC         INTEGER,
  MAX_RATE         INTEGER,
  CURR_EXEC        INTEGER,
  TIME_CYCLE       INTEGER,
  NUMBER_CYCLE     INTEGER,
  TIME_SPEED       INTEGER,
  TIME_TAKE_NUM    INTEGER,
  TIME_SUCC_RATE   NUMBER(18,2),
  NUMBER_SUCC_RATE NUMBER(18,2),
  TRADE_STATUS     VARCHAR2(2),
  TRADE_MSG        VARCHAR2(50),
  IMPL_CLASS       VARCHAR2(50)
);
-- Add comments to the table 
comment on table SYS_TRADE_CONSOLE
  is '交易控制表';
-- Add comments to the columns 
comment on column SYS_TRADE_CONSOLE.ID
  is '主键';
comment on column SYS_TRADE_CONSOLE.TRADE_CODE
  is '交易代码';
comment on column SYS_TRADE_CONSOLE.TRADE_NAME
  is '交易名称';
comment on column SYS_TRADE_CONSOLE.TRADE_GROUP
  is '交易组';
comment on column SYS_TRADE_CONSOLE.MAX_EXEC
  is '交易最大并发';
comment on column SYS_TRADE_CONSOLE.MAX_RATE
  is '交易最大流量';
comment on column SYS_TRADE_CONSOLE.CURR_EXEC
  is '当前并发';
comment on column SYS_TRADE_CONSOLE.TIME_CYCLE
  is '时间周期基数';
comment on column SYS_TRADE_CONSOLE.NUMBER_CYCLE
  is '数量周期基数';
comment on column SYS_TRADE_CONSOLE.TIME_SPEED
  is '时间内平均响应时间 单位秒';
comment on column SYS_TRADE_CONSOLE.TIME_TAKE_NUM
  is '时间内交易笔数';
comment on column SYS_TRADE_CONSOLE.TIME_SUCC_RATE
  is '时间内交易成功率';
comment on column SYS_TRADE_CONSOLE.NUMBER_SUCC_RATE
  is '数量内交易成功率';
comment on column SYS_TRADE_CONSOLE.TRADE_STATUS
  is '当前服务响应级别 ：正常模式，降级模式，断路模式';
comment on column SYS_TRADE_CONSOLE.TRADE_MSG
  is '交易消息';
comment on column SYS_TRADE_CONSOLE.IMPL_CLASS
  is '交易实现类';

-- Create table
create table SYS_SEQ_CONTROL
(
  value_no     VARCHAR2(20) primary key,
  value_index  VARCHAR2(20),
  value_length INTEGER not null,
  value_curr   INTEGER not null,
  max_value    INTEGER not null,
  min_value    INTEGER not null,
  cache_size   INTEGER not null
);
-- Add comments to the table 
comment on table SYS_SEQ_CONTROL
  is '序列控制表';
-- Add comments to the columns 
comment on column SYS_SEQ_CONTROL.value_no
  is '序列类型';
comment on column SYS_SEQ_CONTROL.value_index
  is '序列前驱';
comment on column SYS_SEQ_CONTROL.value_length
  is '序列总长度';
comment on column SYS_SEQ_CONTROL.value_curr
  is '序列当前值';
comment on column SYS_SEQ_CONTROL.max_value
  is '最大值';
comment on column SYS_SEQ_CONTROL.min_value
  is '最小值';
comment on column SYS_SEQ_CONTROL.cache_size
  is '缓存大小';

insert into sys_seq_control (VALUE_NO, VALUE_INDEX, VALUE_LENGTH, VALUE_CURR, MAX_VALUE, MIN_VALUE, CACHE_SIZE)
values ('PERIODS', 'PR', 8, 1, 99999999, 1, 20);

insert into sys_seq_control (VALUE_NO, VALUE_INDEX, VALUE_LENGTH, VALUE_CURR, MAX_VALUE, MIN_VALUE, CACHE_SIZE)
values ('REQSEQ', 'BC5', 12, 1, 999999999, 1, 20);

insert into sys_seq_control (VALUE_NO, VALUE_INDEX, VALUE_LENGTH, VALUE_CURR, MAX_VALUE, MIN_VALUE, CACHE_SIZE)
values ('TRANSSEQ', 'TS', 24, 1, 999999999, 1, 20);


CREATE SEQUENCE SEQ_SYS_PARAM START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 CACHE 20 ORDER;

-- Create table
create table SYS_PARAM
(
  id           INTEGER primary key,
  name         VARCHAR2(50),
  value        VARCHAR2(100),
  type         VARCHAR2(255),
  subtype      VARCHAR2(255),
  status       VARCHAR2(2),
  remark       VARCHAR2(100),
  conduct_user INTEGER,
  conduct_time CHAR(14)
);
-- Add comments to the table 
comment on table SYS_PARAM
  is '系统参数表';
-- Add comments to the columns 
comment on column SYS_PARAM.id
  is '主键';
comment on column SYS_PARAM.name
  is '参数名称';
comment on column SYS_PARAM.value
  is '参数值';
comment on column SYS_PARAM.type
  is '参数类型';
comment on column SYS_PARAM.subtype
  is '子类型';
comment on column SYS_PARAM.status
  is '状态';
comment on column SYS_PARAM.remark
  is '备注';
comment on column SYS_PARAM.conduct_user
  is '操作人';
comment on column SYS_PARAM.conduct_time
  is '操作时间';



CREATE SEQUENCE SEQ_SYS_ALTER_RECORD START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 CACHE 20 ORDER;
-- Create table
create table SYS_ALTER_RECORD
(
  id          INTEGER,
  periods     VARCHAR2(20),
  time_stamp  CHAR(14),
  object_name VARCHAR2(30),
  field_name  VARCHAR2(50),
  field_value VARCHAR2(128)
);
-- Add comments to the table 
comment on table SYS_ALTER_RECORD
  is '修订记录表';
-- Add comments to the columns 
comment on column SYS_ALTER_RECORD.id
  is '主键';
comment on column SYS_ALTER_RECORD.periods
  is '期数版本';
comment on column SYS_ALTER_RECORD.time_stamp
  is '时间戳';
comment on column SYS_ALTER_RECORD.object_name
  is '类名';
comment on column SYS_ALTER_RECORD.field_name
  is '字段名';
comment on column SYS_ALTER_RECORD.field_value
  is '字段值';


-- Create table
create table SYS_EXCEPTION_INFO
(
  code         VARCHAR2(32),
  text         VARCHAR2(128),
  type         VARCHAR2(16),
  remark       VARCHAR2(100),
  conduct_user INTEGER,
  conduct_time CHAR(14)
);
-- Add comments to the table 
comment on table SYS_EXCEPTION_INFO
  is '异常信息配置表';
-- Add comments to the columns 
comment on column SYS_EXCEPTION_INFO.code
  is '异常代码';
comment on column SYS_EXCEPTION_INFO.text
  is '异常消息';
comment on column SYS_EXCEPTION_INFO.type
  is '异常类型';
comment on column SYS_EXCEPTION_INFO.remark
  is '备注';
comment on column SYS_EXCEPTION_INFO.conduct_user
  is '操作人';
comment on column SYS_EXCEPTION_INFO.conduct_time
  is '操作时间';

insert into sys_exception_info (CODE, TEXT, TYPE, REMARK, CONDUCT_USER, CONDUCT_TIME)
values ('9999', '系统信息', '', '', 1, '20190101000000');

insert into sys_exception_info (CODE, TEXT, TYPE, REMARK, CONDUCT_USER, CONDUCT_TIME)
values ('9990', '系统信息', '', '', 1, '20190101000000');

insert into sys_exception_info (CODE, TEXT, TYPE, REMARK, CONDUCT_USER, CONDUCT_TIME)
values ('0002', '序号生成器索引不存在', '', '', 1, '20190101000000');

insert into sys_exception_info (CODE, TEXT, TYPE, REMARK, CONDUCT_USER, CONDUCT_TIME)
values ('0001', '用户变量获取失败，请重新登录', '', '', 1, '20190101000000');

insert into sys_exception_info (CODE, TEXT, TYPE, REMARK, CONDUCT_USER, CONDUCT_TIME)
values ('0003', '参数验证不通过', '', '', 1, '20190101000000');

insert into sys_exception_info (CODE, TEXT, TYPE, REMARK, CONDUCT_USER, CONDUCT_TIME)
values ('1005', 'json数据解析异常', '', '', 1, '20190101000000');

insert into sys_exception_info (CODE, TEXT, TYPE, REMARK, CONDUCT_USER, CONDUCT_TIME)
values ('7001', '权限不足', '', '', 1, '20190101000000');

insert into sys_exception_info (CODE, TEXT, TYPE, REMARK, CONDUCT_USER, CONDUCT_TIME)
values ('CRM2001', '数据校验失败', '', '', 1, '20190101000000');