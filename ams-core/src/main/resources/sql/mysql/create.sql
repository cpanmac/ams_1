/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2020-07-28 09:58:32                          */
/*==============================================================*/


drop table if exists SYS_ALTER_RECORD;

drop table if exists SYS_EXCEPTION_INFO;

drop table if exists SYS_EXCEPTION_LOG;

drop table if exists SYS_INFO;

drop table if exists SYS_PARAM;

drop table if exists SYS_SEQ_CONTROL;

drop table if exists SYS_TRADE_CONSOLE;

drop table if exists SYS_TRADE_LOG;

/*==============================================================*/
/* Table: SYS_ALTER_RECORD                                      */
/*==============================================================*/
create table SYS_ALTER_RECORD
(
   ID                   int comment '主键',
   PERIODS              varchar(20) comment '期数版本',
   TIME_STAMP           char(14) comment '时间戳',
   OBJECT_NAME          varchar(30) comment '类名',
   FIELD_NAME           varchar(50) comment '字段名',
   FIELD_VALUE          varchar(128) comment '字段值'
);

alter table SYS_ALTER_RECORD comment '修订记录表';

/*==============================================================*/
/* Table: SYS_EXCEPTION_INFO                                    */
/*==============================================================*/
create table SYS_EXCEPTION_INFO
(
   CODE                 varchar(32) comment '异常代码',
   TEXT                 varchar(128) comment '异常消息',
   TYPE                 varchar(16) comment '异常类型',
   REMARK               varchar(100) comment '备注',
   CONDUCT_USER         int comment '操作人',
   CONDUCT_TIME         char(14) comment '操作时间'
);

alter table SYS_EXCEPTION_INFO comment '异常信息配置表';


create table SYS_EXCEPTION_LOG
(
  TRANS_SEQ     VARCHAR(30),
  ERROR_CODE    VARCHAR(32),
  ERROR_MSG    VARCHAR(1024),
  ERROR_DATE    VARCHAR(8),
  ERROR_TIME    VARCHAR(6),
  USER_INFO    VARCHAR(32),
  STACK_TRACE   Blob
);
alter table SYS_EXCEPTION_LOG comment '异常信息记录表';
/*==============================================================*/
/* Table: SYS_INFO                                              */
/*==============================================================*/
create table SYS_INFO
(
   ID                   int not null auto_increment,
   SYS_ID               varchar(8),
   SYS_SHORT            varchar(8) comment '系统简称',
   SYS_NAME             varchar(50) comment '系统名称',
   CHANNEL_ID           varchar(8),
   BUSI_DATE            varchar(8) comment '交易日期',
   LBAT_DATE            varchar(8) comment '上一日期',
   NBAT_DATE            varchar(8) comment '下一日期',
   INSP_TYPE            varchar(2) comment '是否检查物理日期 1检查 2不检查',
   STATUS               varchar(2) comment '系统状态',
   primary key (ID)
);

alter table SYS_INFO comment '系统信息表';

insert into SYS_INFO (ID, SYS_ID, SYS_SHORT, SYS_NAME, CHANNEL_ID, BUSI_DATE, LBAT_DATE, NBAT_DATE, INSP_TYPE, STATUS)
values (1, 'AAA', 'AMS', '统一开发平台', 'AMS', '20180711', '20180727', '20180729', '1', '2');


/*==============================================================*/
/* Table: SYS_PARAM                                             */
/*==============================================================*/
create table SYS_PARAM
(
   ID                   int not null auto_increment comment '主键',
   NAME                 varchar(50) comment '参数名称',
   VALUE                varchar(100) comment '参数值',
   TYPE                 varchar(255) comment '参数类型',
   SUBTYPE              varchar(255) comment '子类型',
   STATUS               varchar(2) comment '状态',
   REMARK               varchar(100) comment '备注',
   CONDUCT_USER         int comment '操作人',
   CONDUCT_TIME         char(14) comment '操作时间',
   primary key (ID)
);

alter table SYS_PARAM comment '系统参数表';

/*==============================================================*/
/* Table: SYS_SEQ_CONTROL                                       */
/*==============================================================*/
create table SYS_SEQ_CONTROL
(
   VALUE_NO             varchar(20) not null comment '序列类型',
   VALUE_INDEX          varchar(20) comment '序列前驱',
   VALUE_LENGTH         int not null comment '序列总长度',
   VALUE_CURR           int not null comment '序列当前值',
   MAX_VALUE            int not null comment '最大值',
   MIN_VALUE            int not null comment '最小值',
   CACHE_SIZE           int not null comment '缓存大小',
   primary key (VALUE_NO)
);

alter table SYS_SEQ_CONTROL comment '序列控制表';

/*==============================================================*/
/* Table: SYS_TRADE_CONSOLE                                     */
/*==============================================================*/
create table SYS_TRADE_CONSOLE
(
   ID                   int not null auto_increment comment '主键',
   TRADE_CODE           varchar(30) comment '交易代码',
   TRADE_NAME           varchar(30) comment '交易名称',
   TRADE_GROUP          varchar(30) comment '交易组',
   MAX_EXEC             int comment '交易最大并发',
   MAX_RATE             int comment '交易最大流量',
   CURR_EXEC            int comment '当前并发',
   TIME_CYCLE           int comment '时间周期基数',
   NUMBER_CYCLE         int comment '数量周期基数',
   TIME_SPEED           int comment '时间内平均响应时间 单位秒',
   TIME_TAKE_NUM        int comment '时间内交易笔数',
   TIME_SUCC_RATE       numeric(18,2) comment '时间内交易成功率',
   NUMBER_SUCC_RATE     numeric(18,2) comment '数量内交易成功率',
   TRADE_STATUS         varchar(2) comment '当前服务响应级别 ：正常模式，降级模式，断路模式',
   TRADE_MSG            varchar(50) comment '交易消息',
   IMPL_CLASS           varchar(50) comment '交易实现类',
   primary key (ID)
);

alter table SYS_TRADE_CONSOLE comment '交易控制表';

/*==============================================================*/
/* Table: SYS_TRADE_LOG                                         */
/*==============================================================*/
create table SYS_TRADE_LOG
(
   ID                   int not null auto_increment comment '主键',
   TRADE_CODE           varchar(32) comment '交易代码',
   TRADE_NAME           varchar(32) comment '交易名称',
   TRADE_TYPE           varchar(32) comment '交易类型 10:受理交易 20:发起交易',
   TRADE_NODE           varchar(32) comment '执行节点IP',
   TRADE_LEVEL          varchar(2) comment '交易层级 1为顶层',
   USE_TIME             int comment '执行耗时',
   REQ_SEQ              varchar(32) comment '请求流水',
   GLOBAL_SEQ           varchar(32) comment '全局流水',
   TRANS_SEQ            varchar(32) comment '内部交易流水',
   REQ_SYS              varchar(8) comment '请求系统',
   REQ_DATE             varchar(8) comment '请求日期',
   REQ_TIME             varchar(6) comment '请求时间',
   SERVICE_ID           varchar(32) comment '服务代码',
   SVC_SCN              varchar(8) comment '场景代码',
   RESP_SEQ             varchar(32) comment '返回流水',
   RESP_DATE            varchar(8) comment '返回日期',
   RESP_TIME            varchar(6) comment '返回时间',
   RESP_RCV_TIME        varchar(14) comment '返回物理时间',
   TRANS_STATUS         varchar(8) comment '交易状态',
   RET_CODE             varchar(32) comment '返回码',
   RET_MSG              varchar(2048) comment '返回消息',
   BUSI_SIGN            varchar(64) comment '业务标识',
   TIME_STAMP           numeric(18,0) comment '处理时间戳',
   APP_VERSION          varchar(32) comment '应用版本',
   REMARK               varchar(512) comment '备注',
   primary key (ID)
);

alter table SYS_TRADE_LOG comment '交易日志表';

create index idx_date on sys_trade_log(req_date);

create index idx_dateseq on sys_trade_log(req_date,trans_seq);

