package com.njcb.ams.repository.dao;

import com.njcb.ams.BaseTest;
import com.njcb.ams.repository.entity.SysInfo;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test") //指定使用application-test.yml
public class SysInfoDAOTest extends BaseTest {

	@Autowired
    private SysInfoDAO sysInfoDAO;
    
    @Test
    public void testSelectSysInfo(){
    	SysInfo sysInfo = sysInfoDAO.selectSysInfo();
    	Assert.assertEquals("开发平台", sysInfo.getSysName());
    }
}
