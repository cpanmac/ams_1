package com.njcb.ams.application;

import com.njcb.ams.BaseTest;
import com.njcb.ams.repository.dao.SysInfoDAO;
import com.njcb.ams.repository.entity.SysInfo;
import com.njcb.ams.repository.entity.SysTradeConsole;
import com.njcb.ams.store.stable.TradeConsoleService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

/**
 * 系统基础功能
 * 
 * @author liuyanlong
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class BusinessOpsManagerTest extends BaseTest {
	@InjectMocks
	private BusinessOPSManager businessOPSManager;
	@Mock
	private SysInfoDAO sysInfoDAO;

	@Test
	public void testAppInfo() {
		SysInfo mockSysInfo = new SysInfo();
		mockSysInfo.setSysId("AAA");
		Mockito.when(sysInfoDAO.selectSysInfo()).thenReturn(mockSysInfo);
		SysInfo sysInfo = businessOPSManager.appInfo();
		Assert.assertEquals("AAA", sysInfo.getSysId());
	}

}
