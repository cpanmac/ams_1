package com.njcb.ams.application;

import java.util.List;

import com.njcb.ams.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import com.njcb.ams.repository.entity.SysTradeConsole;
import com.njcb.ams.store.stable.TradeConsoleService;

/**
 * 系统基础功能
 * 
 * @author liuyanlong
 *
 */
@ActiveProfiles("test") // 指定使用application-test.yml
public class BusinessCoreManagerTest extends BaseTest {
	private static final Logger logger = LoggerFactory.getLogger(BusinessCoreManagerTest.class);

	@Autowired
	private TradeConsoleService tradeConsoleService;

	@Autowired
	private BusinessCoreManager businessCoreManager;

	@Before
	public void setContext() throws Exception {
		tradeConsoleService.loadServerConsole();
	}
	

	@Test
	public void testGetServerConsole() {
		List<SysTradeConsole> list = businessCoreManager.getServerConsole(null);
		logger.info("交易个数{}", list.size());
		
	}

	public void testTradeLogQuery() {
		businessCoreManager.tradeLogQuery(null);
	}

}
