package com.njcb.ams.assembler;

import com.njcb.ams.repository.entity.SysTaskJob;
import com.njcb.ams.scheduler.bean.ScheduleJob;
import com.njcb.ams.scheduler.utils.JobConstant;
import com.njcb.ams.util.AmsDateUtils;
import com.njcb.ams.util.AmsJsonUtils;
import com.njcb.ams.util.AmsUtils;
import org.springframework.stereotype.Component;

/**
 * @author LOONG
 */
@Component
public class SysTaskJobConvertor {

    public static ScheduleJob initScheduleJob(SysTaskJob taskJob) {
        return initScheduleJob(null, taskJob);
    }

    public static ScheduleJob initScheduleJob(ScheduleJob job, SysTaskJob taskJob) {
        if (job == null) {
            job = new ScheduleJob();
        }
        job.setJobId(taskJob.getId());
        job.setJobGroup(JobConstant.GROUP_NAME);
        job.setJobName(taskJob.getJobName());
        job.setTriggerType(taskJob.getTriggerType());
        job.setCronExpression(taskJob.getCronExpression());
        job.setRepeatCount(taskJob.getRepeatCount());
        job.setRepeatInterval(taskJob.getRepeatTime());
        job.setClassName(taskJob.getProcessFunction());
        if(AmsUtils.isNotNull(taskJob.getStartTime())){
            job.setStartTime(AmsDateUtils.parseDate(taskJob.getStartTime(),"yyyyMMddHHmmss"));
        }
        if(AmsUtils.isNotNull(taskJob.getEndTime())){
            job.setEndTime(AmsDateUtils.parseDate(taskJob.getEndTime(),"yyyyMMddHHmmss"));
        }
        job.setJobParam(AmsJsonUtils.jsonToMap(taskJob.getProcessParam()));
        return job;
    }

    public static SysTaskJob initSysTaskJob(ScheduleJob scheduleJob) {
        SysTaskJob sysTaskJob = new SysTaskJob();
        sysTaskJob.setId(scheduleJob.getJobId());
        sysTaskJob.setJobName(scheduleJob.getJobName());
        sysTaskJob.setTriggerType(scheduleJob.getTriggerType());
        sysTaskJob.setCronExpression(scheduleJob.getCronExpression());
        sysTaskJob.setProcessFunction(scheduleJob.getClassName());
        sysTaskJob.setRepeatCount(scheduleJob.getRepeatCount());
        sysTaskJob.setRepeatTime(scheduleJob.getRepeatInterval());
        return sysTaskJob;
    }
}
