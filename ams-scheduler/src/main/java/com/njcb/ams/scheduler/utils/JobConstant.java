package com.njcb.ams.scheduler.utils;

/**
 * 定时任务常量
 */

public class JobConstant {

	// 任务有效
	public static final String JOB_STATUS_ACTIVE = "01";
	// 任务无效
	public static final String JOB_STATUS_INVALID = "02";

	
	public static final String GROUP_NAME = "AmsGroup";
	
	public static final String JOBBEAN_KEY = "taskjobbeanid";
	
}
