package com.njcb.ams.scheduler.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 刘彦龙
 *
 */
public class ScheduleJob implements Serializable {
	private static final long serialVersionUID = 1L;
	/** 任务id */
	private Integer jobId;
	/** 任务名称 */
	private String jobName;
	/** 任务分组 */
	private String jobGroup;
	/** 任务状态 */
	@JsonIgnore
	private String jobStatus;
	/** className */
	private String className;
	/** 任务参数 */
	private Map<String, Object> jobParam;
	/** 触发方式 */
	private String triggerType;
	/** 任务运行时间表达式 */
	private String cronExpression;
	/** 任务描述 */
	private String desc;
	/** 开始执行时间 */
	private Date startTime;
	/** 结束执行时间 */
	private Date endTime;
	/** 上次一次执行时间 */
	private Date lastTime;
	/** 下一次执行时间 */
	private Date nextTime;
	/** 重试次数 */
	private Integer repeatCount;
	/** 重试间隔时间 */
	private Integer repeatInterval;

	public ScheduleJob() {
	}

	public ScheduleJob(String jobName, String jobGroup) {
		this.jobName = jobName;
		this.jobGroup = jobGroup;
	}

	public enum AmsTriggerStatus {
		NONE("无状态"), NORMAL("正常"), PAUSED("暂停"), COMPLETE("完成"), ERROR("错误"), BLOCKED("阻塞");

		/**
		 * 枚举中文含义
		 */
		private String value;

		AmsTriggerStatus(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public Integer getJobId() {
		return jobId;
	}

	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobGroup() {
		return jobGroup;
	}

	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		AmsTriggerStatus amsjobStatus = AmsTriggerStatus.valueOf(jobStatus);
		this.jobStatus = amsjobStatus.getValue();
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Map<String, Object> getJobParam() {
		return jobParam;
	}

	public void setJobParam(Map<String, Object> jobParam) {
		this.jobParam = jobParam;
	}

	public void putJobParam(String key, String value) {
		if (null != this.jobParam) {
			this.jobParam.put(key, value);
		} else {
			this.jobParam = new HashMap<String, Object>();
			this.jobParam.put(key, value);
		}
	}

	public String getTriggerType() {
		return triggerType;
	}

	public void setTriggerType(String triggerType) {
		this.triggerType = triggerType;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getLastTime() {
		return lastTime;
	}

	public void setLastTime(Date lastTime) {
		this.lastTime = lastTime;
	}

	public Date getNextTime() {
		return nextTime;
	}

	public void setNextTime(Date nextTime) {
		this.nextTime = nextTime;
	}

	public void setRepeatCount(Integer repeatCount) {
		this.repeatCount = repeatCount;
	}

	public void setRepeatInterval(Integer repeatInterval) {
		this.repeatInterval = repeatInterval;
	}

	public Integer getRepeatCount() {
		return repeatCount;
	}

	public Integer getRepeatInterval() {
		return repeatInterval;
	}

}
