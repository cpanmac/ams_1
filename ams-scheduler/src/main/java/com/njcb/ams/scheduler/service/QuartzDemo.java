package com.njcb.ams.scheduler.service;

import com.njcb.ams.pojo.enumvalue.JobResult;
import com.njcb.ams.scheduler.bean.BaseJobDetail;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

/**
 * @author LOONG
 */
public class QuartzDemo extends BaseJobDetail {

	private static final Logger logger = LoggerFactory.getLogger(QuartzDemo.class);

	@Override
	public JobResult executeJob(JobExecutionContext context) throws Exception {
		Map<String, Object> dataMap = context.getJobDetail().getJobDataMap();
		dataMap.put("testkey", "测试值" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
		System.out.println(dataMap.get("name"));
		logger.info("参数name的值为[{}]", dataMap.get("name"));
		String msg = "";
		msg += "##################################################################\n";
		msg += "#                                                                #\n";
		msg += "#       @ @ @ @ @    @        @      @ @ @ @     @     @ @       #\n";
		msg += "#       @            @        @    @             @   @           #\n";
		msg += "#       @            @        @    @             @ @             #\n";
		msg += "#       @            @        @    @             @ @             #\n";
		msg += "#       @            @        @    @             @  @            #\n";
		msg += "#       @ @ @ @ @    @        @    @             @   @           #\n";
		msg += "#       @            @        @    @             @    @          #\n";
		msg += "#       @            @        @    @             @     @         #\n";
		msg += "#       @            @        @    @             @      @        #\n";
		msg += "#       @              @ @@ @        @ @ @ @     @       @       #\n";
		msg += "#                                                                #\n";
		msg += "##################################################################\n";
		logger.info("\n"+msg);
		return JobResult.RESULT_SUCCESS;
	}

	@Override
	public void afterReturning(JobExecutionContext context, JobResult result) throws Exception {
		logger.info("执行结果:" + result);
		logger.info("测试传输值[{}]", context.getJobDetail().getJobDataMap().get("testkey"));
	}

}
