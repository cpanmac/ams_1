/*
 * SysTaskJobLogMapper.java
 * Copyright(C) trythis.cn
 * 2020年06月02日 14时43分16秒Created
 */
package com.njcb.ams.repository.dao.mapper;

import com.njcb.ams.repository.entity.SysTaskJobLog;
import com.njcb.ams.repository.entity.SysTaskJobLogExample;
import org.springframework.stereotype.Component;

@Component
public interface SysTaskJobLogMapper extends BaseMapper<SysTaskJobLog, SysTaskJobLogExample, Integer> {
}