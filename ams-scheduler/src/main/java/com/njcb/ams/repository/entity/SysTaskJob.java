/*
 * SysTaskJob.java
 * Copyright(C) trythis.cn
 * 2020年06月02日 14时43分16秒Created
 */
package com.njcb.ams.repository.entity;

import com.njcb.ams.pojo.enumvalue.TriggerType;
import com.njcb.ams.support.validation.constraints.EnumValue;

public class SysTaskJob {
    private Integer id;

    private String jobName;

    private String processFunction;

    private String processParam;

    @EnumValue(enumClass = TriggerType.class, message = "触发类型码值不正确")
    private String triggerType;

    private String cronExpression;

    private String jobStatus;

    /**
     * 间隔时间(秒)
     */
    private Integer repeatTime;

    /**
     * 触发次数
     */
    private Integer repeatCount;

    private String startTime;

    private String endTime;

    private String lastRunTime;

    private String nextFireTime;

    private String triggerState;

    private String remark;

    private Integer conductUser;

    private String conductTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getProcessFunction() {
        return processFunction;
    }

    public void setProcessFunction(String processFunction) {
        this.processFunction = processFunction;
    }

    public String getProcessParam() {
        return processParam;
    }

    public void setProcessParam(String processParam) {
        this.processParam = processParam;
    }

    public String getTriggerType() {
        return triggerType;
    }

    public void setTriggerType(String triggerType) {
        this.triggerType = triggerType;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public Integer getRepeatTime() {
        return repeatTime;
    }

    public void setRepeatTime(Integer repeatTime) {
        this.repeatTime = repeatTime;
    }

    public Integer getRepeatCount() {
        return repeatCount;
    }

    public void setRepeatCount(Integer repeatCount) {
        this.repeatCount = repeatCount;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getLastRunTime() {
        return lastRunTime;
    }

    public void setLastRunTime(String lastRunTime) {
        this.lastRunTime = lastRunTime;
    }

    public String getNextFireTime() {
        return nextFireTime;
    }

    public void setNextFireTime(String nextFireTime) {
        this.nextFireTime = nextFireTime;
    }

    public String getTriggerState() {
        return triggerState;
    }

    public void setTriggerState(String triggerState) {
        this.triggerState = triggerState;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getConductUser() {
        return conductUser;
    }

    public void setConductUser(Integer conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }
}