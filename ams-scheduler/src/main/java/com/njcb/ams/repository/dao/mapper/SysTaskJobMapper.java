/*
 * SysTaskJobMapper.java
 * Copyright(C) trythis.cn
 * 2020年06月02日 14时43分16秒Created
 */
package com.njcb.ams.repository.dao.mapper;

import com.njcb.ams.repository.entity.SysTaskJob;
import com.njcb.ams.repository.entity.SysTaskJobExample;
import org.springframework.stereotype.Component;

/**
 * @author LOONG
 */
@Component
public interface SysTaskJobMapper extends BaseMapper<SysTaskJob, SysTaskJobExample, Integer> {
}