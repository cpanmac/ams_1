/*
 * SysTaskJobLog.java
 * Copyright(C) trythis.cn
 * 2020年06月02日 14时43分16秒Created
 */
package com.njcb.ams.repository.entity;

import com.njcb.ams.store.page.Page;

public class SysTaskJobLog extends Page {
    private Integer id;

    private String jobName;

    private String processFunction;

    private String excuteTime;

    private String excuteResult;

    private String excuteNode;

    private String triggerState;

    private String exceptionMsg;

    private String remark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getProcessFunction() {
        return processFunction;
    }

    public void setProcessFunction(String processFunction) {
        this.processFunction = processFunction;
    }

    public String getExcuteTime() {
        return excuteTime;
    }

    public void setExcuteTime(String excuteTime) {
        this.excuteTime = excuteTime;
    }

    public String getExcuteResult() {
        return excuteResult;
    }

    public void setExcuteResult(String excuteResult) {
        this.excuteResult = excuteResult;
    }

    public String getExcuteNode() {
        return excuteNode;
    }

    public void setExcuteNode(String excuteNode) {
        this.excuteNode = excuteNode;
    }

    public String getTriggerState() {
        return triggerState;
    }

    public void setTriggerState(String triggerState) {
        this.triggerState = triggerState;
    }

    public String getExceptionMsg() {
        return exceptionMsg;
    }

    public void setExceptionMsg(String exceptionMsg) {
        this.exceptionMsg = exceptionMsg;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}