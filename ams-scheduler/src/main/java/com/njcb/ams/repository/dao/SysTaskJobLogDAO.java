/*
 * SysTaskJobLog.java
 * Copyright(C) trythis.cn
 * 2020年06月02日 14时43分16秒Created
 */
package com.njcb.ams.repository.dao;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.SysTaskJobLogMapper;
import com.njcb.ams.repository.entity.SysTaskJobLog;
import com.njcb.ams.repository.entity.SysTaskJobLogExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SysTaskJobLogDAO extends BaseMyBatisDAO<SysTaskJobLog, SysTaskJobLogExample, Integer> {
    @Autowired
    private SysTaskJobLogMapper mapper;

    public SysTaskJobLogDAO() {
        this.entityClass = SysTaskJobLog.class;
    }

    @Override
    @SuppressWarnings("unchecked")
    public SysTaskJobLogMapper getMapper() {
        return mapper;
    }
}