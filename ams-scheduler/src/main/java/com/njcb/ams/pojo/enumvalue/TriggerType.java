package com.njcb.ams.pojo.enumvalue;

import com.njcb.ams.support.annotation.DataDic;
import com.njcb.ams.support.codevalue.EnumCode;

/**
 * 触发类型
 * @author LOONG
 */
@DataDic(dataType = "TriggerType", dataTypeName = "触发类型")
public enum TriggerType implements EnumCode {

    CRON("10", "定时"), REPEAT("20", "重复");

    private String code;
    private String desc;

    TriggerType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
