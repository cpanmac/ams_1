package com.njcb.ams.pojo.enumvalue;

import com.njcb.ams.support.annotation.DataDic;
import com.njcb.ams.support.codevalue.EnumCode;

/**
 * RESULT_SUCCESS        任务运行结果_成功_轮询未结束则继续轮询
 * RESULT_SUCCESS_TO_END 任务运行结果_成功_结束轮询
 * RESULT_FAIL           任务运行结果_失败_轮询未结束则继续轮询
 * RESULT_FAIL_TO_END    任务运行结果_失败_结束轮询
 * RESULT_IGNORE         任务运行结果_忽略_不计入轮询次数
 */
@DataDic(dataType = "JobResult", dataTypeName = "任务结果")
public enum JobResult implements EnumCode {
    RESULT_SUCCESS("1", "成功_轮询未结束则继续轮询"),
    RESULT_SUCCESS_TO_END("2", "成功_结束轮询"),
    RESULT_FAIL("3", "失败_轮询未结束则继续轮询"),
    RESULT_FAIL_TO_END("4", "失败_结束轮询"),
    RESULT_IGNORE("5", "忽略_不计入轮询次数");
    private String code;
    private String desc;

    JobResult(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
